\section{Schémas numériques}

\begin{frame}{Cas Linéaire - Système associé}
  \begin{equation}
    \left\{
      \begin{array}{lll}
        - \nabla \cdot (K \gradient h(x)) = f_1 \\
        \frac{\partial C}{\partial t} + \nabla \cdot (\mathbf{q}C) = \nabla \cdot (D \vv{\nabla} C) + f_2 \\
        \mathbf{q} = - K \gradient h
      \end{array}
    \right.
  \end{equation}
  \vspace*{1cm}
  \begin{itemize}
  \item En linéaire, la perméabilité $K$ ne dépend pas de la pression h
  \item Étape de calculs: \\
    \begin{enumerate}
    \item Calcul de la pression $h$ (condition au bord de Dirichlet)
    \item Calcul de la vélocité $\mathbf{q}$ (condition au bord de Neumann homogène)
    \item Détermination de la concentration $C$ en fonction du temps
    \end{enumerate}		
  \end{itemize}
\end{frame}



\subsection{Discrétisation du domaine}

%%%%%%%%%%%%%%%% DOMAINE DE TRAVAIL %%%%%%%%%%%%%%%%%%%
\begin{frame}{{\color{gray}\secname~---~} Discrétisation du domaine}

  \vspace{-\topsep}
  \begin{figure}[h]
    \begin{columns}
      \column{.55\paperwidth}
      \includegraphics[width=.55\paperwidth]{figures/rectangular-grid.pdf}
      \column{.4\linewidth}
      \caption{Domaine d'étude discrétisé}
      \label{figure:rectangular-grid}
    \end{columns}
  \end{figure}
  
  \vspace{-\topsep}
  \begin{itemize}
  \item Domaine d'étude : $\Omega = [0, 1] \times [0, 1]$ 
  \item Maillage structuré constitué de $N_xN_y$ cellules $(\mathcal{C}_{ij})_{ij}$
  \item Maillage tronqué $\mathcal{T}$ : maillage initial privé des cellules de bord
  \item $\Delta x = \frac{1}{N_x-1}$, $\Delta y = \frac{1}{N_y-1}$ 
  \item Mono-indice $k = i + (N_x - 1)j$, $\forall (i, j) \in [0, N_x - 1 ] \times [ 0, N_y - 1 ]$
  \end{itemize}

\end{frame}


%%%%%%%%%%%%%% MISE EN OEUVRE DES VOLUMES FINIS %%%%%%%%%%%%%%%%%

\subsection{Schéma diamant pour la pression}

\begin{frame}{{\color{gray}\secname~---~} Volumes finis pour la pression}
  
  {\bfseries Mise en \oe{}uvre des volumes finis pour la pression}
  \medbreak
  On intègre la première équation sur chaque cellule $\mathcal{C}_k$ de $\mathcal{T}$ :
  \begin{align*}
    \int_{\mathcal{C}_k}{f_1 d\mathbf{x}} = -\int_{\mathcal{C}_k}{\nabla \cdot (K \vv{\nabla} h) d\mathbf{x}}
    \Longrightarrow
    \vert\mathcal{C}_k\vert f_{1,\mathcal{C}_k} = - \sum_{l\in \mathcal{N}_k}{\int_{\gamma_{k,l}}{K \vv{\nabla} h \cdot \mathbf{n}_{\vert\gamma_{k,l}}\: d\sigma}}
  \end{align*}
  avec :
  \begin{itemize}
  \item $\gamma_{k,l}$ l'interface entre les cellules $\mathcal{C}_k$ et $\mathcal{C}_l$ ;
  \item $\mathcal{N}_k$ l'ensemble des mono-indices des cellules voisines de $\mathcal{C}_k$ ;
  \item $f_{1,\mathcal{C}_k}$ la moyenne de $f_1$ sur $\mathcal{C}_k$ ;
  \end{itemize}

  \bigbreak
  {\bfseries Problème~:} comment évaluer $\vv{\nabla}h$ sur $\gamma_{k,l}$ ?
  
\end{frame}


%%%%%%%%%%%%%% PRESENTATION SCHEMA DIAMANT %%%%%%%%%%%%%%%%%

\begin{frame}{{\color{gray}\secname~---~} La cellule diamant}		

  \begin{figure}[h]
    \begin{columns}
      \column{.3\paperwidth}
      \includegraphics[width=0.3\paperwidth]{figures/diamond-cell.pdf}
      \column{.6\linewidth}
      \caption{Cellule diamant centrée sur le bord $\gamma_{k, k+1}$}
      \label{figure:diamond-cell}
    \end{columns}
  \end{figure}
  \vspace{-\topsep}
  \begin{itemize}
  \item On définit $\mathcal{D}_{k, l}$ le diamant construit entre les cellules $C_k$ et $C_l$
  \item Approximation : $\vv{\nabla}h$ est constant sur chaque cellule diamant $\mathcal{D}_{k,l}$
  \item Il faut considérer la pression au niveau de l'intersection de 4 cellules. Si $\tilde{h}$ est la pression exacte, un développement limité simple en $(x_i+\Delta x/2,\: y_i\pm\Delta y/2)$ nous donne :
    \begin{align*}
      \tilde{h}_k^\pm = \frac{\tilde{h}_k + \tilde{h}_{k+1} + \tilde{h}_{k\pm N_x} + \tilde{h}_{k+1\pm N_x}}{4} + \mathcal{O}(\Delta x^2 + \Delta y^2)
    \end{align*}
    % \item $h_k^-$ est approché par : $(h_k+h_{k+1}+h_{k-N_x}+h_{k-N_x+1})/4$
    % \item $h_k^+ = h_{k+N_}$
  \end{itemize}
  
\end{frame}


%%%%%%%%%%%%%% CARACTERISATION DU DIAMANT %%%%%%%%%%%%%%%%%

\begin{frame}{{\color{gray}\secname~---~} La cellule diamant}

  {\bfseries Caractérisation de la cellule diamant $\mathcal{D}_{k, l}$}
  \smallbreak
  \begin{columns}
    \column{.25\paperwidth}
    \begin{figure}[h]
      \begin{tikzpicture}[scale=1]
        \draw (0,0) -- (1,1) -- (0,2) -- (-1,1) -- (0,0);
        \node at (0,1) {$\mathcal{D}_{k,l}$};
        \node[below right, shift={(-.125,.125)}] at (0.5,0.5) {$d_1$};
        \node[above right, shift={(-.125,-.125)}] at (0.5,1.5) {$d_2$};
        \node[above left, shift={(.125,-.125)}] at (-0.5,1.5) {$d_3$};
        \node[below left, shift={(.125,.125)}] at (-0.5,0.5) {$d_4$};
      \end{tikzpicture}
    \end{figure}\vfill

    \column{.75\paperwidth}
    \begin{itemize}
    \item $\mathcal{D}_{k,l}$ constitué de 4 segments $d_1,...,d_4$ ($\partial \mathcal{D}_{k,l} = \bigcup_{1 \leq i \leq 4} d_i$) ;
    \item $\mathbf{n}_{d_i}$ normale sortante associée à $d_i$ ;
    \item $\vert\mathcal{D}_{k,l}\vert = \frac{1}{2}\vert\mathcal{C}_k\vert$ ;
    \end{itemize}
  \end{columns}

  \medbreak
  L'approximation selon laquelle $\vv{\nabla}h = cste$ sur $\mathcal{D}_{k,l}$ permet d'écrire :
  \begin{align*}
    \int_{\mathcal{D}_{k,l}}{\vv{\nabla}h d\mathbf{x}} = \int_{\partial\mathcal{D}_{k,l}}{h \mathbf{n}_{k,l} d\sigma}
    \Longrightarrow
    \vv{\nabla}h_{\vert\gamma_{k,l}} = \frac{1}{\vert\mathcal{D}_{k,l}\vert} \sum_{i=1}^4{\int_{d_i}{h \mathbf{n}_{d_i} d\sigma}}
  \end{align*}
  avec par exemple $\int_{d_1}{h \mathbf{n}_{d_1} d\sigma} = \vert d_1\vert \frac{h_k^-+h_{k+1}}{2}\:\mathbf{n}_{d_1}$.
  
\end{frame}


%%%%%%%%%%%%%% OBTENTION DU SCHEMA DIAMANT %%%%%%%%%%%%%%%%%

\begin{frame}{{\color{gray}\secname~---~} Obtention du schema diamant}

  On utilise l'approximation de $\vv{\nabla}h$ pour obtenir :
  \begin{align*}
    \vert\mathcal{C}_k\vert f_{1,\mathcal{C}_k}
    & = - \sum_{l\in \mathcal{N}_k}{\int_{\gamma_{k,l}}{K \vv{\nabla} h \cdot \mathbf{n}_{\vert\gamma_{k,l}}\: d\sigma}} \\
    & = - \sum_{l\in \mathcal{N}_k}{\sum_{i=1}^4{\int_{\gamma_{k,l}}{K \Big(\int_{d_i}{h \mathbf{n}_{d_i} ds}\Big) \cdot \mathbf{n}_{\vert\gamma_{k,l}}\: d\sigma}}}
  \end{align*}
  On peut réécrire de façon simplifiée : $b_k = \sum_{l}{R_{k,l}h_l}$.

  \bigbreak
  On obtient un système linéaire $R\mathbf{h} = \mathbf{b}$ à résoudre, et dont la $k^{\text{ième}}$ ligne correspond à l'équation ci-dessus.

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%% CALCUL DE LA PRESSION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Calcul de la vélocité}

\begin{frame}{{\color{gray}\secname~---~} Calcul de la vélocité}

  Le calcul de la vélocité se fait par le biais de différences finies :
  \begin{align*}
    & \vv{\nabla} \tilde{h}_{\vert \gamma_{k,l}} = \begin{pmatrix}
      (\tilde{h}_l-\tilde{h}_k) / \Delta x \\[.8em]
      (\tilde{h}_{k\pm 1}^+-\tilde{h}_{k\pm 1}^-) / \Delta y
    \end{pmatrix} + \vv{\varepsilon}(\Delta x, \Delta y) \quad \text{si}\ l=k\pm 1 \\[.8em]
    & \vv{\nabla} \tilde{h}_{\vert \gamma_{k,l}} = \begin{pmatrix}
      (\tilde{h}_k^\pm-\tilde{h}_{k-1}^\pm) / \Delta x \\[.8em]
      (\tilde{h}_l-\tilde{h}_k) / \Delta y
    \end{pmatrix} + \vv{\varepsilon}(\Delta x, \Delta y) \quad \text{si}\ l=k\pm N_x
  \end{align*}

  \bigbreak
  Enfin, on calcule simplement : $\mathbf{q}_{\vert\gamma_{k,l}} = K^{\gamma_{k,l}} \vv{\nabla} h_{\vert \gamma_{k,l}}$.
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CALCUL DE LA CONCENTRATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Calcul de la concentration}

\begin{frame}{{\color{gray}\secname~---~} Calcul de la concentration}

  On construit un schéma décentré-amont en utilisant les volumes finis.
  \smallbreak
  On intègre l'équation d'advection sur $\mathcal{C}_k$ entre $t_n$ et $t_{n+1}$ :
  \begin{align*}
    & \int_{t_n}^{t_{n+1}}{\int_{\mathcal{C}_k}{\frac{\partial C}{\partial t}} d\mathbf{x} dt} + \int_{t_n}^{t_{n+1}}{\int_{\mathcal{C}_k}{\nabla \cdot (\mathbf{q}C)} d\mathbf{x} dt} = \int_{t_n}^{t_{n+1}}{\int_{\mathcal{C}_k}{f_2d\mathbf{x} dt}} \\
    \Longrightarrow{}
    & \vert \mathcal{C}_k \vert (C_k^{n+1}-C_k^n) = - \int_{t_n}^{t_{n+1}}{\int_{\partial\mathcal{C}_k}{(\mathbf{q} \cdot \mathbf{n}) C} d\sigma dt} + \Delta t \vert \mathcal{C}_k \vert f_{2,\mathcal{C}_k} \\
    \Longrightarrow{}
    & C_k^{n+1} = C_k^n - \frac{\Delta t}{\vert\mathcal{C}_k\vert} \sum_{l\in \mathcal{N}_k}{\Big( \vert \gamma_{k,l} \vert\: \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}}\: \phi_{\gamma_{k,l}}(C^n, \mathbf{q}) \Big)} + \Delta t\: f_{2,\mathcal{C}_k}
  \end{align*}

  \bigbreak
  avec $\phi_{\gamma_{k,l}}(C^n, \mathbf{q})$ le flux de $C$ au niveau de $\gamma_{k,l}$.
\end{frame}


\begin{frame}{{\color{gray}\secname~---~} Calcul de la concentration}

  \begin{align*}
    C_k^{n+1} = C_k^n - \frac{\Delta t}{\vert\mathcal{C}_k\vert} \sum_{l\in \mathcal{N}_k}{\Big( \vert \gamma_{k,l} \vert\: \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}}\: \phi_{\gamma_{k,l}}(C^n, \mathbf{q}) \Big)} + \Delta t\: f_{2,\mathcal{C}_k}
  \end{align*}

  \bigbreak
  avec $\phi_{\gamma_{k,l}}(C^n, \mathbf{q})$ le flux de $C$ au niveau de $\gamma_{k,l}$ :
  \begin{align*}
    \forall k ,\ \forall l\in \mathcal{N}_k,\ \phi_{\gamma_{k,l}}(C^n, \mathbf{q}) = \left\{ \begin{array}{ll}
                                                                                              C^n_k & \text{si}\ \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}} \geq 0 \\
                                                                                              C^n_{k-N_x} & \text{si}\ \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}} \leq 0\ \text{et}\ l=k-N_x \\
                                                                                              C^n_{k-1} & \text{si}\ \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}} \leq 0\ \text{et}\ l=k-1 \\
                                                                                              C^n_{k+1} & \text{si}\ \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}} \leq 0\ \text{et}\ l=k+1 \\
                                                                                              C^n_{k+N_x} & \text{si}\ \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}} \leq 0\ \text{et}\ l=k+N_x
                                                                                            \end{array} \right.
  \end{align*}
\end{frame}


\begin{frame}{{\color{gray}\secname~---~} Calcul de la concentration}

  De la même façon, on discrétise le terme de diffusion $\nabla \cdot (D\vv{\nabla}C)$ par :
  \begin{align*}
    \frac{1}{\vert \mathcal{C}_k \vert} \int_{t_n}^{t_{n+1}}{\int_{\mathcal{C}_k}{\nabla\cdot(D\vv{\nabla}C) \: d\mathbf{x}}}
    &= \frac{1}{\vert \mathcal{C}_k \vert} \int_{t_n}^{t_{n+1}}{\int_{\partial\mathcal{C}_k}{D\vv{\nabla}C \cdot \mathbf{n}_{\vert\gamma_{k,l}} \: d\sigma}} \\
    &= \frac{\Delta t}{\vert \mathcal{C}_k \vert} \sum_{l\in\mathcal{N}_k}{\vert \gamma_{k,l} \vert D^{\gamma_{k,l}}\vv{\nabla}C_{\vert\gamma_{k,l}} \cdot \mathbf{n}_{\vert\gamma_{k,l}}}
  \end{align*}

  \bigbreak
  Finalement, on trouve :
  \begin{align}\label{eq:diffusive-flux-scheme}
    C_k^{n+1} = C_k^n -& \frac{\Delta t}{\vert\mathcal{C}_k\vert} \sum_{l\in \mathcal{N}_k}{\Big( \vert \gamma_{k,l} \vert\: \mathbf{q}_{\vert \gamma_{k,l}} \cdot \mathbf{n}_{\vert \gamma_{k,l}}\: \phi_{\gamma_{k,l}}(C^n, \mathbf{q}) \Big)} \nonumber\\
    +& \frac{\Delta t}{\vert \mathcal{C}_k \vert} \sum_{l\in\mathcal{N}_k}{\vert \gamma_{k,l} \vert D^{\gamma_{k,l}}\vv{\nabla}C_{\vert\gamma_{k,l}} \cdot \mathbf{n}_{\vert\gamma_{k,l}}} + \Delta t\: f_{2,\mathcal{C}_k}
  \end{align}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EQUATION MODIFIEE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{includes/stabilite_du_schema.tex}


% \subsection{Simulations numériques}
