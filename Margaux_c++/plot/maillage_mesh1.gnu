set terminal svg enhanced background rgb 'white' size 1000,800
set output 'plot/maillage_mesh1.svg'
set title 'Maillage' 
set time
set xlabel 'discretisation en x'
set ylabel 'discretisation en y'
plot 'plot/data_mesh1.dat' with points
