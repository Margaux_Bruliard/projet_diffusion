/*
*	BRULIARD - RIGAL
*	Projet Numérique - MACS 2
*/

#include "../mesh.hpp"
#include <iostream>

int main ()
{
	MeshRegulier lectMesh("data/mesh/mesh1.dat");
	
	
	lectMesh.genererGrapheGnuplot("plot/data_mesh1.dat", "plot/maillage_mesh1.gnu");
	//lectMesh.lancerGraphe("plot/maillage_mesh1.gnu");
	
	std::cout << "aire de la cellule C0 du mesh: \t" << lectMesh._cells[0].surfaceCellule() << std::endl;
	return 0;
}
