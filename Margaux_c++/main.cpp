/*
*	BRULIARD Margaux - RIGAL Mathieu
* 	Projet Numérique - MACS 2
*/

/*
*	Le but de ce programme est de calculer la concentration en fonction du temps
*/

/*
*	VARIABLES MISE EN OEUVRE
*	@h la pression
*	@C la concentration
*	@Mesh le mesh d'étude	
*/


#include <iostream>

#include "matrix.hpp"

int main ()
{
	
	// Message de lancement du programme
	std::cout << "****\nProgramme de Résolution d'un système advection-diffusion\n***" << std::endl;
	
	Matrice K ("data/permeability_matrix/K_test.dat");
	std::cout << "Matrice de Permeabilite: \n" << K << std::endl;
	
	
	
	return 0;
}
