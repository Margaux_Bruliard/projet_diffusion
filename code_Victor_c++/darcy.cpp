#include "darcy.hpp"
#include "dense_matrix.hpp"
#include "permeability_matrix.hpp"
#include <iostream>
#include <math.h>
#include <stdio.h>
#include "constants.hpp"
#include "mesh.hpp"

using namespace std;

darcy::darcy() {

}

darcy::darcy(double _x_min, double _x_max, double _y_min, double _y_max, int _n_x, int _n_y, mesh *mesh_c, permeability_matrix _K, functiontype2 _function_boundary_condition, functiontype1 _function_f, bool streamer) {
	//domain [_x_min;_x_max]*[_y_min;_y_max]
	//_n_x steps in [_x_min;_x_max]
	//_n_y steps in [_y_min;_y_max]
	this->delta_x = (_x_max - _x_min)/(_n_x+1);
	this->delta_y = (_y_max - _y_min)/(_n_y+1);
	this->delta_x_vec = dense_matrix(_n_x, 1);
	this->delta_y_vec = dense_matrix(_n_y, 1);
	for(int i=0; i<_n_x; i++) {
		this->delta_x_vec(i, 0) = (_x_max - _x_min)/(_n_x+1);
		if(this->delta_x_vec(i, 0) < this->delta_x) {
			this->delta_x = this->delta_x_vec(i, 0);
		}
	}
	for(int i=0; i<_n_y; i++) {
		this->delta_y_vec(i, 0) = (_y_max - _y_min)/(_n_y+1);
		if(this->delta_y_vec(i, 0) < this->delta_y) {
			this->delta_y = this->delta_y_vec(i, 0);
		}
	}
	this->x_coord = dense_matrix(_n_x, 1);
	this->y_coord = dense_matrix(_n_y, 1);
	this->x_coord(0, 0) = _x_min + this->delta_x_vec(0, 0);
	for(int i=1; i<_n_x; i++) {
		this->x_coord(i, 0) = this->x_coord(i-1, 0) + this->delta_x_vec(i, 0);
	}
	this->y_coord(0, 0) = _y_min + this->delta_y_vec(0, 0);
	for(int i=1; i<_n_y; i++) {
		this->y_coord(i, 0) = this->y_coord(i-1, 0) + this->delta_y_vec(i, 0);
	}

	this->delta_x_vec = mesh_c->dx;
	this->delta_y_vec = mesh_c->dy;
	this->x_coord = mesh_c->x;
	this->y_coord = mesh_c->y;

	this->n_x = x_coord.get_rows();
	this->n_y = y_coord.get_rows();

	this->n_x = x_coord.get_rows();
	this->n_y = y_coord.get_rows();
	cout << "this->n_x : " << this->n_x << endl;
	cout << "this->n_y : " << this->n_y << endl;


	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;
	this->u = dense_matrix(_n_x*_n_y, 1);
	this->A = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->f = dense_matrix(this->n_x*this->n_y, 1);
	this->boundary_condition = dense_matrix(this->n_x*this->n_y, 1);
	//this->n_x = _n_x;
	//this->n_y = _n_y;
	this->K = _K;
	this->function_boundary_condition = _function_boundary_condition;
	this->function_f = _function_f;
	set_A_f();
	//set_A_f_streamer();
}

void darcy::set_A_f() {
	int i, j, k;
	double x, y;
	double dx = this->delta_x;
	double dy = this->delta_y;
	double dxx = dx*dx;
	double dyy = dy*dy;
	double dxy = dx*dy;
	int N = this->n_x;
	for(i=1; i<this->n_y-1; i++) {	//goes through each cells "in"
		for(j=1; j<this->n_x-1; j++) {
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			dxx = dx*dx;
			dyy = dy*dy;
			dxy = dx*dy;
			//x = dx*(double)j + this->x_min + dx;
			//y = dy*(double)i + this->y_min + dy;
			x = this->x_coord(j, 0);
			y = this->y_coord(i, 0);
			//this->set_f_line(k);
			this->f(k, 0) = this->function_f(x, y);
			//this->set_A_line(k);
			//Uk
			this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
			//Uk+1
			this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-1
			this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk+N
			this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N
			this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk+N+1
			this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
			//Uk+N-1
			this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N+1
			this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-N-1
			this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		}
	}
	//borders now
	//4 corners
	//bottom left
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
	//Uk+1
	this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 0)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N
	this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 0)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk+N+1
	this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 0)*(this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 0)*(this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 0)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//this->f(k, 0) = 4.74;

	//bottom right
	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 2)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-1
	this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N
	this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 2)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 2)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
	//Uk+N-1
	this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 2)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 2)*(-this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	
	//top left
	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
	//Uk+1
	this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 5)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk+N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 5)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N
	this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 5)*(-this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 5)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N+1
	this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 5)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));

	//top right
	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 7)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-1
	this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk+N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 7)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N
	this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 7)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 7)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 7)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-N-1
	this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));

	//sides now
	//bottom
	i=0;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 1)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 1)*(this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk-N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 1)*(-this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	}

	//left
	j=0;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 3)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk+N+1
		this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 3)*(this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk-N+1
		this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 3)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}

	//right
	j=n_x-1;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 4)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 4)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 4)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}

	//top
	i=n_y-1;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 6)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 6)*(-this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk+N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 6)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}
	this->f = this->f + this->boundary_condition;
}

void darcy::set_A_f_streamer() {
	int i, j, k;
	double x, y;
	double dx = this->delta_x;
	double dy = this->delta_y;
	double dxx = dx*dx;
	double dyy = dy*dy;
	double dxy = dx*dy;
	int N = this->n_x;
	for(i=1; i<this->n_y-1; i++) {	//goes through each cells "in"
		for(j=1; j<this->n_x-1; j++) {
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			dxx = dx*dx;
			dyy = dy*dy;
			dxy = dx*dy;
			//x = dx*(double)j + this->x_min + dx;
			//y = dy*(double)i + this->y_min + dy;
			x = this->x_coord(j, 0);
			y = this->y_coord(i, 0);
			//this->set_f_line(k);
			this->f(k, 0) = this->function_f(x, y);
			//this->set_A_line(k);
			//Uk
			this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
			//Uk+1
			this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-1
			this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk+N
			this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N
			this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk+N+1
			this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
			//Uk+N-1
			this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N+1
			this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-N-1
			this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		}
	}
	//borders now
	//4 corners
	//bottom left
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
	//Uk+1
	this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 0)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N
	this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 0)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Neumann
	//dU=0, Uk-N = Uk
	this->A.add_element(k, k, -this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk+N+1
	this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 0)*(this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 0)*(this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Neumann
	//dU=0, Uk-N+1 = Uk+1
	this->A.add_element(k, k+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 0)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));

	//bottom right
	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 2)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-1
	this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N
	this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 2)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//Neumann
	//dU=0, Uk-N = Uk
	this->A.add_element(k, k, -this->K.get_K_boundary(k, 4)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 2)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
	//Uk+N-1
	this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 2)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 2)*(-this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Neumann
	//dU=0, Uk-N-1 = Uk-1
	this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	
	//top left
	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
	//Uk+1
	this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 5)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk+N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 5)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Neumann
	//dU=0, Uk+N = Uk
	this->A.add_element(k, k, -this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N
	this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 5)*(-this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Neumann
	//dU=0, Uk+N+1 = Uk+1
	this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 5)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N+1
	this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 5)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));

	//top right
	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	dxx = dx*dx;
	dyy = dy*dy;
	dxy = dx*dy;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 7)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-1
	this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk+N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 7)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Neumann
	//dU=0, Uk+N = Uk
	this->A.add_element(k, k, -this->K.get_K_boundary(k, 4)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N
	this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 7)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 7)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Neumann
	//dU=0, Uk+N-1 = Uk-1
	this->A.add_element(k, k-1, this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 7)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-N-1
	this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));

	//sides now
	//bottom
	i=0;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 1)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Neumann
		//dU=0, Uk-N = Uk
		this->A.add_element(k, k, -this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 1)*(this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
		//Neumann
		//dU=0, Uk-N+1 = Uk+1
		this->A.add_element(k, k+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk-N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 1)*(-this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Neumann
		//dU=0, Uk-N-1 = Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	}

	//left
	j=0;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 3)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk+N+1
		this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 3)*(this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk-N+1
		this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 3)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}

	//right
	j=n_x-1;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 4)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 4)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 4)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}

	//top
	i=n_y-1;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		dxx = dx*dx;
		dyy = dy*dy;
		dxy = dx*dy;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 6)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Neumann
		//dU=0, Uk+N = Uk
		this->A.add_element(k, k, -this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 6)*(-this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Neumann
		//dU=0, Uk+N+1 = Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk+N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 6)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Neumann
		//dU=0, Uk+N-1 = Uk-1
		this->A.add_element(k, k-1, this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}
	this->f = this->f + this->boundary_condition;
}

void darcy::set_f_boundary() {
	int i, j, k;
	double x, y;

	double dx_c, dy_c; 	//dx dy of the cell
	double dx_r, dx_l, dy_u, dy_d;	//to neighboors

	int N = this->n_x;
	for(i=1; i<this->n_y-1; i++) {	//goes through each cells "in"
		for(j=1; j<this->n_x-1; j++) {
			k = i*this->n_x+j;
			//x = dx*(double)j + this->x_min + dx;
			//y = dy*(double)i + this->y_min + dy;
			x = this->x_coord(j, 0);
			y = this->y_coord(i, 0);
			//this->set_f_line(k);
			this->f(k, 0) = this->function_f(x, y);
			//this->set_A_line(k);
		}
	}
	//borders now
	//4 corners
	//bottom left
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y, 0)*(-this->K.get_K_boundary(k, 1)/(dx_c*dx_l) + this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	//Uk-N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy_d, 0)*(-this->K.get_K_boundary(k, 4)/(dy_c*dy_d) + this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y+dy_u, 0)*(this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y-dy_d, 0)*(this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//WRONG
	//this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y-dy)*(-this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y-dy_d, 0)*(-this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));

	//bottom right
	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y, 2)*(-this->K.get_K_boundary(k, 1)/(dx_c*dx_r) - this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	//Uk-N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy_d, 2)*(-this->K.get_K_boundary(k, 4)/(dy_c*dy_d) + this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) - this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y+dy_u, 2)*(-this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) - this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y-dy_d, 2)*(this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y-dy_d, 2)*(-this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c));
	
	//top left
	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y, 5)*(-this->K.get_K_boundary(k, 1)/(dx_c*dx_l) + this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c) - this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));
	//Uk+N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy_u, 5)*(-this->K.get_K_boundary(k, 4)/(dy_c*dy_u) - this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y+dy_u, 5)*(-this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y+dy_u, 5)*(this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c));
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y-dy_d, 5)*(-this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) - this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));

	//top right
	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y, 7)*(-this->K.get_K_boundary(k, 1)/(dx_c*dx_r) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c) + this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));
	//Uk+N
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy_u, 7)*(-this->K.get_K_boundary(k, 4)/(dy_c*dy_u) - this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) + this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y+dy_u, 7)*(-this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y+dy_u, 7)*(this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c) + this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c));
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y-dy_d, 7)*(this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) + this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));

	//sides now
	//bottom
	i=0;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = dy_u;
		dx_c = dx_r/2.0 + dx_l/2.0;
		dy_c = dy_u/2.0 + dy_d/2.0;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk-N
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy_d, 1)*(-this->K.get_K_boundary(k, 4)/(dy_c*dy_d) + this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) - this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c));
		//Uk-N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y-dy_d, 1)*(this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
		//Uk-N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y-dy_d, 1)*(-this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
	}

	//left
	j=0;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = dx_r;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y, 3)*(-this->K.get_K_boundary(k, 1)/(dx_c*dx_l) + this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c) - this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));
		//Uk+N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y+dy_u, 3)*(this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c) + this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c));
		//Uk-N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y-dy_d, 3)*(-this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) - this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));
	}

	//right
	j=n_x-1;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dx_r = dx_l;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y, 4)*(-this->K.get_K_boundary(k, 1)/(dx_c*dx_r) - this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c) + this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));
		//Uk+N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y+dy_u, 4)*(-this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) - this->K.get_K(k, k+N, 1)/(4*dx_c*dy_c));
		//Uk-N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y-dy_d, 4)*(this->K.get_K_boundary(k, 2)/(4*dx_c*dy_c) + this->K.get_K(k-N, k, 1)/(4*dx_c*dy_c));
	}

	//top
	i=n_y-1;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dy_u = dy_d;
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk+N
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy_u, 6)*(-this->K.get_K_boundary(k, 4)/(dy_c*dy_u) - this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) + this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c));
		//Uk+N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y+dy_u, 6)*(-this->K.get_K(k, k+1, 2)/(4*dx_c*dy_c) - this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c));
		//Uk+N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y+dy_u, 6)*(this->K.get_K_boundary(k, 3)/(4*dx_c*dy_c) + this->K.get_K(k-1, k, 2)/(4*dx_c*dy_c));
	}
	this->f = this->f + this->boundary_condition;
}

void darcy::set_f_boundary_streamer() {
	int i, j, k;
	double x, y;

	double dx_c, dy_c; 	//dx dy of the cell
	double dx_r, dx_l, dy_u, dy_d;	//to neighboors

	int N = this->n_x;
	for(i=1; i<this->n_y-1; i++) {	//goes through each cells "in"
		for(j=1; j<this->n_x-1; j++) {
			k = i*this->n_x+j;
			x = this->x_coord(j, 0);
			y = this->y_coord(i, 0);
			this->f(k, 0) = this->function_f(x, y);
		}
	}
	//borders now
	//4 corners
	//bottom left
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y, 0)*(-1/(dx_c*dx_l));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;

	//bottom right
	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y, 2)*(-1/(dx_c*dx_r));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;
	
	//top left
	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y, 5)*(-1/(dx_c*dx_l));
	//Uk+N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;
	//Uk-N-1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;

	//top right
	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//this->set_f_line(k);
	this->f(k, 0) = this->function_f(x, y);
	//Uk+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y, 7)*(-1/(dx_c*dx_r));
	//Uk+N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;
	//Uk-N+1
	//doesn't exist
	//this->set_boundary_condition_line(k);
	//Dirichlet
	this->boundary_condition(k, 0) -= 0;

	//sides now
	//bottom
	i=0;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		this->f(k, 0) = this->function_f(x, y);
	}

	//left
	j=0;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = dx_r;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx_l, y, 3)*(-1/(dx_c*dx_l));
		//Uk+N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= 0;
		//Uk-N-1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= 0;
	}

	//right
	j=n_x-1;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dx_r = dx_l;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
		//Uk+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx_r, y, 4)*(-1/(dx_c*dx_r));
		//Uk+N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= 0;
		//Uk-N+1
		//doesn't exist
		//this->set_boundary_condition_line(k);
		//Dirichlet
		this->boundary_condition(k, 0) -= 0;
	}

	//top
	i=n_y-1;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//this->set_f_line(k);
		this->f(k, 0) = this->function_f(x, y);
	}
	this->f = this->f + this->boundary_condition;
}

void darcy::print() {
	this->A.print();
	//cout << endl;
	this->f.print();
}

int darcy::get_n_x() {
	return this->n_x;
}

int darcy::get_n_y() {
	return this->n_y;
}

double darcy::get_delta_x() {
	return this->delta_x;
}

double darcy::get_delta_y() {
	return this->delta_y;
}

double darcy::error_L2_square(dense_matrix *exact_sol) {
	double error = 0.0;
	for(int i=0; i<(*exact_sol).get_rows(); i++) {
		double tmp = this->u(i, 0) - (*exact_sol)(i, 0);
		error += tmp*tmp;
	}
	return error*this->delta_x*this->delta_y;
}

void darcy::compute_grad_C(dense_matrix *concentration, dense_matrix *grad_C_x_left, dense_matrix *grad_C_y_left, dense_matrix *grad_C_x_down, dense_matrix *grad_C_y_down, functiontype3 neumann_condition_C) {	//u is the solution of darcy's equation
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;

	double grad_C_x = 0.0;
	double grad_C_y = 0.0;

	int i, j, k;
	int k_nx1, k_ny1;

	double x, y;
			
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//diffusion=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
			dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
			dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			dx_c = dx_r/2.0 + dx_l/2.0;
			dy_c = dy_u/2.0 + dy_d/2.0;

			//right edge
			
			//up edge
			
			//left edge
			//left hand side
			//right hand side
			grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
			grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
			k_nx1 = i*(this->n_x+1)+j;
			(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
			(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge
			

			//down edge
			//left hand side
			//right hand side
			grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx_c;
			grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
			k_ny1 = i*this->n_x+j;
			(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
			(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
		}
	}

	//borders now
	//4 corners
	//bottom left
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2.0 + dx_l/2.0;
	dy_c = dy_u/2.0 + dy_d/2.0;
	//right edge
	
	//up edge

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x-dx_l/2.0, y, 1, true);
	grad_C_y = neumann_condition_C(x-dx_l/2.0, y, 1, false);
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
	(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y-dy_d/2.0, 0, true);
	grad_C_y = neumann_condition_C(x, y-dy_d/2.0, 0, false);
	k_ny1 = i*this->n_x+j;
	(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
	(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge

	//bottom right
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dx_c = dx_r/2.0 + dx_l/2.0;
	dy_c = dy_u/2.0 + dy_d/2.0;

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x+dx_r/2.0, y, 2, true);
	grad_C_y = neumann_condition_C(x+dx_r/2.0, y, 2, false);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
	(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

	//up edge

	//left edge
	//left hand side
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
	grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
	(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y-dy_d/2.0, 0, true);
	grad_C_y = neumann_condition_C(x, y-dy_d/2.0, 0, false);
	k_ny1 = i*this->n_x+j;
	(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
	(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
	
	//top left
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2.0 + dx_l/2.0;
	dy_c = dy_u/2.0 + dy_d/2.0;
	//right edge

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y+dy_u/2.0, 3, true);
	grad_C_y = neumann_condition_C(x, y+dy_u/2.0, 3, false);
	k_ny1 = (i+1)*this->n_x+j;
	(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
	(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x-dx_l/2.0, y, 1, true);
	grad_C_y = neumann_condition_C(x-dx_l/2.0, y, 1, false);
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
	(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

	//down edge
	//left hand side
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx_c;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
	(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge

	//top right
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2.0 + dx_l/2.0;
	dy_c = dy_u/2.0 + dy_d/2.0;
	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x+dx_r/2.0, y, 2, true);
	grad_C_y = neumann_condition_C(x+dx_r/2.0, y, 2, false);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
	(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y+dy_u/2.0, 3, true);
	grad_C_y = neumann_condition_C(x, y+dy_u/2.0, 3, false);
	k_ny1 = (i+1)*this->n_x+j;
	(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
	(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge

	//left edge
	//left hand side
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
	grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
	(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

	//down edge
	//left hand side
	//right hand side
	grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx_c;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
	(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
	
	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = dy_u;
		dx_c = dx_r/2.0 + dx_l/2.0;
		dy_c = dy_u/2.0 + dy_d/2.0;
		//right edge

		//up edge

		//left edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
		grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
		(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

		//down edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x, y-dy_d/2.0, 0, true);
		grad_C_y = neumann_condition_C(x, y-dy_d/2.0, 0, false);
		k_ny1 = i*this->n_x+j;
		(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
		(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = dx_r;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2.0 + dx_l/2.0;
		dy_c = dy_u/2.0 + dy_d/2.0;
		//right edge

		//up edge

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x-dx_l/2.0, y, 1, true);
		grad_C_y = neumann_condition_C(x-dx_l/2.0, y, 1, false);
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
		(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge

		//down edge
		//left hand side
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx_c;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
		(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
	}
	//right
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dx_r = dx_l;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2.0 + dx_l/2.0;
		dy_c = dy_u/2.0 + dy_d/2.0;
		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x+dx_r/2.0, y, 2, true);
		grad_C_y = neumann_condition_C(x+dx_r/2.0, y, 2, false);
		k_nx1 = i*(this->n_x+1)+j+1;
		(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
		(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge
	
		//up edge

		//left edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
		grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
		(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge
		
		//down edge
		//left hand side
		//right hand side
		grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx_c;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
		(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
	}

	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dy_u = dy_d;
		dx_c = dx_r/2.0 + dx_l/2.0;
		dy_c = dy_u/2.0 + dy_d/2.0;
		
		//right edge

		//up edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x, y+dy_u/2.0, 3, true);
		grad_C_y = neumann_condition_C(x, y+dy_u/2.0, 3, false);
		k_ny1 = (i+1)*this->n_x+j;
		(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
		(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge

		//left edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
		grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
		(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge
		
		//down edge
		//left hand side
		//right hand side
		grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx_c;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
		(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
	}
}

void darcy::compute_diffusion_C(dense_matrix *flux, dense_matrix *grad_C_x_left, dense_matrix *grad_C_y_left, dense_matrix *grad_C_x_down, dense_matrix *grad_C_y_down, permeability_matrix *D, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double DC_x = 0.0;
	double DC_y = 0.0;
	double grad_C_x = 0.0;
	double grad_C_y = 0.0;

	double speed_diffusion = 0.0;

	int k;
	int k_nx1, k_ny1;

	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			//right edge
			//right hand side
			k_nx1 = i*(this->n_x+1)+j;
			grad_C_x = (*grad_C_x_left)(k_nx1 + 1, 0);	//on edge
			grad_C_y = (*grad_C_y_left)(k_nx1 + 1, 0);	//on edge
			if(j==this->n_x-1) {
				DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
			} else {
				DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			}
			//cout << "[" << k << "]: " << dx_r << " | " << dx_l << " | " << dy_u << " | " << dy_d  << endl;
			//cout << "[" << k << "]1DC_x: " << DC_x << endl;
			(*flux)(k, 0) -= dy_c*DC_x;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//right hand side
			k_ny1 = i*this->n_x+j;
			grad_C_x = (*grad_C_x_down)(k_ny1 + N, 0);	//on edge
			grad_C_y = (*grad_C_y_down)(k_ny1 + N, 0);	//on edge
			if(i==this->n_y-1) {
				DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
			} else {
				DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			}
			//cout << "[" << k << "]2DC_y: " << DC_y << endl;
			(*flux)(k, 0) -= dx_c*DC_y;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//right hand side
			k_nx1 = i*(this->n_x+1)+j;
			grad_C_x = (*grad_C_x_left)(k_nx1, 0);	//on edge
			grad_C_y = (*grad_C_y_left)(k_nx1, 0);	//on edge
			if(j==0) {
				DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
			} else {
				DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			}
			//test
			DC_x = -DC_x;
			//cout << "[" << k << "]3DC_x: " << DC_x << endl;
			(*flux)(k, 0) -= dy_c*DC_x;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//right hand side
			k_ny1 = i*this->n_x+j;
			grad_C_x = (*grad_C_x_down)(k_ny1, 0);	//on edge
			grad_C_y = (*grad_C_y_down)(k_ny1, 0);	//on edge
			if(i==0) {
				DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
				speed_diffusion = max(fabs( D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
			} else {
				DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			}
			//test
			DC_y = -DC_y;
			//cout << "[" << k << "]4DC_y: " << DC_y << endl;
			(*flux)(k, 0) -= dx_c*DC_y;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		}
	}
	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			dxy = dx_c*dy_c;
			(*flux)(k, 0) /= dxy;
		}
	}
}

void darcy::compute_grad_h(dense_matrix *grad_h_x_left, dense_matrix *grad_h_y_left, dense_matrix *grad_h_x_down, dense_matrix *grad_h_y_down, functiontype3 neumann_condition_u) {	//u is the solution of darcy's equation
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;

	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	int i, j, k;
	int k_nx1, k_ny1;

	double x, y;
			
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//convection=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
			dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
			dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			//right edge

			//up edge

			//left edge
			//left hand side
			grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
			grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
			k_nx1 = i*(this->n_x+1)+j;
			(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
			(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

			//down edge
			//left hand side
			grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx_c;
			grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
			k_ny1 = i*this->n_x+j;
			(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
			(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
		}
	}

	//borders now
	//4 corners
	//bottom left
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//right edge

	//up edge

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
	(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	k_ny1 = i*this->n_x+j;
	(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
	(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge

	//bottom right
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
	(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

	//up edge

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
	grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
	(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	k_ny1 = i*this->n_x+j;
	(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
	(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
	
	//top left
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//right edge

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	k_ny1 = (i+1)*this->n_x+j;
	(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
	(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
	(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx_c;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
	(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
	
	//top right
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
	(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	k_ny1 = (i+1)*this->n_x+j;
	(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
	(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
	grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
	(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx_c;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
	(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = dy_u;
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//right edge

		//up edge

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
		grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
		(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

		//down edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 0, true);
		grad_u_y = neumann_condition_u(x, y, 0, false);
		k_ny1 = i*this->n_x+j;
		(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
		(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = dx_r;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;

		//right edge

		//up edge

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 1, true);
		grad_u_y = neumann_condition_u(x, y, 1, false);
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
		(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx_c;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
		(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
	}
	//right
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dx_r = dx_l;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 2, true);
		grad_u_y = neumann_condition_u(x, y, 2, false);
		k_nx1 = i*(this->n_x+1)+j+1;
		(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
		(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge
	
		//up edge

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
		grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
		(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge	
		
		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx_c;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
		(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
	}

	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dy_u = dy_d;
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		
		//right edge

		//up edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 3, true);
		grad_u_y = neumann_condition_u(x, y, 3, false);
		k_ny1 = (i+1)*this->n_x+j;
		(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
		(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
		grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
		(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge
		
		//down edge
		//left hand side
		grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx_c;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
		(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
	}
}

void darcy::compute_velocity_h(dense_matrix *grad_h_x_left, dense_matrix *grad_h_y_left, dense_matrix *grad_h_x_down, dense_matrix *grad_h_y_down, dense_matrix *ve_x_left, dense_matrix *ve_y_down, double *max_speed_convection) {	//u is the solution of darcy's equation
	int N = this->n_x;

	double q_x = 0.0;
	double q_y = 0.0;
	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	double speed_convection = 0.0;

	int i, j, k;
	int k_nx1, k_ny1;
			
	
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//convection=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			//right edge

			//up edge

			//left edge
			k_nx1 = i*(this->n_x+1)+j;
			grad_u_x = (*grad_h_x_left)(k_nx1, 0);
			grad_u_y = (*grad_h_y_left)(k_nx1, 0);
			q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			q_x = -q_x;	//multiply by edge's normal
			k_nx1 = i*(this->n_x+1)+j;
			(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
			speed_convection = fabs(q_x);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//down edge
			k_ny1 = i*this->n_x+j;
			grad_u_x = (*grad_h_x_down)(k_ny1, 0);
			grad_u_y = (*grad_h_y_down)(k_ny1, 0);
			q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
			q_y = -q_y;	//multiply by edge's normal
			k_ny1 = i*this->n_x+j;
			(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
			speed_convection = fabs(q_y);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	//right edge

	//up edge

	//left edge
	//doesn't exist
	//neumann condition
	k_nx1 = i*(this->n_x+1)+j;
	grad_u_x = (*grad_h_x_left)(k_nx1, 0);
	grad_u_y = (*grad_h_y_left)(k_nx1, 0);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	q_x = -q_x;	//multiply by edge's normal
	(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition
	k_ny1 = i*this->n_x+j;
	grad_u_x = (*grad_h_x_down)(k_ny1, 0);
	grad_u_y = (*grad_h_y_down)(k_ny1, 0);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	q_y = -q_y;	//multiply by edge's normal
	k_ny1 = i*this->n_x+j;
	(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//bottom right
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;

	//right edge
	//doesn't exist
	//neumann condition
	k_nx1 = i*(this->n_x+1)+j+1;
	grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
	grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	q_x = -q_x;	//multiply by edge's normal
	k_nx1 = i*(this->n_x+1)+j+1;
	(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge

	//left edge
	k_nx1 = i*(this->n_x+1)+j;
	grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
	grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	q_x = -q_x;	//multiply by edge's normal
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition
	k_ny1 = i*this->n_x+j;
	grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
	grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	q_y = -q_y;	//multiply by edge's normal
	k_ny1 = i*this->n_x+j;
	(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//top left
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	//right edge

	//up edge
	//doesn't exist
	//neumann condition
	k_ny1 = (i+1)*this->n_x+j;
	grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
	grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	q_y = -q_y;	//multiply by edge's normal
	k_ny1 = (i+1)*this->n_x+j;
	(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//doesn't exist
	//neumann condition
	k_nx1 = i*(this->n_x+1)+j;
	grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
	grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	q_x = -q_x;	//multiply by edge's normal
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	k_ny1 = i*this->n_x+j;
	grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
	grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	q_y = -q_y;	//multiply by edge's normal
	k_ny1 = i*this->n_x+j;
	(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//top right
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;

	//right edge
	//doesn't exist
	//neumann condition
	k_nx1 = i*(this->n_x+1)+j+1;
	grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
	grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	q_x = -q_x;	//multiply by edge's normal
	k_nx1 = i*(this->n_x+1)+j+1;
	(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//doesn't exist
	//neumann condition
	k_ny1 = i*this->n_x+j;
	grad_u_x = (*grad_h_x_down)(k_ny1 + N, 0);	//on edge
	grad_u_y = (*grad_h_y_down)(k_ny1 + N, 0);	//on edge
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	q_y = -q_y;	//multiply by edge's normal
	k_ny1 = (i+1)*this->n_x+j;
	(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	k_nx1 = i*(this->n_x+1)+j;
	grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
	grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	q_x = -q_x;	//multiply by edge's normal
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	k_ny1 = i*this->n_x+j;
	grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
	grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	q_y = -q_y;	//multiply by edge's normal
	k_ny1 = i*this->n_x+j;
	(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		k = i*this->n_x+j;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		//right edge

		//up edge

		//left edge
		k_nx1 = i*(this->n_x+1)+j;
		grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
		grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		q_x = -q_x;	//multiply by edge's normal
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		//doesn't exist
		//neumann condition
		k_ny1 = i*this->n_x+j;
		grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
		grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		q_y = -q_y;	//multiply by edge's normal
		speed_convection = fabs(q_y);
		k_ny1 = i*this->n_x+j;
		(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		k = i*this->n_x+j;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		//right edge

		//up edge

		//left edge
		//doesn't exist
		//neumann condition
		k_nx1 = i*(this->n_x+1)+j;
		grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
		grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		q_x = -q_x;	//multiply by edge's normal
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		k_ny1 = i*this->n_x+j;
		grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
		grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		q_y = -q_y;	//multiply by edge's normal
		k_ny1 = i*this->n_x+j;
		(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//right
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		k = i*this->n_x+j;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		//right edge
		//doesn't exist
		//neumann condition
		k_nx1 = i*(this->n_x+1)+j+1;
		grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
		grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		q_x = -q_x;	//multiply by edge's normal
		k_nx1 = i*(this->n_x+1)+j+1;
		(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
		//up edge

		//left edge
		k_nx1 = i*(this->n_x+1)+j;
		grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
		grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge			
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		q_x = -q_x;	//multiply by edge's normal
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		k_ny1 = i*this->n_x+j;
		grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
		grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		q_y = -q_y;	//multiply by edge's normal
		k_ny1 = i*this->n_x+j;
		(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}

	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		k = i*this->n_x+j;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		//right edge

		//up edge
		//doesn't exist
		//neumann condition
		k_ny1 = (i+1)*this->n_x+j;
		grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
		grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		q_y = -q_y;	//multiply by edge's normal
		k_ny1 = (i+1)*this->n_x+j;
		(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		k_nx1 = i*(this->n_x+1)+j;
		grad_u_x = (*grad_h_x_left)(k_nx1, 0);	//on edge
		grad_u_y = (*grad_h_y_left)(k_nx1, 0);	//on edge
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		q_x = -q_x;	//multiply by edge's normal
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		k_ny1 = i*this->n_x+j;
		grad_u_x = (*grad_h_x_down)(k_ny1, 0);	//on edge
		grad_u_y = (*grad_h_y_down)(k_ny1, 0);	//on edge
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		q_y = -q_y;	//multiply by edge's normal
		k_ny1 = i*this->n_x+j;
		(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
}

void darcy::compute_convection_h(dense_matrix *concentration, dense_matrix *flux, dense_matrix *ve_x_left, dense_matrix *ve_y_down) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}

	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double ve_x_r = 0.0;
	double ve_x_l = 0.0;

	double ve_y_u = 0.0;
	double ve_y_d = 0.0;

	double C = 0.0;

	int k;
	int k_nx1, k_ny1;

	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			k_nx1 = i*(this->n_x+1)+j;
			k_ny1 = i*this->n_x+j;
			ve_x_r = -(*ve_x_left)(k_nx1 + 1, 0);
			ve_x_l = (*ve_x_left)(k_nx1, 0);

			ve_y_u = -(*ve_y_down)(k_ny1 + N, 0);
			ve_y_d = (*ve_y_down)(k_ny1, 0);

			/*ve_x_r = -1;
			ve_x_u = -1;
			ve_x_l = -1;
			ve_x_d = -1;

			ve_y_r = 0;
			ve_y_u = 0;
			ve_y_l = 0;
			ve_y_d = 0;*/

			//right edge
			C = 0.0;
			if(ve_x_r>0) { //goes from left to right
				C = (*concentration)(k, 0);
			} else if(ve_x_r<0) { //goes from right to left
				if(j!=this->n_x-1) {
					C = (*concentration)(k+1, 0);
				}
			}
			(*flux)(k, 0) += dy_c*ve_x_r*C;

			//up edge
			C = 0.0;
			if(ve_y_u>0) { //goes from down to up
				C = (*concentration)(k, 0);
			} else if(ve_y_u<0) { //goes from up to down
				if(i!=this->n_y-1) {
					C = (*concentration)(k+N, 0);
				}
			}
			(*flux)(k, 0) += dx_c*ve_y_u*C;

			//left edge
			C = 0.0;
			if(ve_x_l>0) { //goes from left to right
				C = (*concentration)(k, 0);
			} else if(ve_x_l<0) { //goes from right to left
				if(j!=0) {
					C = (*concentration)(k-1, 0);
				}
			}
			(*flux)(k, 0) += dy_c*ve_x_l*C;

			//down edge
			C = 0.0;
			if(ve_y_d>0) { //goes from down to up
				C = (*concentration)(k, 0);
			} else if(ve_y_d<0) { //goes from up to down
				if(i!=0) {
					C = (*concentration)(k-N, 0);
				}
			}
			(*flux)(k, 0) += dx_c*ve_y_d*C;
		}
	}
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			dxy = dx_c*dy_c;
			
			(*flux)(k, 0) /= dxy;
		}
	}
}

void darcy::compute_convection_h_MUSCL(dense_matrix *concentration, dense_matrix *flux, dense_matrix *ve_x_left, dense_matrix *ve_y_down, functiontype4 limiter, string limiter_name) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}

	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double ve_x_r = 0.0;
	double ve_x_l = 0.0;

	double ve_y_u = 0.0;
	double ve_y_d = 0.0;

	double C = 0.0;

	double r_f_numerator = 0.0;	//Sweby's r-factor
	double r_f_denominator = 0.0;	//Sweby's r-factor

	int k;
	int k_nx1, k_ny1;

	//cout << "middle" << endl;
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			k_nx1 = i*(this->n_x+1)+j;
			k_ny1 = i*this->n_x+j;
			ve_x_r = -(*ve_x_left)(k_nx1 + 1, 0);
			ve_x_l = (*ve_x_left)(k_nx1, 0);

			ve_y_u = -(*ve_y_down)(k_ny1 + N, 0);
			ve_y_d = (*ve_y_down)(k_ny1, 0);

			/*ve_x_r = -1;
			ve_x_u = -1;
			ve_x_l = -1;
			ve_x_d = -1;

			ve_y_r = 0;
			ve_y_u = 0;
			ve_y_l = 0;
			ve_y_d = 0;*/

			//right edge
			C = 0.0;
			if(ve_x_r>0) { //goes from left to right
				if(j!=0 && j!=this->n_x-1) {
					//C = (*concentration)(k, 0);
					r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
					r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
					C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
				} else {	//ordre 1, we don't know Ck+1...
					C = (*concentration)(k, 0);
				}
			} else if(ve_x_r<0) { //goes from right to left
				if(j == this->n_x-2) {
					C = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
				} else if(j!=this->n_x-1) {	//else it doesn't exist
					r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
					C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
				}
			}
			(*flux)(k, 0) += dy_c*ve_x_r*C;

			//up edge
			C = 0.0;
			if(ve_y_u>0) { //goes from down to up
				if(i!=0 && i!=this->n_y-1) {
				//C = (*concentration)(k, 0);
					r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
					r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
					C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
				} else {	//ordre 1, we don't know Ck+N...
					C = (*concentration)(k, 0);
				}
			} else if(ve_y_u<0) { //goes from up to down
				if(i == this->n_y-2) {
					C = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
				} else if(i!=this->n_y-1) {
					r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
					C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
				}
			}
			(*flux)(k, 0) += dx_c*ve_y_u*C;

			//left edge
			C = 0.0;
			if(ve_x_l>0) { //goes from left to right
				if(j!=0 && j!=this->n_x-1) {
					//C = (*concentration)(k, 0);
					r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
					r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
					C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
				} else {	//ordre 1, we don't know Ck-1...
					C = (*concentration)(k, 0);
				}
			} else if(ve_x_l<0) { //goes from right to left
				if(j == 1) {
					C = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
				} else if(j!=0) {
					r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
					C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
				}
			}
			(*flux)(k, 0) += dy_c*ve_x_l*C;

			//down edge
			C = 0.0;
			if(ve_y_d>0) { //goes from down to up
				if(i!=0 && i!=this->n_y-1) {
					//C = (*concentration)(k, 0);
					r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
					r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
					C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
				} else {	//ordre 1, we don't know Ck+N or Ck-N...
					C = (*concentration)(k, 0);
				}
			} else if(ve_y_d<0) { //goes from up to down
				if(i == 1) {
					C = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
				} else if(i!=0) {
					r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
					C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
				}
			}
			(*flux)(k, 0) += dx_c*ve_y_d*C;
		}
	}
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			dxy = dx_c*dy_c;
			
			(*flux)(k, 0) /= dxy;
		}
	}
}

void darcy::compute_flux_backward(dense_matrix *concentration, sparse_matrix *flux_system, dense_matrix *flux_boundary, functiontype3 neumann_condition_u, permeability_matrix *D, functiontype3 neumann_condition_C, double *max_speed_convection, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	/*for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}*/
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux_boundary)(k, 0) = 0.0;
	}
	flux_system->clear();
	int N = this->n_x;

	double dx = this->delta_x;
	double dy = this->delta_y;
	double delta_1_x_y = 1/(dx*dy);

	double q_x = 0.0;
	double q_y = 0.0;
	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	double DC_x = 0.0;
	double DC_y = 0.0;
	double grad_C_x = 0.0;
	double grad_C_y = 0.0;

	double speed_convection = 0.0;
	double speed_diffusion = 0.0;

	int i, j, k;

	double x, y;

	//cout << "middle" << endl;
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//cout << "flux(" << i << "," << j << ")" << endl;
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			//cout << k << endl;
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			//right edge
			//left hand side
			grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
			q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
			//right hand side
			grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
			grad_C_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
			DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
			flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
			flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N
			flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N+1
			flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N
			flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N+1
			if(q_x>0) { //goes from left to right
				//C = (*concentration)(k, 0);
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			} else if(q_x<0) { //goes from right to left
				//C = (*concentration)(k+1, 0);
				flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dy*q_x*C;
			//(*flux)(k, 0) -= dy*DC_x;
			speed_convection = fabs(q_x);
			speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//left hand side
			grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
			grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
			q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
			//right hand side
			grad_C_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
			grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
			DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
			flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+1
			flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck-1
			flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
			flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+N+1
			flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck+N-1
			if(q_y>0) { //goes from down to up
				//C = (*concentration)(k, 0);
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			} else if(q_y<0) { //goes from up to down
				//C = (*concentration)(k+N, 0);
				flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dx*q_y*C;
			//(*flux)(k, 0) -= dx*DC_y;
			speed_convection = fabs(q_y);
			speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//left hand side
			grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
			q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			//right hand side
			grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
			grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
			DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
			//test
			q_x = -q_x;
			DC_x = -DC_x;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
			flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
			flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N
			flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N
			flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N-1
			flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N-1
			if(q_x>0) { //goes from left to right
				//C = (*concentration)(k, 0);
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			} else if(q_x<0) { //goes from right to left
				//C = (*concentration)(k-1, 0);
				flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dy*q_x*C;
			//(*flux)(k, 0) -= dy*DC_x;
			speed_convection = fabs(q_x);
			speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//left hand side
			grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
			grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
			q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
			//right hand side
			grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
			grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
			DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
			//test
			q_y = -q_y;
			DC_y = -DC_y;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
			flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck+1
			flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-1
			flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
			flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck-N+1
			flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-N-1
			if(q_y>0) { //goes from down to up
				//C = (*concentration)(k, 0);
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			} else if(q_y<0) { //goes from up to down
				//C = (*concentration)(k-N, 0);
				flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dx*q_y*C;
			//(*flux)(k, 0) -= dx*DC_y;
			speed_convection = fabs(q_y);
			speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//cout << "flux(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
	DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N
	flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck+1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);
		flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
	grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
	flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N+1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 1, true);
	grad_C_y = neumann_condition_C(x, y, 1, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 0, true);
	grad_C_y = neumann_condition_C(x, y, 0, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//bottom right
	//cout << "flux(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 2, true);
	grad_C_y = neumann_condition_C(x, y, 2, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
	grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck-1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N
	flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N-1
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
	DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck
	flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N-1
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);
		flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 0, true);
	grad_C_y = neumann_condition_C(x, y, 0, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
	//top left
	//cout << "flux(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
	DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+1
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N
	flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N+1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);
		flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 3, true);
	grad_C_y = neumann_condition_C(x, y, 3, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 1, true);
	grad_C_y = neumann_condition_C(x, y, 1, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
	flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N+1
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);
		flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
	//top right
	//cout << "flux(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 2, true);
	grad_C_y = neumann_condition_C(x, y, 2, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 3, true);
	grad_C_y = neumann_condition_C(x, y, 3, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck-1
	flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N-1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);
		flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-1
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N
	flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N-1
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);
		flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck+1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);
			flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+1
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+N+1
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck+N-1
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N-1
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);
			flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 0, true);
		grad_u_y = neumann_condition_u(x, y, 0, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 0, true);
		grad_C_y = neumann_condition_C(x, y, 0, false);
		DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N+1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N+1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);
			flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N+1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 1, true);
		grad_u_y = neumann_condition_u(x, y, 1, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 1, true);
		grad_C_y = neumann_condition_C(x, y, 1, false);
		DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N+1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);
			flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 2, true);
		grad_u_y = neumann_condition_u(x, y, 2, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 2, true);
		grad_C_y = neumann_condition_C(x, y, 2, false);
		DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
		(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N-1
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N-1
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N-1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);
			flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N-1
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);
			flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N+1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);
			flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 3, true);
		grad_u_y = neumann_condition_u(x, y, 3, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 3, true);
		grad_C_y = neumann_condition_C(x, y, 3, false);
		DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
		(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck-1
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N-1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);
			flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck+1
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck-N+1
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-N-1
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);
			flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}

	//for(int k=0; k<this->n_x*this->n_y; k++) {
	//	flux_system->add_element(k, k, 1.0);	//Ck
	//}
	//(*flux).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "flux");
	//return max_speed;
}

void darcy::compute_convection_backward(dense_matrix *concentration, sparse_matrix *convection_system, dense_matrix *convection_boundary, functiontype3 neumann_condition_u, permeability_matrix *D, functiontype3 neumann_condition_C, double *max_speed_convection) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*convection_boundary)(k, 0) = 0.0;
	}
	convection_system->clear();
	int N = this->n_x;

	double dx = this->delta_x;
	double dy = this->delta_y;
	double delta_1_x_y = 1/(dx*dy);

	double q_x = 0.0;
	double q_y = 0.0;
	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	double speed_convection = 0.0;

	int i, j, k;

	double x, y;

	//cout << "middle" << endl;
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//cout << "convection(" << i << "," << j << ")" << endl;
			//q=-K*grad(u)
			//convection=q*concentration*mesure(edge)
			//cout << k << endl;
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			//right edge
			//left hand side
			grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
			q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
			//right hand side
			if(q_x>0) { //goes from left to right
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			} else if(q_x<0) { //goes from right to left
				//C = (*concentration)(k+1, 0);
				convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dy*q_x*C;
			speed_convection = fabs(q_x);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//up edge
			//left hand side
			grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
			grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
			q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
			//right hand side
			if(q_y>0) { //goes from down to up
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			} else if(q_y<0) { //goes from up to down
				//C = (*concentration)(k+N, 0);
				convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dx*q_y*C;
			speed_convection = fabs(q_y);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//left edge
			//left hand side
			grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
			q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			//right hand side
			//test
			q_x = -q_x;
			if(q_x>0) { //goes from left to right
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			} else if(q_x<0) { //goes from right to left
				//C = (*concentration)(k-1, 0);
				convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dy*q_x*C;
			speed_convection = fabs(q_x);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//down edge
			//left hand side
			grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
			grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
			q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
			//right hand side
			//test
			q_y = -q_y;
			if(q_y>0) { //goes from down to up
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			} else if(q_y<0) { //goes from up to down
				//C = (*concentration)(k-N, 0);
				convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dx*q_y*C;
			speed_convection = fabs(q_y);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//cout << "convection(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);
		convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//bottom right
	//cout << "convection(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);
		convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//top left
	//cout << "convection(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);
		convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);
		convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//top right
	//cout << "convection(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);
		convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;


	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);
		convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//cout << "convection(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);
			convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//up edge
		//left hand side
		grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);
			convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 0, true);
		grad_u_y = neumann_condition_u(x, y, 0, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "convection(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);
			convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 1, true);
		grad_u_y = neumann_condition_u(x, y, 1, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);
			convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "convection(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 2, true);
		grad_u_y = neumann_condition_u(x, y, 2, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);
			convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);
			convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);
			convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//up edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 3, true);
		grad_u_y = neumann_condition_u(x, y, 3, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);
			convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		//left hand side
		grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);
			convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}

	//for(int k=0; k<this->n_x*this->n_y; k++) {
	//	convection_system->add_element(k, k, 1.0);	//Ck
	//}
	//(*convection).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "convection");
	//return max_speed;
}

void darcy::compute_diffusion_backward(dense_matrix *concentration, sparse_matrix *diffusion_system, dense_matrix *diffusion_boundary, functiontype3 neumann_condition_u, permeability_matrix *D, functiontype3 neumann_condition_C, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	/*for(int k=0; k<this->n_x*this->n_y; k++) {
		(*diffusion)(k, 0) = 0.0;
	}*/
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*diffusion_boundary)(k, 0) = 0.0;
	}
	diffusion_system->clear();
	int N = this->n_x;

	double dx = this->delta_x;
	double dy = this->delta_y;
	double delta_1_x_y = 1/(dx*dy);

	double DC_x = 0.0;
	double DC_y = 0.0;
	double grad_C_x = 0.0;
	double grad_C_y = 0.0;

	double speed_diffusion = 0.0;

	int i, j, k;

	double x, y;

	//cout << "middle" << endl;
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//cout << "diffusion(" << i << "," << j << ")" << endl;
			//q=-K*grad(u)
			//diffusion=q*concentration*mesure(edge)
			//cout << k << endl;
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			//right edge
			//left hand side
			//right hand side
			grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
			grad_C_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
			DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
			diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
			diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
			diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N
			diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N+1
			diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N
			diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N+1
			//(*diffusion)(k, 0) -= dy*DC_x;
			speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//left hand side
			//right hand side
			grad_C_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
			grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
			DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
			diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
			diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+1
			diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck-1
			diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
			diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+N+1
			diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck+N-1
			//(*diffusion)(k, 0) -= dx*DC_y;
			speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//left hand side
			//right hand side
			grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
			grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
			DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
			//test
			DC_x = -DC_x;
			diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
			diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
			diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N
			diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N
			diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N-1
			diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N-1
			//(*diffusion)(k, 0) -= dy*DC_x;
			speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//left hand side
			//right hand side
			grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
			grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
			DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
			//test
			DC_y = -DC_y;
			diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
			diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck+1
			diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-1
			diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
			diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck-N+1
			diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-N-1
			//(*diffusion)(k, 0) -= dx*DC_y;
			speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//cout << "diffusion(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	//right hand side
	grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
	DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
	diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
	diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N
	diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N+1
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck
	diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck+1
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
	grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
	diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+1
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck
	diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
	diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N+1
	diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 1, true);
	grad_C_y = neumann_condition_C(x, y, 1, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	//test
	DC_x = -DC_x;
	(*diffusion_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 0, true);
	grad_C_y = neumann_condition_C(x, y, 0, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	//test
	DC_y = -DC_y;
	(*diffusion_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//bottom right
	//cout << "diffusion(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 2, true);
	grad_C_y = neumann_condition_C(x, y, 2, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	(*diffusion_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
	grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck
	diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck-1
	diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
	diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N
	diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N-1
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
	DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
	//test
	DC_x = -DC_x;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
	diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
	diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck
	diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N-1
	diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-1
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 0, true);
	grad_C_y = neumann_condition_C(x, y, 0, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	//test
	DC_y = -DC_y;
	(*diffusion_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
	//top left
	//cout << "diffusion(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	//right hand side
	grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
	DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
	diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck
	diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+1
	diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N
	diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N+1
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 3, true);
	grad_C_y = neumann_condition_C(x, y, 3, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	(*diffusion_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 1, true);
	grad_C_y = neumann_condition_C(x, y, 1, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	//test
	DC_x = -DC_x;
	(*diffusion_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
	//test
	DC_y = -DC_y;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
	diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck+1
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck
	diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
	diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N+1
	diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
	//top right
	//cout << "diffusion(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//diffusion=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 2, true);
	grad_C_y = neumann_condition_C(x, y, 2, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	(*diffusion_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 3, true);
	grad_C_y = neumann_condition_C(x, y, 3, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	(*diffusion_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
	//test
	DC_x = -DC_x;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
	diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck
	diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N
	diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck-1
	diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N-1
	//(*diffusion)(k, 0) -= dy*DC_x;
	speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	//right hand side
	grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
	//test
	DC_y = -DC_y;
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
	diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck
	diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-1
	diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
	diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N
	diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N-1
	//(*diffusion)(k, 0) -= dx*DC_y;
	speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//cout << "diffusion(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N
		diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N+1
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck+1
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		//right hand side
		grad_C_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+1
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck-1
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+N+1
		diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck+N-1
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		DC_x = -DC_x;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck
		diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N-1
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-1
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 0, true);
		grad_C_y = neumann_condition_C(x, y, 0, false);
		DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
		//test
		DC_y = -DC_y;
		(*diffusion_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "diffusion(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N
		diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N+1
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N
		diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N+1
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+1
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		diffusion_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N+1
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 1, true);
		grad_C_y = neumann_condition_C(x, y, 1, false);
		DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
		//test
		DC_x = -DC_x;
		(*diffusion_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//left hand side
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		DC_y = -DC_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck+1
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N+1
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "diffusion(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 2, true);
		grad_C_y = neumann_condition_C(x, y, 2, false);
		DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
		(*diffusion_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
		//up edge
		//left hand side
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck-1
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N
		diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N-1
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		DC_x = -DC_x;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		diffusion_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N
		diffusion_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N-1
		diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N-1
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		//right hand side
		grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		DC_y = -DC_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-1
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N
		diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N-1
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//diffusion=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+1
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N
		diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N+1
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 3, true);
		grad_C_y = neumann_condition_C(x, y, 3, false);
		DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
		(*diffusion_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		DC_x = -DC_x;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck-1
		diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N-1
		//(*diffusion)(k, 0) -= dy*DC_x;
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		//right hand side
		grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		DC_y = -DC_y;
		diffusion_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		diffusion_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck+1
		diffusion_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-1
		diffusion_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		diffusion_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck-N+1
		diffusion_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-N-1
		//(*diffusion)(k, 0) -= dx*DC_y;
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}

	//for(int k=0; k<this->n_x*this->n_y; k++) {
	//	diffusion_system->add_element(k, k, 1.0);	//Ck
	//}
	//(*diffusion).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "diffusion");
	//return max_speed;
}

void darcy::compute_flux_backward_MUSCL_limiter(dense_matrix *concentration, sparse_matrix *flux_system, dense_matrix *flux_boundary, functiontype4 limiter, string limiter_name, functiontype3 neumann_condition_u, permeability_matrix *D, functiontype3 neumann_condition_C, double *max_speed_convection, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	/*for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}*/
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux_boundary)(k, 0) = 0.0;
	}
	flux_system->clear();
	int N = this->n_x;

	double dx = this->delta_x;
	double dy = this->delta_y;
	double delta_1_x_y = 1/(dx*dy);

	double q_x = 0.0;
	double q_y = 0.0;
	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	double DC_x = 0.0;
	double DC_y = 0.0;
	double grad_C_x = 0.0;
	double grad_C_y = 0.0;

	double speed_convection = 0.0;
	double speed_diffusion = 0.0;

	double r_f_numerator = 0.0;	//Sweby's r-factor
	double r_f_denominator = 0.0;	//Sweby's r-factor

	int i, j, k;

	double x, y;

	//cout << "middle" << endl;
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//cout << "flux(" << i << "," << j << ")" << endl;
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			//cout << k << endl;
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			//right edge
			//left hand side
			grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
			q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
			//right hand side
			grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
			grad_C_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
			DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
			flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
			flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N
			flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N+1
			flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N
			flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N+1
			if(q_x>0) { //goes from left to right
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				//(*flux)(k, 0) += dy*q_x*C;
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
				flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
				flux_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_x<0) { //goes from right to left
				if(j == this->n_x-2) {
					//C = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
					flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
				} else {
					r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
					//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
					flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
					flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					flux_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
				}
				//C = (*concentration)(k+1, 0);
				//flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dy*q_x*C;
			//(*flux)(k, 0) -= dy*DC_x;
			speed_convection = fabs(q_x);
			speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//left hand side
			grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
			grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
			q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
			//right hand side
			grad_C_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
			grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
			DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
			flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+1
			flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck-1
			flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
			flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+N+1
			flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck+N-1
			if(q_y>0) { //goes from down to up
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
				flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
				flux_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_y<0) { //goes from up to down
				if(i == this->n_y-2) {
					//C = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
					flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
				} else {
					r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
					//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
					flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
					flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					flux_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
				}
				//C = (*concentration)(k+N, 0);
				//flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dx*q_y*C;
			//(*flux)(k, 0) -= dx*DC_y;
			speed_convection = fabs(q_y);
			speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//left hand side
			grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
			q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			//right hand side
			grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
			grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
			DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
			//test
			q_x = -q_x;
			DC_x = -DC_x;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
			flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
			flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N
			flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N
			flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N-1
			flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N-1
			if(q_x>0) { //goes from left to right
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
				flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
				flux_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_x<0) { //goes from right to left
				if(j == 1) {
					//C = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
					flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
				} else {
					r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
					//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
					flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
					flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					flux_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
				}
				//C = (*concentration)(k-1, 0);
				//flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dy*q_x*C;
			//(*flux)(k, 0) -= dy*DC_x;
			speed_convection = fabs(q_x);
			speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//left hand side
			grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
			grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
			q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
			//right hand side
			grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
			grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
			DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
			//test
			q_y = -q_y;
			DC_y = -DC_y;
			flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
			flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck+1
			flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-1
			flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
			flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck-N+1
			flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-N-1
			if(q_y>0) { //goes from down to up
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
				flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
				flux_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_y<0) { //goes from up to down
				if(i == 1) {
					//C = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
					flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
				} else {
					r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
					//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
					flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
					flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					flux_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
				}
				//C = (*concentration)(k-N, 0);
				//flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				//C = 0.0;
			}
			//(*flux)(k, 0) += dx*q_y*C;
			//(*flux)(k, 0) -= dx*DC_y;
			speed_convection = fabs(q_y);
			speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//cout << "flux(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
	DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N
	flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck+1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);
		r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
		//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
		flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
		//C = (*concentration)(k+1, 0);
		//flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
	grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
	flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N+1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0);
		flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 1, true);
	grad_C_y = neumann_condition_C(x, y, 1, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 0, true);
	grad_C_y = neumann_condition_C(x, y, 0, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//bottom right
	//cout << "flux(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 2, true);
	grad_C_y = neumann_condition_C(x, y, 2, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
	grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck-1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N
	flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N-1
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0);
		flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
	DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
	flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck
	flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N-1
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0);
		flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 0, true);
	grad_C_y = neumann_condition_C(x, y, 0, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
	//top left
	//cout << "flux(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
	DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+1
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N
	flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N+1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
		//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
		//C = (*concentration)(k+1, 0);
		flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 3, true);
	grad_C_y = neumann_condition_C(x, y, 3, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 1, true);
	grad_C_y = neumann_condition_C(x, y, 1, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck+1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
	flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N+1
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0);
		flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
	//top right
	//cout << "flux(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 2, true);
	grad_C_y = neumann_condition_C(x, y, 2, false);
	DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
	(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	grad_C_x = neumann_condition_C(x, y, 3, true);
	grad_C_y = neumann_condition_C(x, y, 3, false);
	DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
	(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
	//test
	q_x = -q_x;
	DC_x = -DC_x;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck-1
	flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N-1
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0);
		flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dy*q_x*C;
	//(*flux)(k, 0) -= dy*DC_x;
	speed_convection = fabs(q_x);
	speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
	grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
	//test
	q_y = -q_y;
	DC_y = -DC_y;
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
	flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck
	flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-1
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
	flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N
	flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N-1
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0);
		flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		flux_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*flux)(k, 0) += dx*q_y*C;
	//(*flux)(k, 0) -= dx*DC_y;
	speed_convection = fabs(q_y);
	speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+N+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck+1
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			flux_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == this->n_x-2) {
				//C = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
				flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
				flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			}
			//C = (*concentration)(k+1, 0);
			//flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+1
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.25/dx));	//Ck+N+1
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.25/dx));	//Ck+N-1
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
			//C = (*concentration)(k+N, 0);
			flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			flux_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck+N-1
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-1
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			flux_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == 1) {
				//C = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
				flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
				flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			}
			//C = (*concentration)(k-1, 0);
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 0, true);
		grad_u_y = neumann_condition_u(x, y, 0, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 0, true);
		grad_C_y = neumann_condition_C(x, y, 0, false);
		DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.25/dy));	//Ck+N+1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.25/dy));	//Ck-N+1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
			//C = (*concentration)(k+1, 0);
			flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			flux_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		flux_system->add_element(k, k+N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N+1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			flux_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			if(i == this->n_y-2) {
				//C = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
				flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
				flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			}
			//C = (*concentration)(k+N, 0);
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 1, true);
		grad_u_y = neumann_condition_u(x, y, 1, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 1, true);
		grad_C_y = neumann_condition_C(x, y, 1, false);
		DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N+1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			flux_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			if(i == 1) {
				//C = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
				flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
				flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			}
			//C = (*concentration)(k-N, 0);
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 2, true);
		grad_u_y = neumann_condition_u(x, y, 2, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 2, true);
		grad_C_y = neumann_condition_C(x, y, 2, false);
		DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
		(*flux_boundary)(k, 0) += delta_1_x_y*dy*DC_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
		grad_C_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 2)*(1/dy));	//Ck+N
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(0.5/dx));	//Ck+N
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k+N, 1)*(-0.5/dx));	//Ck+N-1
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			flux_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			if(i == this->n_y-2) {
				//C = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
				flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
				flux_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			}
			//C = (*concentration)(k+N, 0);
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		flux_system->add_element(k, k+N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N
		flux_system->add_element(k, k+N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.25/dy));	//Ck+N-1
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.25/dy));	//Ck-N-1
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
			//C = (*concentration)(k-1, 0);
			flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			flux_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.5/dx));	//Ck-N
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.5/dx));	//Ck-N-1
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			flux_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			if(i == 1) {
				//C = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
				flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
				flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
				flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			}
			//C = (*concentration)(k-N, 0);
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}
	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 1)*(1/dx));	//Ck+1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(0.5/dy));	//Ck+1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dy*D->get_K(k, k+1, 2)*(-0.5/dy));	//Ck-N+1
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			flux_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == this->n_x-2) {
				//C = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
				flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
				flux_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			}
			//C = (*concentration)(k+1, 0);
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 3, true);
		grad_u_y = neumann_condition_u(x, y, 3, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		grad_C_x = neumann_condition_C(x, y, 3, true);
		grad_C_y = neumann_condition_C(x, y, 3, false);
		DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
		(*flux_boundary)(k, 0) -= delta_1_x_y*dx*DC_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_C_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
		//test
		q_x = -q_x;
		DC_x = -DC_x;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(-1/dx));	//Ck
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 1)*(1/dx));	//Ck-1
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(-0.5/dy));	//Ck-1
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dy*D->get_K(k-1, k, 2)*(0.5/dy));	//Ck-N-1
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			flux_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == 1) {
				//C = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
				flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
				flux_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
				flux_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				flux_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			}
			//C = (*concentration)(k-1, 0);
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dy*q_x*C;
		//(*flux)(k, 0) -= dy*DC_x;
		speed_convection = fabs(q_x);
		speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
		grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
		//test
		q_y = -q_y;
		DC_y = -DC_y;
		flux_system->add_element(k, k, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(-1/dy));	//Ck
		flux_system->add_element(k, k+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck+1
		flux_system->add_element(k, k-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-1
		flux_system->add_element(k, k-N, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 2)*(1/dy));	//Ck-N
		flux_system->add_element(k, k-N+1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(-0.25/dx));	//Ck-N+1
		flux_system->add_element(k, k-N-1, -1.0*delta_1_x_y*dx*D->get_K(k, k-N, 1)*(0.25/dx));	//Ck-N-1
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
			//C = (*concentration)(k-N, 0);
			flux_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			flux_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			flux_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*flux)(k, 0) += dx*q_y*C;
		//(*flux)(k, 0) -= dx*DC_y;
		speed_convection = fabs(q_y);
		speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	}

	//for(int k=0; k<this->n_x*this->n_y; k++) {
	//	flux_system->add_element(k, k, 1.0);	//Ck
	//}
	//(*flux).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "flux");
	//return max_speed;
}

void darcy::compute_convection_backward_MUSCL_limiter(dense_matrix *concentration, sparse_matrix *convection_system, dense_matrix *convection_boundary, functiontype4 limiter, string limiter_name, functiontype3 neumann_condition_u, permeability_matrix *D, functiontype3 neumann_condition_C, double *max_speed_convection) {	//u is the solution of darcy's equation
	/*for(int k=0; k<this->n_x*this->n_y; k++) {
		(*convection)(k, 0) = 0.0;
	}*/
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*convection_boundary)(k, 0) = 0.0;
	}
	convection_system->clear();
	int N = this->n_x;

	double dx = this->delta_x;
	double dy = this->delta_y;
	double delta_1_x_y = 1/(dx*dy);

	double q_x = 0.0;
	double q_y = 0.0;
	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	double speed_convection = 0.0;

	double r_f_numerator = 0.0;	//Sweby's r-factor
	double r_f_denominator = 0.0;	//Sweby's r-factor

	int i, j, k;

	double x, y;

	//cout << "middle" << endl;
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//cout << "convection(" << i << "," << j << ")" << endl;
			//q=-K*grad(u)
			//convection=q*concentration*mesure(edge)
			//cout << k << endl;
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			//right edge
			//left hand side
			grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
			q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
			//right hand side
			if(q_x>0) { //goes from left to right
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
				convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
				convection_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_x<0) { //goes from right to left
				if(j == this->n_x-2) {
					//C = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
					convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
				} else {
					r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
					//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
					convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
					convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					convection_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
				}
				//C = (*concentration)(k+1, 0);
				//convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dy*q_x*C;
			speed_convection = fabs(q_x);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//up edge
			//left hand side
			grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
			grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
			q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
			//right hand side
			if(q_y>0) { //goes from down to up
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
				convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
				convection_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_y<0) { //goes from up to down
				if(i == this->n_y-2) {
					//C = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
					convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
				} else {
					r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
					//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
					convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
					convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					convection_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
				}
				//C = (*concentration)(k+N, 0);
				//convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dx*q_y*C;
			speed_convection = fabs(q_y);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//left edge
			//left hand side
			grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
			grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
			q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			//right hand side
			//test
			q_x = -q_x;
			if(q_x>0) { //goes from left to right
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
				convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
				convection_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_x<0) { //goes from right to left
				if(j == 1) {
					//C = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
					convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
				} else {
					r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
					//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
					convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
					convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					convection_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
				}
				//C = (*concentration)(k-1, 0);
				//convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dy*q_x*C;
			speed_convection = fabs(q_x);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//down edge
			//left hand side
			grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
			grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
			q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
			//right hand side
			//test
			q_y = -q_y;
			if(q_y>0) { //goes from down to up
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
				//C = (*concentration)(k, 0);
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
				convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
				convection_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			} else if(q_y<0) { //goes from up to down
				if(i == 1) {
					//C = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
					convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
				} else {
					r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
					//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
					convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
					convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
					convection_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
				}
				//C = (*concentration)(k-N, 0);
				//convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				//C = 0.0;
			}
			//(*convection)(k, 0) += dx*q_y*C;
			speed_convection = fabs(q_y);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//cout << "convection(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);
		r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
		//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
		convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
		//C = (*concentration)(k+1, 0);
		//convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0);
		convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//bottom right
	//cout << "convection(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);
		r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
		//C = (*concentration)(k+N, 0);
		convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0);
		convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 0, true);
	grad_u_y = neumann_condition_u(x, y, 0, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//top left
	//cout << "convection(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
		//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
		//C = (*concentration)(k+1, 0);
		convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 1, true);
	grad_u_y = neumann_condition_u(x, y, 1, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0);
		convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//top right
	//cout << "convection(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//convection=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 2, true);
	grad_u_y = neumann_condition_u(x, y, 2, false);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	//right hand side
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//doesn't exist
	//neumann condition
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_u_x = neumann_condition_u(x, y, 3, true);
	grad_u_y = neumann_condition_u(x, y, 3, false);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	//right hand side
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		//C = 0.0;
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//left hand side
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
	//right hand side
	//test
	q_x = -q_x;
	if(q_x>0) { //goes from left to right
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
	} else if(q_x<0) { //goes from right to left
		r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
		//C = (*concentration)(k-1, 0);
		convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
		convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dy*q_x*C;
	speed_convection = fabs(q_x);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//left hand side
	grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//right hand side
	//test
	q_y = -q_y;
	if(q_y>0) { //goes from down to up
		//C = (*concentration)(k, 0);
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
	} else if(q_y<0) { //goes from up to down
		r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
		//C = (*concentration)(k-N, 0);
		convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
		convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		convection_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
	} else {
		//C = 0.0;
	}
	//(*convection)(k, 0) += dx*q_y*C;
	speed_convection = fabs(q_y);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//cout << "convection(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			convection_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == this->n_x-2) {
				//C = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
				convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
				convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			}
			//C = (*concentration)(k+1, 0);
			//convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//up edge
		//left hand side
		grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
			//C = (*concentration)(k+N, 0);
			convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			convection_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			convection_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == 1) {
				//C = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
				convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
				convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			}
			//C = (*concentration)(k-1, 0);
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 0, true);
		grad_u_y = neumann_condition_u(x, y, 0, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "convection(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
			//C = (*concentration)(k+1, 0);
			convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			convection_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			convection_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			if(i == this->n_y-2) {
				//C = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
				convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
				convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			}
			//C = (*concentration)(k+N, 0);
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 1, true);
		grad_u_y = neumann_condition_u(x, y, 1, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			convection_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			if(i == 1) {
				//C = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
				convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
				convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			}
			//C = (*concentration)(k-N, 0);
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "convection(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 2, true);
		grad_u_y = neumann_condition_u(x, y, 2, false);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
		//up edge
		//left hand side
		grad_u_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			convection_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);
			if(i == this->n_y-2) {
				//C = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
				convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
			} else {
				r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				//C = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
				convection_system->add_element(k, k+N, delta_1_x_y*dx*q_y);	//Ck+N
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k+N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+N
			}
			//C = (*concentration)(k+N, 0);
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
		} else if(q_x<0) { //goes from right to left
			r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
			//C = (*concentration)(k-1, 0);
			convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			convection_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		//left hand side
		grad_u_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
			convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			convection_system->add_element(k, k, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_y<0) { //goes from up to down
			if(i == 1) {
				//C = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
				convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			} else {
				r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
				convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
				convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
			}
			//C = (*concentration)(k-N, 0);
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//convection=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		//right hand side
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			convection_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == this->n_x-2) {
				//C = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
				convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
			} else {
				r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				//C = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
				convection_system->add_element(k, k+1, delta_1_x_y*dy*q_x);	//Ck+1
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k+1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck+1
			}
			//C = (*concentration)(k+1, 0);
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//up edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_u_x = neumann_condition_u(x, y, 3, true);
		grad_u_y = neumann_condition_u(x, y, 3, false);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		//right hand side
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);	//doesn't exist
			//C = 0.0;
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//left hand side
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//right hand side
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dy*q_x);	//Ck
			convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			convection_system->add_element(k, k, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
		} else if(q_x<0) { //goes from right to left
			if(j == 1) {
				//C = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
				convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
			} else {
				r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				//C = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
				convection_system->add_element(k, k-1, delta_1_x_y*dy*q_x);	//Ck-1
				convection_system->add_element(k, k, delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
				convection_system->add_element(k, k-1, -delta_1_x_y*dy*q_x*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-1
			}
			//C = (*concentration)(k-1, 0);
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dy*q_x*C;
		speed_convection = fabs(q_x);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		//left hand side
		grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//right hand side
		//test
		q_y = -q_y;
		if(q_y>0) { //goes from down to up
			//C = (*concentration)(k, 0);
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y);	//Ck
		} else if(q_y<0) { //goes from up to down
			r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			//C = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
			//C = (*concentration)(k-N, 0);
			convection_system->add_element(k, k-N, delta_1_x_y*dx*q_y);	//Ck-N
			convection_system->add_element(k, k, delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck
			convection_system->add_element(k, k-N, -delta_1_x_y*dx*q_y*0.5*limiter(r_f_numerator, r_f_denominator, limiter_name));	//Ck-N
		} else {
			//C = 0.0;
		}
		//(*convection)(k, 0) += dx*q_y*C;
		speed_convection = fabs(q_y);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}

	//for(int k=0; k<this->n_x*this->n_y; k++) {
	//	convection_system->add_element(k, k, 1.0);	//Ck
	//}
	//(*convection).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "convection");
	//return max_speed;
}

void darcy::compute_flux_streamer(dense_matrix *concentration, dense_matrix *flux, dense_matrix *ve_norm, dense_matrix *E_norm, dense_matrix *E_x, dense_matrix *E_y, dense_matrix *ve_x, dense_matrix *ve_y, functiontype3 neumann_condition_u, permeability_matrix *D, functiontype3 neumann_condition_C, double *max_speed_convection, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}
	int N = this->n_x;
	double dx = this->delta_x;
	double dy = this->delta_y;

	double ve_x_r = 0.0;
	double ve_x_u = 0.0;
	double ve_x_l = 0.0;
	double ve_x_d = 0.0;
	double ve_x_average = 0.0;
	double ve_y_r = 0.0;
	double ve_y_u = 0.0;
	double ve_y_l = 0.0;
	double ve_y_d = 0.0;
	double ve_y_average = 0.0;
	double ne = 0.0;
	double grad_V_x = 0.0;
	double grad_V_y = 0.0;
	double norm_E_r = 0.0;	//right
	double norm_E_u = 0.0;	//up
	double norm_E_l = 0.0;	//left
	double norm_E_d = 0.0;	//down
	double drift_cste = 0.0;
	//double De_cste = 0.0;
	double De_cste = 3564.8;	//1D TEST

	double Dne_x = 0.0;
	double Dne_y = 0.0;
	double grad_ne_x = 0.0;
	double grad_ne_y = 0.0;
	double norm_ve_r = 0.0;	//right
	double norm_ve_u = 0.0;	//up
	double norm_ve_l = 0.0;	//left
	double norm_ve_d = 0.0;	//down

	double speed_convection = 0.0;
	double speed_diffusion = 0.0;

	int i, j, k;
	int k_nx1, k_ny1;

	double x, y;

	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			//right edge
			//left hand side
			grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
			grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
			ve_x_r = -grad_V_x;
			ve_y_r = -grad_V_y;
			norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_r /= norm_E_r;
			ve_y_r /= norm_E_r;
			if(norm_E_r == 0.0) {
				//cout << "la !!" << endl;
				ve_x_r = 0.0;
				ve_y_r = 0.0;
			}
			if(norm_E_r/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
			} else if(norm_E_r/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
			} else if(norm_E_r/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
			}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
			ve_x_r *= drift_cste;
			ve_y_r *= drift_cste;
			/*if(ve_x_r>0) { //goes from left to right
				ne = (*concentration)(k, 0);
			} else if(ve_x_r<0) { //goes from right to left
				ne = (*concentration)(k+1, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dy*ve_x_r*ne;*/
			//right hand side
			grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
			grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
			Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
			norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
			De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
			if(norm_E_r == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_x *= De_cste;
			(*flux)(k, 0) -= dy*Dne_x;
			speed_convection = fabs(ve_x_r);
			//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//left hand side
			grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
			grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
			ve_x_u = -grad_V_x;
			ve_y_u = -grad_V_y;
			norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_u /= norm_E_u;
			ve_y_u /= norm_E_u;
			if(norm_E_u == 0.0) {
				//cout << "la !!" << endl;
				ve_x_u = 0.0;
				ve_y_u = 0.0;
			}
			if(norm_E_u/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
			} else if(norm_E_u/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
			} else if(norm_E_u/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
			}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
			ve_x_u *= drift_cste;
			ve_y_u *= drift_cste;
			/*if(ve_y_u>0) { //goes from down to up
				ne = (*concentration)(k, 0);
			} else if(ve_y_u<0) { //goes from up to down
				ne = (*concentration)(k+N, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dx*ve_y_u*ne;*/
			//right hand side
			grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
			grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
			Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
			norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
			De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
			if(norm_E_u == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_y *= De_cste;
			(*flux)(k, 0) -= dx*Dne_y;
			speed_convection = fabs(ve_y_u);
			//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//left hand side
			grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
			grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
			ve_x_l = -grad_V_x;
			ve_y_l = -grad_V_y;
			norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_l /= norm_E_l;
			ve_y_l /= norm_E_l;
			if(norm_E_l == 0.0) {
				//cout << "la !!" << endl;
				ve_x_l = 0.0;
				ve_y_l = 0.0;
			}
			if(norm_E_l/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
			} else if(norm_E_l/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
			} else if(norm_E_l/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
			}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
			ve_x_l *= drift_cste;
			ve_y_l *= drift_cste;
			//test
			k_nx1 = i*(this->n_x+1)+j;
			(*ve_x)(k_nx1, 0) = ve_x_l;
			(*E_x)(k_nx1, 0) = grad_V_x;
			ve_x_l = -ve_x_l;	//multiply by edge's normal
			/*if(ve_x_l>0) { //goes from left to right
				ne = (*concentration)(k, 0);
			} else if(ve_x_l<0) { //goes from right to left
				ne = (*concentration)(k-1, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dy*ve_x_l*ne;*/
			//right hand side
			grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
			grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
			Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
			//test
			Dne_x = -Dne_x;
			norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
			De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
			if(norm_E_l == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_x *= De_cste;
			(*flux)(k, 0) -= dy*Dne_x;
			speed_convection = fabs(ve_x_l);
			//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//left hand side
			grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
			grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
			ve_x_d = -grad_V_x;
			ve_y_d = -grad_V_y;
			norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_d /= norm_E_d;
			ve_y_d /= norm_E_d;
			if(norm_E_d == 0.0) {
				//cout << "la !!" << endl;
				ve_x_d = 0.0;
				ve_y_d = 0.0;
			}
			if(norm_E_d == 0.0) {
				//cout << "la !!" << endl;
				ve_x_d = 0.0;
				ve_y_d = 0.0;
			}
			if(norm_E_d/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
			} else if(norm_E_d/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
			} else if(norm_E_d/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
			}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
			ve_x_d *= drift_cste;
			ve_y_d *= drift_cste;
			//test
			k_ny1 = i*this->n_x+j;
			(*ve_y)(k_ny1, 0) = ve_y_d;
			(*E_y)(k_ny1, 0) = grad_V_y;
			ve_y_d = -ve_y_d;
			/*if(ve_y_d>0) { //goes from down to up
				ne = (*concentration)(k, 0);
			} else if(ve_y_d<0) { //goes from up to down
				ne = (*concentration)(k-N, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dx*ve_y_d*ne;*/
			//right hand side
			grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
			grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
			Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
			//test
			Dne_y = -Dne_y;
			norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
			De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
			if(norm_E_d == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_y *= De_cste;
			(*flux)(k, 0) -= dx*Dne_y;
			speed_convection = fabs(ve_y_d);
			//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//Roe scheme
			ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
			ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
			ne = (*concentration)(k, 0);
			//right edge
			(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
			(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1
			(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
			(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
			//up edge
			(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
			(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N
			(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
			(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
			//left edge
			(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
			(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1
			(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
			(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
			//down edge
			(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
			(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N
			(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
			(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk

			//Source term Se
			(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
			(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
			/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
			norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
			Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
			if(Se_cste > 1.5e-15) {
				Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
			} else {
				Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
			}
			(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/
		}
	}

	//borders now
	//4 corners
	//bottom left
	//cout << "flux(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_V_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		ne = (*concentration)(k+1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
	Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_V_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		ne = (*concentration)(k+N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 1, true);
	//grad_V_y = neumann_condition_u(x, y, 1, false);
	//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 0))/dx;
	grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 0) + this->u(k+N, 0) + this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 0) + this->u(k, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x)(k_nx1, 0) = ve_x_l;
	(*E_x)(k_nx1, 0) = grad_V_x;
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	//neumann condition nek-N = nek, nek+N-1 = nek+N, nek-N-1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 1, true);
	//grad_ne_y = neumann_condition_C(x, y, 1, false);
	//grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	//grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 0, true);
	//grad_V_y = neumann_condition_u(x, y, 0, false);
	//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	k_ny1 = i*this->n_x+j;
	(*ve_y)(k_ny1, 0) = ve_y_d;
	(*E_y)(k_ny1, 0) = grad_V_y;
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	//neumann condition nek-N = nek, nek-N+1 = nek+1, nek-1 = nek, nek-N-1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 0, true);
	//grad_ne_y = neumann_condition_C(x, y, 0, false);
	//grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
	//grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
	//(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1	//doesn't exist
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
	(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
	//up edge
	(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
	//(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N	//doesn't exist
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
	(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
	//left edge
	(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
	(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
	//(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk	//doesn't exist
	//down edge
	(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
	(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
	//(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk	//doesn't exist

	//Source term Se
	(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/

	//bottom right
	//cout << "flux(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 2, true);
	//grad_V_y = neumann_condition_u(x, y, 2, false);
	//grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	cout << "bottom right\t" << this->u(k, 0) << ": " << this->function_boundary_condition(x+dx, y, 2) << endl;
	grad_V_x = (this->function_boundary_condition(x+dx, y, 2) - this->u(k, 0))/dx;
	grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx, y+dy, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y-dy, 2)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	k_nx1 = i*(this->n_x+1)+j+1;
	(*ve_x)(k_nx1, 0) = ve_x_r;
	(*E_x)(k_nx1, 0) = grad_V_x;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	//neumann condition nek+1 = nek, nek+N+1 = nek+N, nek-N = nek, nek-N+1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 2, true);
	//grad_ne_y = neumann_condition_C(x, y, 2, false);
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_V_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
	grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		ne = (*concentration)(k+N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
	grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x)(k_nx1, 0) = ve_x_l;
	(*E_x)(k_nx1, 0) = grad_V_x;
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		ne = (*concentration)(k-1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
	Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N-1 = Uk-1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 0, true);
	//grad_V_y = neumann_condition_u(x, y, 0, false);
	//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y-dy, 2)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	k_ny1 = i*this->n_x+j;
	(*ve_y)(k_ny1, 0) = ve_y_d;
	(*E_y)(k_ny1, 0) = grad_V_y;
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	//neumann condition nek+1 = nek, nek-N = nek, nek-N+1 = nek, nek-N-1 = nek-1
	//grad_ne_x = neumann_condition_C(x, y, 0, true);
	//grad_ne_y = neumann_condition_C(x, y, 0, false);
	//grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
	//grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
	(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
	//(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk	//doesn't exist
	//up edge
	(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
	//(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N	//doesn't exist
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
	(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
	//left edge
	(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
	//(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1	//doesn't exist
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
	(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
	//down edge
	(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
	(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
	//(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk	//doesn't exist
	
	//Source term Se
	(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/

	//top left
	//cout << "flux(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_V_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		ne = (*concentration)(k+1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
	Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 3, true);
	//grad_V_y = neumann_condition_u(x, y, 3, false);
	//grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx, y+dy, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	k_ny1 = (i+1)*this->n_x+j;
	(*ve_y)(k_ny1, 0) = ve_y_u;
	(*E_y)(k_ny1, 0) = grad_V_y;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	//neumann condition nek+N = nek, nek+N+1 = nek+1, nek+N-1 = nek, nek-1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 3, true);
	//grad_ne_y = neumann_condition_C(x, y, 3, false);
	//grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	//grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 1, true);
	//grad_V_y = neumann_condition_u(x, y, 1, false);
	//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 5))/dx;
	grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 5) + this->u(k-N, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x)(k_nx1, 0) = ve_x_l;
	(*E_x)(k_nx1, 0) = grad_V_x;
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	//grad_ne_x = neumann_condition_C(x, y, 1, true);
	//grad_ne_y = neumann_condition_C(x, y, 1, false);
	//neumann condition nek-1 = nek, nek+N-1 = nek, nek+N = nek, nek-N-1 = nek-N
	//grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	//grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	k_ny1 = i*this->n_x+j;
	(*ve_y)(k_ny1, 0) = ve_y_d;
	(*E_y)(k_ny1, 0) = grad_V_y;
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		ne = (*concentration)(k-N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
	//(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1	//doesn't exist
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
	(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
	//up edge
	(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
	(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
	//(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk	//doesn't exist
	//left edge
	(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
	(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
	//(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk	//doesn't exist
	//down edge
	(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
	//(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N	//doesn't exist
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
	(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk
		
	//Source term Se
	(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/

	//top right
	//cout << "flux(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 2, true);
	//grad_V_y = neumann_condition_u(x, y, 2, false);
	//grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	grad_V_x = (this->function_boundary_condition(x+dx, y, 7) - this->u(k, 0))/dx;
	grad_V_y = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y+dy, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7) + this->u(k-N, 0) + this->function_boundary_condition(x+dx, y-dy, 7)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	k_nx1 = i*(this->n_x+1)+j+1;
	(*ve_x)(k_nx1, 0) = ve_x_r;
	(*E_x)(k_nx1, 0) = grad_V_x;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	//neumann condition nek+1 = nek, nek+N = nek, nek+N+1 = nek, nek-N+1 = nek-N
	//grad_ne_x = neumann_condition_C(x, y, 2, true);
	//grad_ne_y = neumann_condition_C(x, y, 2, false);
	//grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	//grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N-1 = Uk-1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 3, true);
	//grad_V_y = neumann_condition_u(x, y, 3, false);
	//grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y+dy, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	k_ny1 = (i+1)*this->n_x+j;
	(*ve_y)(k_ny1, 0) = ve_y_u;
	(*E_y)(k_ny1, 0) = grad_V_y;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	//neumann condition nek+N = nek, nek+N+1 = nek, nek+1 = nek, nek+N-1 = nek-1
	//grad_ne_x = neumann_condition_C(x, y, 3, true);
	//grad_ne_y = neumann_condition_C(x, y, 3, false);
	//grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	//grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x)(k_nx1, 0) = ve_x_l;
	(*E_x)(k_nx1, 0) = grad_V_x;
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		ne = (*concentration)(k-1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	k_ny1 = i*this->n_x+j;
	(*ve_y)(k_ny1, 0) = ve_y_d;
	(*E_y)(k_ny1, 0) = grad_V_y;
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		ne = (*concentration)(k-N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
	(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
	//(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk	//doesn't exist
	//up edge
	(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
	(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
	//(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk	//doesn't exist
	//left edge
	(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
	//(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1	//doesn't exist
	(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
	(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
	//down edge
	(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
	//(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N	//doesn't exist
	(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
	(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk
		
	//Source term Se
	(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_V_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			ne = (*concentration)(k+1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
		Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			ne = (*concentration)(k+N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x)(k_nx1, 0) = ve_x_l;
		(*E_x)(k_nx1, 0) = grad_V_x;
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			ne = (*concentration)(k-1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
		Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//doesn't exist
		//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1, Uk-N-1 = Uk-1
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 0, true);
		//grad_V_y = neumann_condition_u(x, y, 0, false);
		//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		k_ny1 = i*this->n_x+j;
		(*ve_y)(k_ny1, 0) = ve_y_d;
		(*E_y)(k_ny1, 0) = grad_V_y;
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			//ne = (*concentration)(k-N, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		//neumann condition nek-N = nek, nek-N+1 = nek+1, nek-1 = nek, nek-N-1 = nek, nek-N = nek
		//grad_ne_x = neumann_condition_C(x, y, 0, true);
		//grad_ne_y = neumann_condition_C(x, y, 0, false);
		//grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
		//grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
		(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
		(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
		//up edge
		(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
		//(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N	//doesn't exist
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
		(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
		//left edge
		(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
		(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
		(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
		//down edge
		(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
		(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
		//(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk	//doesn't exist

		//Source term Se
		(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;		
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			ne = (*concentration)(k+1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			ne = (*concentration)(k+N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 1, true);
		//grad_V_y = neumann_condition_u(x, y, 1, false);
		//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 3))/dx;
		grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 3) + this->u(k+N, 0) + this->function_boundary_condition(x-dx, y, 3) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 3) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 3) + this->u(k-N, 0)))/dy;
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x)(k_nx1, 0) = ve_x_l;
		(*E_x)(k_nx1, 0) = grad_V_x;
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			//ne = (*concentration)(k-1, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		//neumann condition nek-1 = nek, nek+N-1 = nek+N, nek-N-1 = nek-N
		//grad_ne_x = neumann_condition_C(x, y, 1, true);
		//grad_ne_y = neumann_condition_C(x, y, 1, false);
		//grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		//grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		k_ny1 = i*this->n_x+j;
		(*ve_y)(k_ny1, 0) = ve_y_d;
		(*E_y)(k_ny1, 0) = grad_V_y;
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			ne = (*concentration)(k-N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
		//(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1	//doesn't exist
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
		(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
		//up edge
		(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
		(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
		(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
		//left edge
		(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
		(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
		//(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk	//doesn't exist
		//down edge
		(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
		(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
		(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk

		//Source term Se
		(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 2, true);
		//grad_V_y = neumann_condition_u(x, y, 2, false);
		//grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		//grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		grad_V_x = (this->function_boundary_condition(x+dx, y, 4) - this->u(k, 0))/dx;
		grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx, y+dy, 4) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 4)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 4) + this->u(k-N, 0) + this->function_boundary_condition(x+dx, y-dy, 4)))/dy;
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		k_nx1 = i*(this->n_x+1)+j+1;
		(*ve_x)(k_nx1, 0) = ve_x_r;
		(*E_x)(k_nx1, 0) = grad_V_x;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			//ne = (*concentration)(k+1, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		//neumann condition nek+1 = nek, nek+N+1 = nek+N, nek-N+1 = ne-N
		//grad_ne_x = neumann_condition_C(x, y, 2, true);
		//grad_ne_y = neumann_condition_C(x, y, 2, false);
		//grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		//grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
		//up edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			ne = (*concentration)(k+N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
		grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;		
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x)(k_nx1, 0) = ve_x_l;
		(*E_x)(k_nx1, 0) = grad_V_x;
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			ne = (*concentration)(k-1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		k_ny1 = i*this->n_x+j;
		(*ve_y)(k_ny1, 0) = ve_y_d;
		(*E_y)(k_ny1, 0) = grad_V_y;
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			ne = (*concentration)(k-N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
		(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
		//(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk	//doesn't exist
		//up edge
		(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
		(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
		(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
		//left edge
		(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
		//(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1	//doesn't exist
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
		(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
		//down edge
		(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
		(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
		(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk

		//Source term Se
		(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/
	}

	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_V_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			ne = (*concentration)(k+1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//doesn't exist
		//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1, Uk+N-1 = Uk-1
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 3, true);
		//grad_V_y = neumann_condition_u(x, y, 3, false);
		//grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		//grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		k_ny1 = (i+1)*this->n_x+j;
		(*ve_y)(k_ny1, 0) = ve_y_u;
		(*E_y)(k_ny1, 0) = grad_V_y;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			//ne = (*concentration)(k+N, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		//neumann condition nek+N = nek, nek+N+1 = nek+1, nek+N-1 = nek-1
		//grad_ne_x = neumann_condition_C(x, y, 3, true);
		//grad_ne_y = neumann_condition_C(x, y, 3, false);
		//grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		//grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x)(k_nx1, 0) = ve_x_l;
		(*E_x)(k_nx1, 0) = grad_V_x;
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			ne = (*concentration)(k-1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		k_ny1 = i*this->n_x+j;
		(*ve_y)(k_ny1, 0) = ve_y_d;
		(*E_y)(k_ny1, 0) = grad_V_y;
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			ne = (*concentration)(k-N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		(*flux)(k, 0) -= dy* 0.5*ne*ve_x_average;	//remove from cell k
		(*flux)(k-1, 0) -= dy* 0.5*ne*ve_x_average;	//add to cell k+1
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_r)*ne;
		(*flux)(k+1, 0) -= -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
		//up edge
		(*flux)(k, 0) -= dx* 0.5*ne*ve_y_average;	//remove from cell k
		(*flux)(k-N, 0) -= dx* 0.5*ne*ve_y_average;	//add to cell k+N
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_u)*ne;
		//(*flux)(k+N, 0) -= -dx* 0.5*fabs(ve_y_u)*ne;	//from uk	//doesn't exist
		//left edge
		(*flux)(k, 0) -= -dy* 0.5*ne*ve_x_average;	//remove from cell k
		(*flux)(k+1, 0) -= -dy* 0.5*ne*ve_x_average;	//add to cell k-1
		(*flux)(k, 0) -= dy* 0.5*fabs(ve_x_l)*ne;
		(*flux)(k-1, 0) -= -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
		//down edge
		(*flux)(k, 0) -= -dx* 0.5*ne*ve_y_average;	//remove from cell k
		//(*flux)(k+N, 0) -= -dx* 0.5*ne*ve_y_average;	//add to cell k-N	//doesn't exist
		(*flux)(k, 0) -= dx* 0.5*fabs(ve_y_d)*ne;
		(*flux)(k-N, 0) -= -dx* 0.5*fabs(ve_y_d)*ne;	//from uk

		//Source term Se
		(*ve_norm)(k, 0) = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		/*norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;*/
	}
	//(*flux).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "flux");
	//return max_speed;
}

void darcy::compute_E(dense_matrix *E_x_left, dense_matrix *E_y_left, dense_matrix *E_x_down, dense_matrix *E_y_down, dense_matrix *E_norm_left, dense_matrix *E_norm_down, functiontype3 neumann_condition_u) {	//u is the solution of darcy's equation
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;

	double grad_V_x = 0.0;
	double grad_V_y = 0.0;
	double norm_E_r = 0.0;	//right
	double norm_E_u = 0.0;	//up
	double norm_E_l = 0.0;	//left
	double norm_E_d = 0.0;	//down

	int i, j, k;
	int k_nx1, k_ny1;

	double x, y;

	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
			dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
			dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			//right edge
			//left hand side

			//up edge
			//left hand side

			//left edge
			//left hand side
			grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
			grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
			norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			k_nx1 = i*(this->n_x+1)+j;
			(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
			(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
			(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge	
			

			//down edge
			//left hand side
			grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx_c;
			grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
			norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			k_ny1 = i*this->n_x+j;
			(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
			(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
			(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge	
		}
	}

	//borders now
	//4 corners
	//bottom left
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//right edge
	//left hand side
	
	//up edge
	//left hand side


	//left edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx_l, y, 0))/dx_l;
	grad_V_y = (0.25*(this->function_boundary_condition(x-dx_l, y+dy_d, 0) + this->u(k+N, 0) + this->function_boundary_condition(x-dx_l, y, 0) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx_l, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx_l, y-dy_d, 0) + this->u(k, 0)))/dy_c;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_nx1 = i*(this->n_x+1)+j;
	(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
	(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
	(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx_l, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx_l, y-dy_d, 0) + this->u(k, 0)))/dx_c;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy_d;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_ny1 = i*this->n_x+j;
	(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
	(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
	(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge	

	//bottom right
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;

	//right edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_V_x = (this->function_boundary_condition(x+dx_r, y, 2) - this->u(k, 0))/dx_r;
	grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx_r, y+dy_d, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 2)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx_r, y-dy_d, 2)))/dy_c;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
	(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
	(*E_norm_left)(k_nx1, 0) = norm_E_r;	//on edge

	//up edge
	//left hand side

	//left edge
	//left hand side
	grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
	grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy_c;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_nx1 = i*(this->n_x+1)+j;
	(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
	(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
	(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N-1 = Uk-1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 0, true);
	//grad_V_y = neumann_condition_u(x, y, 0, false);
	//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx_r, y-dy_d, 2)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx_c;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy_d;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_ny1 = i*this->n_x+j;
	(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
	(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
	(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge

	//top left
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//right edge
	//left hand side

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx_l, y+dy_u, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx_l, y, 5) + this->u(k, 0)))/dx_c;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy_u;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_ny1 = (i+1)*this->n_x+j;
	(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
	(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
	(*E_norm_down)(k_ny1, 0) = norm_E_u;	//on edge

	//left edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx_l, y, 5))/dx_l;
	grad_V_y = (0.25*(this->function_boundary_condition(x-dx_l, y+dy_u, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx_l, y, 5) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx_l, y, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx_l, y-dy_d, 5) + this->u(k-N, 0)))/dy_c;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_nx1 = i*(this->n_x+1)+j;
	(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
	(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
	(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge

	//down edge
	//left hand side
	grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx_c;
	grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_ny1 = i*this->n_x+j;
	(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
	(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
	(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge

	//top right
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//right edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_V_x = (this->function_boundary_condition(x+dx_r, y, 7) - this->u(k, 0))/dx_r;
	grad_V_y = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx_r, y+dy_u, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 7)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 7) + this->u(k-N, 0) + this->function_boundary_condition(x+dx_r, y-dy_d, 7)))/dy_c;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
	(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
	(*E_norm_left)(k_nx1, 0) = norm_E_r;	//on edge

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N-1 = Uk-1
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx_r, y+dy_u, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 7)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx_c;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy_u;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_ny1 = (i+1)*this->n_x+j;
	(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
	(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
	(*E_norm_down)(k_ny1, 0) = norm_E_u;	//on edge

	//left edge
	//left hand side
	grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
	grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_nx1 = i*(this->n_x+1)+j;
	(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
	(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
	(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge

	//down edge
	//left hand side
	grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx_c;
	grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	k_ny1 = i*this->n_x+j;
	(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
	(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
	(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = dy_u;
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx_r;
		grad_V_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy_c;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);

		//up edge
		//left hand side
		grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx_c;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy_u;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
		grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy_c;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_nx1 = i*(this->n_x+1)+j;
		(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
		(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
		(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge

		//down edge
		//doesn't exist
		//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1, Uk-N-1 = Uk-1
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx_c;
		grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy_d;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_ny1 = i*this->n_x+j;
		(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
		(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
		(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = dx_r;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx_r;
		grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy_c;		
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);

		//up edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx_c;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy_u;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		
		//left edge
		//doesn't exist
		//neumann condition
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx_l, y, 3))/dx_l;
		grad_V_y = (0.25*(this->function_boundary_condition(x-dx_l, y+dy_u, 3) + this->u(k+N, 0) + this->function_boundary_condition(x-dx_l, y, 3) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx_l, y, 3) + this->u(k, 0) + this->function_boundary_condition(x-dx_l, y-dy_d, 3) + this->u(k-N, 0)))/dy_c;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_nx1 = i*(this->n_x+1)+j;
		(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
		(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
		(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge

		//down edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx_c;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_ny1 = i*this->n_x+j;
		(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
		(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
		(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge
	}
	//right
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dx_r = dx_l;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//right edge
		//doesn't exist
		//neumann condition
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_V_x = (this->function_boundary_condition(x+dx_r, y, 4) - this->u(k, 0))/dx_r;
		grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx_r, y+dy_u, 4) + this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 4)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx_r, y, 4) + this->u(k-N, 0) + this->function_boundary_condition(x+dx_r, y-dy_d, 4)))/dy_c;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_nx1 = i*(this->n_x+1)+j+1;
		(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
		(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
		(*E_norm_left)(k_nx1, 0) = norm_E_r;	//on edge
	
		//up edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx_c;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy_u;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
		grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;		
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_nx1 = i*(this->n_x+1)+j;
		(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
		(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
		(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge
	
		//down edge
		//left hand side
		grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx_c;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_ny1 = i*this->n_x+j;
		(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
		(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
		(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge
	}
	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dy_u = dy_d;
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx_r;
		grad_V_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy_c;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);

		//up edge
		//doesn't exist
		//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1, Uk+N-1 = Uk-1
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx_c;
		grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy_u;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_ny1 = (i+1)*this->n_x+j;
		(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
		(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
		(*E_norm_down)(k_ny1, 0) = norm_E_u;	//on edge

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
		grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_nx1 = i*(this->n_x+1)+j;
		(*E_x_left)(k_nx1, 0) = grad_V_x;	//on edge
		(*E_y_left)(k_nx1, 0) = grad_V_y;	//on edge
		(*E_norm_left)(k_nx1, 0) = norm_E_l;	//on edge
		
		//down edge
		//left hand side
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx_c;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		k_ny1 = i*this->n_x+j;
		(*E_x_down)(k_ny1, 0) = grad_V_x;	//on edge
		(*E_y_down)(k_ny1, 0) = grad_V_y;	//on edge
		(*E_norm_down)(k_ny1, 0) = norm_E_d;	//on edge
	}
}

void darcy::compute_E_norm(dense_matrix *E_norm, dense_matrix *E_norm_left, dense_matrix *E_norm_down) {	//u is the solution of darcy's equation
	int N = this->n_x;

	double norm_E_r = 0.0;	//right
	double norm_E_u = 0.0;	//up
	double norm_E_l = 0.0;	//left
	double norm_E_d = 0.0;	//down

	int k;
	int k_nx1, k_ny1;

	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			k_nx1 = i*(this->n_x+1)+j;
			k_ny1 = i*this->n_x+j;
			//right edge
			norm_E_r = (*E_norm_left)(k_nx1+1, 0);	//on edge	

			//up edge
			norm_E_u = (*E_norm_down)(k_ny1+N, 0);	//on edge	

			//left edge
			norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge	
			

			//down edge
			norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge	

			(*E_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);	//centered
		}
	}
}

void darcy::compute_velocity_streamer(dense_matrix *E_norm, dense_matrix *E_x_left, dense_matrix *E_y_left, dense_matrix *E_x_down, dense_matrix *E_y_down, dense_matrix *E_norm_left, dense_matrix *E_norm_down, dense_matrix *ve_x_left, dense_matrix *ve_y_left, dense_matrix *ve_x_down, dense_matrix *ve_y_down, dense_matrix *ve_norm_left, dense_matrix *ve_norm_down, double *max_speed_convection) {	//u is the solution of darcy's equation
	int N = this->n_x;

	double ve_x_r = 0.0;
	double ve_x_u = 0.0;
	double ve_x_l = 0.0;
	double ve_x_d = 0.0;
	double ve_y_r = 0.0;
	double ve_y_u = 0.0;
	double ve_y_l = 0.0;
	double ve_y_d = 0.0;
	double drift_cste = 0.0;

	double grad_V_x = 0.0;
	double grad_V_y = 0.0;

	double norm_E_r = 0.0;
	double norm_E_u = 0.0;
	double norm_E_l = 0.0;
	double norm_E_d = 0.0;

	double ve_norm_r = 0.0;	//right
	double ve_norm_u = 0.0;	//up
	double ve_norm_l = 0.0;	//left
	double ve_norm_d = 0.0;	//down

	double speed_convection = 0.0;

	int i, j;
	int k_nx1, k_ny1;

	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			//right edge

			//up edge

			//left edge
			//left hand side
			//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
			//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
			k_nx1 = i*(this->n_x+1)+j;
			grad_V_x = (*E_x_left)(k_nx1, 0);
			grad_V_y = (*E_y_left)(k_nx1, 0);
			ve_x_l = -grad_V_x;
			ve_y_l = -grad_V_y;
			//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			norm_E_l = (*E_norm_left)(k_nx1, 0);	
			ve_x_l /= norm_E_l;
			ve_y_l /= norm_E_l;
			if(norm_E_l == 0.0) {
				ve_x_l = 0.0;
				ve_y_l = 0.0;
			}
			if(norm_E_l/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
			} else if(norm_E_l/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
			} else if(norm_E_l/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
			}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
			ve_x_l *= drift_cste;
			ve_y_l *= drift_cste;
			ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
			k_nx1 = i*(this->n_x+1)+j;
			(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
			(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
			(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
			ve_x_l = -ve_x_l;	//multiply by edge's normal
			speed_convection = fabs(ve_x_l);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//down edge
			//left hand side
			//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
			//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
			k_ny1 = i*this->n_x+j;
			grad_V_x = (*E_x_down)(k_ny1, 0);
			grad_V_y = (*E_y_down)(k_ny1, 0);
			ve_x_d = -grad_V_x;
			ve_y_d = -grad_V_y;
			//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			norm_E_d = (*E_norm_down)(k_ny1, 0);
			ve_x_d /= norm_E_d;
			ve_y_d /= norm_E_d;
			if(norm_E_d == 0.0) {
				ve_x_d = 0.0;
				ve_y_d = 0.0;
			}
			if(norm_E_d/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
			} else if(norm_E_d/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
			} else if(norm_E_d/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
			}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
			ve_x_d *= drift_cste;
			ve_y_d *= drift_cste;
			ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
			k_ny1 = i*this->n_x+j;
			(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
			(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
			(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge	
			ve_y_d = -ve_y_d;
			speed_convection = fabs(ve_y_d);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	//right edge

	//up edge

	//left edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	//left hand side
	//grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 0))/dx;
	//grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 0) + this->u(k+N, 0) + this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 0) + this->u(k, 0)))/dy;
	k_nx1 = i*(this->n_x+1)+j;
	grad_V_x = (*E_x_left)(k_nx1, 0);
	grad_V_y = (*E_y_left)(k_nx1, 0);
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_l = (*E_norm_left)(k_nx1, 0);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
	(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
	(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	speed_convection = fabs(ve_x_l);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1
	//left hand side
	//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 0) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	k_ny1 = i*this->n_x+j;
	grad_V_x = (*E_x_down)(k_ny1, 0);
	grad_V_y = (*E_y_down)(k_ny1, 0);
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_d = (*E_norm_down)(k_ny1, 0);	
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	k_ny1 = i*this->n_x+j;
	(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
	(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
	(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
	ve_y_d = -ve_y_d;
	speed_convection = fabs(ve_y_d);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//bottom right
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = 0; j = n_x-1;

	//right edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	//left hand side
	//grad_V_x = (this->function_boundary_condition(x+dx, y, 2) - this->u(k, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx, y+dy, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y-dy, 2)))/dy;
	k_nx1 = i*(this->n_x+1)+j+1;
	grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
	grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	//norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_r = (*E_norm_left)(k_nx1, 0);	//on edge
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	ve_norm_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*ve_x_left)(k_nx1, 0) = ve_x_r;	//on edge
	(*ve_y_left)(k_nx1, 0) = ve_y_r;	//on edge
	(*ve_norm_left)(k_nx1, 0) = ve_norm_r;	//on edge
	speed_convection = fabs(ve_x_r);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge

	//left edge
	//left hand side
	//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	//grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
	k_nx1 = i*(this->n_x+1)+j;
	grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
	grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
	(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
	(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	speed_convection = fabs(ve_x_l);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N-1 = Uk-1
	//left hand side
	//grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y-dy, 2)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	k_ny1 = i*this->n_x+j;
	grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
	grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	k_ny1 = i*this->n_x+j;
	(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
	(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
	(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
	ve_y_d = -ve_y_d;
	speed_convection = fabs(ve_y_d);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//top left
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	//right edge

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1
	//left hand side
	//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx, y+dy, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	k_ny1 = (i+1)*this->n_x+j;
	grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
	grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	//norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_u = (*E_norm_down)(k_ny1, 0);	//on edge
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	ve_norm_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	k_ny1 = (i+1)*this->n_x+j;
	(*ve_x_down)(k_ny1, 0) = ve_x_u;	//on edge
	(*ve_y_down)(k_ny1, 0) = ve_y_u;	//on edge
	(*ve_norm_down)(k_ny1, 0) = ve_norm_u;	//on edge
	speed_convection = fabs(ve_y_u);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	//left hand side
	//grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 5))/dx;
	//grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 5) + this->u(k-N, 0)))/dy;
	k_nx1 = i*(this->n_x+1)+j;
	grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
	grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
	(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
	(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	speed_convection = fabs(ve_x_l);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//left hand side
	//grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	k_ny1 = i*this->n_x+j;
	grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
	grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	k_ny1 = i*this->n_x+j;
	(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
	(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
	(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
	ve_y_d = -ve_y_d;
	speed_convection = fabs(ve_y_d);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
	//top right
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	//right edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	//left hand side
	//grad_V_x = (this->function_boundary_condition(x+dx, y, 7) - this->u(k, 0))/dx;
	//grad_V_y = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y+dy, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7) + this->u(k-N, 0) + this->function_boundary_condition(x+dx, y-dy, 7)))/dy;
	k_nx1 = i*(this->n_x+1)+j+1;
	grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
	grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	//norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_r = (*E_norm_left)(k_nx1, 0);	//on edge
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	ve_norm_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	k_nx1 = i*(this->n_x+1)+j+1;
	(*ve_x_left)(k_nx1, 0) = ve_x_r;	//on edge
	(*ve_y_left)(k_nx1, 0) = ve_y_r;	//on edge
	(*ve_norm_left)(k_nx1, 0) = ve_norm_r;	//on edge
	speed_convection = fabs(ve_x_r);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N-1 = Uk-1
	//left hand side
	//grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y+dy, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	k_ny1 = i*this->n_x+j;
	grad_V_x = (*E_x_down)(k_ny1 + N, 0);	//on edge
	grad_V_y = (*E_y_down)(k_ny1 + N, 0);	//on edge
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	//norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_u = (*E_norm_down)(k_ny1 + N, 0);	//on edge
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	ve_norm_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	k_ny1 = (i+1)*this->n_x+j;
	(*ve_x_down)(k_ny1, 0) = ve_x_u;	//on edge
	(*ve_y_down)(k_ny1, 0) = ve_y_u;	//on edge
	(*ve_norm_down)(k_ny1, 0) = ve_norm_u;	//on edge
	speed_convection = fabs(ve_y_u);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//left edge
	//left hand side
	//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	//grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	k_nx1 = i*(this->n_x+1)+j;
	grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
	grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	k_nx1 = i*(this->n_x+1)+j;
	(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
	(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
	(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
	ve_x_l = -ve_x_l;	//multiply by edge's normal
	speed_convection = fabs(ve_x_l);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//down edge
	//left hand side
	//grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	k_ny1 = i*this->n_x+j;
	grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
	grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	k_ny1 = i*this->n_x+j;
	(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
	(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
	(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
	ve_y_d = -ve_y_d;
	speed_convection = fabs(ve_y_d);
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		//right edge

		//up edge

		//left edge
		//left hand side
		//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		//grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
		k_nx1 = i*(this->n_x+1)+j;
		grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
		grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
		(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
		(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		speed_convection = fabs(ve_x_l);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		//doesn't exist
		//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1, Uk-N-1 = Uk-1
		//left hand side
		//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		//grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
		k_ny1 = i*this->n_x+j;
		grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
		grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		k_ny1 = i*this->n_x+j;
		(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
		(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
		(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
		ve_y_d = -ve_y_d;
		speed_convection = fabs(ve_y_d);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		//right edge

		//up edge

		//left edge
		//doesn't exist
		//neumann condition
		//left hand side
		//grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 3))/dx;
		//grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 3) + this->u(k+N, 0) + this->function_boundary_condition(x-dx, y, 3) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 3) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 3) + this->u(k-N, 0)))/dy;
		k_nx1 = i*(this->n_x+1)+j;
		grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
		grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
		(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
		(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		speed_convection = fabs(ve_x_l);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//down edge
		//left hand side
		//grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
		//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		k_ny1 = i*this->n_x+j;
		grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
		grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		k_ny1 = i*this->n_x+j;
		(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
		(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
		(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
		ve_y_d = -ve_y_d;
		speed_convection = fabs(ve_y_d);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
	//right
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		//right edge
		//doesn't exist
		//neumann condition
		//left hand side
		//grad_V_x = (this->function_boundary_condition(x+dx, y, 4) - this->u(k, 0))/dx;
		//grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx, y+dy, 4) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 4)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 4) + this->u(k-N, 0) + this->function_boundary_condition(x+dx, y-dy, 4)))/dy;
		k_nx1 = i*(this->n_x+1)+j+1;
		grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
		grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		//norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_r = (*E_norm_left)(k_nx1, 0);	//on edge
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_r;
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		ve_norm_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		k_nx1 = i*(this->n_x+1)+j+1;
		(*ve_x_left)(k_nx1, 0) = ve_x_r;	//on edge
		(*ve_y_left)(k_nx1, 0) = ve_y_r;	//on edge
		(*ve_norm_left)(k_nx1, 0) = ve_norm_r;	//on edge
		speed_convection = fabs(ve_x_r);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	
		//up edge

		//left edge
		//left hand side
		//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		k_nx1 = i*(this->n_x+1)+j;
		grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
		grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge	
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
		(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
		(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		speed_convection = fabs(ve_x_l);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		//left hand side
		//grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
		//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		k_ny1 = i*this->n_x+j;
		grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
		grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		k_ny1 = i*this->n_x+j;
		(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
		(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
		(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
		ve_y_d = -ve_y_d;
		speed_convection = fabs(ve_y_d);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}

	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		//right edge

		//up edge
		//doesn't exist
		//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1, Uk+N-1 = Uk-1
		//left hand side
		//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		//grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
		k_ny1 = (i+1)*this->n_x+j;
		grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
		grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		//norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_u = (*E_norm_down)(k_ny1, 0);	//on edge
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_u;
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		ve_norm_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		k_ny1 = (i+1)*this->n_x+j;
		(*ve_x_down)(k_ny1, 0) = ve_x_u;	//on edge
		(*ve_y_down)(k_ny1, 0) = ve_y_u;	//on edge
		(*ve_norm_down)(k_ny1, 0) = ve_norm_u;	//on edge
		speed_convection = fabs(ve_y_u);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

		//left edge
		//left hand side
		//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		//grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		k_nx1 = i*(this->n_x+1)+j;
		grad_V_x = (*E_x_left)(k_nx1, 0);	//on edge
		grad_V_y = (*E_y_left)(k_nx1, 0);	//on edge
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		//norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_l = (*E_norm_left)(k_nx1, 0);	//on edge
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_l;
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		k_nx1 = i*(this->n_x+1)+j;
		(*ve_x_left)(k_nx1, 0) = ve_x_l;	//on edge
		(*ve_y_left)(k_nx1, 0) = ve_y_l;	//on edge
		(*ve_norm_left)(k_nx1, 0) = ve_norm_l;	//on edge
		ve_x_l = -ve_x_l;	//multiply by edge's normal
		speed_convection = fabs(ve_x_l);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		
		//down edge
		//left hand side
		//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		k_ny1 = i*this->n_x+j;
		grad_V_x = (*E_x_down)(k_ny1, 0);	//on edge
		grad_V_y = (*E_y_down)(k_ny1, 0);	//on edge
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		//norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		norm_E_d = (*E_norm_down)(k_ny1, 0);	//on edge
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
	//drift_cste = 500;
	//drift_cste *= norm_E_d;
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		k_ny1 = i*this->n_x+j;
		(*ve_x_down)(k_ny1, 0) = ve_x_d;	//on edge
		(*ve_y_down)(k_ny1, 0) = ve_y_d;	//on edge
		(*ve_norm_down)(k_ny1, 0) = ve_norm_d;	//on edge
		ve_y_d = -ve_y_d;
		speed_convection = fabs(ve_y_d);
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	}
}

void darcy::compute_velocity_norm(dense_matrix *ve_norm, dense_matrix *ve_norm_left, dense_matrix *ve_norm_down) {	//u is the solution of darcy's equation
	int N = this->n_x;

	double norm_E_r = 0.0;	//right
	double norm_E_u = 0.0;	//up
	double norm_E_l = 0.0;	//left
	double norm_E_d = 0.0;	//down

	int k;
	int k_nx1, k_ny1;

	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			k_nx1 = i*(this->n_x+1)+j;
			k_ny1 = i*this->n_x+j;
			//right edge
			norm_E_r = (*ve_norm_left)(k_nx1+1, 0);	//on edge	

			//up edge
			norm_E_u = (*ve_norm_down)(k_ny1+N, 0);	//on edge	

			//left edge
			norm_E_l = (*ve_norm_left)(k_nx1, 0);	//on edge	
			

			//down edge
			norm_E_d = (*ve_norm_down)(k_ny1, 0);	//on edge	

			(*ve_norm)(k, 0) = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);	//centered
		}
	}
}

void darcy::compute_convection_streamer(dense_matrix *concentration, dense_matrix *flux, dense_matrix *ve_x_left, dense_matrix *ve_y_left, dense_matrix *ve_x_down, dense_matrix *ve_y_down) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}

	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double ve_x_r = 0.0;
	double ve_x_u = 0.0;
	double ve_x_l = 0.0;
	double ve_x_d = 0.0;
	double ve_x_average = 0.0;
	double ve_y_r = 0.0;
	double ve_y_u = 0.0;
	double ve_y_l = 0.0;
	double ve_y_d = 0.0;
	double ve_y_average = 0.0;
	double ne = 0.0;

	int k;
	int k_nx1, k_ny1;

	//cout << "middle" << endl;
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			k_nx1 = i*(this->n_x+1)+j;
			k_ny1 = i*this->n_x+j;
			ve_x_r = (*ve_x_left)(k_nx1 + 1, 0);
			ve_x_u = (*ve_x_down)(k_ny1 + N, 0);
			ve_x_l = (*ve_x_left)(k_nx1, 0);
			ve_x_d = (*ve_x_down)(k_ny1, 0);

			ve_y_r = (*ve_y_left)(k_nx1 + 1, 0);
			ve_y_u = (*ve_y_down)(k_ny1 + N, 0);
			ve_y_l = (*ve_y_left)(k_nx1, 0);
			ve_y_d = (*ve_y_down)(k_ny1, 0);

			ve_x_r = -1;
			ve_x_u = 0;
			ve_x_l = -1;
			ve_x_d = 0;

			ve_y_r = 0;
			ve_y_u = 0;
			ve_y_l = 0;
			ve_y_d = 0;

			//Roe scheme
			ve_x_average = 0.25*(ve_x_r + ve_x_u + ve_x_l + ve_x_d);
			ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l + ve_y_d);
			ne = (*concentration)(k, 0);
			//right edge
		/*	(*flux)(k, 0) -= dy_c* 0.5*ne*ve_x_average;	//remove from cell k
			if(j!=0) (*flux)(k-1, 0) -= dy_c* 0.5*ne*ve_x_average;	//add to cell k+1
			(*flux)(k, 0) -= dy_c* 0.5*fabs(ve_x_r)*ne;
			if(j!=this->n_x-1) (*flux)(k+1, 0) -= -dy_c* 0.5*fabs(ve_x_r)*ne;	//from uk
			//up edge
			(*flux)(k, 0) -= dx_c* 0.5*ne*ve_y_average;	//remove from cell k
			if(i!=0) (*flux)(k-N, 0) -= dx_c* 0.5*ne*ve_y_average;	//add to cell k+N
			(*flux)(k, 0) -= dx_c* 0.5*fabs(ve_y_u)*ne;
			if(i!=this->n_y-1) (*flux)(k+N, 0) -= -dx_c* 0.5*fabs(ve_y_u)*ne;	//from uk
			//left edge
			(*flux)(k, 0) -= -dy_c* 0.5*ne*ve_x_average;	//remove from cell k
			if(j!=this->n_x-1) (*flux)(k+1, 0) -= -dy_c* 0.5*ne*ve_x_average;	//add to cell k-1
			(*flux)(k, 0) -= dy_c* 0.5*fabs(ve_x_l)*ne;
			if(j!=0) (*flux)(k-1, 0) -= -dy_c* 0.5*fabs(ve_x_l)*ne;	//from uk
			//down edge
			(*flux)(k, 0) -= -dx_c* 0.5*ne*ve_y_average;	//remove from cell k
			if(i!=this->n_y-1) (*flux)(k+N, 0) -= -dx_c* 0.5*ne*ve_y_average;	//add to cell k-N
			(*flux)(k, 0) -= dx_c* 0.5*fabs(ve_y_d)*ne;
			if(i!=0) (*flux)(k-N, 0) -= -dx_c* 0.5*fabs(ve_y_d)*ne;	//from uk*/
		}
	}
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			dxy = dx_c*dy_c;
			
			(*flux)(k, 0) /= dxy;
		}
	}
}

void darcy::compute_convection_uncentered_streamer(dense_matrix *concentration, dense_matrix *flux, dense_matrix *ve_x_left, dense_matrix *ve_y_left, dense_matrix *ve_x_down, dense_matrix *ve_y_down) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}

	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double ve_x_r = 0.0;
	double ve_x_u = 0.0;
	double ve_x_l = 0.0;
	double ve_x_d = 0.0;
	double ve_x_average = 0.0;
	double ve_y_r = 0.0;
	double ve_y_u = 0.0;
	double ve_y_l = 0.0;
	double ve_y_d = 0.0;
	double ve_y_average = 0.0;
	double ne = 0.0;

	int k;
	int k_nx1, k_ny1;

	//cout << "middle" << endl;
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			k_nx1 = i*(this->n_x+1)+j;
			k_ny1 = i*this->n_x+j;
			ve_x_r = (*ve_x_left)(k_nx1 + 1, 0);
			ve_x_u = (*ve_x_down)(k_ny1 + N, 0);
			ve_x_l = (*ve_x_left)(k_nx1, 0);
			ve_x_d = (*ve_x_down)(k_ny1, 0);

			ve_y_r = (*ve_y_left)(k_nx1 + 1, 0);
			ve_y_u = (*ve_y_down)(k_ny1 + N, 0);
			ve_y_l = (*ve_y_left)(k_nx1, 0);
			ve_y_d = (*ve_y_down)(k_ny1, 0);

			/*ve_x_r = -1;
			ve_x_u = -1;
			ve_x_l = -1;
			ve_x_d = -1;

			ve_y_r = 0;
			ve_y_u = 0;
			ve_y_l = 0;
			ve_y_d = 0;*/

			//Roe scheme
			ve_x_average = 0.25*(ve_x_r + ve_x_u + ve_x_l + ve_x_d);
			ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l + ve_y_d);
			ne = (*concentration)(k, 0);
			/*//right edge
			if(j!=0) (*flux)(k-1, 0) -= dy_c* 0.5*ne*ve_x_average;	//add to cell k+1	//ERROR ON GPU
			(*flux)(k, 0) -= -dy_c* 0.5*fabs(ve_x_r)*ne;
			if(j!=this->n_x-1) (*flux)(k+1, 0) -= dy_c* 0.5*fabs(ve_x_r)*ne;	//from uk
			//up edge
			if(i!=0) (*flux)(k-N, 0) -= dx_c* 0.5*ne*ve_y_average;	//add to cell k+N
			(*flux)(k, 0) -= -dx_c* 0.5*fabs(ve_y_u)*ne;
			if(i!=this->n_y-1) (*flux)(k+N, 0) -= dx_c* 0.5*fabs(ve_y_u)*ne;	//from uk
			//left edge
			if(j!=this->n_x-1) (*flux)(k+1, 0) -= -dy_c* 0.5*ne*ve_x_average;	//add to cell k-1
			(*flux)(k, 0) -= -dy_c* 0.5*fabs(ve_x_l)*ne;
			if(j!=0) (*flux)(k-1, 0) -= dy_c* 0.5*fabs(ve_x_l)*ne;	//from uk
			//down edge
			if(i!=this->n_y-1) (*flux)(k+N, 0) -= -dx_c* 0.5*ne*ve_y_average;	//add to cell k-N
			(*flux)(k, 0) -= -dx_c* 0.5*fabs(ve_y_d)*ne;
			if(i!=0) (*flux)(k-N, 0) -= dx_c* 0.5*fabs(ve_y_d)*ne;	//from uk*/


			//right edge
			(*flux)(k, 0) -= -dy_c* 0.5*fabs(ve_x_r)*ne;
			(*flux)(k, 0) -= -dx_c* 0.5*fabs(ve_y_u)*ne;
			(*flux)(k, 0) -= -dy_c* 0.5*fabs(ve_x_l)*ne;
			(*flux)(k, 0) -= -dx_c* 0.5*fabs(ve_y_d)*ne;
			if(j!=this->n_x-1) (*flux)(k+1, 0) -= dy_c* 0.5*fabs(ve_x_r)*ne;	//from uk
			if(j!=this->n_x-1) (*flux)(k+1, 0) -= -dy_c* 0.5*ne*ve_x_average;	//add to cell k-1
			if(j!=0) (*flux)(k-1, 0) -= dy_c* 0.5*ne*ve_x_average;	//add to cell k+1	//ERROR ON GPU
			if(j!=0) (*flux)(k-1, 0) -= dy_c* 0.5*fabs(ve_x_l)*ne;	//from uk
			if(i!=0) (*flux)(k-N, 0) -= dx_c* 0.5*ne*ve_y_average;	//add to cell k+N
			if(i!=0) (*flux)(k-N, 0) -= dx_c* 0.5*fabs(ve_y_d)*ne;	//from uk
			if(i!=this->n_y-1) (*flux)(k+N, 0) -= dx_c* 0.5*fabs(ve_y_u)*ne;	//from uk
			if(i!=this->n_y-1) (*flux)(k+N, 0) -= -dx_c* 0.5*ne*ve_y_average;	//add to cell k-N
			//up edge
			//left edge
			//down edge
		}
	}
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			dxy = dx_c*dy_c;
			
			(*flux)(k, 0) /= dxy;
		}
	}
}

void darcy::compute_grad_ne(dense_matrix *concentration, dense_matrix *grad_ne_x_left, dense_matrix *grad_ne_y_left, dense_matrix *grad_ne_x_down, dense_matrix *grad_ne_y_down) {	//u is the solution of darcy's equation
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;

	double grad_ne_x = 0.0;
	double grad_ne_y = 0.0;


	int i, j, k;
	int k_nx1, k_ny1;

	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
			dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
			dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			//right edge

			//up edge
			
			//left edge
			//right hand side
			grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
			grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
			k_nx1 = i*(this->n_x+1)+j;
			(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
			(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
			
			//down edge
			//right hand side
			grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx_c;
			grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
			k_ny1 = i*this->n_x+j;
			(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
			(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
		}
	}

	//borders now
	//4 corners
	//bottom left
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = dy_u;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//right edge
	
	//up edge
	
	//left edge
	//doesn't exist
	//right hand side
	//neumann condition nek-N = nek, nek+N-1 = nek+N, nek-N-1 = nek
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx_l;
	grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
	
	//down edge
	//doesn't exist
	//right hand side
	//neumann condition nek-N = nek, nek-N+1 = nek+1, nek-1 = nek, nek-N-1 = nek
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx_c;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	
	//bottom right
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;

	//right edge
	//doesn't exist
	//right hand side
	//neumann condition nek+1 = nek, nek+N+1 = nek+N, nek-N = nek, nek-N+1 = nek
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx_r;
	grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j+1;
	(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
	
	//up edge
	
	//left edge
	//right hand side
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
	grad_ne_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
	
	//down edge
	//doesn't exist
	//right hand side
	//neumann condition nek+1 = nek, nek-N = nek, nek-N+1 = nek, nek-N-1 = nek-1
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy_d;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge

	//top left
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
	dx_l = dx_r;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;
	//right edge
	
	//up edge
	//doesn't exist
	//right hand side
	//neumann condition nek+N = nek, nek+N+1 = nek+1, nek+N-1 = nek, nek-1 = nek
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx_c;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy_u;
	k_ny1 = (i+1)*this->n_x+j;
	(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	
	//left edge
	//doesn't exist
	//right hand side
	//neumann condition nek-1 = nek, nek+N-1 = nek, nek+N = nek, nek-N-1 = nek-N
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx_l;
	grad_ne_y = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
	
	//down edge
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx_c;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	
	//top right
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
	dx_r = dx_l;
	dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
	dy_u = dy_d;
	dx_c = dx_r/2 + dx_l/2;
	dy_c = dy_u/2 + dy_d/2;

	//right edge
	//doesn't exist
	//right hand side
	//neumann condition nek+1 = nek, nek+N = nek, nek+N+1 = nek, nek-N+1 = nek-N
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx_r;
	grad_ne_y = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j+1;
	(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
	
	//up edge
	//doesn't exist
	//right hand side
	//neumann condition nek+N = nek, nek+N+1 = nek, nek+1 = nek, nek+N-1 = nek-1
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx_c;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy_u;
	k_ny1 = (i+1)*this->n_x+j;
	(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	
	//left edge
	//right hand side
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
	grad_ne_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
	k_nx1 = i*(this->n_x+1)+j;
	(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
	
	//down edge
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx_c;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
	k_ny1 = i*this->n_x+j;
	(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
	(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	
	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = dy_u;
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		//right edge
		
		//up edge
		
		//left edge
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
		grad_ne_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
		
		//down edge
		//doesn't exist
		//right hand side
		//neumann condition nek-N = nek, nek-N+1 = nek+1, nek-1 = nek, nek-N-1 = nek, nek-N = nek
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx_c;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = dx_r;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;

		//right edge
		
		//up edge
		
		//left edge
		//doesn't exist
		//neumann condition
		//right hand side
		//neumann condition nek-1 = nek, nek+N-1 = nek+N, nek-N-1 = nek-N
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx_l;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
		
		//down edge
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx_c;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	}
	//right
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dx_r = dx_l;
		dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;

		//right edge
		//doesn't exist
		//neumann condition
		//right hand side
		//neumann condition nek+1 = nek, nek+N+1 = nek+N, nek-N+1 = ne-N
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx_r;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j+1;
		(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge
	
		//up edge

		//left edge
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
		grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge

		//down edge
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx_c;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	}

	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
		dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
		dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
		dy_u = dy_d;
		dx_c = dx_r/2 + dx_l/2;
		dy_c = dy_u/2 + dy_d/2;
		
		//right edge

		//up edge
		//doesn't exist
		//right hand side
		//neumann condition nek+N = nek, nek+N+1 = nek+1, nek+N-1 = nek-1
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx_c;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy_u;
		k_ny1 = (i+1)*this->n_x+j;
		(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge

		//left edge
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
		grad_ne_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
		k_nx1 = i*(this->n_x+1)+j;
		(*grad_ne_x_left)(k_nx1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_left)(k_nx1, 0) = grad_ne_y;	//on edge

		//down edge
		//right hand side
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx_c;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
		k_ny1 = i*this->n_x+j;
		(*grad_ne_x_down)(k_ny1, 0) = grad_ne_x;	//on edge
		(*grad_ne_y_down)(k_ny1, 0) = grad_ne_y;	//on edge
	}
}

void darcy::compute_diffusion_streamer(dense_matrix *flux, dense_matrix *grad_ne_x_left, dense_matrix *grad_ne_y_left, dense_matrix *grad_ne_x_down, dense_matrix *grad_ne_y_down, dense_matrix *ve_norm_left, dense_matrix *ve_norm_down, dense_matrix *E_norm_left, dense_matrix *E_norm_down, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double E_norm_r = 0.0;	//right
	double E_norm_u = 0.0;	//up
	double E_norm_l = 0.0;	//left
	double E_norm_d = 0.0;	//down
	double De_cste = 0.0;
	//double De_cste = 3564.8;	//1D TEST

	double Dne_x = 0.0;
	double Dne_y = 0.0;
	double grad_ne_x = 0.0;
	double grad_ne_y = 0.0;
	double ve_norm_r = 0.0;	//right
	double ve_norm_u = 0.0;	//up
	double ve_norm_l = 0.0;	//left
	double ve_norm_d = 0.0;	//down

	double speed_diffusion = 0.0;

	int k;
	int k_nx1, k_ny1;

	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			//right edge
			//right hand side
			k_nx1 = i*(this->n_x+1)+j;
			grad_ne_x = (*grad_ne_x_left)(k_nx1 + 1, 0);	//on edge
			grad_ne_y = (*grad_ne_y_left)(k_nx1 + 1, 0);	//on edge
			Dne_x = grad_ne_x;
			//ve_norm_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
			ve_norm_r = (*ve_norm_left)(k_nx1 + 1, 0);	//on edge
			E_norm_r = (*E_norm_left)(k_nx1 + 1, 0);	//on edge
			De_cste = 0.3341e9*pow(E_norm_r/neutral_d, 0.54069)*ve_norm_r/E_norm_r;
			if(E_norm_r == 0.0) {
				De_cste = 0.0;
			}
			Dne_x *= De_cste;
			(*flux)(k, 0) -= dy_c*Dne_x;
			//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			speed_diffusion = De_cste;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//right hand side
			k_ny1 = i*this->n_x+j;
			grad_ne_x = (*grad_ne_x_down)(k_ny1 + N, 0);	//on edge
			grad_ne_y = (*grad_ne_y_down)(k_ny1 + N, 0);	//on edge
			Dne_y = grad_ne_y;
			//ve_norm_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
			ve_norm_u = (*ve_norm_down)(k_ny1 + N, 0);	//on edge
			E_norm_u = (*E_norm_down)(k_ny1 + N, 0);	//on edge
			De_cste = 0.3341e9*pow(E_norm_u/neutral_d, 0.54069)*ve_norm_u/E_norm_u;
			if(E_norm_u == 0.0) {
				De_cste = 0.0;
			}
			Dne_y *= De_cste;
			(*flux)(k, 0) -= dx_c*Dne_y;
			//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			speed_diffusion = De_cste;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//right hand side
			k_nx1 = i*(this->n_x+1)+j;
			grad_ne_x = (*grad_ne_x_left)(k_nx1, 0);	//on edge
			grad_ne_y = (*grad_ne_y_left)(k_nx1, 0);	//on edge
			Dne_x = grad_ne_x;
			//test
			Dne_x = -Dne_x;
			//ve_norm_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
			ve_norm_l = (*ve_norm_left)(k_nx1, 0);	//on edge
			E_norm_l = (*E_norm_left)(k_nx1, 0);	//on edge
			De_cste = 0.3341e9*pow(E_norm_l/neutral_d, 0.54069)*ve_norm_l/E_norm_l;
			if(E_norm_l == 0.0) {
				De_cste = 0.0;
			}
			Dne_x *= De_cste;
			(*flux)(k, 0) -= dy_c*Dne_x;
			//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			speed_diffusion = De_cste;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//right hand side
			k_ny1 = i*this->n_x+j;
			grad_ne_x = (*grad_ne_x_down)(k_ny1, 0);	//on edge
			grad_ne_y = (*grad_ne_y_down)(k_ny1, 0);	//on edge
			Dne_y = grad_ne_y;
			//test
			Dne_y = -Dne_y;
			//ve_norm_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
			ve_norm_d = (*ve_norm_down)(k_ny1, 0);	//on edge
			E_norm_d = (*E_norm_down)(k_ny1, 0);	//on edge
			De_cste = 0.3341e9*pow(E_norm_d/neutral_d, 0.54069)*ve_norm_d/E_norm_d;
			if(E_norm_d == 0.0) {
				De_cste = 0.0;
			}
			Dne_y *= De_cste;
			(*flux)(k, 0) -= dx_c*Dne_y;
			//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			speed_diffusion = De_cste;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		}
	}
	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else if(j == this->n_x-1) {
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
				dx_r = dx_l;
			} else {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = dx_r;
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else if(i == this->n_y-1) {
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
				dy_u = dy_d;
			} else {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = dy_u;
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			dxy = dx_c*dy_c;
			(*flux)(k, 0) /= dxy;
		}
	}
}

void darcy::compute_flux_streamer_MUSCL_limiter(dense_matrix *concentration, dense_matrix *flux, dense_matrix *Se, functiontype3 neumann_condition_u, functiontype4 limiter, string limiter_name, permeability_matrix *D, functiontype3 neumann_condition_C, double *max_speed_convection, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}
	cout << "LA ordre 2 !!!!!!!!!" << endl;
	int N = this->n_x;
	double dx = this->delta_x;
	double dy = this->delta_y;

	double ve_x_r = 0.0;
	double ve_x_u = 0.0;
	double ve_x_l = 0.0;
	double ve_x_d = 0.0;
	double ve_x_average = 0.0;
	double ve_y_r = 0.0;
	double ve_y_u = 0.0;
	double ve_y_l = 0.0;
	double ve_y_d = 0.0;
	double ve_y_average = 0.0;
	double ne = 0.0;
	double grad_V_x = 0.0;
	double grad_V_y = 0.0;
	double norm_E_r = 0.0;	//right
	double norm_E_u = 0.0;	//up
	double norm_E_l = 0.0;	//left
	double norm_E_d = 0.0;	//down
	double norm_E_average = 0.0;	//edge's avergae only used for source term Se
	double drift_cste = 0.0;
	double De_cste = 0.0;
	double Se_cste = 0.0;

	double Dne_x = 0.0;
	double Dne_y = 0.0;
	double grad_ne_x = 0.0;
	double grad_ne_y = 0.0;
	double norm_ve_r = 0.0;	//right
	double norm_ve_u = 0.0;	//up
	double norm_ve_l = 0.0;	//left
	double norm_ve_d = 0.0;	//down
	double norm_ve_average = 0.0;	//edge's avergae only used for source term Se

	double speed_convection = 0.0;
	double speed_diffusion = 0.0;

	double r_f_numerator = 0.0;	//Sweby's r-factor
	double r_f_denominator = 0.0;	//Sweby's r-factor

	int i, j, k;

	double x, y;

	//cout << "middle" << endl;
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//cout << "flux(" << i << "," << j << ")" << endl;
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			//cout << k << endl;
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			//right edge
			//left hand side
			grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
			grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
			ve_x_r = -grad_V_x;
			ve_y_r = -grad_V_y;
			norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_r /= norm_E_r;
			ve_y_r /= norm_E_r;
			if(norm_E_r == 0.0) {
				//cout << "la !!" << endl;
				ve_x_r = 0.0;
				ve_y_r = 0.0;
			}
			if(norm_E_r/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
			} else if(norm_E_r/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
			} else if(norm_E_r/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
			}
			ve_x_r *= drift_cste;
			ve_y_r *= drift_cste;
			/*if(ve_x_r>0) { //goes from left to right
				ne = (*concentration)(k, 0);
			} else if(ve_x_r<0) { //goes from right to left
				ne = (*concentration)(k+1, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dy*ve_x_r*ne;*/
			//right hand side
			grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
			grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
			Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
			norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
			De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
			if(norm_E_r == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_x *= De_cste;
			(*flux)(k, 0) -= dy*Dne_x;
			speed_convection = fabs(ve_x_r);
			//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//left hand side
			grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
			grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
			ve_x_u = -grad_V_x;
			ve_y_u = -grad_V_y;
			norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_u /= norm_E_u;
			ve_y_u /= norm_E_u;
			if(norm_E_u == 0.0) {
				//cout << "la !!" << endl;
				ve_x_u = 0.0;
				ve_y_u = 0.0;
			}
			if(norm_E_u/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
			} else if(norm_E_u/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
			} else if(norm_E_u/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
			}
			ve_x_u *= drift_cste;
			ve_y_u *= drift_cste;
			/*if(ve_y_u>0) { //goes from down to up
				ne = (*concentration)(k, 0);
			} else if(ve_y_u<0) { //goes from up to down
				ne = (*concentration)(k+N, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dx*ve_y_u*ne;*/
			//right hand side
			grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
			grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
			Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
			norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
			De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
			if(norm_E_u == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_y *= De_cste;
			(*flux)(k, 0) -= dx*Dne_y;
			speed_convection = fabs(ve_y_u);
			//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//left hand side
			grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
			grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
			ve_x_l = -grad_V_x;
			ve_y_l = -grad_V_y;
			norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_l /= norm_E_l;
			ve_y_l /= norm_E_l;
			if(norm_E_l == 0.0) {
				//cout << "la !!" << endl;
				ve_x_l = 0.0;
				ve_y_l = 0.0;
			}
			if(norm_E_l/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
			} else if(norm_E_l/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
			} else if(norm_E_l/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
			}
			ve_x_l *= drift_cste;
			ve_y_l *= drift_cste;
			//test
			ve_x_l = -ve_x_l;
			/*if(ve_x_l>0) { //goes from left to right
				ne = (*concentration)(k, 0);
			} else if(ve_x_l<0) { //goes from right to left
				ne = (*concentration)(k-1, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dy*ve_x_l*ne;*/
			//right hand side
			grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
			grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
			Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
			//test
			Dne_x = -Dne_x;
			norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
			De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
			if(norm_E_l == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_x *= De_cste;
			(*flux)(k, 0) -= dy*Dne_x;
			speed_convection = fabs(ve_x_l);
			//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//left hand side
			grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
			grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
			ve_x_d = -grad_V_x;
			ve_y_d = -grad_V_y;
			norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
			ve_x_d /= norm_E_d;
			ve_y_d /= norm_E_d;
			if(norm_E_d == 0.0) {
				//cout << "la !!" << endl;
				ve_x_d = 0.0;
				ve_y_d = 0.0;
			}
			if(norm_E_d == 0.0) {
				//cout << "la !!" << endl;
				ve_x_d = 0.0;
				ve_y_d = 0.0;
			}
			if(norm_E_d/neutral_d > 2.0e-15) {
				drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
			} else if(norm_E_d/neutral_d > 1.0e-16) {
				drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
			} else if(norm_E_d/neutral_d > 2.6e-17) {
				drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
			} else {
				drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
			}
			ve_x_d *= drift_cste;
			ve_y_d *= drift_cste;
			//test
			ve_y_d = -ve_y_d;
			/*if(ve_y_d>0) { //goes from down to up
				ne = (*concentration)(k, 0);
			} else if(ve_y_d<0) { //goes from up to down
				ne = (*concentration)(k-N, 0);
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dx*ve_y_d*ne;*/
			//right hand side
			grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
			grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
			Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
			//test
			Dne_y = -Dne_y;
			norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
			De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
			if(norm_E_d == 0.0) {
				//cout << "here !!" << endl;
				De_cste = 0.0;
			}
			Dne_y *= De_cste;
			(*flux)(k, 0) -= dx*Dne_y;
			speed_convection = fabs(ve_y_d);
			//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			speed_diffusion = De_cste;
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//Roe scheme
			ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
			ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
			ne = (*concentration)(k, 0);
			//right edge
			if(ve_x_r>0) { //goes from left to right
				//ne = (*concentration)(k, 0);
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
				ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
			} else if(ve_x_r<0) { //goes from right to left
				//ne = (*concentration)(k+1, 0);
				if(j == this->n_x-2) {
					ne = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
				} else {
					r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
					ne = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
				}
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
			(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1
			(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
			(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
			//up edge
			if(ve_y_u>0) { //goes from down to up
				//ne = (*concentration)(k, 0);
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
				ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
			} else if(ve_y_u<0) { //goes from up to down
				//ne = (*concentration)(k+N, 0);
				if(i == this->n_y-2) {
					ne = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
				} else {
					r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
					ne = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
				}
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
			(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N
			(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
			(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
			//left edge
			if(ve_x_l>0) { //goes from left to right
				//ne = (*concentration)(k, 0);
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
				ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
			} else if(ve_x_l<0) { //goes from right to left
				//ne = (*concentration)(k-1, 0);
				if(j == 1) {
					ne = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
				} else {
					r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
					ne = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
				}
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
			(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1
			(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
			(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
			//down edge
			if(ve_y_d>0) { //goes from down to up
				//ne = (*concentration)(k, 0);
				r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
				ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
			} else if(ve_y_d<0) { //goes from up to down
				//ne = (*concentration)(k-N, 0);
				if(i == 1) {
					ne = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
				} else {
					r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
					r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
					ne = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
				}
			} else {
				ne = 0.0;
			}
			(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
			(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N
			(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
			(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk

			//Source term Se
			norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
			norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
			//cout << "||E|| = (" << norm_E_r << ", " << norm_E_u << ", " << norm_E_l << ", " << norm_E_d << ") : " << norm_E_average << endl;
			//cout << "||ve|| = (" << norm_ve_r << ", " << norm_ve_u << ", " << norm_ve_l << ", " << norm_ve_d << ") : " << norm_ve_average << endl;
			Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
			//cout << "|E|/N : " << Se_cste << endl;
			if(Se_cste > 1.5e-15) {
				Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
			} else {
				Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
			}
			//cout << "Se_cste : " << Se_cste << endl;
			(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;
		}
	}

	//borders now
	//4 corners
	//bottom left
	//cout << "flux(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
	i = 0; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_V_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		ne = (*concentration)(k+1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
	Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_V_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		ne = (*concentration)(k+N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 1, true);
	//grad_V_y = neumann_condition_u(x, y, 1, false);
	//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 0))/dx;
	grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 0) + this->u(k+N, 0) + this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 0) + this->u(k, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	ve_x_l = -ve_x_l;
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	//neumann condition nek-N = nek, nek+N-1 = nek+N, nek-N-1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 1, true);
	//grad_ne_y = neumann_condition_C(x, y, 1, false);
	//grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	//grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 0, true);
	//grad_V_y = neumann_condition_u(x, y, 0, false);
	//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 0) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	//neumann condition nek-N = nek, nek-N+1 = nek+1, nek-1 = nek, nek-N-1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 0, true);
	//grad_ne_y = neumann_condition_C(x, y, 0, false);
	//grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
	//grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-1...
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);
		r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
		ne = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	//(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1	//doesn't exist
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
	(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
	//up edge
	if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-N...
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);
		r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
		ne = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	//(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N	//doesn't exist
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
	(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
	//left edge
	if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-1...
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
	//(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk	//doesn't exist
	//down edge
	if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-N...
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
	//(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk	//doesn't exist


	//Source term Se
	norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;

	//bottom right
	//cout << "flux(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition Uk-N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 2, true);
	//grad_V_y = neumann_condition_u(x, y, 2, false);
	//grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	grad_V_x = (this->function_boundary_condition(x+dx, y, 2) - this->u(k, 0))/dx;
	grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx, y+dy, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y-dy, 2)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	//neumann condition nek+1 = nek, nek+N+1 = nek+N, nek-N = nek, nek-N+1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 2, true);
	//grad_ne_y = neumann_condition_C(x, y, 2, false);
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//left hand side
	grad_V_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
	grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		ne = (*concentration)(k+N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
	grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	ve_x_l = -ve_x_l;
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		ne = (*concentration)(k-1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
	Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//doesn't exist
	//neumann condition Uk-N = Uk, Uk-N-1 = Uk-1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 0, true);
	//grad_V_y = neumann_condition_u(x, y, 0, false);
	//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
	//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 2) + this->u(k, 0) + this->function_boundary_condition(x+dx, y-dy, 2)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	//neumann condition nek+1 = nek, nek-N = nek, nek-N+1 = nek, nek-N-1 = nek-1
	//grad_ne_x = neumann_condition_C(x, y, 0, true);
	//grad_ne_y = neumann_condition_C(x, y, 0, false);
	//grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
	//grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+1...
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
	//(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk	//doesn't exist
	//up edge
	if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-N...
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);
		r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
		ne = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	//(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N	//doesn't exist
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
	(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
	//left edge
	if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+1...
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);
		r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
		ne = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	//(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1	//doesn't exist
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
	(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
	//down edge
	if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-N...
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
	//(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk	//doesn't exist
	
	//Source term Se
	norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;

	//top left
	//cout << "flux(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);
	//right edge
	//left hand side
	grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	grad_V_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		ne = (*concentration)(k+1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
	Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 3, true);
	//grad_V_y = neumann_condition_u(x, y, 3, false);
	//grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->function_boundary_condition(x-dx, y+dy, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	//neumann condition nek+N = nek, nek+N+1 = nek+1, nek+N-1 = nek, nek-1 = nek
	//grad_ne_x = neumann_condition_C(x, y, 3, true);
	//grad_ne_y = neumann_condition_C(x, y, 3, false);
	//grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	//grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 1, true);
	//grad_V_y = neumann_condition_u(x, y, 1, false);
	//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 5))/dx;
	grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 5) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 5) + this->u(k-N, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	ve_x_l = -ve_x_l;
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	//grad_ne_x = neumann_condition_C(x, y, 1, true);
	//grad_ne_y = neumann_condition_C(x, y, 1, false);
	//neumann condition nek-1 = nek, nek+N-1 = nek, nek+N = nek, nek-N-1 = nek-N
	//grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	//grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		ne = (*concentration)(k-N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-1...
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);
		r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
		ne = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	//(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1	//doesn't exist
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
	(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
	//up edge
	if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+N...
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
	//(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk	//doesn't exist
	//left edge
	if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-1...
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
	//(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk	//doesn't exist
	//down edge
	if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+N...
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);
		r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
		ne = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	//(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N	//doesn't exist
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
	(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk

		
	//Source term Se
	norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;

	//top right
	//cout << "flux(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	dx = this->delta_x_vec(j, 0);
	dy = this->delta_y_vec(i, 0);

	//right edge
	//doesn't exist
	//neumann condition Uk+N = Uk
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 2, true);
	//grad_V_y = neumann_condition_u(x, y, 2, false);
	//grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
	//grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
	grad_V_x = (this->function_boundary_condition(x+dx, y, 7) - this->u(k, 0))/dx;
	grad_V_y = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y+dy, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7) + this->u(k-N, 0) + this->function_boundary_condition(x+dx, y-dy, 7)))/dy;
	ve_x_r = -grad_V_x;
	ve_y_r = -grad_V_y;
	norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_r /= norm_E_r;
	ve_y_r /= norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "la !!" << endl;
		ve_x_r = 0.0;
		ve_y_r = 0.0;
	}
	if(norm_E_r/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
	} else if(norm_E_r/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
	} else if(norm_E_r/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
	}
	ve_x_r *= drift_cste;
	ve_y_r *= drift_cste;
	/*if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_r*ne;*/
	//right hand side
	//neumann condition nek+1 = nek, nek+N = nek, nek+N+1 = nek, nek-N+1 = nek-N
	//grad_ne_x = neumann_condition_C(x, y, 2, true);
	//grad_ne_y = neumann_condition_C(x, y, 2, false);
	//grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
	//grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
	grad_ne_y = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
	Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
	norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
	De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
	if(norm_E_r == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_r);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//up edge
	//doesn't exist
	//neumann condition Uk+N = Uk, Uk+N-1 = Uk-1
	//x = dx*(double)j + this->x_min + dx;
	//y = dy*(double)i + this->y_min + dy;
	x = this->x_coord(j, 0);
	y = this->y_coord(i, 0);
	//left hand side
	//grad_V_x = neumann_condition_u(x, y, 3, true);
	//grad_V_y = neumann_condition_u(x, y, 3, false);
	//grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	//grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
	grad_V_x = (0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y+dy, 7) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 7)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
	ve_x_u = -grad_V_x;
	ve_y_u = -grad_V_y;
	norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_u /= norm_E_u;
	ve_y_u /= norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "la !!" << endl;
		ve_x_u = 0.0;
		ve_y_u = 0.0;
	}
	if(norm_E_u/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
	} else if(norm_E_u/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
	} else if(norm_E_u/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
	}
	ve_x_u *= drift_cste;
	ve_y_u *= drift_cste;
	/*if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_u*ne;*/
	//right hand side
	//neumann condition nek+N = nek, nek+N+1 = nek, nek+1 = nek, nek+N-1 = nek-1
	//grad_ne_x = neumann_condition_C(x, y, 3, true);
	//grad_ne_y = neumann_condition_C(x, y, 3, false);
	//grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	//grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
	grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
	Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
	norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
	De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
	if(norm_E_u == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_u);
	//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//left edge
	//left hand side
	grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
	grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
	ve_x_l = -grad_V_x;
	ve_y_l = -grad_V_y;
	norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_l /= norm_E_l;
	ve_y_l /= norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "la !!" << endl;
		ve_x_l = 0.0;
		ve_y_l = 0.0;
	}
	if(norm_E_l/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
	} else if(norm_E_l/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
	} else if(norm_E_l/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
	}
	ve_x_l *= drift_cste;
	ve_y_l *= drift_cste;
	//test
	ve_x_l = -ve_x_l;
	/*if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);
	} else if(ve_x_l<0) { //goes from right to left
		ne = (*concentration)(k-1, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy*ve_x_l*ne;*/
	//right hand side
	grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
	grad_ne_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
	Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
	//test
	Dne_x = -Dne_x;
	norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
	De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
	if(norm_E_l == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_x *= De_cste;
	(*flux)(k, 0) -= dy*Dne_x;
	speed_convection = fabs(ve_x_l);
	//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//down edge
	//left hand side
	grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
	grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
	ve_x_d = -grad_V_x;
	ve_y_d = -grad_V_y;
	norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
	ve_x_d /= norm_E_d;
	ve_y_d /= norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d == 0.0) {
		//cout << "la !!" << endl;
		ve_x_d = 0.0;
		ve_y_d = 0.0;
	}
	if(norm_E_d/neutral_d > 2.0e-15) {
		drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
	} else if(norm_E_d/neutral_d > 1.0e-16) {
		drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
	} else if(norm_E_d/neutral_d > 2.6e-17) {
		drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
	} else {
		drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
	}
	ve_x_d *= drift_cste;
	ve_y_d *= drift_cste;
	//test
	ve_y_d = -ve_y_d;
	/*if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);
	} else if(ve_y_d<0) { //goes from up to down
		ne = (*concentration)(k-N, 0);
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx*ve_y_d*ne;*/
	//right hand side
	grad_ne_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
	grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
	Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
	//test
	Dne_y = -Dne_y;
	norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
	De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
	if(norm_E_d == 0.0) {
		//cout << "here !!" << endl;
		De_cste = 0.0;
	}
	Dne_y *= De_cste;
	(*flux)(k, 0) -= dx*Dne_y;
	speed_convection = fabs(ve_y_d);
	//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
	speed_diffusion = De_cste;
	if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
	if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

	//Roe scheme
	ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
	ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
	ne = (*concentration)(k, 0);
	//right edge
	if(ve_x_r>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+1...
	} else if(ve_x_r<0) { //goes from right to left
		//ne = (*concentration)(k+1, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
	//(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk	//doesn't exist
	//up edge
	if(ve_y_u>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+N...
	} else if(ve_y_u<0) { //goes from up to down
		//ne = (*concentration)(k+N, 0);	//doesn't exist
		ne = 0.0;
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
	//(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk	//doesn't exist
	//left edge
	if(ve_x_l>0) { //goes from left to right
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+1...
	} else if(ve_x_l<0) { //goes from right to left
		//ne = (*concentration)(k-1, 0);
		r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
		ne = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
	//(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1	//doesn't exist
	(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
	(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
	//down edge
	if(ve_y_d>0) { //goes from down to up
		ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+N...
	} else if(ve_y_d<0) { //goes from up to down
		//ne = (*concentration)(k-N, 0);
		r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
		r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
		ne = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
	} else {
		ne = 0.0;
	}
	(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
	//(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N	//doesn't exist
	(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
	(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk

		
	//Source term Se
	norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
	norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
	Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
	if(Se_cste > 1.5e-15) {
		Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
	} else {
		Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
	}
	(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;

	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells on the side
		int i=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_V_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))/dy;
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			ne = (*concentration)(k+1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)))/dy;
		Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			ne = (*concentration)(k+N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_V_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))/dy;
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		ve_x_l = -ve_x_l;
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			ne = (*concentration)(k-1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)))/dy;
		Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//doesn't exist
		//neumann condition Uk-N = Uk, Uk-N+1 = Uk+1, Uk-N-1 = Uk-1
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 0, true);
		//grad_V_y = neumann_condition_u(x, y, 0, false);
		//grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		//grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			//ne = (*concentration)(k-N, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		//neumann condition nek-N = nek, nek-N+1 = nek+1, nek-1 = nek, nek-N-1 = nek, nek-N = nek
		//grad_ne_x = neumann_condition_C(x, y, 0, true);
		//grad_ne_y = neumann_condition_C(x, y, 0, false);
		//grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
		//grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		
		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		if(ve_x_r>0) { //goes from left to right
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
		} else if(ve_x_r<0) { //goes from right to left
			//ne = (*concentration)(k+1, 0);
			if(j == this->n_x-2) {
				ne = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
			} else {
				r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				ne = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
		(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
		//up edge
		if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-N...
		} else if(ve_y_u<0) { //goes from up to down
			//ne = (*concentration)(k+N, 0);
			r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			ne = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		//(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N	//doesn't exist
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
		(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
		//left edge
		if(ve_x_l>0) { //goes from left to right
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
		} else if(ve_x_l<0) { //goes from right to left
			//ne = (*concentration)(k-1, 0);
			if(j == 1) {
				ne = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
			} else {
				r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				ne = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
		(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
		//down edge
		if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-N...
		} else if(ve_y_d<0) { //goes from up to down
			//ne = (*concentration)(k-N, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
		//(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk	//doesn't exist

		//Source term Se
		norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;		
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			ne = (*concentration)(k+1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			ne = (*concentration)(k+N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k+N+1, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 1, true);
		//grad_V_y = neumann_condition_u(x, y, 1, false);
		//grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		//grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		grad_V_x = (this->u(k, 0) - this->function_boundary_condition(x-dx, y, 3))/dx;
		grad_V_y = (0.25*(this->function_boundary_condition(x-dx, y+dy, 3) + this->u(k+N, 0) + this->function_boundary_condition(x-dx, y, 3) + this->u(k, 0)) - 0.25*(this->function_boundary_condition(x-dx, y, 3) + this->u(k, 0) + this->function_boundary_condition(x-dx, y-dy, 3) + this->u(k-N, 0)))/dy;
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		ve_x_l = -ve_x_l;
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			//ne = (*concentration)(k-1, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		//neumann condition nek-1 = nek, nek+N-1 = nek+N, nek-N-1 = nek-N
		//grad_ne_x = neumann_condition_C(x, y, 1, true);
		//grad_ne_y = neumann_condition_C(x, y, 1, false);
		//grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		//grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//down edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			ne = (*concentration)(k-N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k+1, 0) + (*concentration)(k-N+1, 0)) - 0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-1...
		} else if(ve_x_r<0) { //goes from right to left
			//ne = (*concentration)(k+1, 0);
			r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			ne = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		//(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1	//doesn't exist
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
		(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
		//up edge
		if(ve_y_u>0) { //goes from down to up
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
		} else if(ve_y_u<0) { //goes from up to down
			//ne = (*concentration)(k+N, 0);
			if(i == this->n_y-2) {
				ne = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
			} else {
				r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				ne = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
		(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
		//left edge
		if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek-1...
		} else if(ve_x_l<0) { //goes from right to left
			//ne = (*concentration)(k-1, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
		//(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk	//doesn't exist
		//down edge
		if(ve_y_d>0) { //goes from down to up
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
		} else if(ve_y_d<0) { //goes from up to down
			//ne = (*concentration)(k-N, 0);
			if(i == 1) {
				ne = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
			} else {
				r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				ne = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
		(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk


		//Source term Se
		norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);

		//right edge
		//doesn't exist
		//neumann condition
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 2, true);
		//grad_V_y = neumann_condition_u(x, y, 2, false);
		//grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		//grad_V_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		grad_V_x = (this->function_boundary_condition(x+dx, y, 4) - this->u(k, 0))/dx;
		grad_V_y = (0.25*(this->u(k+N, 0) + this->function_boundary_condition(x+dx, y+dy, 4) + this->u(k, 0) + this->function_boundary_condition(x+dx, y, 4)) - 0.25*(this->u(k, 0) + this->function_boundary_condition(x+dx, y, 4) + this->u(k-N, 0) + this->function_boundary_condition(x+dx, y-dy, 4)))/dy;
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			//ne = (*concentration)(k+1, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		//neumann condition nek+1 = nek, nek+N+1 = nek+N, nek-N+1 = ne-N
		//grad_ne_x = neumann_condition_C(x, y, 2, true);
		//grad_ne_y = neumann_condition_C(x, y, 2, false);
		//grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		//grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N, 0) + (*concentration)(k, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k, 0) + (*concentration)(k, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K_boundary(k, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
	
		//up edge
		//left hand side
		grad_V_x = (0.5*(this->u(k+N, 0) + this->u(k, 0)) - 0.5*(this->u(k+N-1, 0) + this->u(k-1, 0)))/dx;
		grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			ne = (*concentration)(k+N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k+N, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k+N-1, 0) + (*concentration)(k-1, 0)))/dx;
		grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K(k, k+N, 2)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_V_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;		
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		ve_x_l = -ve_x_l;
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			ne = (*concentration)(k-1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_ne_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_V_x = (0.5*(this->u(k, 0) + this->u(k-N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k-N-1, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			ne = (*concentration)(k-N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		grad_ne_x = (0.5*((*concentration)(k, 0) + (*concentration)(k-N, 0)) - 0.5*((*concentration)(k-1, 0) + (*concentration)(k-N-1, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		
		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+1...
		} else if(ve_x_r<0) { //goes from right to left
			//ne = (*concentration)(k+1, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		//(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
		(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk	//doesn't exist
		//up edge
		if(ve_y_u>0) { //goes from down to up
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			r_f_denominator = ((*concentration)(k+N, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+N, 0) - (*concentration)(k, 0));
		} else if(ve_y_u<0) { //goes from up to down
			//ne = (*concentration)(k+N, 0);
			if(i == this->n_y-2) {
				ne = (*concentration)(k+N, 0);	//ordre 1, we don't know Ck+2N...
			} else {
				r_f_numerator = ((*concentration)(k+N, 0) - (*concentration)(k+2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
				ne = (*concentration)(k+N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+N, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
		(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk
		//left edge
		if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+1...
		} else if(ve_x_l<0) { //goes from right to left
			//ne = (*concentration)(k-1, 0);
			//ne = (*concentration)(k-1, 0);
			r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			ne = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		//(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1	//doesn't exist
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
		(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
		//down edge
		if(ve_y_d>0) { //goes from down to up
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+N, 0));
			r_f_denominator = ((*concentration)(k-N, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-N, 0) - (*concentration)(k, 0));
		} else if(ve_y_d<0) { //goes from up to down
			//ne = (*concentration)(k-N, 0);
			if(i == 1) {
				ne = (*concentration)(k-N, 0);	//ordre 1, we don't know Ck-2N...
			} else {
				r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
				ne = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
		(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk


		//Source term Se
		norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;
	}

	//top
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		dx = this->delta_x_vec(j, 0);
		dy = this->delta_y_vec(i, 0);
		
		//right edge
		//left hand side
		grad_V_x = (this->u(k+1, 0) - this->u(k, 0))/dx;
		grad_V_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))/dy;
		ve_x_r = -grad_V_x;
		ve_y_r = -grad_V_y;
		norm_E_r = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_r /= norm_E_r;
		ve_y_r /= norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "la !!" << endl;
			ve_x_r = 0.0;
			ve_y_r = 0.0;
		}
		if(norm_E_r/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_r/neutral_d) + 7.1e6;
		} else if(norm_E_r/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_r/neutral_d) + 1.3e6;
		} else if(norm_E_r/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_r/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_r/neutral_d) + 3.38e4;
		}
		ve_x_r *= drift_cste;
		ve_y_r *= drift_cste;
		/*if(ve_x_r>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_r<0) { //goes from right to left
			ne = (*concentration)(k+1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_r*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k+1, 0) - (*concentration)(k, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.5*((*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)))/dy;
		Dne_x = D->get_K(k, k+1, 1)*grad_ne_x;
		norm_ve_r = sqrt(ve_x_r*ve_x_r + ve_y_r*ve_y_r);
		De_cste = 0.3341e9*pow(norm_E_r/neutral_d, 0.54069)*norm_ve_r/norm_E_r;
		if(norm_E_r == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_r);
		//speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//up edge
		//doesn't exist
		//neumann condition Uk+N = Uk, Uk+N+1 = Uk+1, Uk+N-1 = Uk-1
		//x = dx*(double)j + this->x_min + dx;
		//y = dy*(double)i + this->y_min + dy;
		x = this->x_coord(j, 0);
		y = this->y_coord(i, 0);
		//left hand side
		//grad_V_x = neumann_condition_u(x, y, 3, true);
		//grad_V_y = neumann_condition_u(x, y, 3, false);
		//grad_V_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		//grad_V_y = (this->u(k+N, 0) - this->u(k, 0))/dy;
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-1, 0) + this->u(k, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k, 0))/dy;
		ve_x_u = -grad_V_x;
		ve_y_u = -grad_V_y;
		norm_E_u = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_u /= norm_E_u;
		ve_y_u /= norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "la !!" << endl;
			ve_x_u = 0.0;
			ve_y_u = 0.0;
		}
		if(norm_E_u/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_u/neutral_d) + 7.1e6;
		} else if(norm_E_u/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_u/neutral_d) + 1.3e6;
		} else if(norm_E_u/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_u/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_u/neutral_d) + 3.38e4;
		}
		ve_x_u *= drift_cste;
		ve_y_u *= drift_cste;
		/*if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_u<0) { //goes from up to down
			//ne = (*concentration)(k+N, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_u*ne;*/
		//right hand side
		//neumann condition nek+N = nek, nek+N+1 = nek+1, nek+N-1 = nek-1
		//grad_ne_x = neumann_condition_C(x, y, 3, true);
		//grad_ne_y = neumann_condition_C(x, y, 3, false);
		//grad_ne_x = (0.25*((*concentration)(k+N, 0) + (*concentration)(k+N+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		//grad_ne_y = ((*concentration)(k+N, 0) - (*concentration)(k, 0))/dy;
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k, 0) + (*concentration)(k+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k, 0))/dy;
		Dne_y = D->get_K_boundary(k, 4)*grad_ne_y;
		norm_ve_u = sqrt(ve_x_u*ve_x_u + ve_y_u*ve_y_u);
		De_cste = 0.3341e9*pow(norm_E_u/neutral_d, 0.54069)*norm_ve_u/norm_E_u;
		if(norm_E_u == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_u);
		//speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

		//left edge
		//left hand side
		grad_V_x = (this->u(k, 0) - this->u(k-1, 0))/dx;
		grad_V_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))/dy;
		ve_x_l = -grad_V_x;
		ve_y_l = -grad_V_y;
		norm_E_l = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_l /= norm_E_l;
		ve_y_l /= norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "la !!" << endl;
			ve_x_l = 0.0;
			ve_y_l = 0.0;
		}
		if(norm_E_l/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_l/neutral_d) + 7.1e6;
		} else if(norm_E_l/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_l/neutral_d) + 1.3e6;
		} else if(norm_E_l/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_l/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_l/neutral_d) + 3.38e4;
		}
		ve_x_l *= drift_cste;
		ve_y_l *= drift_cste;
		//test
		ve_x_l = -ve_x_l;
		/*if(ve_x_l>0) { //goes from left to right
			ne = (*concentration)(k, 0);
		} else if(ve_x_l<0) { //goes from right to left
			ne = (*concentration)(k-1, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy*ve_x_l*ne;*/
		//right hand side
		grad_ne_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx;
		grad_ne_y = (0.5*((*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.5*((*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy;
		Dne_x = D->get_K(k-1, k, 1)*grad_ne_x;
		//test
		Dne_x = -Dne_x;
		norm_ve_l = sqrt(ve_x_l*ve_x_l + ve_y_l*ve_y_l);
		De_cste = 0.3341e9*pow(norm_E_l/neutral_d, 0.54069)*norm_ve_l/norm_E_l;
		if(norm_E_l == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_x *= De_cste;
		(*flux)(k, 0) -= dy*Dne_x;
		speed_convection = fabs(ve_x_l);
		//speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		
		//down edge
		//left hand side
		grad_V_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx;
		grad_V_y = (this->u(k, 0) - this->u(k-N, 0))/dy;
		ve_x_d = -grad_V_x;
		ve_y_d = -grad_V_y;
		norm_E_d = sqrt(grad_V_x*grad_V_x + grad_V_y*grad_V_y);
		ve_x_d /= norm_E_d;
		ve_y_d /= norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d == 0.0) {
			//cout << "la !!" << endl;
			ve_x_d = 0.0;
			ve_y_d = 0.0;
		}
		if(norm_E_d/neutral_d > 2.0e-15) {
			drift_cste = (7.4e21 * norm_E_d/neutral_d) + 7.1e6;
		} else if(norm_E_d/neutral_d > 1.0e-16) {
			drift_cste = (1.03e22 * norm_E_d/neutral_d) + 1.3e6;
		} else if(norm_E_d/neutral_d > 2.6e-17) {
			drift_cste = (7.2973e21 * norm_E_d/neutral_d) + 1.63e6;
		} else {
			drift_cste = (6.87e22 * norm_E_d/neutral_d) + 3.38e4;
		}
		ve_x_d *= drift_cste;
		ve_y_d *= drift_cste;
		//test
		ve_y_d = -ve_y_d;
		/*if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);
		} else if(ve_y_d<0) { //goes from up to down
			ne = (*concentration)(k-N, 0);
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx*ve_y_d*ne;*/
		//right hand side
		grad_ne_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx;
		grad_ne_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy;
		Dne_y = D->get_K(k, k-N, 2)*grad_ne_y;
		//test
		Dne_y = -Dne_y;
		norm_ve_d = sqrt(ve_x_d*ve_x_d + ve_y_d*ve_y_d);
		De_cste = 0.3341e9*pow(norm_E_d/neutral_d, 0.54069)*norm_ve_d/norm_E_d;
		if(norm_E_d == 0.0) {
			//cout << "here !!" << endl;
			De_cste = 0.0;
		}
		Dne_y *= De_cste;
		(*flux)(k, 0) -= dx*Dne_y;
		speed_convection = fabs(ve_y_d);
		//speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
		speed_diffusion = De_cste;
		if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;


		//Roe scheme
		ve_x_average = 0.25*(ve_x_r + ve_x_u - ve_x_l + ve_x_d);
		ve_y_average = 0.25*(ve_y_r + ve_y_u + ve_y_l - ve_y_d);
		ne = (*concentration)(k, 0);
		//right edge
		if(ve_x_r>0) { //goes from left to right
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
			r_f_denominator = ((*concentration)(k+1, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k+1, 0) - (*concentration)(k, 0));
		} else if(ve_x_r<0) { //goes from right to left
			//ne = (*concentration)(k+1, 0);
			if(j == this->n_x-2) {
				ne = (*concentration)(k+1, 0);	//ordre 1, we don't know Ck+2...
			} else {
				r_f_numerator = ((*concentration)(k+1, 0) - (*concentration)(k+2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
				ne = (*concentration)(k+1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k+1, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		(*flux)(k-1, 0) += dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k+1
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_r)*ne;
		(*flux)(k+1, 0) += -dy* 0.5*fabs(ve_x_r)*ne;	//from uk
		//up edge
		if(ve_y_u>0) { //goes from down to up
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+N...
		} else if(ve_y_u<0) { //goes from up to down
			//ne = (*concentration)(k+N, 0);	//doesn't exist
			ne = 0.0;
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		(*flux)(k-N, 0) += dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k+N
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_u)*ne;
		//(*flux)(k+N, 0) += -dx* 0.5*fabs(ve_y_u)*ne;	//from uk	//doesn't exist
		//left edge
		if(ve_x_l>0) { //goes from left to right
			//ne = (*concentration)(k, 0);
			r_f_numerator = ((*concentration)(k, 0) - (*concentration)(k+1, 0));
			r_f_denominator = ((*concentration)(k-1, 0) - (*concentration)(k, 0));
			ne = (*concentration)(k, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k-1, 0) - (*concentration)(k, 0));
		} else if(ve_x_l<0) { //goes from right to left
			//ne = (*concentration)(k-1, 0);
			if(j == 1) {
				ne = (*concentration)(k-1, 0);	//ordre 1, we don't know Ck-2...
			} else {
				r_f_numerator = ((*concentration)(k-1, 0) - (*concentration)(k-2, 0));
				r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-1, 0));
				ne = (*concentration)(k-1, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-1, 0));
			}
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//remove from cell k
		(*flux)(k+1, 0) += -dy* 0.5*(*concentration)(k, 0)*ve_x_average;	//add to cell k-1
		(*flux)(k, 0) += dy* 0.5*fabs(ve_x_l)*ne;
		(*flux)(k-1, 0) += -dy* 0.5*fabs(ve_x_l)*ne;	//from uk
		//down edge
		if(ve_y_d>0) { //goes from down to up
			ne = (*concentration)(k, 0);	//ordre 1, we don't know nek+N...
		} else if(ve_y_d<0) { //goes from up to down
			//ne = (*concentration)(k-N, 0);
			r_f_numerator = ((*concentration)(k-N, 0) - (*concentration)(k-2*N, 0));
			r_f_denominator = ((*concentration)(k, 0) - (*concentration)(k-N, 0));
			ne = (*concentration)(k-N, 0) + 0.5*limiter(r_f_numerator, r_f_denominator, limiter_name)*((*concentration)(k, 0) - (*concentration)(k-N, 0));
		} else {
			ne = 0.0;
		}
		(*flux)(k, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//remove from cell k
		//(*flux)(k+N, 0) += -dx* 0.5*(*concentration)(k, 0)*ve_y_average;	//add to cell k-N	//doesn't exist
		(*flux)(k, 0) += dx* 0.5*fabs(ve_y_d)*ne;
		(*flux)(k-N, 0) += -dx* 0.5*fabs(ve_y_d)*ne;	//from uk


		//Source term Se
		norm_E_average = 0.25*(norm_E_r + norm_E_u + norm_E_l + norm_E_d);
		norm_ve_average = 0.25*(norm_ve_r + norm_ve_u + norm_ve_l + norm_ve_d);
		Se_cste = norm_E_average/neutral_d;	//temporary, avoids a division
		if(Se_cste > 1.5e-15) {
			Se_cste = 2.0e-16*exp(-7.248e-15/(Se_cste));
		} else {
			Se_cste = 6.619e-17*exp(-5.593e-15/(Se_cste));
		}
		(*Se)(k, 0) = Se_cste*norm_ve_average*(*concentration)(k, 0)*neutral_d;
	}
	//(*flux).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "flux");
	//return max_speed;
}
