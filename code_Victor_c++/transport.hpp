#ifndef TRANSPORT_HPP_INCLUDED
#define TRANSPORT_HPP_INCLUDED

#include "dense_matrix.hpp"
#include "darcy.hpp"
#include "functions_type.hpp"
#include "solver.hpp"
#include "solver_double.hpp"
#include "starter.hpp"
#include "permeability_matrix.hpp"
#include "mesh.hpp"


#include "functions_type.hpp"
class transport {
	protected :
		double dx;
		double dy;
		double x_max;
		double y_max;
		double x_min;
		double y_min;
		int n_x;	//#of points on x axis
		int n_y;	//#of points on y axis
		functiontype3 neumann_condition_u;
		double cfl;
        permeability_matrix D;	//transport ... = div(Dgrad(C)) + f2
        dense_matrix f2;	//transport ... = div(Dgrad(C)) + f2
        functiontype1 function_f2;	//transport ... = div(Dgrad(C)) + f2
        functiontype3 neumann_condition_C;	//transport ... = div(Dgrad(C)) + f2
		dense_matrix flux_right_hand_side;	//(A*cn+1 = )cn for backward euler
		dense_matrix flux_boundary;	//(A*cn+1 = )cn for backward euler
		dense_matrix convection_boundary;	//(A*cn+1 = )cn for backward euler
		dense_matrix diffusion_boundary;	//(A*cn+1 = )cn for backward euler
		dense_matrix ve_x_left;
		dense_matrix ve_y_down;

	public :
		transport(double, double, double, double, int, int, mesh *, functiontype3, double, permeability_matrix, functiontype1, functiontype3);
		void compute_iterativ(double, darcy *, starter *, functiontype4 limiter = NULL, sparse_matrix * = NULL, darcy_double * = NULL);
		void compute_iterativ_backward(double, darcy *, starter *);
		void compute_iterativ_backward_coupled(double, darcy *, starter *, sparse_matrix * = NULL, darcy_double * = NULL);
		void compute_iterativ_backward_MUSCL_limiter(double, darcy *, starter *, functiontype4);
		void compute_iterativ_backward_MUSCL_limiter_coupled(double, darcy *, starter *, functiontype4, sparse_matrix * = NULL, darcy_double * = NULL);
		dense_matrix x_coord;
		dense_matrix y_coord;
		dense_matrix concentration;
        dense_matrix concentration_old;
		dense_matrix flux;
		dense_matrix convection;
		dense_matrix diffusion;
		dense_matrix grad_h_x_left; //gradx u
		dense_matrix grad_h_y_left; //grady u
		dense_matrix grad_h_x_down; //gradx u
		dense_matrix grad_h_y_down; //grady u
		sparse_matrix flux_system;	//A(*cn+1 = cn) for backward euler
		sparse_matrix convection_system;	//A(*cn+1 = cn) for backward euler
		sparse_matrix diffusion_system;	//A(*cn+1 = cn) for backward euler
		dense_matrix grad_C_x_left;
		dense_matrix grad_C_y_left;
		dense_matrix grad_C_x_down;
		dense_matrix grad_C_y_down;
		double time;
		void set_centered_gaussian(double, double, double, double, double);
		void set_step_wave(double, double, double, double, double, double);
		void write_grid(ofstream *);
		void write_frame(ofstream *);
		double set_dt(double, double, double);
		void solve_darcy(darcy *, darcy_double *, solver *, solver_double *, starter *, sparse_matrix *);
};

#endif // TRANSPORT_HPP_INCLUDED
