#include "mesh.hpp"
#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>
#include "dense_matrix.hpp"

mesh::mesh() {

}

mesh::mesh(char *name) {
	ifstream file(name);
	int N_domain_x = 0;
	int N_domain_y = 0;
	double delta_x, delta_y;
	double delta_x_prev, delta_y_prev;
	double delta_x_next, delta_y_next;
	int smooth_level_x = 0;
	int smooth_level_y = 0;
	if(file.is_open()) {
		string param_name;
		//get all file values
		//for x axis
		file >> param_name >> N_domain_x >> param_name >> smooth_level_x;	//number of domains on the x axis
		vector<double> pos_axis_x(N_domain_x + 1);
		vector<int> cells_axis_x(N_domain_x + 1);
		cells_axis_x[0] = 0;
		for(int i=0; i<N_domain_x; i++) {
			file >> param_name >> pos_axis_x[i] >> param_name >> cells_axis_x[i+1];
		}
		file >> param_name >> pos_axis_x[N_domain_x];
		//cumsum will be easier to work with
		for(int i=1; i<N_domain_x+1; i++) {
			if(cells_axis_x[i] < 2) {
				cout << "ERROR NOT ENOUGH XPOINT IN MESH" << endl;
			}
			cells_axis_x[i] += cells_axis_x[i-1];
		}
		
		//for y axis
		file >> param_name >> N_domain_y >> param_name >> smooth_level_y;	//number of domains on the x axis
		vector<double> pos_axis_y(N_domain_y + 1);
		vector<int> cells_axis_y(N_domain_y + 1);
		cells_axis_y[0] = 0;
		for(int i=0; i<N_domain_y; i++) {
			file >> param_name >> pos_axis_y[i] >> param_name >> cells_axis_y[i+1];
		}
		file >> param_name >> pos_axis_y[N_domain_y];
		//cumsum will be easier to work with
		for(int i=1; i<N_domain_y+1; i++) {
			if(cells_axis_y[i] < 2) {
				cout << "ERROR NOT ENOUGH YPOINT IN MESH" << endl;
			}
			cells_axis_y[i] += cells_axis_y[i-1];
		}

		//find desired size of the complete x axis and y axis
		this->nx = cells_axis_x.back()-1;
		this->ny = cells_axis_y.back()-1;
		cout << "cells_axis_x: " << endl;
		for(int i=0; i<cells_axis_x.size(); i++) {
			cout << cells_axis_x[i] << " ";
		}
		cout << endl << "cells_axis_y: " << endl;
		for(int i=0; i<cells_axis_y.size(); i++) {
			cout << cells_axis_y[i] << " ";
		}
		cout << endl;
		//initialise
		this->dx = dense_matrix(this->nx, 1);
		this->dy = dense_matrix(this->ny, 1);
		this->x = dense_matrix(this->nx, 1);
		this->y = dense_matrix(this->ny, 1);
	
		//fill vectors x
		//first domain
		//first pass
		vector<int> index_non_linear_domain;
		vector<double> coord_non_linear_domain;
		for(int i=0; i<N_domain_x; i++) {
			delta_x = (pos_axis_x[i+1] - pos_axis_x[i]) / (cells_axis_x[i+1] - cells_axis_x[i]);
			if(i < N_domain_x-2) {
				delta_x_next = (pos_axis_x[i+2] - pos_axis_x[i+1]) / (cells_axis_x[i+2] - cells_axis_x[i+1]);
			} else if(i == N_domain_x-2) {
				delta_x_next = (pos_axis_x[N_domain_x] - pos_axis_x[N_domain_x-1]) / (cells_axis_x[N_domain_x] - cells_axis_x[N_domain_x-1] + 1);
			} else if(i == N_domain_x-1 && i!=0) {
				delta_x = (pos_axis_x[N_domain_x] - pos_axis_x[N_domain_x-1]) / (cells_axis_x[N_domain_x] - cells_axis_x[N_domain_x-1] + 1);
				delta_x_next = delta_x;
			} else {
				delta_x_next = delta_x;
			}
			int start = cells_axis_x[i];
			int end = cells_axis_x[i+1];
			double radius = 0.2;	//[0, radius] included, ]1-radius, 1] excluded

			int first_part_end = (end - start)*radius + start;
			int third_part_start = end - (end - start)*radius;
			if(i==0) {
				this->x(0, 0) = pos_axis_x[0] + delta_x;
				start = 1;
				delta_x_prev = delta_x;
			}
			double j_percentage;
			double delta_step;
			//y = 1.062138 + (-1.610701e-17 - 1.062138)/(1 + (x/0.2906544)^2.248583)
			if(i>0) {
				index_non_linear_domain.push_back(first_part_end);
				coord_non_linear_domain.push_back(delta_x);
			}
			if(i<N_domain_x-1) {
				index_non_linear_domain.push_back(third_part_start);
				coord_non_linear_domain.push_back(delta_x);
			}
			for(int j=start; j<first_part_end; j++) {
				if(i==0) {
					j_percentage = (double)(j+1 - (start-1))/(double)(end - (start-1));
					delta_step = j_percentage*(pos_axis_x[i+1] - pos_axis_x[i]);
				} else {
					j_percentage = (double)(j+1 - start)/(double)(end - start);
					delta_step = j_percentage*(pos_axis_x[i+1] - pos_axis_x[i]);
				}
				this->dx(j, 0) = delta_x;
				this->x(j, 0) = pos_axis_x[i] + delta_step;
			}
			for(int j=first_part_end; j<third_part_start; j++) {
				if(i==0) {
					j_percentage = (double)(j+1 - (start-1))/(double)(end - (start-1));
					delta_step = j_percentage*(pos_axis_x[i+1] - pos_axis_x[i]);
				} else {
					j_percentage = (double)(j+1 - start)/(double)(end - start);
					delta_step = j_percentage*(pos_axis_x[i+1] - pos_axis_x[i]);
				}
				this->dx(j, 0) = delta_x;
				this->x(j, 0) = pos_axis_x[i] + delta_step;
			}
			for(int j=third_part_start; j<min(end, this->nx); j++) {
				if(i==0) {
					j_percentage = (double)(j+1 - (start-1))/(double)(end - (start-1));
					delta_step = j_percentage*(pos_axis_x[i+1] - pos_axis_x[i]);
				} else {
					j_percentage = (double)(j+1 - start)/(double)(end - start);
					delta_step = j_percentage*(pos_axis_x[i+1] - pos_axis_x[i]);
				}
				this->dx(j, 0) = delta_x;
				this->x(j, 0) = pos_axis_x[i] + delta_step;
				if(j == this->nx-1) {
					break;
				}
			}
			delta_x_prev = delta_x;
		}
		//second pass
/*		double power = 1.0;
		for(int i=0; i< index_non_linear_domain.size(); i=i+2) {
			double coord_min = this->x(index_non_linear_domain[i]-1, 0);
			double coord_max = this->x(index_non_linear_domain[i+1], 0);
			double delta_left = coord_non_linear_domain[i];
			double delta_right = coord_non_linear_domain[i+1];
			int n = index_non_linear_domain[i+1] - index_non_linear_domain[i];
			for(int j=0; j<n; j++) {
				double coef = (double)(j+1)/(double)(n+1);
				double t;
				if(delta_left > delta_right) {
					t = pow(1 - pow(1-coef, power), 1.0/power);
				} else {
					t = 1-pow(1-pow(coef, power), 1.0/power);
				}
				this->x(index_non_linear_domain[i] + j, 0) = coord_min + (coord_max - coord_min)*pow(coef, power);
				this->x(index_non_linear_domain[i] + j, 0) = coord_min + (coord_max - coord_min)*t;
			}
		}
*/
		//second pass
		dense_matrix x_tmp = this->x;
		for(int i=0; i<smooth_level_x; i++) {
			for(int j=1; j<this->nx-1; j++) {
				x_tmp(j, 0) = 0.25*(this->x(j-1, 0) + 2*this->x(j, 0) + this->x(j+1, 0));
			}
			this->x = x_tmp;
		}
		for(int j=0; j<this->nx-1; j++) {
			this->dx(j, 0) = this->x(j+1, 0) - this->x(j, 0);
		}
		this->dx(this->nx-1, 0) = this->x(this->nx-1, 0) - this->x(this->nx-2, 0);
		//this->x = x_tmp;
		
	
		//fill vectors y
		//first domain
		//first pass
		index_non_linear_domain.clear();
		coord_non_linear_domain.clear();
		for(int i=0; i<N_domain_y; i++) {
			delta_y = (pos_axis_y[i+1] - pos_axis_y[i]) / (cells_axis_y[i+1] - cells_axis_y[i]);
			if(i < N_domain_y-2) {
				delta_y_next = (pos_axis_y[i+2] - pos_axis_y[i+1]) / (cells_axis_y[i+2] - cells_axis_y[i+1]);
			} else if(i == N_domain_y-2) {
				delta_y_next = (pos_axis_y[N_domain_y] - pos_axis_y[N_domain_y-1]) / (cells_axis_y[N_domain_y] - cells_axis_y[N_domain_y-1] + 1);
			} else if(i == N_domain_y-1 && i!=0) {
				delta_y = (pos_axis_y[N_domain_y] - pos_axis_y[N_domain_y-1]) / (cells_axis_y[N_domain_y] - cells_axis_y[N_domain_y-1] + 1);
				delta_y_next = delta_y;
			} else {
				delta_y_next = delta_y;
			}
			int start = cells_axis_y[i];
			int end = cells_axis_y[i+1];
			double radius = 1.0/4.0;	//[0, radius] included, ]1-radius, 1] excluded

			int first_part_end = (end - start)*radius + start;
			int third_part_start = end - (end - start)*radius;
			if(i==0) {
				this->y(0, 0) = pos_axis_y[0] + delta_y;
				start = 1;
				delta_y_prev = delta_y;
			}
			double j_percentage;
			double delta_step;
			//y = 1.062138 + (-1.610701e-17 - 1.062138)/(1 + (x/0.2906544)^2.248583)
			if(i>0) {
				index_non_linear_domain.push_back(first_part_end);
				coord_non_linear_domain.push_back(delta_y);
			}
			if(i<N_domain_y-1) {
				index_non_linear_domain.push_back(third_part_start);
				coord_non_linear_domain.push_back(delta_y);
			}
			for(int j=start; j<first_part_end; j++) {
				if(i==0) {
					j_percentage = (double)(j+1 - (start-1))/(double)(end - (start-1));
					delta_step = j_percentage*(pos_axis_y[i+1] - pos_axis_y[i]);
				} else {

					j_percentage = (double)(j+1 - start)/(double)(end - start);
					delta_step = j_percentage*(pos_axis_y[i+1] - pos_axis_y[i]);
				}
				this->dy(j, 0) = delta_y;
				this->y(j, 0) = pos_axis_y[i] + delta_step;
			}
			for(int j=first_part_end; j<third_part_start; j++) {
				if(i==0) {
					j_percentage = (double)(j+1 - (start-1))/(double)(end - (start-1));
					delta_step = j_percentage*(pos_axis_y[i+1] - pos_axis_y[i]);
				} else {
					j_percentage = (double)(j+1 - start)/(double)(end - start);
					delta_step = j_percentage*(pos_axis_y[i+1] - pos_axis_y[i]);
				}
				this->dy(j, 0) = delta_y;
				this->y(j, 0) = pos_axis_y[i] + delta_step;
			}
			for(int j=third_part_start; j<min(end, this->ny); j++) {
				if(i==0) {
					j_percentage = (double)(j+1 - (start-1))/(double)(end - (start-1));
					delta_step = j_percentage*(pos_axis_y[i+1] - pos_axis_y[i]);
				} else {
					j_percentage = (double)(j+1 - start)/(double)(end - start);
					delta_step = j_percentage*(pos_axis_y[i+1] - pos_axis_y[i]);
				}
				this->dy(j, 0) = delta_y;
				this->y(j, 0) = pos_axis_y[i] + delta_step;
				if(j == this->ny-1) {
					break;
				}
			}
			delta_y_prev = delta_y;
		}
		//second pass
/*		power = 1.0;
		for(int i=0; i< index_non_linear_domain.size(); i=i+2) {
			double coord_min = this->y(index_non_linear_domain[i]-1, 0);
			double coord_max = this->y(index_non_linear_domain[i+1], 0);
			double delta_left = coord_non_linear_domain[i];
			double delta_right = coord_non_linear_domain[i+1];
			int n = index_non_linear_domain[i+1] - index_non_linear_domain[i];
			for(int j=0; j<n; j++) {
				double coef = (double)(j+1)/(double)(n+1);
				double t;
				if(delta_left > delta_right) {
					t = pow(1 - pow(1-coef, power), 1.0/power);
				} else {
					t = 1-pow(1-pow(coef, power), 1.0/power);
				}
				this->y(index_non_linear_domain[i] + j, 0) = coord_min + (coord_max - coord_min)*pow(coef, power);
				this->y(index_non_linear_domain[i] + j, 0) = coord_min + (coord_max - coord_min)*t;
			}
		}
*/
		//second pass
		dense_matrix y_tmp = this->y;
		/*cout << "y_prev: " << endl;
		this->y.print();*/
		for(int i=0; i<smooth_level_y; i++) {
			for(int j=1; j<this->ny-1; j++) {
				y_tmp(j, 0) = 0.25*(this->y(j-1, 0) + 2*this->y(j, 0) + this->y(j+1, 0));
			}
			this->y = y_tmp;
		}
		for(int j=0; j<this->ny-1; j++) {
			this->dy(j, 0) = this->y(j+1, 0) - this->y(j, 0);
		}
		this->dy(this->ny-1, 0) = this->y(this->ny-1, 0) - this->y(this->ny-2, 0);

		index_non_linear_domain.clear();
		coord_non_linear_domain.clear();
		file.close();
		/*cout << "x:" << endl;
		this->x.print();
		cout << "y:" << endl;
		this->y.print();*/
		
	} else {
		cout << "Could not open domain file !" << endl;
	}
}

double mesh::interpolated_delta(int i, int i_min, int i_max, double d_prev, double d, double d_next, double radius, double power) {
	//radius belongs to [0.0; 0.5]
	return d;
	double i_percentage = (double)(i - i_min)/(double)(i_max - i_min);
	if(i_percentage < 0.0 || i_percentage > 1.0) {
		cout << "ERROR INTERPOLATION DELTA" << endl;
	}
	if(i_percentage < radius) {	//left interpolation between d_prev and d
		return (1-pow(i_percentage/radius, power))*(d_prev + d)/2.0 + pow((i_percentage/radius), power)*d;
	} else if(i_percentage > 1.0 - radius) {	//right interpolation between d and d_next
		return (1.0 - pow((i_percentage - (1.0 - radius))/radius, power))*d + pow((i_percentage - (1.0 - radius))/radius, power)*(d + d_next)/2.0;
	} else {
		return d;
	}
}
