#include "starter.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <vector>

using namespace std;

starter::starter(char *name) {
	cout << name << endl;
	ifstream file(name);
	if(file.is_open()) {
		string param_name;
		string line;
		istringstream iss;

		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->x_min;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->x_max;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->y_min;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->y_max;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->n_x;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->n_y;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->solver_darcy;
		//param's for GMRES with restart
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->talkative;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->n_before_restart;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->max_it;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->tol;
		iss.clear();
		//param's for GMRES without inv
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->gmres_without_inv_max_it;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->gmres_without_inv_tol;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->darcy_device;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->N_threads_darcy;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->const_poisson;
		iss.clear();
		//transport parameters
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->end_time;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->concentration_type;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->x_pos_gaussian;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->y_pos_gaussian;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->x_var_gaussian;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->y_var_gaussian;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->const_gaussian;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->background_density;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->x_min_wave;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->x_max_wave;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->y_min_wave;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->y_max_wave;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->low_value_wave;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->high_value_wave;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->transport_integration;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->solver_transport;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->backward_dt;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->transport_mode;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->cfl;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->ordre_transport_space;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->limiter_name;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->transport_device;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->N_threads_transport;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->iter_max;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->streamer;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->convection;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->diffusion;
		iss.clear();
		
		getline(file, line);
		iss.str(line);
		iss >> param_name >> this->output;

		file.close();

		this->limiter_name_int = -1;
		if(this->limiter_name == "vanleer") {
			this->limiter_name_int = 0;
		}
		if(this->limiter_name == "superbee") {
			this->limiter_name_int = 1;
		}
		if(this->limiter_name == "minmod") {
			this->limiter_name_int = 2;
		}
		if(this->limiter_name == "vanalbada") {
			this->limiter_name_int = 3;
		}
		if(this->limiter_name == "barthjespersen") {
			this->limiter_name_int = 4;
		}
	} else {
		cout << "Could not open starter file !" << endl;
	}
}
