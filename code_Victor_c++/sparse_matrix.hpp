/*
Victor ARTIGUES  - MACS 2 in 2017
Projet Numérique industriel
*/



#ifndef SPARSE_MATRIX_HPP_INCLUDED
#define SPARSE_MATRIX_HPP_INCLUDED

#include <vector>
#include "dense_matrix.hpp"
using namespace std;

class sparse_matrix {
    protected :
        int rows;
        int cols;
        /*  DOUBLE INDEX
        m(0,0)          m(0,1)      ...     m(0,cols-1)
        m(1,0)          m(1,1)      ...     m(1,cols-1)
        ...             ...         ...     ...
        m(rows-1,0)     m(rows-1,1) ...     m(rows-1,cols-1)
        */
		//ordered line by line
        /*  LINEAR
        m(0)                m(1)                ...     m(cols-1)
        m(cols)             m(cols+1)           ...     m(2*cols-1)
        ...                 ...                 ...     ...
        m((rows-1)*cols)    m((rows-1)*cols+1)  ...     m(rows*cols-1)
        */

    public :
		//ordered line by line
		//we will double that with a column by column arrangement
        vector<int> cols_index;
        vector<int> rows_first_position;
        vector<double> values;
        int nnz;
        
        
        sparse_matrix();
        sparse_matrix(int, int, int);
        int get_rows();
        int get_cols();
        double operator()(int, int);
        void add_element(int, int, double);
       	void set_element(int, int, double);
        int i_index(int) const;
        int j_index(int) const;
        void print();
        int linear_index(int, int) const;
		void mult_vec(dense_matrix *, dense_matrix *);
		void mult_scal(double);
		void set_column(int, dense_matrix *);
		dense_matrix get_column(int);
		void extract(int, int, int, int, sparse_matrix *);
		void clear();
};

#endif // SPARSE_MATRIX_HPP_INCLUDED
