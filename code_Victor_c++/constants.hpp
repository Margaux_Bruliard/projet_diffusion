#ifndef CONSTANTS_HPP_INCLUDED
#define CONSTANTS_HPP_INCLUDED

const double e_charge = 1.60217662e-19;	//electron charge
const double e_permittivity = 1e11;	//dielectric constant
const double neutral_d = 2.5e19;	//neutral gas density

//1e-4*e/eps = 1.8096e-8

#endif // CONSTANTS_HPP_INCLUDED
