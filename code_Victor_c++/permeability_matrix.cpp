#include <iostream>
#include "permeability_matrix.hpp"
#include <fstream>

using namespace std;

permeability_matrix::permeability_matrix() : dense_matrix() {

}
permeability_matrix::permeability_matrix(double _x_min, double _x_max, double _y_min, double _y_max, int _n_x, int _n_y, mesh *mesh_c) : dense_matrix(2*_n_x, 2*_n_y) {
	cout << _n_x << " " << _n_y << endl;
	this->dx = (_x_max - _x_min)/(_n_x+1);
	this->dy = (_y_max - _y_min)/(_n_y+1);
	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;
	this->n_x = _n_x;
	this->n_y = _n_y;
	this->x_coord = mesh_c->x;
	this->y_coord = mesh_c->y;
	//this->n_x = x_coord.get_rows();	//inverted x and y
	//this->n_y = y_coord.get_rows();	//inverted x and y
	for(int i=0; i<_n_x-1; i++) {
		if(this->dx > this->x_coord(i+1, 0) - this->x_coord(i, 0)) {
			this->dx = this->x_coord(i+1, 0) - this->x_coord(i, 0);	//find min
		}
	}
	for(int i=0; i<_n_y-1; i++) {
		if(this->dy > this->y_coord(i+1, 0) - this->y_coord(i, 0)) {
			this->dy = this->y_coord(i+1, 0) - this->y_coord(i, 0);	//find min
		}
	}

	if(_n_x <= 1 || _n_y <= 1) {
		cout << "Permeability matrix needs at least 2 rows and 2 cols !" << endl;
	}
}

void permeability_matrix::set_with_domains(char *name) {
	ifstream file(name);
	int n = 0;
	if(file.is_open()) {
		string param_name;
		file >> param_name >> n;	//number of domains without the default one
		if(n > 0) {
			this->domain = vector<permeability_domain>(n);
			double domain_x_min;
			double domain_x_max;
			double domain_y_min;
			double domain_y_max;
			double domain_v1;
			double domain_v2;
			double domain_v3;
			double domain_v4;
			//create all domains
			file >> param_name >> domain_v1 >> param_name >> domain_v2 >> param_name >> domain_v3 >> param_name >> domain_v4;	//default values
			this->domain[0] = permeability_domain(this->x_min, this->x_max, this->y_min, this->y_max, domain_v1, domain_v2, domain_v3, domain_v4);	//default domain
			for(int i=1; i<n; i++) {
				file >> param_name >> domain_x_min >> param_name >> domain_x_max >> param_name >> domain_y_min >> param_name >> domain_y_max >> param_name >> domain_v1 >> param_name >> domain_v2 >> param_name >> domain_v3 >> param_name >> domain_v4;	//domain and values
				this->domain[i] = permeability_domain(domain_x_min, domain_x_max, domain_y_min, domain_y_max, domain_v1, domain_v2, domain_v3, domain_v4);
			}
		} else {
			cout << "At least one default domain is required !!!" << endl;
			return;
		}
		file.close();
		for(int i=0; i<this->n_y; i++) {
			for(int j=0; j<this->n_x; j++) {
				//double x = this->dx*(double)j + this->x_min + this->dx;
				//double y = this->dy*(double)i + this->y_min + this->dy;
				double x = this->x_coord(i, 0);	//inverted x and y
				double y = this->y_coord((this->n_x-1) - j, 0);	//inverted x and y
				for(int k=0; k<n; k++) {
					if(this->domain[k].is_in(x, y)) {
						(*this)(2*j, 2*i) = this->domain[k].v1;
						(*this)(2*j, 2*i+1) = this->domain[k].v2;
						(*this)(2*j+1, 2*i) = this->domain[k].v3;
						(*this)(2*j+1, 2*i+1) = this->domain[k].v4;
					}
				}
			}
		}
	} else {
		cout << "Could not open domain file !" << endl;
	}
}

double permeability_matrix::get_K(int k1, int k2, int i) {
	if(k1 >= this->rows*this->cols/4 || k2 >= this->rows*this->cols/4) {
		cout << this->rows << " " << this->cols << " " << "ERROR PERMEABILITY !!!" << endl;
	}
	if(k1 < 0 || k2 < 0) {
		cout << "ERROR PERMEABILITY !!!!!!!!!" << endl;
	}
	int a=0, b=0, c=0, d=0;
	int i1=this->flip_coordinates(this->i_index(2*k1)*2); //m_rows are 2* the number of rows of 2by2 matrix
	int j1=this->j_index(2*k1);
	int i2=this->flip_coordinates(this->i_index(2*k2)*2);
	int j2=this->j_index(2*k2);
	//cout << "i1:" << i1 << "\tflip row i1:" << flip_coordinates(i1) << endl;
	//cout << "i2:" << i2 << "\tflip row i2:" << flip_coordinates(i2) << endl;
	//cout << "k1:" << k1 << "\ti1:" << i1 << "\tj1:" << j1 << endl;
	//cout << "k2:" << k2 << "\ti2:" << i2 << "\tj2:" << j2 << endl;
	if(k1-k2 == 1 || k1-k2 == -1) { //can only be asking for first line : K(1, 1) or K(1, 2)
		if(i==1) {  //return K(1, 1)
			a=i1;
			b=j1;
			c=i2;
			d=j2;
		} else if(i==2) {  //return K(1, 2)
			a=i1;
			b=j1+1;
			c=i2;
			d=j2+1;
		}
	} else if(k1-k2 == this->cols/2 || k1-k2 == -this->cols/2) { //can only be asking for second line : K(2, 1) or K(2, 2)
		if(i==1) {  //return K(2, 1)
			a=i1+1;
			b=j1;
			c=i2+1;
			d=j2;
		} else if(i==2) {  //return K(2, 2)
			a=i1+1;
			b=j1+1;
			c=i2+1;
			d=j2+1;
		}
	} else {
		cout << endl << "Out of range in permeability matrix" << endl;
	}
	return 0.5*(this->m[this->linear_index(a, b)]+this->m[this->linear_index(c, d)]);
}

double permeability_matrix::get_K_boundary(int k1, int i) {
	int a=0, b=0;
	int i1=this->flip_coordinates(this->i_index(2*k1)*2); //m_rows are 2* the number of rows of 2by2 matrix
	int j1=this->j_index(2*k1);
	//cout << "i1:" << i1 << "\tflip row i1:" << flip_coordinates(i1) << endl;
	//cout << "k1:" << k1 << "\ti1:" << i1 << "\tj1:" << j1 << endl;
	if(i==1) {  //return K(1, 1)
		a=i1;
		b=j1;
	} else if(i==2) {  //return K(1, 2)
		a=i1;
		b=j1+1;
	} else if(i==3) {  //return K(2, 1)
		a=i1+1;
		b=j1;
	} else if(i==4) {  //return K(2, 2)
		a=i1+1;
		b=j1+1;
	} else {
		cout << endl << "Out of range in permeability matrix" << endl;
	}
	return this->m[this->linear_index(a, b)];
}

int permeability_matrix::flip_coordinates(int i) {
	return this->rows - i - 2;
}
