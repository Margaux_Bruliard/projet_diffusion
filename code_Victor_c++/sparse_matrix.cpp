#include "sparse_matrix.hpp"
#include <iostream>
#include <fstream>


sparse_matrix::sparse_matrix() {
}

sparse_matrix::sparse_matrix(int _rows, int _cols, int _size) {
	this->cols = _cols;
	this->rows = _rows;
	if(_size < 1) {
		this->cols_index = vector<int>();
		//this->rows_first_position = vector<int>();
		this->rows_first_position = vector<int>(_rows+1);
		this->values = vector<double>();
	} else {
		this->cols_index = vector<int>(_size);
		this->rows_first_position = vector<int>(_rows+1);
		this->values = vector<double>(_size);
	}
	this->nnz = 0;
}

int sparse_matrix::linear_index(int _i, int _j) const {
	return _i*this->cols + _j;
}

int sparse_matrix::i_index(int _k) const {
	return int(_k/this->cols);  //int division
}

int sparse_matrix::j_index(int _k) const {
	return _k%this->cols;
}

int sparse_matrix::get_rows() {
	return this->rows;
}

int sparse_matrix::get_cols() {
	return this->cols;
}

double sparse_matrix::operator()(int _i, int _j) {
	for(int k=this->rows_first_position[_i]; k<this->rows_first_position[_i+1]; k++) {
		if(cols_index[k] == _j) {
			return this->values[k];
		}
	}
	return 0.0;
}

void sparse_matrix::add_element(int _i, int _j, double _v) {
	if(_v != 0) {
		for(int k=this->rows_first_position[_i]; k<this->rows_first_position[_i+1]; k++) {  //already an element in this line
			if(this->cols_index[k] < _j) {
				if(k+1 == this->rows_first_position[_i+1]) {  //no element in column >= _j at line _i
					int tmp = k+1;
					this->values.insert(this->values.begin()+tmp, _v);
					this->cols_index.insert(this->cols_index.begin()+tmp, _j);
					this->nnz++;
					for(int m=_i+1; m<this->rows+1; m++) {
						this->rows_first_position[m]++;
					}
					return;
				}
			} else if(this->cols_index[k] == _j) {   //there is already one, just add
				this->values[k] += _v;  //add the value to the existing one
				return;
			} else if(this->cols_index[k] > _j) { //no element before column _j at line _i
				this->cols_index.insert(this->cols_index.begin()+k, _j);
				this->values.insert(this->values.begin()+k, _v);
				this->nnz++;
				for(int l=_i+1; l<this->rows+1; l++) {
					this->rows_first_position[l]++;
				}
				return;
			}
		}
		//no element in this line
		if(_i > 0) {
			int k = this->rows_first_position[_i];
			this->rows_first_position[_i+1] = k + 1;
			for(int l=_i+2; l<this->rows+1; l++) {
				this->rows_first_position[l]++;
			}
			this->cols_index.insert(this->cols_index.begin()+k, _j);
			this->values.insert(this->values.begin()+k, _v);
			this->nnz++;
		} else {
			this->rows_first_position[0] = 0;
			for(int l=1; l<this->rows+1; l++) {
				this->rows_first_position[l]++;
			}
			this->cols_index.insert(this->cols_index.begin(), _j);
			this->values.insert(this->values.begin(), _v);
			this->nnz++;
		}
	}
}

void sparse_matrix::set_element(int _i, int _j, double _v) {
	//if(_v != 0) {	//should be optimized !!! if _v == 0 we have to delete the element, remove the lines/columns pointers...
		for(int k=rows_first_position[_i]; k<rows_first_position[_i+1]; k++) {  //already an element in this line
			if(cols_index[k] < _j) {
				if(k+1 == rows_first_position[_i+1]) {  //no element in column >= _j at line _i
					int tmp = k+1;
					this->values.insert(this->values.begin()+tmp, _v);
					this->cols_index.insert(this->cols_index.begin()+tmp, _j);
					this->nnz++;
					for(int m=_i+1; m<this->rows+1; m++) {
						this->rows_first_position[m]++;
					}
					return;
				}
			} else if(cols_index[k] == _j) {   //there is already one, just replace
				this->values[k] = _v;  //replace the value of the existing one
				return;
			} else if(cols_index[k] > _j) { //no element before column _j at line _i
				this->cols_index.insert(this->cols_index.begin()+k, _j);
				this->values.insert(this->values.begin()+k, _v);
				this->nnz++;
				for(int l=_i+1; l<this->rows+1; l++) {
					this->rows_first_position[l]++;
				}
				return;
			}
		}
		//no element in this line
		if(_i > 0) {
			int k = this->rows_first_position[_i];
			this->rows_first_position[_i+1] = k + 1;
			for(int l=_i+2; l<this->rows+1; l++) {
				this->rows_first_position[l]++;
			}
			this->cols_index.insert(this->cols_index.begin()+k, _j);
			this->values.insert(this->values.begin()+k, _v);
			this->nnz++;
		} else {
			this->rows_first_position[0] = 0;
			for(int l=1; l<this->rows+1; l++) {
				this->rows_first_position[l]++;
			}
			this->cols_index.insert(this->cols_index.begin(), _j);
			this->values.insert(this->values.begin(), _v);
			this->nnz++;
		}
	//}
}

void sparse_matrix::print() {
	/*for(int k=0; k<this->nnz; k++) {
		cout << "(" << this->rows_index[k] << "," << this->cols_index[k] << ")=" << this->values[k] << endl;
	}*/
	cout << endl;
	cout << "cols_index length :" << cols_index.size() << endl;
	cout << "rows_first_position length :" << rows_first_position.size() << endl;
	cout << "values length :" << values.size() << endl;
	cout << "[";
	for(unsigned int i = 0; i< this->cols_index.size(); i++) {
		cout << this->cols_index[i] << "; " ;
	}
	cout << "]" << endl;
	cout << "[";
	for(unsigned int i = 0; i< this->rows_first_position.size(); i++) {
		cout << this->rows_first_position[i] << "; " ;
	}
	cout << "]" << endl;
	cout << "[";
	for(unsigned int i = 0; i< this->values.size(); i++) {
		cout << this->values[i] << "; " ;
	}
	cout << "]" << endl << endl;
	int j = 0;
	cout << "rows: " << this->rows << "\tcols: " << this->cols << "\tnnz: " << this->nnz << endl;
	for(int i=0; i<this->rows; i++) {
		for(int k=this->rows_first_position[i]; k<this->rows_first_position[i+1]; k++) {  //already an element in this line
			j++;
			cout << "A(" << (i+1) << "," << (this->cols_index[k]+1) << ")=" << this->values[k] << ";" << endl;
			if(j>this->nnz) {
				return;
			}
		}
	}
}

void sparse_matrix::mult_vec(dense_matrix *b, dense_matrix *result) {
	int i=0, j=0, k=0;	//i for line, j for column index
	double tmp = 0.0;
	for(k=0; k<this->values.size(); k++) {	//go through each nnz
		if(k >= this->rows_first_position[i+1]) {	//increment line if necessary
			(*result)(i, 0) = tmp;
			tmp = 0.0;
			i++;
		}
		j=this->cols_index[k];	//get the column
		tmp += this->values[k]*(*b)(j, 0);	//add the product of A(i,j)*b(j)
		//cout << "k :" << k << " i :" << i << " j :" << j << " tmp :" << tmp << " v[k] :" << this->values[k] << " b(j) :" << (*b)(j, 0) << endl;
	}
	if(k >= this->rows_first_position[i+1]) {	//increment line if necessary
		(*result)(i, 0) = tmp;
	}
}

void sparse_matrix::mult_scal(double c) {
	for(int k=0; k<this->values.size(); k++) {	//go through each nnz
		this->values[k] *= c;
	}
}

void sparse_matrix::set_column(int col, dense_matrix *v) {
	for(int i=0; i<this->rows; i++) {
		this->set_element(i, col, (*v)(i, 0));
	}
}

dense_matrix sparse_matrix::get_column(int col) {
	dense_matrix v(this->rows, 1);
	for(int i=0; i<this->rows; i++) {
		v(i, 0) = (*this)(i, col);
	}
	return v;
}

void sparse_matrix::extract(int first_row, int last_row, int first_col, int last_col, sparse_matrix *M) {
	for(int i=first_row; i<=last_row; i++) {
		for(int j=first_col; j<=last_col; j++) {
			M->set_element(i-first_row, j-first_col, (*this)(i, j));
		}
	}
}

void sparse_matrix::clear() {
	//this->cols = _cols;
	//this->rows = _rows;
	this->cols_index = vector<int>();
	//this->rows_first_position = vector<int>();
	this->rows_first_position = vector<int>(this->rows+1);
	this->values = vector<double>();
	this->nnz = 0;
}
