#include <iostream>
#include "dense_matrix.hpp"
#include "darcy.hpp"
#include "darcy_double.hpp"
#include "permeability_matrix.hpp"
#include "functions_type.hpp"
#include "solver.hpp"
#include "solver_double.hpp"
#include "sparse_matrix.hpp"
#include "sparse_matrix_double.hpp"
#include "transport.hpp"
#include "starter.hpp"
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include "mesh.hpp"

//#include <Eigen/Dense>
//#include <Eigen/Sparse>
//using namespace Eigen;

using namespace std;
double dirichlet_darcy(double, double, int);
double function_f(double, double);
double function_f2(double, double);
double neumann_condition_u(double, double, int, bool);
double neumann_condition_C(double, double, int, bool);
double limiter(double, double, string);

int main(int argc, char *argv[])
{
	/*******************/
	//init of darcy's equation
	/*******************/
	//domain
	if(argc != 5) {
		cout << "Please execute with, and only with, a starter file, a domain_K file, a domain_D file and a mesh file: \"./main starter.txt domain_K.txt domain_D.txt mesh_1.txt\"" << endl;
		return 0;
	}
	starter start(argv[1]);

	//mesh

	mesh mesh_c(argv[4]);

	start.n_x = mesh_c.x.get_rows();
	start.n_y = mesh_c.y.get_rows();

	//matrices K
	cout << "set K..." << endl;
	permeability_matrix K(start.x_min, start.x_max, start.y_min, start.y_max, start.n_y, start.n_x, &mesh_c);	//inverted n_x n_y in permeability_domain_save
	/*//set to identity
	double delta_x = (start.x_max - start.x_min)/(start.n_x+1);
	double delta_y = (start.y_max - start.y_min)/(start.n_y+1);
	for(int i=0; i<start.n_y; i++) {
		for(int j=0; j<start.n_x; j++) {
			//int k = i*start.n_x+j;
			K(2*i, 2*j) = 1.0;
			K(2*i, 2*j+1) = 0.0;
			K(2*i+1, 2*j) = 0.0;
			K(2*i+1, 2*j+1) = 1.0;
		}
	}*/
	K.set_with_domains(argv[2]);
	cout << "set darcy..." << endl;

	//init darcy
	clock_t a_clock = clock();
	darcy d(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, &mesh_c, K, dirichlet_darcy, function_f, start.streamer);
	clock_t b_clock = clock();
	double a_darcy = (b_clock - a_clock)/(double)(CLOCKS_PER_SEC/1000);
	cout << "init darcy: " << a_darcy << "ms" << endl;

	darcy_double d_double;
	if(start.solver_darcy == "LU_double") {
		d_double = darcy_double(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, K, dirichlet_darcy, function_f);
	}
	//init preconditioner
	//cout << "set preconditioner for GMRES..." << endl;
	sparse_matrix M(d.A.get_rows(), d.A.get_cols(), 0);
	/*for(int i=0; i<M.get_rows(); i++) {	//Jacobi's preconditioner ???
		M.add_element(i, i, d.A(i,i));
		//M.add_element(i, i, 1.0);
	}*/

	cout << "solve darcy:" << endl;
	
	//solving darcy
	/*********************************************************/
	//every *_double is using the sparse matrix with the double system : ordered by rows and by cols
	/*********************************************************/
	solver s;
	solver_double s_double;

	clock_t start_clock = clock();
	if(start.darcy_device == "CPU") {
		if(start.solver_darcy == "LU") {
			s.solve_darcy_LU(&d);
			//save to files
			d.u.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, "result_darcy_LU");
		} else if(start.solver_darcy == "GMRES") {
			s.solve_darcy_GMRES(&d, &M, start.n_before_restart, start.max_it, start.tol);
			//save to files
			d.u.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, "result_darcy_GMRES");
		} else if(start.solver_darcy == "GMRES_without_inver") {
			s.solve_darcy_GMRES_without_inver(&d, start.gmres_without_inv_max_it, start.gmres_without_inv_tol, start.talkative);
			//save to files
			d.u.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, "result_darcy_GMRES_without_inver");
		} else if(start.solver_darcy == "LU_double") {
			s_double.solve_darcy_LU(&d_double);
			//save to files
			d_double.u.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, "result_darcy_LU_double");
			d.u = d_double.u;
		} else if(start.solver_darcy == "LAPACK_chol") {
			cout << "LAPACK is not available on CPU to solve darcy" << endl;
			return 0;
		}
	} else if(start.darcy_device == "GPU") {
		cout << "GPU not available" << endl;
	}
	if(start.streamer) {
		d.u = dense_matrix(start.n_x*start.n_y, 1);
	}
	clock_t end_clock = clock();

	//compare to exact solution
	
	dense_matrix exact_sol(start.n_x*start.n_y, 1);
	for(int k=0; k<start.n_x*start.n_y; k++) {
		//int i = d.mesh.i_index(k);
		//int j = d.mesh.j_index(k);
		int j = k%start.n_x;
		int i = int(k/start.n_x);  //int division
		double x = d.get_delta_x()*(double)j + d.x_min + d.get_delta_x();
		double y = d.get_delta_y()*(double)i + d.y_min + d.get_delta_y();
		x = mesh_c.x(j, 0);
		y = mesh_c.y(i, 0);
		exact_sol(k, 0) = dirichlet_darcy(x, y, 0);
	}
	//exact_sol.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, "exact_sol");
	exact_sol.save_reshape_non_uniform(&(mesh_c.x), &(mesh_c.y), "exact_sol");
	//output error
	cout << "error using " << start.solver_darcy << " on " << start.darcy_device << ": " << d.error_L2_square(&exact_sol) << endl;
	

	//compare times
	double time_darcy = (end_clock - start_clock)/(double)(CLOCKS_PER_SEC/1000);
	cout << "Time using " << start.solver_darcy << " on " << start.darcy_device << ": " << time_darcy << "ms" << endl;
	

	/*******************/
	//init of transport's equation
	//matrices D
	cout << "set D..." << endl;
	permeability_matrix D(start.x_min, start.x_max, start.y_min, start.y_max, start.n_y, start.n_x, &mesh_c);	//inverted n_x n_y in permeability_domain_save
	/*//set to identity
	for(int i=0; i<start.n_y; i++) {
		for(int j=0; j<start.n_x; j++) {
			//int k = i*start.n_x+j;
			D(2*i, 2*j) = 0.0;
			D(2*i, 2*j+1) = 0.0;
			D(2*i+1, 2*j) = 0.0;
			D(2*i+1, 2*j+1) = 0.0;
		}
	}*/
	D.set_with_domains(argv[3]);

	transport trans(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, &mesh_c, neumann_condition_u, start.cfl, D, function_f2, neumann_condition_C);
	transport trans_exact_sol(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, &mesh_c, neumann_condition_u, start.cfl, D, function_f2, neumann_condition_C);

	cout << "set concentration..." << endl;
	if(start.concentration_type == "gaussian") {
		//trans.set_centered_gaussian(start.x_pos_gaussian, start.y_pos_gaussian, start.x_var_gaussian, start.y_var_gaussian, 1.0);
		trans.set_centered_gaussian(start.x_pos_gaussian, start.y_pos_gaussian, sqrt(0.5)*sqrt(start.x_var_gaussian*start.x_var_gaussian), sqrt(0.5)*sqrt(start.y_var_gaussian*start.y_var_gaussian), (start.x_var_gaussian / sqrt(start.x_var_gaussian*start.x_var_gaussian))* (start.x_var_gaussian / sqrt(start.x_var_gaussian*start.x_var_gaussian)));	//"exact solution" doesn't take boundary conditions...
		
	} else if(start.concentration_type == "wave") {
		trans.set_step_wave(start.x_min_wave, start.x_max_wave, start.y_min_wave, start.y_max_wave, start.low_value_wave, start.high_value_wave);
	}
	cout << "solve transport:" << endl;
	
	if(start.output == "screen") {
		cout << "Screen output not available on CPU" << endl;
	} else if(start.output == "file") {
		start_clock = clock();
		//If we don't do streamer
		if(!start.streamer) {
			if(start.transport_integration == "forward") {
				//if(start.transport_mode == "simple") {
					if(start.transport_device == "CPU") {
						trans.compute_iterativ(start.end_time, &d, &start, limiter);
					} else if(start.transport_device == "GPU") {
						cout << "GPU not available" << endl;
					}
			} else if(start.transport_integration == "backward") {
				if(start.transport_device == "CPU") {
					if(start.transport_mode == "simple") {
						//trans.compute_iterativ_backward(start.end_time, &d, &start);
						if(start.ordre_transport_space == 1) {
							trans.compute_iterativ_backward(start.end_time, &d, &start);
						} else if(start.ordre_transport_space == 2) {
							trans.compute_iterativ_backward_MUSCL_limiter(start.end_time, &d, &start, limiter);
						}
					} else if(start.transport_mode == "coupled") {
						//trans.compute_iterativ_backward_coupled(start.end_time, &d, &start, &M, &d_double);
						if(start.ordre_transport_space == 1) {
							trans.compute_iterativ_backward_coupled(start.end_time, &d, &start, &M, &d_double);
						} else if(start.ordre_transport_space == 2) {
							trans.compute_iterativ_backward_MUSCL_limiter_coupled(start.end_time, &d, &start, limiter, &M, &d_double);
						}
					}
				} else if(start.transport_device == "GPU") {
					cout << "GPU not available" << endl;
				}
			}
			trans.concentration.save_reshape_non_uniform(&(mesh_c.x), &(mesh_c.y), "concentration_final");
		} else if(start.streamer) {
			cout << "No streamer available on CPU" << endl;
		}
		end_clock = clock();
		cout << "Time transport ordre " << start.ordre_transport_space << " " << start.transport_integration << " integration on " << start.transport_device << ": " << (end_clock - start_clock)/(double)(CLOCKS_PER_SEC/1000) << "ms" << endl;

		
		/*
		char name_ext[120];
		char tmp_string[12];
		strcpy(name_ext, "model_");
		sprintf(tmp_string, "%d", start.n_x);
		strcat(name_ext, tmp_string);
		strcat(name_ext, "_");
		sprintf(tmp_string, "%s", start.transport_integration.c_str());
		strcat(name_ext, tmp_string);
		
		if(start.transport_integration == "backward") {
			strcat(name_ext, "_");
			sprintf(tmp_string, "%lf", start.cfl);
			for(int i=0; i<12; i++) {
				if(tmp_string[i] == '.') {
					tmp_string[i] = '_';
				}
			}
			strcat(name_ext, tmp_string);
		}
		strcat(name_ext, "_");
		sprintf(tmp_string, "%d", start.ordre_transport_space);
		strcat(name_ext, tmp_string);
		if(start.ordre_transport_space == 2) {
			strcat(name_ext, "_");
			sprintf(tmp_string, "%s", start.limiter_name.c_str());
			strcat(name_ext, tmp_string);
		}
		trans.concentration.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, name_ext);*/
	}
	return 1;
}

double dirichlet_darcy(double x, double y, int position) {   //also exact_sol if the function_f is set right
	/*position :
	5|...6...|7
	_		  _
	.		  .
	3		  4
	.		  .
	_		  _
	0|...1...|2
	*/
	double borcond = -0.5*(-x+1-y+1);
	//borcond = 0.5*(y+1);
	//borcond = (-25000*x+1);
	borcond = (+x+1);
	//borcond = (25000*x);
	borcond = 25000+(-25000*x);
	//borcond = 50000+(-49663.106*x);
	//borcond = 25000+(-25000*y);
	//borcond = 0.0;
	borcond = -0.5*(y+1);
	return borcond;
	if(position < 4) {
		if(position < 2) {
			if(position == 0) {	//bottom left corner
				return -1;
			} else {	//bottom side
				return -1;
			}
		} else {
			if(position == 2) {	//bottom right corner
				return -1;
			} else {	//left side
				return 0;
			}

		}
	} else {
		if(position < 6) {
			if(position == 4) {	//right side
				return 0;
			} else {	//top left corner
				return -1;
			}
		} else {
			if(position == 6) {	//top side
				return -1;
			} else {	//top right corner
				return -1;
			}

		}
	}
	return 0;
	//return	log(x+2);
	//return log((-x+2)/(-y+2));
	//return sin(3*x)*cos(4*y);
	//return (+x+y);
	//return (y + 1.0);
}

double function_f(double x, double y) {
	//return (1/(x+2))*(1/(x+2));	
	//return (1/(x+2))*(1/(x+2))-(1/(y+2))*(1/(y+2));
	//return 9*sin(3*x)*cos(4*y)+16*sin(3*x)*cos(4*y);
	return 0.0;
	//return 0.0;
}

double function_f2(double x, double y) {
	//return 0.01*exp(-(((x-0.0)*(x-0.0)/(2*0.05*0.05))+((y-0.0)*(y-0.0)/(2*0.05*0.05))));
	return 0.0;
}

double neumann_condition_u(double x, double y, int position, bool grad_x) {
	/*position :
		 3
	.|.......|.
	_		  _
	.		  .
   1.		  .2
	.		  .
	_		  _
	.|.......|.
		 0
	*/
	/*grad :
	true = dx
	false = dy*/
	//return 0.0;
	if(position < 2) {
		if(position == 0) {	//bottom side
			if(grad_x) {	//dx
				return 0;
			} else {	//dy
				return 0.5;
			}
		} else {	//left side
			if(grad_x) {	//dx
				return 0.5;
			} else {	//dy
				return 0;
			}
		}
	} else {
		if(position == 2) {	//right side
			if(grad_x) {	//dx
				return -0.5;
			} else {	//dy
				return 0;
			}
		} else {	//top side
			if(grad_x) {	//dx
				return 0;
			} else {	//dy
				return -0.5;
			}
		}
	}
	return 0.0;
}

double neumann_condition_C(double x, double y, int position, bool grad_x) {
	/*position :
		 3
	.|.......|.
	_		  _
	.		  .
   1.		  .2
	.		  .
	_		  _
	.|.......|.
		 0
	*/
	/*grad :
	true = dx
	false = dy*/
	//return 0.0;
	if(position < 2) {
		if(position == 0) {	//bottom side
			if(grad_x) {	//dx
				return 0;
			} else {	//dy
				return 0;
			}
		} else {	//left side
			if(grad_x) {	//dx
				return 0;
			} else {	//dy
				return 0;
			}
		}
	} else {
		if(position == 2) {	//right side
			if(grad_x) {	//dx
				return 0;
			} else {	//dy
				return 0;
			}
		} else {	//top side
			if(grad_x) {	//dx
				return 0;
			} else {	//dy
				return 0;
			}
		}
	}
	return 0.0;
}

double limiter(double r_numerator, double r_denominator, string name) {
	if(r_numerator < 0.000000001 && -r_numerator < 0.000000001) return 0;
	if(r_denominator < 0.000000001 && -r_denominator < 0.000000001) {	//limits r -> infinity
		if(name == "vanleer") {
			return 2.0;	//MUSCL van leer
		}
		if(name == "superbee") {
			return 2.0;	//SUPERBEE
		}
		if(name == "minmod") {
			return 1.0;	//MINMOD
		}
		if(name == "vanalbada") {
			return 1.0;	//van albada
		}
		if(name == "barthjespersen") {
			return 2.0;	//barth-jespersen
		}
	}
	double r = r_numerator/r_denominator;
	if(r<=0) {
		return 0.0;
	}
	if(name == "vanleer") {
		return (r + fabs(r))/(1.0 + fabs(r));	//MUSCL van leer
	}
	if(name == "superbee") {
		return max(max(0.0, min(1.0,2.0*r)), min(2.0, r));	//SUPERBEE
	}
	if(name == "minmod") {
		return max(0.0, min(1.0,r));	//MINMOD
	}
	if(name == "vanalbada") {
		return (r*r + r)/(r*r + 1);	//MINMOD
	}
	if(name == "barthjespersen") {
		return 0.5*(r+1)*min(min(1.0, 4*r/(r+1)), min(1.0, 4/(r+1)));	//MINMOD
	}
	cout << "Check limiter name" << endl;
	return 2;
	return 1;
	return 0;
}
