#include "sparse_matrix_double.hpp"
#include <iostream>
#include <fstream>
sparse_matrix_double::sparse_matrix_double() {
}

sparse_matrix_double::sparse_matrix_double(int _rows, int _cols, int _size) {
    this->cols = _cols;
    this->rows = _rows;
    if(_size < 1) {
        this->cols_index_line_by_line = vector<int>();
        //this->rows_first_position_line_by_line = vector<int>();
        this->rows_first_position_line_by_line = vector<int>(_rows+1);
        this->values_line_by_line = vector<double>();
    } else {
        this->cols_index_line_by_line = vector<int>(_size);
        this->rows_first_position_line_by_line = vector<int>(_rows+1);
        this->values_line_by_line = vector<double>(_size);
    }
    if(_size < 1) {
        this->rows_index_column_by_column = vector<int>();
        //this->cols_first_position_column_by_column = vector<int>();
        this->cols_first_position_column_by_column= vector<int>(_cols+1);
        this->values_column_by_column = vector<double>();
    } else {
        this->rows_index_column_by_column = vector<int>(_size);
        this->cols_first_position_column_by_column = vector<int>(_cols+1);
        this->values_column_by_column = vector<double>(_size);
    }
    this->nnz_line_by_line = 0;
    this->nnz_column_by_column = 0;
}

int sparse_matrix_double::linear_index(int _i, int _j) const {
    return _i*this->cols + _j;
}

int sparse_matrix_double::i_index(int _k) const {
    return int(_k/this->cols);  //int division
}

int sparse_matrix_double::j_index(int _k) const {
    return _k%this->cols;
}

int sparse_matrix_double::get_rows() {
    return this->rows;
}

int sparse_matrix_double::get_cols() {
    return this->cols;
}

double sparse_matrix_double::operator()(int _i, int _j) {
    /*int index_first_i = find_row(_i);
    if(index_first_i != -1) {
        for(int k=index_first_i; k<this->nnz_line_by_line; k++) {
            if(rows_index[k] == _i && cols_index_line_by_line[k] == _j) {
                //cout << "found @ " << k << endl;
                return this->values_line_by_line[k];
                break;
            } else if(rows_index[k] > _i) {
                return 0.0;
                break;
            }
        }
    }
    return 0.0;*/
    for(int k=rows_first_position_line_by_line[_i]; k<rows_first_position_line_by_line[_i+1]; k++) {
        if(cols_index_line_by_line[k] == _j) {
            return this->values_line_by_line[k];
        }
    }
    return 0.0;
}

void sparse_matrix_double::add_element(int _i, int _j, double _v) {
	this->add_element_line_by_line(_i, _j, _v);
	this->add_element_column_by_column(_i, _j, _v);
}

void sparse_matrix_double::add_element_line_by_line(int _i, int _j, double _v) {
	//ordered line by line
    if(_v != 0) {
        for(int k=rows_first_position_line_by_line[_i]; k<rows_first_position_line_by_line[_i+1]; k++) {  //already an element in this line
            if(cols_index_line_by_line[k] < _j) {
                if(k+1 == rows_first_position_line_by_line[_i+1]) {  //no element in column >= _j at line _i
                    int tmp = k+1;
                    this->values_line_by_line.insert(this->values_line_by_line.begin()+tmp, _v);
                    this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin()+tmp, _j);
                    this->nnz_line_by_line++;
                    for(int m=_i+1; m<this->rows+1; m++) {
                        this->rows_first_position_line_by_line[m]++;
                    }
                    return;
                }
            } else if(cols_index_line_by_line[k] == _j) {   //there is already one, just add
                this->values_line_by_line[k] += _v;  //add the value to the existing one
                return;
            } else if(cols_index_line_by_line[k] > _j) { //no element before column _j at line _i
                this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin()+k, _j);
                this->values_line_by_line.insert(this->values_line_by_line.begin()+k, _v);
                this->nnz_line_by_line++;
                for(int l=_i+1; l<this->rows+1; l++) {
                    this->rows_first_position_line_by_line[l]++;
                }
                return;
            }
        }
        //no element in this line
        if(_i > 0) {
            int k = this->rows_first_position_line_by_line[_i];
            this->rows_first_position_line_by_line[_i+1] = k + 1;
            for(int l=_i+2; l<this->rows+1; l++) {
                this->rows_first_position_line_by_line[l]++;
            }
            this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin()+k, _j);
            this->values_line_by_line.insert(this->values_line_by_line.begin()+k, _v);
            this->nnz_line_by_line++;
        } else {
            this->rows_first_position_line_by_line[0] = 0;
            for(int l=1; l<this->rows+1; l++) {
                this->rows_first_position_line_by_line[l]++;
            }
            this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin(), _j);
            this->values_line_by_line.insert(this->values_line_by_line.begin(), _v);
            this->nnz_line_by_line++;
        }
    }
}


void sparse_matrix_double::add_element_column_by_column(int _i, int _j, double _v) {
	//ordered column by column
    if(_v != 0) {
        for(int k=cols_first_position_column_by_column[_j]; k<cols_first_position_column_by_column[_j+1]; k++) {  //already an element in this line
            if(rows_index_column_by_column[k] < _i) {
                if(k+1 == cols_first_position_column_by_column[_j+1]) {  //no element in row >= _i at column _j
                    int tmp = k+1;
                    this->values_column_by_column.insert(this->values_column_by_column.begin()+tmp, _v);
                    this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin()+tmp, _i);
                    this->nnz_column_by_column++;
                    for(int m=_j+1; m<this->cols+1; m++) {
                        this->cols_first_position_column_by_column[m]++;
                    }
                    return;
                }
            } else if(rows_index_column_by_column[k] == _i) {   //there is already one, just add
                this->values_column_by_column[k] += _v;  //add the value to the existing one
                return;
            } else if(rows_index_column_by_column[k] > _i) { //no element before line _i at column _j
                this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin()+k, _i);
                this->values_column_by_column.insert(this->values_column_by_column.begin()+k, _v);
                this->nnz_column_by_column++;
                for(int l=_j+1; l<this->cols+1; l++) {
                    this->cols_first_position_column_by_column[l]++;
                }
                return;
            }
        }
        //no element in this column
        if(_j > 0) {
            int k = this->cols_first_position_column_by_column[_j];
            this->cols_first_position_column_by_column[_j+1] = k + 1;
            for(int l=_j+2; l<this->cols+1; l++) {
                this->cols_first_position_column_by_column[l]++;
            }
            this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin()+k, _i);
            this->values_column_by_column.insert(this->values_column_by_column.begin()+k, _v);
            this->nnz_column_by_column++;
        } else {
            this->cols_first_position_column_by_column[0] = 0;
            for(int l=1; l<this->cols+1; l++) {
                this->cols_first_position_column_by_column[l]++;
            }
            this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin(), _i);
            this->values_column_by_column.insert(this->values_column_by_column.begin(), _v);
            this->nnz_column_by_column++;
        }
    }
}

void sparse_matrix_double::set_element(int _i, int _j, double _v) {
	this->set_element_line_by_line(_i, _j, _v);
	this->set_element_column_by_column(_i, _j, _v);
}

void sparse_matrix_double::set_element_line_by_line(int _i, int _j, double _v) {
    if(_v != 0) {
        for(int k=rows_first_position_line_by_line[_i]; k<rows_first_position_line_by_line[_i+1]; k++) {  //already an element in this line
            if(cols_index_line_by_line[k] < _j) {
                if(k+1 == rows_first_position_line_by_line[_i+1]) {  //no element in column >= _j at line _i
                    this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin()+k+1, _j);
                    for(int l=_i+1; l<this->cols+1; l++) {
                        this->rows_first_position_line_by_line[l]++;
                    }
                    this->values_line_by_line.insert(this->values_line_by_line.begin()+k+1, _v);
                    this->nnz_line_by_line++;
                }
            } else if(cols_index_line_by_line[k] == _j) {   //there is already one, just add
                this->values_line_by_line[k] = _v;  //replace the existing one
                return;
            } else if(cols_index_line_by_line[k] > _j) { //no element before column _j at line _i
                this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin()+k, _j);
                this->values_line_by_line.insert(this->values_line_by_line.begin()+k, _v);
                this->nnz_line_by_line++;
                for(int l=_i+1; l<this->nnz_line_by_line; l++) {
                    this->rows_first_position_line_by_line[l]++;
                }
                return;
            }
        }
        //no element in this line
        if(_i >= 0) {
            int k = this->rows_first_position_line_by_line[_i];
            this->rows_first_position_line_by_line[_i+1] = k + 1;
            for(int l=_i+2; l<this->rows+1; l++) {
                this->rows_first_position_line_by_line[l]++;
            }
            this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin()+k, _j);
            this->values_line_by_line.insert(this->values_line_by_line.begin()+k, _v);
            this->nnz_line_by_line++;
        } else {
            this->rows_first_position_line_by_line[0] = 0;
            this->rows_first_position_line_by_line[1] = 1;
            for(int l=1; l<this->rows+1; l++) {
                this->rows_first_position_line_by_line[l]++;
            }
            this->cols_index_line_by_line.insert(this->cols_index_line_by_line.begin(), _j);
            this->values_line_by_line.insert(this->values_line_by_line.begin(), _v);
            this->nnz_line_by_line++;
        }
    }
}

void sparse_matrix_double::set_element_column_by_column(int _i, int _j, double _v) {
    if(_v != 0) {
        for(int k=cols_first_position_column_by_column[_j]; k<cols_first_position_column_by_column[_j+1]; k++) {  //already an element in this column
            if(rows_index_column_by_column[k] < _i) {
                if(k+1 == cols_first_position_column_by_column[_j+1]) {  //no element in line >= _i at column _j
                    this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin()+k+1, _i);
                    for(int l=_j+1; l<this->rows+1; l++) {
                        this->cols_first_position_column_by_column[l]++;
                    }
                    this->values_column_by_column.insert(this->values_column_by_column.begin()+k+1, _v);
                    this->nnz_column_by_column++;
                }
            } else if(rows_index_column_by_column[k] == _i) {   //there is already one, just add
                this->values_column_by_column[k] = _v;  //replace the existing one
                return;
            } else if(rows_index_column_by_column[k] > _i) { //no element before line _i at column _j
                this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin()+k, _i);
                this->values_column_by_column.insert(this->values_column_by_column.begin()+k, _v);
                this->nnz_column_by_column++;
                for(int l=_j+1; l<this->nnz_column_by_column; l++) {
                    this->cols_first_position_column_by_column[l]++;
                }
                return;
            }
        }
        //no element in this column
        if(_j >= 0) {
            int k = this->cols_first_position_column_by_column[_j];
            this->cols_first_position_column_by_column[_j+1] = k + 1;
            for(int l=_j+2; l<this->cols+1; l++) {
                this->cols_first_position_column_by_column[l]++;
            }
            this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin()+k, _i);
            this->values_column_by_column.insert(this->values_column_by_column.begin()+k, _v);
            this->nnz_column_by_column++;
        } else {
            this->cols_first_position_column_by_column[0] = 0;
            this->cols_first_position_column_by_column[1] = 1;
            for(int l=1; l<this->cols+1; l++) {
                this->cols_first_position_column_by_column[l]++;
            }
            this->rows_index_column_by_column.insert(this->rows_index_column_by_column.begin(), _i);
            this->values_column_by_column.insert(this->values_column_by_column.begin(), _v);
            this->nnz_column_by_column++;
        }
    }
}

void sparse_matrix_double::print_line_by_line() {
    /*for(int k=0; k<this->nnz_line_by_line; k++) {
        cout << "(" << this->rows_index[k] << "," << this->cols_index_line_by_line[k] << ")=" << this->values_line_by_line[k] << endl;
    }*/
    cout << endl;
    cout << "[";
    for(unsigned int i = 0; i< this->cols_index_line_by_line.size(); i++) {
        cout << this->cols_index_line_by_line[i] << "; " ;
    }
    cout << "]" << endl;
    cout << "[";
    for(unsigned int i = 0; i< this->rows_first_position_line_by_line.size(); i++) {
        cout << this->rows_first_position_line_by_line[i] << "; " ;
    }
    cout << "]" << endl;
    cout << "[";
    for(unsigned int i = 0; i< this->values_line_by_line.size(); i++) {
        cout << this->values_line_by_line[i] << "; " ;
    }
    cout << "]" << endl << endl;
    int j = 0;
    for(int i=0; i<this->rows; i++) {
        for(int k=this->rows_first_position_line_by_line[i]; k<this->rows_first_position_line_by_line[i+1]; k++) {  //already an element in this line
            j++;
            cout << "(" << i << "," << this->cols_index_line_by_line[k] << ")=" << this->values_line_by_line[k] << endl;
            if(j>this->nnz_line_by_line) {
                return;
            }
        }
    }
}

void sparse_matrix_double::print_column_by_column() {
    /*for(int k=0; k<this->nnz_line_by_line; k++) {
        cout << "(" << this->rows_index[k] << "," << this->cols_index_line_by_line[k] << ")=" << this->values_line_by_line[k] << endl;
    }*/
    cout << endl;
    cout << "[";
    for(unsigned int i = 0; i< this->rows_index_column_by_column.size(); i++) {
        cout << this->rows_index_column_by_column[i] << "; " ;
    }
    cout << "]" << endl;
    cout << "[";
    for(unsigned int i = 0; i< this->cols_first_position_column_by_column.size(); i++) {
        cout << this->cols_first_position_column_by_column[i] << "; " ;
    }
    cout << "]" << endl;
    cout << "[";
    for(unsigned int i = 0; i< this->values_column_by_column.size(); i++) {
        cout << this->values_column_by_column[i] << "; " ;
    }
    cout << "]" << endl << endl;
    int j = 0;
    for(int i=0; i<this->cols; i++) {
        for(int k=this->cols_first_position_column_by_column[i]; k<this->cols_first_position_column_by_column[i+1]; k++) {  //already an element in this line
            j++;
            cout << "(" << this->rows_index_column_by_column[k] << "," << i << ")=" << this->values_column_by_column[k] << endl;
            if(j>this->nnz_column_by_column) {
                return;
            }
        }
    }
}

/*
int sparse_matrix_double::find_row(int _row) {
    int left = 0;
    int right = this->nnz_line_by_line-1;
    int mid = (left+right)/2;
    //cout << "left: " << left << " right: " << right << endl;
    if(right>0) {
        while(left+1 != right) {
            if(this->rows_index[mid] < _row) {
                left = mid;
            } else {
                right = mid;
            }
            mid = (left+right)/2;
            //cout << "left: " << left << " right: " << right << endl;
        }
        if(right > this->nnz_line_by_line || this->rows_index[right] != _row) {
            if(this->rows_index[left] == _row) {
                return left;
            }
            return -1;  //didn't find it
        }
        if(this->rows_index[left] == _row) {
            return left;
        }
        return right;   //index of first occurence
    } else {
        return 0;
    }
}*/
