#ifndef SPARSE_MATRIX_DOUBLE_HPP_INCLUDED
#define SPARSE_MATRIX_DOUBLE_HPP_INCLUDED

#include <vector>
using namespace std;

class sparse_matrix_double {
    protected :
        int rows;
        int cols;
        int find_row(int);
        /*  DOUBLE INDEX
        m(0,0)          m(0,1)      ...     m(0,cols-1)
        m(1,0)          m(1,1)      ...     m(1,cols-1)
        ...             ...         ...     ...
        m(rows-1,0)     m(rows-1,1) ...     m(rows-1,cols-1)
        */
		//ordered line by line
        /*  LINEAR
        m(0)                m(1)                ...     m(cols-1)
        m(cols)             m(cols+1)           ...     m(2*cols-1)
        ...                 ...                 ...     ...
        m((rows-1)*cols)    m((rows-1)*cols+1)  ...     m(rows*cols-1)
        */

    public :
		//ordered line by line
		//we will double that with a column by column arrangement
        vector<int> cols_index_line_by_line;
        vector<int> rows_first_position_line_by_line;
        vector<double> values_line_by_line;
        int nnz_line_by_line;
        vector<int> rows_index_column_by_column;
        vector<int> cols_first_position_column_by_column;
        vector<double> values_column_by_column;
        int nnz_column_by_column;
        
        
        sparse_matrix_double();
        sparse_matrix_double(int, int, int);
        int get_rows();
        int get_cols();
        double operator()(int, int);
        void add_element(int, int, double);
        void add_element_line_by_line(int, int, double);
        void add_element_column_by_column(int, int, double);
        void set_element(int, int, double);
        void set_element_line_by_line(int, int, double);
        void set_element_column_by_column(int, int, double);
        int i_index(int) const;
        int j_index(int) const;
        void print_line_by_line();
        void print_column_by_column();
        void print_reshape(int, int);
        int linear_index(int, int) const;
};

#endif // SPARSE_MATRIX_DOUBLE_HPP_INCLUDED
