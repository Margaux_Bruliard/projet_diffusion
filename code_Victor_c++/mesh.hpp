#ifndef MESH_HPP_INCLUDED
#define MESH_HPP_INCLUDED

#include <vector>
#include "dense_matrix.hpp"

using namespace std;

class mesh {
    protected :
		double x_min;
		double y_min;
		double x_max;
		double y_max;
		int nx, ny;	//total number of cells on each axis
		
    public :
		dense_matrix dx;
		dense_matrix dy;
		dense_matrix x;
		dense_matrix y;
		mesh();
        mesh(char *);
		double interpolated_delta(int, int, int, double, double, double, double, double);
};

#endif // MESH_HPP_INCLUDED
