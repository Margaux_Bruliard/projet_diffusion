#ifndef PERMEABILITY_DOMAIN_HPP_INCLUDED
#define PERMEABILITY_DOMAIN_HPP_INCLUDED

#include <vector>
using namespace std;

class permeability_domain {
    protected :
		double x_min;
		double y_min;
		double x_max;
		double y_max;
		/*K = (v1 v2)
			  (v3 v4)*/

    public :
		double v1;
		double v2;
		double v3;
		double v4;
        permeability_domain();
        permeability_domain(double, double, double, double, double, double, double, double);
		bool is_in(double, double);
};

#endif // PERMEABILITY_DOMAIN_HPP_INCLUDED
