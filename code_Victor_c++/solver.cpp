#include <iostream>
#include "solver.hpp"
#include <ctime>
#include <cmath>

using namespace std;

solver::solver() {
	//this->rayon_sup = -1; //only for 9 point stencil
	//this->rayon_inf = -1; //only for 9 point stencil
}

void solver::solve_darcy_LU(darcy *d) {
	this->rayon_sup = (*d).get_n_x() + 2; //only for 9 point stencil
	this->rayon_inf = (*d).get_n_x() + 2; //only for 9 point stencil
	//cout << "rayon_sup: " << (*d).get_n_x() + 1 << endl;
	//cout << "rayon_inf: " << (*d).get_n_x() + 1 << endl;
	this->solve_LU(&d->A, &d->f, &(*d).u);
}

void solver::solve_darcy_GMRES(darcy *d, sparse_matrix *M, int n_before_restart, int max_it, double tol) {
	d->u = dense_matrix(d->A.get_rows(), 1);
	this->GMRES(&d->A, &d->u, &d->f, M, n_before_restart, max_it, tol);
}

void solver::solve_darcy_GMRES_without_inver(darcy *d, int max_it, double tol, bool talkative) {
	d->u = dense_matrix(d->A.get_rows(), 1);
	this->GMRES_without_inver(&d->A, &d->u, &d->f, max_it, tol, talkative);
}

void solver::solve_LU(sparse_matrix *A, dense_matrix *f, dense_matrix *x) {   //Ax=f
	this->fact_LU(A);   //A=LU
	/*A->print();
	cout << endl;
	this->L.print();
	cout << endl;
	this->U.print();*/
	dense_matrix y;
	this->res_tri_inf(&this->L, f, &y);	//Ly=f
	this->res_tri_sup(&this->U, &y, x, true);	//Ux=y
}

void solver::fact_LU(sparse_matrix *A) {
	int n = A->get_cols();  //square cols=rows
	this->L = sparse_matrix(n, n, 0);
	this->U = sparse_matrix(n, n, 0);
	//int number_of_zeros = 0;
	//int number_of_non_zeros = 0;
	for(int i=0; i<n; i++) {	//for each ligne of U and column of L
		int lim_sup = i+this->rayon_sup;
		if(lim_sup>n) {
			lim_sup = n;
		}
		int lim_inf = i-this->rayon_sup;
		if(lim_inf<0) {
			lim_inf = 0;
		}
		//for(int j=i; j<n; j++) {
		/*****method B******/
		//int index_cols_A = (*A).rows_first_position[i];
		/*****method B******/
		for(int j=i; j<lim_sup; j++) {  //for each element in the upper triangle of U
			//this->U.add_element(i, j, this->set_LU_line(A, i, j));
			/*****method A******/
			double value = (*A)(i, j);
			/*****method A******/
			/*****method B******/
			/*double value = 0.0;
			while((*A).cols_index[index_cols_A] < j && index_cols_A <(*A).rows_first_position[i+1]) {
				index_cols_A++;
			}
			if((*A).cols_index[index_cols_A] == j) {
				value = (*A).values[index_cols_A];
				index_cols_A++;
			}
			/*****method B******/
			//for(int k=0; k<i; k++) {
			for(int k=lim_inf; k<i; k++) {
				value = value - this->L(i, k)*this->U(k, j);
			}
			/*if(value < 0.000001 && -value < 0.000001) {
				//cout << endl << "U ZERO HERE ! " << value << " @ " << i << " " << j << endl;
				number_of_zeros++;
			} else {
				number_of_non_zeros++;
			}*/
			this->U.add_element(i, j, value);
		}
		/*lim_sup = i+this->rayon_inf;
		if(lim_sup>n) {
			lim_sup = n;
		}
		lim_inf = i-this->rayon_sup;
		if(lim_inf<0) {
			lim_inf = 0;
		}*/
		//for(int j=i+1; j<n; j++) {
		for(int j=i+1; j<lim_sup; j++) {  //for each element in the lower triangle of L
			//this->L.add_element(j, i, this->set_LU_column(A, i, j));
			double value = (*A)(j, i);
			//for(int k=0; k<i; k++) {
			for(int k=lim_inf; k<i; k++) {
				value = value - this->L(j, k)*this->U(k, i);
			}
			value = value/U(i, i);
			/*if(value < 0.000001 && -value < 0.000001) {
				//cout << endl << "L ZERO HERE ! " << value << " @ " << j << " " << i << endl;
				number_of_zeros++;
			} else {
				number_of_non_zeros++;
			}*/
			this->L.add_element(j, i, value);
		}
		this->L.add_element(i, i, 1.0);
	}
	//cout << "number of zeros: " << number_of_zeros << " | " << 100*number_of_zeros/(number_of_zeros+number_of_non_zeros) << "%" << endl;	
	//cout << "number of non zeros: " << number_of_non_zeros << endl;
}

double solver::set_LU_line(sparse_matrix *A, int i, int j) {
	double value = (*A)(i, j);
	for(int k=0; k<i; k++) {
		value = value - this->L(i, k)*this->U(k, j);
	}
	return value;
}

double solver::set_LU_column(sparse_matrix *A, int i, int j) {
	double value = (*A)(j, i);
	for(int k=0; k<i; k++) {
		value = value - this->L(j, k)*this->U(k, i);
	}
	value = value/U(i, i);
	return value;
}

void solver::print() {
	cout << endl << "L:" << endl;
	this->L.print();
	cout << endl << "U:" << endl;
	this->U.print();
}

void solver::res_tri_inf(sparse_matrix *L, dense_matrix *b, dense_matrix *x) {
	int n = L->get_cols();  //square cols=rows
	(*x) = dense_matrix(n, 1);
	for(int i=0; i<n; i++) {
		//cout << "(" << (*b)(i, 0) << "-" << this->somme_rti(L, &x, i) << ")/" << (*L)(i, i) << endl;
		(*x)(i, 0) = ((*b)(i, 0) - this->somme_rti(L, x, i))/(*L)(i, i);
	}
}

double solver::somme_rti(sparse_matrix *L, dense_matrix *x, int i) {
	double value = 0.0;
	int lim = i - this->rayon_inf;
	if(lim<0) {
		lim = 0;
	}
	for(int j=lim; j<i; j++) {
		value = value + (*L)(i, j)*(*x)(j, 0);
	}
	return value;
}

void solver::res_tri_sup(sparse_matrix *U, dense_matrix *b, dense_matrix *x, bool quick_sparse) {	//use quick_sparse = true only for solving LU using the rayon_sup
	int n = U->get_cols();  //square cols=rows
	(*x) = dense_matrix(n, 1);
	//cout << (this->somme_rts(U, x, n-2, n, quick_sparse)) << endl;
	for(int i=n-1; i>=0; i--) {
		//cout << "(" << (*b)(i, 0) << "-" << this->somme_rts(U, &x, i, n) << ")/" << (*U)(i, i) << endl;
		(*x)(i, 0) = ((*b)(i, 0) - this->somme_rts(U, x, i, n, quick_sparse))/(*U)(i, i);
	}
}

double solver::somme_rts(sparse_matrix *U, dense_matrix *x, int i, int n, bool quick_sparse) {
	double value = 0.0;
	int lim = i+1 + this->rayon_sup;
	if(lim>n || !quick_sparse) {
		lim = n;
	}
	for(int j=i+1; j<lim; j++) {
		value = value + (*U)(i, j)*(*x)(j, 0);
	}
	return value;
}

void solver::GMRES(sparse_matrix *A, dense_matrix *x, dense_matrix *b, sparse_matrix *M, int n_before_restart, int max_it, double tol) {	//A, x, b, Mconditioner has to be diagonal for now, n_before_restart, max_it, tol
	/********************************************/
	//http://www.netlib.org/templates/matlab/gmres.m
	/********************************************/
	//initialization
	double temp;
	int iter=0;
	double norm_b = b->vec_norm2();
	if(norm_b < 0.00001 && -norm_b < 0.00001) {
		norm_b = 1.0;
	}
	dense_matrix r(x->get_rows(), x->get_cols());	//has to be of size (n,1)
	dense_matrix V_vector(x->get_rows(), x->get_cols());	//has to be of size (n,1)
	dense_matrix V_tmp_vector(x->get_rows(), x->get_cols());	//has to be of size (n,1)
	dense_matrix w(x->get_rows(), x->get_cols());	//has to be of size (n,1)
	dense_matrix f(x->get_rows(), x->get_cols());	//has to be of size (n,1)	//for f=(b-A*x)
	dense_matrix s(x->get_rows(), x->get_cols());	//has to be of size (n,1)
	//r = M \ (b- A*x)
	A->mult_vec(x, &f);	//f = A*x
	f = (*b)-f;
	this->conditioner_solve(M, &f, &r);
	double error = r.vec_norm2()/norm_b;
	if(error < tol) {
		return;
	}

	//initialize workspace
	int n = A->get_rows();	//A.row==A.cols
	int m = n_before_restart;
	sparse_matrix V(n, m+1, 0);
	sparse_matrix H(m+1, m, 0);
	dense_matrix cs(m, 1);
	dense_matrix sn(m, 1);
	dense_matrix e1(n, 1);
	for(int i=0; i<m; i++) {
		cs(i, 0) = 0.0;
		sn(i, 0) = 0.0;
	}
	for(int i=1; i<n; i++) {
		e1(i, 0) = 0.0;
	}
	e1(0, 0) = 1.0;
	//begin iteration
	for(iter=1; iter <= max_it; iter++) {
		//r = M \ (b- A*x)
		A->mult_vec(x, &f);	//f = A*x
		f = (*b)-f;
		this->conditioner_solve(M, &f, &r);
		double norm_r = r.vec_norm2();
		r = r*(1/norm_r);
		V.set_column(0, &r);
		s = e1*norm_r;
		//construct orthonormal
		for(int i=0; i<m; i++) {
			sparse_matrix H_dim_square(i+1, i+1, 0);
			sparse_matrix V_dim_square(n, i+1, 0);
			dense_matrix s_dim_square(i+1, 1);
			dense_matrix y(i+1, 1);
			//basis using Gram-Schmidt
			V_vector = V.get_column(i);	
			A->mult_vec(&V_vector, &V_tmp_vector);
			this->conditioner_solve(M, &V_tmp_vector, &w);
			for(int k = 0; k<=i; k++) {
				V_tmp_vector = V.get_column(k);
				H.set_element(k,i, w.vec_dot_product(&V_tmp_vector));
				w = w - V_tmp_vector*H(k,i);
			}
			H.set_element(i+1,i,w.vec_norm2());
			V_tmp_vector = w*(1/H(i+1,i));
			V.set_column(i+1, &V_tmp_vector);
			//apply Givens rotation
			for(int k=0; k<i; k++) {
				temp = cs(k,0)*H(k,i) + sn(k,0)*H(k+1,i);
				H.set_element(k+1, i, -sn(k,0)*H(k,i) + cs(k,0)*H(k+1,i));
				H.set_element(k, i, temp);
			}
			//compute the Givens rotation for H(i,i) and H(i+1,i) 
			if(H(i+1,i) == 0) {
				cs(i,0) = 1.0;
				sn(i,0) = 0.0;
			} else if(abs(H(i+1,i)) > abs(H(i,i))) {
				  temp = H(i,i) / H(i+1,i);
				  sn(i,0) = 1.0 / sqrt( 1.0 + temp*temp );
				  cs(i,0) = temp * sn(i,0);
			} else {
				  temp = H(i+1,i) / H(i,i);
				  cs(i,0) = 1.0 / sqrt( 1.0 + temp*temp );
				  sn(i,0) = temp * cs(i,0);
			}
			//approximate residual norm
			temp = cs(i,0)*s(i, 0);
			s(i+1, 0) = -sn(i, 0)*s(i, 0);
			s(i, 0) = temp;
			H.set_element(i,i,cs(i,0)*H(i,i) + sn(i, 0)*H(i+1,i));
			H.set_element(i+1,i, 0.0);
			error = abs(s(i+1, 0)) / norm_b;
			//update approximation
			if(error <= tol) {
				for(int l1=0; l1<i+1; l1++) {
					for(int l2 = 0; l2<i+1; l2++) {
						H_dim_square.add_element(l1, l2, H(l1, l2));
					}
					s_dim_square(l1,0) = s(l1, 0);
					for(int l2=0; l2<n; l2++) {
						V_dim_square.add_element(l2, l1, V(l2,l1));
					}
				}
				//H is upper triangular matrix ???
				this->res_tri_sup(&H_dim_square, &s_dim_square, &y, false);				
				V_dim_square.mult_vec(&y, &V_tmp_vector);
				(*x) = (*x) + V_tmp_vector;
				//and exit
				break;
			}
		}
		if(error <= tol) {
			break;
		}
		//update approximation
		sparse_matrix H_dim_square(m, m, 0);
		sparse_matrix V_dim_square(n, m, 0);
		dense_matrix s_dim_square(m, 1);
		dense_matrix y(m, 1);
		for(int l1=0; l1<m; l1++) {
			for(int l2 = 0; l2<m; l2++) {
				H_dim_square.add_element(l1, l2, H(l1, l2));
			}
			s_dim_square(l1,0) = s(l1, 0);
			for(int l2=0; l2<n; l2++) {
				V_dim_square.add_element(l2, l1, V(l2,l1));
			}
		}
		this->res_tri_sup(&H_dim_square, &s_dim_square, &y, false);
		V_dim_square.mult_vec(&y, &V_tmp_vector);
		(*x) = (*x) + V_tmp_vector;
		//compute residual
		//r = M \ (b- A*x)
		A->mult_vec(x, &f);	//f = A*x
		f = (*b)-f;
		this->conditioner_solve(M, &f, &r);
		s(m+1, 0) = r.vec_norm2();
		error = s(m+1, 0) / norm_b;
		//check convergence
		if(error <= tol) {
			break;
		}
	}
}


void solver::GMRES_without_inver(sparse_matrix *A, dense_matrix *x, dense_matrix *b, int max_it, double tol, bool talkative) {	//A, x, b, Mconditioner has to be diagonal for now, n_before_restart, max_it, tol
	int N = b->get_rows();
	double rho = b->vec_norm2();
	dense_matrix vector1_N(N, 1);	//has to be of size (N,1)
	dense_matrix vector2_N(N, 1);	//has to be of size (N,1)
	dense_matrix vector3_N(N, 1);	//has to be of size (N,1)
	dense_matrix c(max_it, 1);	//has to be of size (N,1)
	dense_matrix s(max_it, 1);	//has to be of size (N,1)
	sparse_matrix h(max_it+1, max_it, 0);
	dense_matrix g(max_it+1, 1);	//has to be of size (max_it+1,1)
	dense_matrix v(N, max_it);	//has to be of size (N,max_it)
	//initialized at the end :
	//y
	//g_dim_square
	//h_dim_square

	g(0, 0) = rho;
	double normh = 0.0;
	tol = tol*rho;
	//v(:, 0) = (*b)*(1/rho);
	vector1_N = (*b)*(1/rho);
	v.set_column(0, &vector1_N);
	int k = -1;	//matlab starts array at 1 so k=0 | C++ starts array at 0 so k=-1
	//Begin gmres loop.
	while((rho > tol) && (k < max_it-2)) {
		k++;
		//%  Matrix vector product.
		//v(:,k+1) = A*v(:,k);		%N by 2, 3, 4, ...
		v.get_column(k, &vector1_N);
		A->mult_vec(&vector1_N, &vector2_N);
		v.set_column(k+1, &vector2_N);
		//Begin modified GS. May need to reorthogonalize.
		for(int j=0; j<=k; j++) {
			//h(j,k) = v(:,j)'*v(:,k+1);	%bigger h
			v.get_column(j, &vector1_N);
			v.get_column(k+1, &vector2_N);
			h.set_element(j,k, vector1_N.vec_dot_product(&vector2_N));
			//v(:,k+1) = v(:,k+1) - h(j,k)*v(:,j);
			//vector1_N = v(:,j)
			//vector2_N = v(:,k+1)
			vector3_N = vector2_N - vector1_N*h(j, k);
			v.set_column(k+1, &vector3_N);
		}
		//h(k+1,k) = (v(:,k+1)'*v(:,k+1))^.5;
		v.get_column(k+1, &vector2_N);
		h.set_element(k+1, k, vector2_N.vec_norm2());
		if(h(k+1,k) != 0.0) {
			//v(:,k+1) = v(:,k+1)/h(k+1,k);
			vector3_N = vector2_N*(1/h(k+1,k));
			v.set_column(k+1, &vector3_N);
		}
		//Apply old Givens rotations to h(1:k,k).
		/*if k>1                                
			for i=1:k-1
				hik = c(i)*h(i,k)-s(i)*h(i+1,k);
				hipk       = s(i)*h(i,k)+c(i)*h(i+1,k);
				h(i,k)     = hik;
				h(i+1,k)   = hipk;
			end
		end*/
		if(k>0) {
			for(int i=0; i<=k-1; i++) {
				double hik = c(i, 0)*h(i, k) - s(i, 0)*h(i+1, k);
				double hipk = s(i, 0)*h(i, k) + c(i, 0)*h(i+1, k);
				h.set_element(i,k, hik);
				h.set_element(i+1,k, hipk);
			}
		}
		//normh = norm(h(k:k+1,k));
		normh = sqrt(h(k,k) * h(k, k) + h(k+1,k) * h(k+1, k));
		//May need better Givens implementation. 
		//Define and apply new Givens rotations to h(k:k+1,k).
		/*if normh ~= 0                              
		    c(k)        = h(k,k)/normh;
		    s(k)        = -h(k+1,k)/normh;
		    h(k,k)      = c(k)*h(k,k) - s(k)*h(k+1,k);
		    h(k+1,k)    = 0;
		    gk          = c(k)*g(k) - s(k)*g(k+1);
		    gkp         = s(k)*g(k) + c(k)*g(k+1);
		    g(k)        = gk;
		    g(k+1)      = gkp;
		end  */
		if(normh != 0.0) {
			c(k, 0) = h(k, k)/normh;
			s(k, 0) = -h(k+1, k)/normh;
			h.set_element(k, k, c(k, 0)*h(k, k) - s(k, 0)*h(k+1, k));
			h.set_element(k+1, k, 0.0);
			double gk = c(k, 0)*g(k, 0) - s(k, 0)*g(k+1, 0);
			double gkp = s(k, 0)*g(k, 0) + c(k, 0)*g(k+1, 0);
			g(k, 0) = gk;
			g(k+1, 0) = gkp;
		}
		//rho = abs(g(k+1));
		rho = abs(g(k+1, 0));
	}
	if(k<0) {
		cout << "Check for trivial/near 0 solution or a way to high tolerance" << endl;
		k = 0;
	}

	//End of gmres loop
	//h(1:k,1:k) is upper triangular matrix in QR.
	//y = h(1:k,1:k)\g(1:k);
	dense_matrix y(N, 1);	//has to be of size (N,1)
	sparse_matrix h_dim_square(k, k, 0);
	h.extract(0, k-1, 0, k-1, &h_dim_square);
	dense_matrix g_dim_square(N, 1);	//has to be of size (N,1)
	g.extract(0, k-1, 0, 0, &g_dim_square);

	res_tri_sup(&h_dim_square, &g_dim_square, &y, false);
	//Form linear combination.
	/*for i = 1:k                                 
		x(:) = x(:) + v(:,i)*y(i);
	end*/
	for(int i=0; i<k; i++) {
		v.get_column(i, &vector1_N);
		(*x) = (*x) + vector1_N*y(i, 0);
	}
	//return infos
	if(talkative) {
		cout << "GMRES without inversion" << endl;
		cout << "\tTol :" << tol << "\t Tol reached :" << rho << endl;
		cout << "\tIn " << k+1 << " iterations with a maximum of " << max_it << endl;
	}
}


/*double solver::dot_product(double *d_a, double *d_b, int N, int N_threads) {
	int M_blocks = (N + N_threads-1)/N_threads;	//#of blocks
	int size_dot_product = M_blocks*sizeof(double);
	//cout << "size block :" << M_blocks << endl;

	double *d_dot_product = NULL;
	cudaMalloc((void **)&d_dot_product, size_dot_product);	//if u changes over time, otherwise allocate memory once outside of the transport loop
	double *host_dot_product = (double *)malloc(size_dot_product);

	dot_prod_GPU<<<M_blocks,N_threads,N_threads*sizeof(double)>>>(d_a, d_b, d_dot_product, N);
	//copy values to host
	cudaMemcpy(host_dot_product, d_dot_product, size_dot_product, cudaMemcpyDeviceToHost);

	//cout << "mult : " << endl;
	//cout << host_dot_product[0] << endl;
	double result = host_dot_product[0];
	for(int i=1; i<M_blocks; i++) {
		//cout << host_dot_product[i] << endl;
		result += host_dot_product[i];
	}
	//cout << endl;

	cudaFree(d_dot_product);	//if u changes over time, otherwise allocate memory once outside of the transport loop
	free(host_dot_product);
	return result;
}

double solver::dot_mult(double *d_a, double *d_b, int N, int N_threads) {
	int M_blocks = (N + N_threads-1)/N_threads;	//#of blocks
	int size_dot_mult = N*sizeof(double);
	double *d_result = NULL;
	cudaMalloc((void **)&d_result, size_dot_mult);	//if u changes over time, otherwise allocate memory once outside of the transport loop
	//thrust::device_vector<double> thrust_result = thrust::device_vector<double>(N);
	double *host_result = (double *)malloc(size_dot_mult);

	dot_mult_GPU<<<M_blocks,N_threads>>>(d_a, d_b, d_result, N);
	//dot_mult_GPU<<<M_blocks,N_threads>>>(d_a, d_b, thrust::raw_pointer_cast(thrust_result.data()), N);
	//copy values to host
	//cudaMemcpy(host_result, d_result, size_dot_mult, cudaMemcpyDeviceToHost);
	
	//double result = thrust::reduce(thrust::device, thrust_result.begin(), thrust_result.end(), 0.0);
	//double result = 0.0;
	double result = host_result[0];
	for(int i=1; i<N; i++) {
		result += host_result[i];
	}

	cudaFree(d_result);	//if u changes over time, otherwise allocate memory once outside of the transport loop
	free(host_result);
	return result;
}*/


void solver::conditioner_solve(sparse_matrix *M, dense_matrix *f, dense_matrix *r) {	//Mr=f with f=(b-Ax)
	//x supposed to be of size (n,1)
	//CONDITIONER M IS DIAGONAL MATRIX !!!
	for(int i=0; i<(*r).get_rows(); i++) {
		(*r)(i,0) = (1.0/(*M)(i,i))*(*f)(i,0);
	}
}
