#ifndef PERMEABILITY_MATRIX_HPP_INCLUDED
#define PERMEABILITY_MATRIX_HPP_INCLUDED

#include "dense_matrix.hpp"
#include "permeability_domain.hpp"
#include "mesh.hpp"

class permeability_matrix: public dense_matrix {
    protected :
		double dx;
		double dy;
		dense_matrix x_coord;
		dense_matrix y_coord;
		double x_min;
		double y_min;
		double x_max;
		double y_max;
		double n_x;
		double n_y;
        vector<permeability_domain> domain;
        int flip_coordinates(int);  //it is only necessary to flip rows

    public:
        /*  matrix K for element uk
        ((n_y/2-1)*(n_x/2)+1)   ((n_y/2-1)*(n_x/2)+2)   ... ((n_y/2)*(n_x/2))
        ...                     ...                     ... ...
        (n_x/2+1)               (n_x/2+2)               ... (2*(n_x/2))
        (0)                     (1)                     ... (n_x/2)
        */
        /*  each (i) above is seen as a 2by2 matrix hence the size 2*n_xby2*n_y of the permeability_matrix
         (0):   ((k1,1) (k1,2)) (1):((k1,1) (k1,2))
                ((k2,1) (k2,2))     ((k2,1) (k2,2))
        */
        /*
        the matrices aren't in the same order as the elements in dense_matrix
        this is handled by flip_coordinates(). Only rows needs to be switched.
        It's done so to fit with the elements Uk
        U(y-1)x U(y-1)x+1   ... Uy*x-1
        ...     ...         ... ...
        Ux      Ux+1        ... U2*x-1
        U0      U1          ... Ux-1
        */
        /*
        filling the matrix is still done the same way as for dense_matrix
        */
        permeability_matrix();
        permeability_matrix(double, double, double, double, int, int, mesh *);    //size (2*n_x, 2*n_y)
                                                    //for each uk there is 4 elements
                                                    //K(1, 1)   K(1, 2)
                                                    //K(2, 1)   K(2, 2)
		void set_with_domains(char *name);
        double get_K(int, int, int);
        double get_K_boundary(int, int);
};

#endif // PERMEABILITY_MATRIX_HPP_INCLUDED
