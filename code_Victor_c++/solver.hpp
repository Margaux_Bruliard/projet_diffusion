#ifndef SOLVER_HPP_INCLUDED
#define SOLVER_HPP_INCLUDED

#include "dense_matrix.hpp"
#include "darcy.hpp"

class solver {
    protected :
        sparse_matrix L;
        sparse_matrix U;
        sparse_matrix conditioner;
        int rayon_sup;
        int rayon_inf;
        double set_LU_line(sparse_matrix *, int, int);
        double set_LU_column(sparse_matrix *, int, int);
        double somme_rti(sparse_matrix *, dense_matrix *, int);
        double somme_rts(sparse_matrix *, dense_matrix *, int, int, bool);

    public :
        solver();
        void solve_darcy_LU(darcy *);
        void solve_darcy_GMRES(darcy *, sparse_matrix *, int, int, double);
		void solve_darcy_GMRES_without_inver(darcy *, int, double, bool);
        void solve_LU(sparse_matrix *, dense_matrix *, dense_matrix *);
        void fact_LU(sparse_matrix *);
        void print();
        void res_tri_inf(sparse_matrix *, dense_matrix *, dense_matrix *);
        void res_tri_sup(sparse_matrix *, dense_matrix *, dense_matrix *, bool);
		void GMRES(sparse_matrix *, dense_matrix *, dense_matrix *, sparse_matrix *, int, int, double);	//A, x, b, Mconditioner has to be diagonal for now, n_before_restart, max_it, tol
		void GMRES_without_inver(sparse_matrix *, dense_matrix *, dense_matrix *, int, double, bool);	//A, x, b, n_before_restart, max_it, tol, talkative
		void conditioner_solve(sparse_matrix *, dense_matrix *, dense_matrix *);
		//double dot_product(double *, double *, int, int);
		//double dot_mult(double *, double *, int, int);
};

#endif // SOLVER_HPP_INCLUDED
