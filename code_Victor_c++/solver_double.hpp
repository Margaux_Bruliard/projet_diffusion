#ifndef SOLVER_DOUBLE_HPP_INCLUDED
#define SOLVER_DOUBLE_HPP_INCLUDED

#include "dense_matrix.hpp"
#include "darcy_double.hpp"

class solver_double {
    protected :
        sparse_matrix_double L;
        sparse_matrix_double U;
        double set_LU_line(sparse_matrix *, int, int);
        double set_LU_column(sparse_matrix *, int, int);
        double somme_rti(sparse_matrix_double *, dense_matrix *, int);
        double somme_rts(sparse_matrix_double *, dense_matrix *, int, int);

    public :
        solver_double();
        int rayon_sup;
        int rayon_inf;
        void solve_LU(sparse_matrix_double *, dense_matrix *, dense_matrix *);
        void fact_LU(sparse_matrix_double *);
        void print();
        void res_tri_inf(sparse_matrix_double *, dense_matrix *, dense_matrix *);
        void res_tri_sup(sparse_matrix_double *, dense_matrix *, dense_matrix *);
        void solve_darcy_LU(darcy_double *);
};

#endif // SOLVER_DOUBLE_HPP_INCLUDED
