#include "transport.hpp"
#include "dense_matrix.hpp"
#include "darcy.hpp"
#include "functions_type.hpp"
#include <iostream>
#include <fstream>
#include <math.h>

transport::transport(double _x_min, double _x_max, double _y_min, double _y_max, int _n_x, int _n_y, mesh *mesh_c, functiontype3 _neumann_condition_u, double _cfl, permeability_matrix _D, functiontype1 _function_f2, functiontype3  _neumann_condition_C) {
	this->dx = (_x_max - _x_min)/(_n_x+1);
	this->dy = (_y_max - _y_min)/(_n_y+1);
	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;
	//this->n_x = _n_x;
	//this->n_y = _n_y;
	this->x_coord = mesh_c->x;
	this->y_coord = mesh_c->y;
	this->n_x = x_coord.get_rows();
	this->n_y = y_coord.get_rows();
	for(int i=0; i<_n_x-1; i++) {
		if(this->dx > this->x_coord(i+1, 0) - this->x_coord(i, 0)) {
			this->dx = this->x_coord(i+1, 0) - this->x_coord(i, 0);	//find min
		}
	}
	for(int i=0; i<_n_y-1; i++) {
		if(this->dy > this->y_coord(i+1, 0) - this->y_coord(i, 0)) {
			this->dy = this->y_coord(i+1, 0) - this->y_coord(i, 0);	//find min
		}
	}

	this->neumann_condition_u = _neumann_condition_u;
	this->neumann_condition_C = _neumann_condition_C;
	this->concentration = dense_matrix(_n_x*_n_y, 1);
	this->flux = dense_matrix(_n_x*_n_y, 1);
	this->convection = dense_matrix(_n_x*_n_y, 1);
	this->diffusion = dense_matrix(_n_x*_n_y, 1);
	this->grad_h_x_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_h_y_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_h_x_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->grad_h_y_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->ve_x_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->ve_y_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->f2 = dense_matrix(_n_x*_n_y, 1);
	this->cfl = _cfl;
	this->function_f2 = _function_f2;
	this->D = _D;
	for(int i=0; i<this->n_y; i++) {	//goes through each cells
		for(int j=0; j<this->n_x; j++) {
			int k = i*this->n_x+j;
			double x = this->dx*(double)j + this->x_min + this->dx;
			double y = this->dy*(double)i + this->y_min + this->dy;
			this->f2(k, 0) = this->function_f2(x, y);
		}
	}
	this->flux_system = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->convection_system = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->diffusion_system = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->flux_right_hand_side = dense_matrix(_n_x*_n_y, 1);
	this->flux_boundary = dense_matrix(_n_x*_n_y, 1);
	this->convection_boundary = dense_matrix(_n_x*_n_y, 1);
	this->diffusion_boundary = dense_matrix(_n_x*_n_y, 1);
	this->grad_C_x_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_C_y_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_C_x_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->grad_C_y_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->time = 0.0;
}

void transport::compute_iterativ(double t_end, darcy *d, starter *start, functiontype4 limiter, sparse_matrix *M, darcy_double *d_double) {
	double dt = 0.01;	//dt=cfl*dx/sqrt(v1^2+v2^2);
	this->time = 0.0;
	int iter = 0;
	int iter_max = start->iter_max;

	//set file name for solution output
	const char *file_name;
	if(start->transport_mode == "coupled") {
		if(start->ordre_transport_space == 1) {
			file_name = "play_video_coupled.m";
		} else if(start->ordre_transport_space == 2) {
			file_name = "play_video_coupled_limiter.m";
		}
	} else {
		if(start->ordre_transport_space == 1) {
			file_name = "play_video.m";
		} else if(start->ordre_transport_space == 2) {
			file_name = "play_video_limiter.m";
		}

	}

	//start save file
	ofstream save_file;
	save_file.open(file_name);

	this->write_grid(&save_file);
	this->write_frame(&save_file);

	solver s;
	solver_double s_double;

	while(this->time < t_end && (iter<iter_max || iter_max == -1)) {

		iter++;

		if(start->transport_mode == "coupled") {
			this->solve_darcy(d, d_double, &s, &s_double, start, M);
			this->concentration_old = this->concentration;
		}

		cout << "Time  " << this->time << " Time iteration " << iter-1 << endl;
		double max_speed_convection = 0.0;
		double max_speed_diffusion = 0.0;

		if(start->diffusion) {
			(*d).compute_grad_C(&this->concentration, &this->grad_C_x_left, &this->grad_C_y_left, &this->grad_C_x_down, &this->grad_C_y_down, this->neumann_condition_C);
			(*d).compute_diffusion_C(&this->diffusion, &this->grad_C_x_left, &this->grad_C_y_left, &this->grad_C_x_down, &this->grad_C_y_down, &this->D, &max_speed_diffusion);
		}

		if(start->convection) {
			(*d).compute_grad_h(&this->grad_h_x_left, &this->grad_h_y_left, &this->grad_h_x_down, &this->grad_h_y_down, this->neumann_condition_u);
			(*d).compute_velocity_h(&this->grad_h_x_left, &this->grad_h_y_left, &this->grad_h_x_down, &this->grad_h_y_down, &this->ve_x_left, &this->ve_y_down, &max_speed_convection);
			if(start->ordre_transport_space == 1) {			
				(*d).compute_convection_h(&this->concentration, &this->convection, &this->ve_x_left, &this->ve_y_down);
			} else if(start->ordre_transport_space == 2) {
				(*d).compute_convection_h_MUSCL(&this->concentration, &this->convection, &this->ve_x_left, &this->ve_y_down, limiter, start->limiter_name);
			}
			cout << "ICI this->dx: " << this->dx << " this->dy: " << this->dy << " max_speed_convection: " << max_speed_convection << endl;
			this->grad_h_x_left.save_reshape(start->n_x+1, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "grad_h_x_left_final_CPU");
			this->grad_h_y_down.save_reshape(start->n_x, start->n_y+1, start->x_min, start->x_max, start->y_min, start->y_max, "grad_h_y_down_final_CPU");
			this->ve_y_down.save_reshape(start->n_x, start->n_y+1, start->x_min, start->x_max, start->y_min, start->y_max, "ve_y_down_final_CPU");
			this->convection.save_reshape_non_uniform(&(this->x_coord), &(this->y_coord), "convection_final_CPU");
		}
		if(start->convection && start->diffusion) {
			dt = this->set_dt(max_speed_convection, max_speed_diffusion, t_end);
		} else if(start->convection) {
			dt = this->set_dt(max_speed_convection, -1.0, t_end);
		} else if(start->diffusion) {
			dt = this->set_dt(-1.0, max_speed_diffusion, t_end);
		} else {
			cout << "Add at least a convection term or a diffusion term to solve the transport equation" << endl;
			return;
		}

		if(start->convection) {
			this->concentration = this->concentration - this->convection*dt;	//elementwise operations
		}
		if(start->diffusion) {
			this->concentration = this->concentration - this->diffusion*dt;	//elementwise operations
		}
		this->concentration = this->concentration - this->f2*(dt/(this->dx*this->dy));	//elementwise operations

		this->time += dt;
		
		if(start->transport_mode == "coupled") {
			d->f = this->concentration - this->concentration_old;
			d->f = d->f*(-70*dt) + d->boundary_condition;
		}

		this->write_frame(&save_file);
	}
	this->convection.save_reshape_non_uniform(&(this->x_coord), &(this->y_coord), "convection_final");
	d->u.save_reshape_non_uniform(&(this->x_coord), &(this->y_coord), "u_final");
	cout << "Time  " << this->time << " Time iteration " << iter << endl;
	save_file.close();
	cout << "Transport saved in " << file_name << endl;
}

void transport::compute_iterativ_backward(double t_end, darcy *d, starter *start) {
	double dt = 0.01;	//dt=cfl*dx/sqrt(v1^2+v2^2);
	this->time = 0.0;
	int iter = 0;
	int iter_max = start->iter_max;
	ofstream save_file;
	save_file.open("play_video_backward.m");
	this->write_grid(&save_file);
	this->write_frame(&save_file);

	solver s;
	solver_double s_double;

	while(this->time < t_end && (iter<iter_max || iter_max == -1)) {

		iter++;

		cout << "Time  " << this->time << " Time iteration " << iter-1 << endl;
		
		double max_speed_convection = 0.0;
		double max_speed_diffusion = 0.0;

		if(start->convection && start->diffusion) {
			(*d).compute_flux_backward(&this->concentration, &this->flux_system, &this->flux_boundary, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection, &max_speed_diffusion);
			dt = this->set_dt(max_speed_convection, max_speed_diffusion, t_end);

			this->flux_system.mult_scal(dt);
			this->flux_boundary = this->flux_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->flux_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->flux_boundary;
			this->concentration = dense_matrix(this->flux_system.get_rows(), 1);
			s.GMRES_without_inver(&this->flux_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->convection) {
			(*d).compute_convection_backward(&this->concentration, &this->convection_system, &this->convection_boundary, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection);
			dt=this->cfl* ((this->dx*this->dy) / (this->dx + this->dy)) /max_speed_convection;
			if(this->time+dt > t_end) {
				dt = t_end - this->time;
			}
			this->convection_system.mult_scal(dt);
			this->convection_boundary = this->convection_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->convection_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->convection_boundary;
			this->concentration = dense_matrix(this->convection_system.get_rows(), 1);
			s.GMRES_without_inver(&this->convection_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->diffusion) {
			(*d).compute_diffusion_backward(&this->concentration, &this->diffusion_system, &this->diffusion_boundary, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_diffusion);
			dt = this->cfl* (0.5*(this->dx*this->dx*this->dy*this->dy) / (this->dx*this->dx + this->dy*this->dy)) /max_speed_diffusion;
			if(this->time+dt > t_end) {
				dt = t_end - this->time;
			}
			this->diffusion_system.mult_scal(dt);
			this->diffusion_boundary = this->diffusion_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->diffusion_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->diffusion_boundary;
			this->concentration = dense_matrix(this->diffusion_system.get_rows(), 1);
			s.GMRES_without_inver(&this->diffusion_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else {
			cout << "Add at least a convection term or a diffusion term to solve the transport equation" << endl;
			return;
		}

		this->time += dt;

		this->write_frame(&save_file);
	}

	cout << "Time  " << this->time << " Time iteration " << iter  << endl;
	save_file.close();
	cout << "Transport saved in play_video_backward.m" << endl;
}

void transport::compute_iterativ_backward_coupled(double t_end, darcy *d, starter *start, sparse_matrix *M, darcy_double *d_double) {
	double dt = 0.01;	//dt=cfl*dx/sqrt(v1^2+v2^2);
	this->time = 0.0;
	int iter = 0;
	int iter_max = start->iter_max;
	ofstream save_file;
	save_file.open("play_video_backward_coupled.m");
	this->write_grid(&save_file);
	this->write_frame(&save_file);
	solver s;
	solver_double s_double;
	solver s_backward;

	while(this->time < t_end && (iter<iter_max || iter_max == -1)) {
		iter++;

		if(start->transport_mode == "coupled") {
			this->solve_darcy(d, d_double, &s, &s_double, start, M);
		}

		cout << "Time  " << this->time << " Time iteration " << iter-1 << endl;
		this->concentration_old = this->concentration;
		
		double max_speed_convection = 0.0;
		double max_speed_diffusion = 0.0;
		
		if(start->convection && start->diffusion) {
			(*d).compute_flux_backward(&this->concentration, &this->flux_system, &this->flux_boundary, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection, &max_speed_diffusion);
			dt = this->set_dt(max_speed_convection, max_speed_diffusion, t_end);
			this->flux_system.mult_scal(dt);
			this->flux_boundary = this->flux_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->flux_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->flux_boundary;
			this->concentration = dense_matrix(this->flux_system.get_rows(), 1);
			s.GMRES_without_inver(&this->flux_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->convection) {
			(*d).compute_convection_backward(&this->concentration, &this->convection_system, &this->convection_boundary, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection);
			dt=this->cfl* ((this->dx*this->dy) / (this->dx + this->dy)) /max_speed_convection;
			if(this->time+dt > t_end) {
				dt = t_end - this->time;
			}
			this->convection_system.mult_scal(dt);
			this->convection_boundary = this->convection_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->convection_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->convection_boundary;
			this->concentration = dense_matrix(this->convection_system.get_rows(), 1);
			s.GMRES_without_inver(&this->convection_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->diffusion) {
			cout << "The coupled equation requires a convection term" << endl;
			return;
		} else {
			cout << "Add at least a convection term or a diffusion term to solve the transport equation" << endl;
			return;
		}

		this->time += dt;

		d->f = this->concentration - this->concentration_old;
		d->f = d->f*(-70*dt) + d->boundary_condition;

		if(start->solver_darcy == "LU_double") {
			if(d_double == NULL) {
				cout << "ERROR TRANSPORT LIMITER COUPLED LU DOUBLE\tPlease set a darcy_double variable" << endl;
			} else {
				d_double->f = d->f;
			}
		}

		this->write_frame(&save_file);
	}

	cout << "Time  " << this->time << " Time iteration " << iter  << endl;
	save_file.close();
	cout << "Transport saved in play_video_backward_coupled.m" << endl;
	d->f.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "f_final");
	d->u.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "u_final");
	this->flux.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "flux_final");
	this->convection.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "convection_final");
}


void transport::compute_iterativ_backward_MUSCL_limiter(double t_end, darcy *d, starter *start, functiontype4 limiter) {
	double dt = 0.01;	//dt=cfl*dx/sqrt(v1^2+v2^2);
	int iter = 0;
	int iter_max = start->iter_max;
	ofstream save_file;
	save_file.open("play_video_backward_limiter.m");
	this->write_grid(&save_file);
	this->write_frame(&save_file);
	solver s;
	solver_double s_double;

	while(this->time < t_end && (iter<iter_max || iter_max == -1)) {

		iter++;

		cout << "Time  " << this->time << " Time iteration " << iter-1 << endl;
		
		double max_speed_convection = 0.0;
		double max_speed_diffusion = 0.0;
		
		if(start->convection && start->diffusion) {
			(*d).compute_flux_backward_MUSCL_limiter(&this->concentration, &this->flux_system, &this->flux_boundary, limiter, start->limiter_name, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection, &max_speed_diffusion);
			dt = this->set_dt(max_speed_convection, max_speed_diffusion, t_end);

			this->flux_system.mult_scal(dt);
			this->flux_boundary = this->flux_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->flux_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->flux_boundary;
			this->concentration = dense_matrix(this->flux_system.get_rows(), 1);
			s.GMRES_without_inver(&this->flux_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->convection) {
			(*d).compute_convection_backward_MUSCL_limiter(&this->concentration, &this->convection_system, &this->convection_boundary, limiter, start->limiter_name, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection);
			dt=this->cfl* ((this->dx*this->dy) / (this->dx + this->dy)) /max_speed_convection;
			if(this->time+dt > t_end) {
				dt = t_end - this->time;
			}
			this->convection_system.mult_scal(dt);
			this->convection_boundary = this->convection_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->convection_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->convection_boundary;
			this->concentration = dense_matrix(this->convection_system.get_rows(), 1);
			s.GMRES_without_inver(&this->convection_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->diffusion) {
			(*d).compute_diffusion_backward(&this->concentration, &this->diffusion_system, &this->diffusion_boundary, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_diffusion);
			dt = this->cfl* (0.5*(this->dx*this->dx*this->dy*this->dy) / (this->dx*this->dx + this->dy*this->dy)) /max_speed_diffusion;
			if(this->time+dt > t_end) {
				dt = t_end - this->time;
			}
			this->diffusion_system.mult_scal(dt);
			this->diffusion_boundary = this->diffusion_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->diffusion_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->diffusion_boundary;
			this->concentration = dense_matrix(this->diffusion_system.get_rows(), 1);
			s.GMRES_without_inver(&this->diffusion_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else {
			cout << "Add at least a convection term or a diffusion term to solve the transport equation" << endl;
			return;
		}

		this->time += dt;
		//this->concentration = this->concentration - (this->flux - this->f2)*(dt/(this->dx*this->dy));	//elementwise operations
		
		this->write_frame(&save_file);
	}

	cout << "Time  " << this->time << " Time iteration " << iter  << endl;
	save_file.close();
	cout << "Transport saved in play_video_backward_limiter.m" << endl;
}

void transport::compute_iterativ_backward_MUSCL_limiter_coupled(double t_end, darcy *d, starter *start, functiontype4 limiter, sparse_matrix *M, darcy_double *d_double) {
	double dt = 0.01;	//dt=cfl*dx/sqrt(v1^2+v2^2);
	int iter = 0;
	int iter_max = start->iter_max;
	ofstream save_file;
	save_file.open("play_video_backward_limiter_coupled.m");
	this->write_grid(&save_file);
	this->write_frame(&save_file);
	solver s;
	solver_double s_double;
					
	while(this->time < t_end && (iter<iter_max || iter_max == -1)) {
		iter++;

		if(start->solver_darcy == "LU") {
			s.solve_darcy_LU(d);
		} else if(start->solver_darcy == "GMRES") {
			if(M == NULL) {
				cout << "ERROR TRANSPORT LIMITER COUPLED GMRES\tPlease set a conditionner" << endl;
			} else {
				s.solve_darcy_GMRES(d, M, start->n_before_restart, start->max_it, start->tol);
			}
		} else if(start->solver_darcy == "GMRES_without_inver") {
			s.solve_darcy_GMRES_without_inver(d, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->solver_darcy == "LU_double") {
			if(d_double == NULL) {
				cout << "ERROR TRANSPORT LIMITER COUPLED LU DOUBLE\tPlease set a darcy_double variable" << endl;
			} else {
				s_double.solve_darcy_LU(d_double);
				d->u = d_double->u;
			}
		}
		cout << "Time  " << this->time << " Time iteration " << iter-1 << endl;
		this->concentration_old = this->concentration;
		
		double max_speed_convection = 0.0;
		double max_speed_diffusion = 0.0;
		
		if(start->convection && start->diffusion) {
			(*d).compute_flux_backward_MUSCL_limiter(&this->concentration, &this->flux_system, &this->flux_boundary, limiter, start->limiter_name, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection, &max_speed_diffusion);
			dt = this->set_dt(max_speed_convection, max_speed_diffusion, t_end);

			this->flux_system.mult_scal(dt);
			this->flux_boundary = this->flux_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->flux_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->flux_boundary;
			this->concentration = dense_matrix(this->flux_system.get_rows(), 1);
			s.GMRES_without_inver(&this->flux_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->convection) {
			(*d).compute_convection_backward_MUSCL_limiter(&this->concentration, &this->convection_system, &this->convection_boundary, limiter, start->limiter_name, this->neumann_condition_u, &this->D, this->neumann_condition_C, &max_speed_convection);
			dt=this->cfl* ((this->dx*this->dy) / (this->dx + this->dy)) /max_speed_convection;
			if(this->time+dt > t_end) {
				dt = t_end - this->time;
			}
			this->convection_system.mult_scal(dt);
			this->convection_boundary = this->convection_boundary*dt;
			for(int k=0; k<this->n_x*this->n_y; k++) {
				this->convection_system.add_element(k, k, 1.0);	//Ck
			}
			this->flux_right_hand_side = this->concentration + this->convection_boundary;
			this->concentration = dense_matrix(this->convection_system.get_rows(), 1);
			s.GMRES_without_inver(&this->convection_system, &this->concentration, &this->flux_right_hand_side, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->diffusion) {
			cout << "The coupled equation requires a convection term" << endl;
			return;
		} else {
			cout << "Add at least a convection term or a diffusion term to solve the transport equation" << endl;
			return;
		}

		this->time += dt;

		d->f = this->concentration - this->concentration_old;
		d->f = d->f*(-70*dt) + d->boundary_condition;

		if(start->solver_darcy == "LU_double") {
			if(d_double == NULL) {
				cout << "ERROR TRANSPORT LIMITER COUPLED LU DOUBLE\tPlease set a darcy_double variable" << endl;
			} else {
				d_double->f = d->f;
			}
		}

		this->write_frame(&save_file);
	}

	cout << "Time  " << this->time << " Time iteration " << iter  << endl;
	save_file.close();
	cout << "Transport saved in play_video_backward_limiter_coupled.m" << endl;
	d->f.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "f_final");
	d->u.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "u_final");
	this->flux.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "flux_final");
	this->convection.save_reshape(start->n_x, start->n_y, start->x_min, start->x_max, start->y_min, start->y_max, "convection_final");
}


void transport::set_centered_gaussian(double pos_x, double pos_y, double sigma_x, double sigma_y, double l) {
	for(int i=this->n_y-1; i>=0; i--) {
		for(int j=0; j<this->n_x; j++) {
			//double x = this->x_min+(j+1)*this->dx;
			//double y = this->y_min+(i+1)*this->dy;	
			double x = this->x_coord(j, 0);
			double y = this->y_coord(i, 0);		
			this->concentration(i*this->n_x + j, 0) = l*exp(-(((x-pos_x)*(x-pos_x)/(2*sigma_x*sigma_x))+((y-pos_y)*(y-pos_y)/(2*sigma_y*sigma_y))));
		}
	}
}

void transport::set_step_wave(double x_min, double x_max, double y_min, double y_max, double default_value, double step_value) {
	for(int i=this->n_y-1; i>=0; i--) {
		for(int j=0; j<this->n_x; j++) {
			//double x = this->x_min+(j+1)*this->dx;
			//double y = this->y_min+(i+1)*this->dy;	
			double x = this->x_coord(j, 0);
			double y = this->y_coord(i, 0);		
			if(x > x_min && x < x_max && y > y_min && y < y_max) {
				this->concentration(i*this->n_x + j, 0) = step_value;
			} else {
				this->concentration(i*this->n_x + j, 0) = default_value;
			}
		}
	}
}

void transport::write_grid(ofstream *file) {
	//giving the formula to avoid rounding errors (already happend once)
	//(*file) << "X = [" << this->x_min << "+(" << this->x_max << " - " << this->x_min << ")/(" << this->n_x << "+1)" << ":" << "(" << this->x_max << " - " << this->x_min << ")/(" << this->n_x << "+1)" << ":" << this->x_max << "-(" << this->x_max << " - " << this->x_min << ")/(" << this->n_x << "+1)" << "];\n";
	//(*file) << "Y = [" << this->y_min << "+(" << this->y_max << " - " << this->y_min << ")/(" << this->n_y << "+1)" << ":" << "(" << this->y_max << " - " << this->y_min << ")/(" << this->n_y << "+1)" << ":" << this->y_max << "-(" << this->y_max << " - " << this->y_min << ")/(" << this->n_y << "+1)" << "];\n";
	(*file) << "X = [";
	for(int i=0; i<this->n_x-1; i++) {
		(*file) << this->x_coord(i, 0) << ", ";
	}
	(*file) << this->x_coord(this->n_x-1, 0) << "];\n";
	(*file) << "Y = [";
	for(int i=0; i<this->n_y-1; i++) {
		(*file) << this->y_coord(i, 0) << ", ";
	}
	(*file) << this->y_coord(this->n_y-1, 0) << "];\n";
}

void transport::write_frame(ofstream *file) {
	(*file) << "concentration = [";
	for(int i=this->n_y-1; i>=0; i--) {
		for(int j=0; j<this->n_x; j++) {
			(*file) << this->concentration(i*this->n_x + j, 0) << " ";
		}
		if(i!=0) (*file) << "; ";
	}
	(*file) << "];\n";
	(*file) << "pause(0.05);\n";
	(*file) << "surf(X, Y, rot90(concentration.'),'EdgeColor','none');\n";
	(*file) << "xlabel('x');\n";
	(*file) << "ylabel('y');\n";
	if(this->x_max - this->x_min > this->y_max - this->y_min) {
		(*file) << "xlim([" << this->x_min << ", " << this->x_max << "])\n";
		(*file) << "ylim([" << this->x_min << ", " << this->x_max << "])\n";
	} else {
		(*file) << "xlim([" << this->y_min << ", " << this->y_max << "])\n";
		(*file) << "ylim([" << this->y_min << ", " << this->y_max << "])\n";
	}
	(*file) << "view(2)\n";
	//(*file) << "zlim([0, 1])\n";
	//(*file) << "caxis([0.0 1.0])\n";
}

double transport::set_dt(double speed_convection, double speed_diffusion, double t_end) {
	double dxy = this->dx*this->dy;
	double dxx = this->dx*this->dx;
	double dyy = this->dy*this->dy;
	double dt_convection = this->cfl*(dxy/(this->dx + this->dy)) /speed_convection;
	double dt_diffusion = this->cfl*(0.5*(dxx*dyy)/(dxx + dyy)) /speed_diffusion;
	double dt = min(dt_convection, dt_diffusion);
	if(speed_convection == -1.0) {
		dt = dt_diffusion;
	}
	if(speed_diffusion == -1.0) {
		dt = dt_convection;
	}
	if(this->time+dt > t_end && t_end != -1.0) {
		dt = t_end - this->time;
	}
	return dt;
}

void transport::solve_darcy(darcy *d, darcy_double *d_double, solver *s, solver_double *s_double, starter *start, sparse_matrix *M) {
	//solve darcy
	if(start->darcy_device == "CPU") {
		if(start->solver_darcy == "LU") {
			s->solve_darcy_LU(d);
		} else if(start->solver_darcy == "GMRES") {
			if(M == NULL) {
				cout << "ERROR TRANSPORT LIMITER COUPLED GMRES\tPlease set a conditionner" << endl;
			} else {
				s->solve_darcy_GMRES(d, M, start->n_before_restart, start->max_it, start->tol);
			}
		} else if(start->solver_darcy == "GMRES_without_inver") {
			s->solve_darcy_GMRES_without_inver(d, start->gmres_without_inv_max_it, start->gmres_without_inv_tol, start->talkative);
		} else if(start->solver_darcy == "LU_double") {
			if(d_double == NULL) {
				cout << "ERROR TRANSPORT LIMITER COUPLED LU DOUBLE\tPlease set a darcy_double variable" << endl;
			} else {
				s_double->solve_darcy_LU(d_double);
				d->u = d_double->u;
			}
		}
	} else if(start->darcy_device == "GPU") {
		 cout << "GPU not available" << endl;
	}
	//darcy is solved
}
