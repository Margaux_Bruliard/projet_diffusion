#ifndef STARTER_HPP_INCLUDED
#define STARTER_HPP_INCLUDED

#include <string>

using namespace std;

class starter {
    public :
        starter(char *);
		//domain
		double x_min;
		double x_max;
		double y_min;
		double y_max;
		int n_x;	// at least 3 points, 2 on the borders, one to solve
		int n_y;	// at least 3 points, 2 on the borders, one to solve
		string solver_darcy;
		//param's for GMRES with restart
		bool talkative;
		int n_before_restart;
		int max_it;
		double tol;
		//param's for GMRES without inv
		int gmres_without_inv_max_it;
		double gmres_without_inv_tol;
		string darcy_device;
		int N_threads_darcy;
		double const_poisson;
		//transport parameters
		double end_time;
		string concentration_type;
		double x_pos_gaussian;
		double y_pos_gaussian;
		double x_var_gaussian;
		double y_var_gaussian;
		double const_gaussian;
		double background_density;
		double x_min_wave;
		double x_max_wave;
		double y_min_wave;
		double y_max_wave;
		double low_value_wave;
		double high_value_wave;
		string transport_integration;
		double backward_dt;
		string transport_mode;
		string solver_transport;
		double cfl;
		int ordre_transport_space;
		string limiter_name;
		string transport_device;
		int N_threads_transport;
		int iter_max;
		bool streamer;
		bool convection;
		bool diffusion;
		string output;
		
		int limiter_name_int;
};

#endif // STARTER_HPP_INCLUDED
