#ifndef DARCY_HPP_INCLUDED
#define DARCY_HPP_INCLUDED

#include "dense_matrix.hpp"
#include "permeability_matrix.hpp"
#include "functions_type.hpp"
#include "sparse_matrix.hpp"
#include "mesh.hpp"

class darcy {
	protected :
		dense_matrix delta_x_vec;
		dense_matrix delta_y_vec;
		dense_matrix x_coord;
		dense_matrix y_coord;
		double delta_x;
		double delta_y;
		double x_max;
		double y_max;
		int n_x;	//#of points on x axis
		int n_y;	//#of points on y axis
		void set_A_f();	//set A with the 9-points scheme and f with the boundary conditions
		void set_A_f_streamer();	//set A with the 9-points scheme and f with the boundary conditions, Dircihlet on x=xmin, x=xmax and Neumann on y=ymin, y=ymax
		void set_f_boundary();
		void set_f_boundary_streamer();
		functiontype2 function_boundary_condition;
		functiontype1 function_f;

	public :
		permeability_matrix K;
		double *device_K;
		double *device_u;
		double *device_max_speed_convection;
		double *device_max_speed_diffusion;
		double *host_max_speed_convection;
		double *host_max_speed_diffusion;
		double *device_boundary_condition;
		double *device_dx;
		double *device_dy;
		double *device_x;
		double *device_y;
		sparse_matrix A; //Au=f  //need to be accesible from solver
		dense_matrix f; //matrix (n*m, 1) -> vector //need to be accessible from solver
		dense_matrix boundary_condition; //matrix (n*m, 1) -> vector //need to be accessible from solver
		dense_matrix u; //solution
		double x_min;
		double y_min;
		darcy();
		darcy(double, double, double, double, int, int, mesh *, permeability_matrix, functiontype2, functiontype1, bool);
		void print();
		int get_n_x();
		int get_n_y();
		double get_delta_x();
		double get_delta_y();
		double error_L2_square(dense_matrix *);
		void compute_grad_C(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, functiontype3);
		void compute_diffusion_C(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, permeability_matrix *, double *);
		void compute_grad_h(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, functiontype3);
		void compute_velocity_h(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, double *);
		void compute_convection_h(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *);
		void compute_convection_h_MUSCL(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, functiontype4, string);

		void compute_flux_streamer(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, functiontype3, permeability_matrix *, functiontype3, double *, double *);
		void compute_E(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, functiontype3);
		void compute_E_norm(dense_matrix *, dense_matrix *, dense_matrix *);
		void compute_velocity_streamer(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, double *);
		void compute_velocity_norm(dense_matrix *, dense_matrix *, dense_matrix *);
		void compute_convection_streamer(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *);
		void compute_convection_uncentered_streamer(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *);
		void compute_grad_ne(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *);
		void compute_diffusion_streamer(dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, dense_matrix *, double *);
		void compute_flux_streamer_MUSCL_limiter(dense_matrix *, dense_matrix *, dense_matrix *, functiontype3, functiontype4, string, permeability_matrix *, functiontype3, double *, double *);
		void compute_flux_backward(dense_matrix *, sparse_matrix *, dense_matrix *, functiontype3, permeability_matrix *, functiontype3, double *, double *);

        void compute_convection_backward(dense_matrix *, sparse_matrix *, dense_matrix *, functiontype3, permeability_matrix *, functiontype3, double *);
        void compute_diffusion_backward(dense_matrix *, sparse_matrix *, dense_matrix *, functiontype3, permeability_matrix *, functiontype3, double *);

        void compute_flux_backward_MUSCL_limiter(dense_matrix *, sparse_matrix *, dense_matrix *, functiontype4, string, functiontype3, permeability_matrix *, functiontype3, double *, double *);

        void compute_convection_backward_MUSCL_limiter(dense_matrix *, sparse_matrix *, dense_matrix *, functiontype4, string, functiontype3, permeability_matrix *, functiontype3, double *);

};

#endif // DARCY_HPP_INCLUDED
