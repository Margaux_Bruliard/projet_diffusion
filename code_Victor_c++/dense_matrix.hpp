#ifndef DENSE_MATRIX_HPP_INCLUDED
#define DENSE_MATRIX_HPP_INCLUDED

#include <vector>
using namespace std;

class dense_matrix {
    protected :
        int rows;
        int cols;
        /*  DOUBLE INDEX
        m(0,0)          m(0,1)      ...     m(0,cols-1)
        m(1,0)          m(1,1)      ...     m(1,cols-1)
        ...             ...         ...     ...
        m(rows-1,0)     m(rows-1,1) ...     m(rows-1,cols-1)
        */
        /*  LINEAR
        m(0)                m(1)                ...     m(cols-1)
        m(cols)             m(cols+1)           ...     m(2*cols-1)
        ...                 ...                 ...     ...
        m((rows-1)*cols)    m((rows-1)*cols+1)  ...     m(rows*cols-1)
        */

    public :
        vector<double> m;
        dense_matrix();
        dense_matrix(int, int);
        int get_rows();
        int get_cols();
        double& operator()(int, int);
        const double& operator()(int, int) const;
        int i_index(int) const;
        int j_index(int) const;
        void print();
        void print_reshape(int, int);
        void save_reshape(int, int, double, double, double, double, const char *);
		void save_reshape_non_uniform(dense_matrix *, dense_matrix *, const char *);
        int linear_index(int, int) const;
		void set_centered_gaussian();
		double vec_dot_product(dense_matrix *);
        dense_matrix operator-(dense_matrix);
        dense_matrix operator+(dense_matrix);
        dense_matrix operator*(double);
		double vec_norm2();
		void set_column(int, dense_matrix *);
		void get_column(int, dense_matrix *);
		void extract(int, int, int, int, dense_matrix *);
		double *first_value_ptr();
};

#endif // DENSE_MATRIX_HPP_INCLUDED
