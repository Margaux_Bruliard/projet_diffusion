#include "permeability_domain.hpp"
#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>

permeability_domain::permeability_domain() {
}

permeability_domain::permeability_domain(double _x_min, double _x_max, double _y_min, double _y_max, double _v1, double _v2, double _v3, double _v4) {
	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;
	this->v1 = _v1;
	this->v2 = _v2;
	this->v3 = _v3;
	this->v4 = _v4;
}

bool permeability_domain::is_in(double x, double y) {
	return (x >= this->x_min && x <= this->x_max && y >= this->y_min && y <= this->y_max);
}
