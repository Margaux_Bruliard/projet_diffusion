#include "darcy_double.hpp"
#include "dense_matrix.hpp"
#include "permeability_matrix.hpp"
#include <iostream>
#include <math.h>

using namespace std;

darcy_double::darcy_double() {

}

darcy_double::darcy_double(double _x_min, double _x_max, double _y_min, double _y_max, int _n_x, int _n_y, permeability_matrix K, functiontype2 _function_boundary_condition, functiontype1 _function_f) {
	//domain [_x_min;_x_max]*[_y_min;_y_max]
	//_n_x steps in [_x_min;_x_max]
	//_n_y steps in [_y_min;_y_max]
	this->delta_x = (_x_max - _x_min)/(_n_x+1);
	this->delta_y = (_y_max - _y_min)/(_n_y+1);
	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;
	this->A = sparse_matrix_double(_n_x*_n_y, _n_x*_n_y, 0);
	this->f = dense_matrix(_n_x*_n_y, 1);
	this->boundary_condition = dense_matrix(_n_x*_n_y, 1);
	this->n_x = _n_x;
	this->n_y = _n_y;
	this->K = K;
	this->function_boundary_condition = _function_boundary_condition;
	this->function_f = _function_f;
	set_mesh();
	set_A_f();
}

void darcy_double::set_A_f() {
	int i, j, k;
	double x, y;
	double dx = this->delta_x;
	double dy = this->delta_y;
	double dxx = dx*dx;
	double dyy = dy*dy;
	double dxy = dx*dy;
	int N = this->n_x;
	for(i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(j=1; j<this->n_x-1; j++) {
			k = i*this->n_x+j;
			x = this->delta_x*(double)j + this->x_min + this->delta_x;
			y = this->delta_y*(double)i + this->y_min + this->delta_y;
			this->f(k, 0) = this->function_f(x, y);
			//Uk
			this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
			//Uk+1
			this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-1
			this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk+N
			this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N
			this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk+N+1
			this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
			//Uk+N-1
			this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N+1
			this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-N-1
			this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		}
	}
	//borders now
	//4 corners
	//bottom left
	i = 0; j = 0;
	k = i*this->n_x+j;
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
	//Uk+1
	this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 0)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N
	this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N
	//doesn't exist
	//this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 0)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk+N+1
	this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 0)*(this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 0)*(this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 0)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//this->f(k, 0) = 4.74;

	//bottom right
	i = 0; j = n_x-1;
	k = i*this->n_x+j;
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
	//Uk+1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 2)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-1
	this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N
	this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 2)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 2)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
	//Uk+N-1
	this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	//WRONG
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 2)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 2)*(-this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	
	//top left
	i = n_y-1; j = 0;
	k = i*this->n_x+j;
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
	//Uk+1
	this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 5)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk+N
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 5)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N
	this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 5)*(-this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 5)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
	//Uk-N+1
	this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-N-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 5)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));

	//top right
	i = n_y-1; j = n_x-1;
	k = i*this->n_x+j;
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	this->f(k, 0) = this->function_f(x, y);
	//Uk
	this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
	//Uk+1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 7)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-1
	this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk+N
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 7)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N
	this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk+N+1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 7)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	//Uk+N-1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 7)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
	//Uk-N+1
	//doesn't exist
	this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 7)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
	//Uk-N-1
	this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));

	//sides now
	//bottom
	i=0;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K_boundary(k, 4))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y-dy, 1)*(-this->K.get_K_boundary(k, 4)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 1)*(this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk-N-1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 1)*(-this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
	}

	//left
	j=0;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K_boundary(k, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y, 3)*(-this->K.get_K_boundary(k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk+N+1
		this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 3)*(this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K_boundary(k, 2)/(4*dxy));
		//Uk-N+1
		this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y-dy, 3)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}

	//right
	j=n_x-1;
	for(i=1; i<this->n_y-1; i++) {
		k = i*this->n_x+j;
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K_boundary(k, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y, 4)*(-this->K.get_K_boundary(k, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 4)*(-this->K.get_K_boundary(k, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
		//Uk+N-1
		this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y-dy, 4)*(this->K.get_K_boundary(k, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1
		this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}

	//top
	i=n_y-1;
	for(j=1; j<this->n_x-1; j++) {
		k = i*this->n_x+j;
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		this->f(k, 0) = this->function_f(x, y);
		//Uk
		this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K_boundary(k, 4) + this->K.get_K(k, k-N, 2))/dyy);
		//Uk+1
		this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-1
		this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K_boundary(k, 3)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk+N
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x, y+dy, 6)*(-this->K.get_K_boundary(k, 4)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N
		this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk+N+1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x+dx, y+dy, 6)*(-this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K_boundary(k, 3)/(4*dxy));
		//Uk+N-1
		//doesn't exist
		this->boundary_condition(k, 0) -= this->function_boundary_condition(x-dx, y+dy, 6)*(this->K.get_K_boundary(k, 3)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
		//Uk-N+1
		this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
		//Uk-N-1	
		this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
	}
	this->f = this->f + this->boundary_condition;
}

bool darcy_double::is_border(int k) {
	int i = this->mesh.i_index(k);
	int j = this->mesh.j_index(k);
	////cout << "k:" << k << " i:" << i << " j:" << j << endl;
	if(i==0 || i==n_y-1 || j==0 || j==n_x-1)
		return true;
	return false;
}
/*
void darcy_double::set_A_line(int k) {
	double dxx = this->delta_x*this->delta_x;
	double dyy = this->delta_y*this->delta_y;
	double dxy = this->delta_x*this->delta_y;
	int N = this->n_x;
	//Uk
	this->A(k, k) = (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx;
	this->A(k, k) += (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy;


	//Uk+1
	this->A(k, k+1) = -this->K.get_K(k, k+1, 1)/dxx;
	this->A(k, k+1) -= this->K.get_K(k, k+N, 1)/(4*dxy);
	this->A(k, k+1) += this->K.get_K(k-N, k, 1)/(4*dxy);

	//Uk-1
	this->A(k, k-1) = -this->K.get_K(k-1, k, 1)/dxx;
	this->A(k, k-1) += this->K.get_K(k, k+N, 1)/(4*dxy);
	this->A(k, k-1) -= this->K.get_K(k-N, k, 1)/(4*dxy);


	//Uk+N
	this->A(k, k+N) = -this->K.get_K(k, k+N, 2)/dyy;
	this->A(k, k+N) -= this->K.get_K(k, k+1, 2)/(4*dxy);
	this->A(k, k+N) += this->K.get_K(k-1, k, 2)/(4*dxy);

	//Uk-N
	this->A(k, k-N) = -this->K.get_K(k-N, k, 2)/dyy;
	this->A(k, k-N) += this->K.get_K(k, k+1, 2)/(4*dxy);
	this->A(k, k-N) -= this->K.get_K(k-1, k, 2)/(4*dxy);

	//Uk+N+1
	this->A(k, k+N+1) = -this->K.get_K(k, k+1, 2)/(4*dxy);
	this->A(k, k+N+1) -= this->K.get_K(k, k+N, 1)/(4*dxy);

	//Uk+N-1
	this->A(k, k+N-1) = this->K.get_K(k, k+N, 1)/(4*dxy);
	this->A(k, k+N-1) += this->K.get_K(k-1, k, 2)/(4*dxy);

	//Uk-N+1
	this->A(k, k-N+1) = this->K.get_K(k, k+1, 2)/(4*dxy);
	this->A(k, k-N+1) += this->K.get_K(k-N, k, 1)/(4*dxy);

	//Uk-N-1
	this->A(k, k-N-1) = -this->K.get_K(k-1, k, 2)/(4*dxy);
	this->A(k, k-N-1) -= this->K.get_K(k-N, k, 1)/(4*dxy);
}*/

void darcy_double::print() {
	this->A.print_column_by_column();
	//cout << endl;
	this->f.print();
}

void darcy_double::set_mesh() {	//not actually used !!! only for the linear_index
	this->mesh = dense_matrix(this->n_y,this->n_x);
	for(int i=0; i<this->n_y; i++) {
		for(int j=0; j<this->n_x; j++) {
			this->mesh(i, j) = 1.0;   //unfinished !!!
		}
	}
}

int darcy_double::get_n_x() {
	return this->n_x;
}

int darcy_double::get_n_y() {
	return this->n_y;
}

double darcy_double::get_delta_x() {
	return this->delta_x;
}

double darcy_double::get_delta_y() {
	return this->delta_y;
}

double darcy_double::error_L2_square(dense_matrix *exact_sol) {
	double error = 0.0;
	for(int i=0; i<(*exact_sol).get_rows(); i++) {
		double tmp = this->u(i, 0) - (*exact_sol)(i, 0);
		error += tmp*tmp;
	}
	return error*this->delta_x*this->delta_y;
}

double darcy_double::compute_flux(dense_matrix *concentration, dense_matrix *flux, functiontype1 neumann_condition) {	//u is the solution of darcy_double's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}
	dense_matrix save_grad_x_droite(this->n_x*this->n_y, 1);
	dense_matrix save_grad_x_haut(this->n_x*this->n_y, 1);
	dense_matrix save_grad_x_gauche(this->n_x*this->n_y, 1);
	dense_matrix save_grad_x_bas(this->n_x*this->n_y, 1);
	dense_matrix save_grad_y_droite(this->n_x*this->n_y, 1);
	dense_matrix save_grad_y_haut(this->n_x*this->n_y, 1);
	dense_matrix save_grad_y_gauche(this->n_x*this->n_y, 1);
	dense_matrix save_grad_y_bas(this->n_x*this->n_y, 1);
	int N = this->n_x;

	double q_x = 0.0;
	double q_y = 0.0;
	double C = 0.0;
	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	double max_speed = 0.0;

	int i, j, k;

	double x, y;
			
	//cout << "middle" << endl;	
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//cout << "flux(" << i << "," << j << ")" << endl;
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			//cout << k << endl;
			k = i*this->n_x+j;
			//right edge
			grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/this->delta_x;
			grad_u_y = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)))*this->delta_y;
			q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
			if(q_x>0) { //goes from left to right
				C = (*concentration)(k, 0);
			} else if(q_x<0) { //goes from right to left
				C = (*concentration)(k+1, 0);
			} else {
				C = 0.0;
			}
			//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
			(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_droite(k, 0) = grad_u_x;
			save_grad_y_droite(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

			//up edge
			grad_u_x = (0.25*(this->u(k+N, 0) + this->u(k+N+1, 0) + this->u(k, 0) + this->u(k+1, 0)) - 0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)))*this->delta_x;
			grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/this->delta_y;
			q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
			if(q_y>0) { //goes from down to up
				C = (*concentration)(k, 0);
			} else if(q_y<0) { //goes from up to down
				C = (*concentration)(k+N, 0);
			} else {
				C = 0.0;
			}
			//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
			(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_haut(k, 0) = grad_u_x;
			save_grad_y_haut(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

			//left edge
			grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/this->delta_x;
			grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))*this->delta_y;
			q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			//test
			q_x = -q_x;
			if(q_x>0) { //goes from left to right
				C = (*concentration)(k, 0);
			} else if(q_x<0) { //goes from right to left
				C = (*concentration)(k-1, 0);
			} else {
				C = 0.0;
			}
			//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
			(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_gauche(k, 0) = grad_u_x;
			save_grad_y_gauche(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

			//down edge
			grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))*this->delta_x;
			grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/this->delta_y;
			q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
			//test
			q_y = -q_y;
			if(q_y>0) { //goes from down to up
				C = (*concentration)(k, 0);
			} else if(q_y<0) { //goes from up to down
				C = (*concentration)(k-N, 0);
			} else {
				C = 0.0;
			}
			//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
			(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_bas(k, 0) = grad_u_x;
			save_grad_y_bas(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;
		}
	}
	//borders now
	//4 corners
	//bottom left
	//cout << "flux(" << "0" << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)
	
    i = 0; j = 0;
    k = i*this->n_x+j;
	//right edge
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/this->delta_x;
	grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))*this->delta_y;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		C = (*concentration)(k+1, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_y*q_x*C;
	//save_grad_x(k, 0) += q_x;
	save_grad_x_droite(k, 0) = grad_u_x;
	save_grad_y_droite(k, 0) = grad_u_y;
	if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

	//up edge
	grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))*this->delta_x;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/this->delta_y;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_y<0) { //goes from up to down
		C = (*concentration)(k+N, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
	//save_grad_y(k, 0) += q_y;
	save_grad_x_haut(k, 0) = grad_u_x;
	save_grad_y_haut(k, 0) = grad_u_y;
	if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

	//left edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
			//test
			q_x = -q_x;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_x*C;
	//save_grad_y(k, 0) += q_x;
	save_grad_x_gauche(k, 0) = grad_u_x;
	save_grad_y_gauche(k, 0) = grad_u_y;
	if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

	//down edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
			//test
			q_y = -q_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
	//save_grad_y(k, 0) += q_y;
	save_grad_x_bas(k, 0) = grad_u_x;
	save_grad_y_bas(k, 0) = grad_u_y;
	if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

	//bottom right
	//cout << "flux(" << "0" << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

    i = 0; j = n_x-1;
    k = i*this->n_x+j;

	//right edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_x*C;
	//save_grad_y(k, 0) += q_x;
	save_grad_x_droite(k, 0) = grad_u_x;
	save_grad_y_droite(k, 0) = grad_u_y;
	if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

	//up edge
	grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))*this->delta_x;
	grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/this->delta_y;
	q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_y<0) { //goes from up to down
		C = (*concentration)(k+N, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_haut(k, 0) = grad_u_x;
			save_grad_y_haut(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

	//left edge
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/this->delta_x;
	grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))*this->delta_y;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			//test
			q_x = -q_x;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		C = (*concentration)(k-1, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_gauche(k, 0) = grad_u_x;
			save_grad_y_gauche(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

	//down edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
			//test
			q_y = -q_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from up to down
		//C = (*concentration)(k-N, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
	//save_grad_y(k, 0) += q_y;
	save_grad_x_bas(k, 0) = grad_u_x;
	save_grad_y_bas(k, 0) = grad_u_y;
	if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;
	
	//top left
	//cout << "flux(" << this->n_y-1 << "," << "0" << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

    i = n_y-1; j = 0;
    k = i*this->n_x+j;
	//right edge
	grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/this->delta_x;
	grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))*this->delta_y;
	q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		C = (*concentration)(k+1, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_droite(k, 0) = grad_u_x;
			save_grad_y_droite(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

	//up edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
	//save_grad_y(k, 0) += q_y;
	save_grad_x_haut(k, 0) = grad_u_x;
	save_grad_y_haut(k, 0) = grad_u_y;
	if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

	//left edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
			//test
			q_x = -q_x;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k-1, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_x*C;
	//save_grad_y(k, 0) += q_x;
	save_grad_x_gauche(k, 0) = grad_u_x;
	save_grad_y_gauche(k, 0) = grad_u_y;
	if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

	//down edge
	grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))*this->delta_x;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/this->delta_y;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//test
			q_y = -q_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_y<0) { //goes from up to down
		C = (*concentration)(k-N, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_bas(k, 0) = grad_u_x;
			save_grad_y_bas(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;
	
	//top right
	//cout << "flux(" << this->n_y-1 << "," << this->n_x-1 << ")" << endl;
	//q=-K*grad(u)
	//flux=q*concentration*mesure(edge)

    i = n_y-1; j = n_x-1;
    k = i*this->n_x+j;

	//right edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		//C = (*concentration)(k+1, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_x*C;
	//save_grad_y(k, 0) += q_x;
	save_grad_x_droite(k, 0) = grad_u_x;
	save_grad_y_droite(k, 0) = grad_u_y;
	if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

	//up edge
	//doesn't exist
	//neumann condition
	x = this->delta_x*(double)j + this->x_min + this->delta_x;
	y = this->delta_y*(double)i + this->y_min + this->delta_y;
	grad_u_x = neumann_condition(x, y);
	grad_u_y = neumann_condition(x, y);
	q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from up to down
		//C = (*concentration)(k+N, 0);	//doesn't exist
		C = 0.0;
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
	//save_grad_y(k, 0) += q_y;
	save_grad_x_haut(k, 0) = grad_u_x;
	save_grad_y_haut(k, 0) = grad_u_y;
	if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

	//down edge
	grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))*this->delta_x;
	grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/this->delta_y;
	q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
	//test
			q_y = -q_y;
	if(q_y>0) { //goes from down to up
		C = (*concentration)(k, 0);
	} else if(q_y<0) { //goes from up to down
		C = (*concentration)(k-N, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_bas(k, 0) = grad_u_x;
			save_grad_y_bas(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

	//left edge
	grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/this->delta_x;
	grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-N-1, 0) + this->u(k-N, 0)))*this->delta_y;
	q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			//test
			q_x = -q_x;
	if(q_x>0) { //goes from left to right
		C = (*concentration)(k, 0);
	} else if(q_x<0) { //goes from right to left
		C = (*concentration)(k-1, 0);
	} else {
		C = 0.0;
	}
	//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
	(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_gauche(k, 0) = grad_u_x;
			save_grad_y_gauche(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;
	
	//sides now
	//bottom
	for(int j=1; j<this->n_x-1; j++) {  //goes through each cells "in"
		int i=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		//right edge
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/this->delta_x;
		grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))*this->delta_y;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			C = (*concentration)(k+1, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_droite(k, 0) = grad_u_x;
			save_grad_y_droite(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

		//up edge
		grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))*this->delta_x;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/this->delta_y;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_y<0) { //goes from up to down
			C = (*concentration)(k+N, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

		//left edge
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/this->delta_x;
		grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))*this->delta_y;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			C = (*concentration)(k-1, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_gauche(k, 0) = grad_u_x;
			save_grad_y_gauche(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

		//down edge
		//doesn't exist
		//neumann condition
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		grad_u_x = neumann_condition(x, y);
		grad_u_y = neumann_condition(x, y);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
				//test
				q_y = -q_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from up to down
			//C = (*concentration)(k-N, 0);	//doesn't exist
			C = 0.0;
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
		//save_grad_y(k, 0) += q_y;
		save_grad_x_bas(k, 0) = grad_u_x;
		save_grad_y_bas(k, 0) = grad_u_y;
		if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;
		////save_grad_y(k, 0) = 50;
	}
	//left
	for(int i=1; i<this->n_y-1; i++) {
		int j=0;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;

		//right edge
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/this->delta_x;
		grad_u_y = (0.5*(this->u(k+N, 0) + this->u(k+N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k+1, 0)))*this->delta_y;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			C = (*concentration)(k+1, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_droite(k, 0) = grad_u_x;
			save_grad_y_droite(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

		//up edge
		grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))*this->delta_x;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/this->delta_y;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_y<0) { //goes from up to down
			C = (*concentration)(k+N, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_haut(k, 0) = grad_u_x;
			save_grad_y_haut(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

		//left edge
		//doesn't exist
		//neumann condition
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		grad_u_x = neumann_condition(x, y);
		grad_u_y = neumann_condition(x, y);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
				//test
				q_x = -q_x;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k-1, 0);	//doesn't exist
			C = 0.0;
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_x*C;
		//save_grad_y(k, 0) += q_x;
		save_grad_x_gauche(k, 0) = grad_u_x;
		save_grad_y_gauche(k, 0) = grad_u_y;
		if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

		//down edge
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))*this->delta_x;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/this->delta_y;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//test
				q_y = -q_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_y<0) { //goes from up to down
			C = (*concentration)(k-N, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_bas(k, 0) = grad_u_x;
			save_grad_y_bas(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;
		////save_grad_y(k, 0) = 80;
	}
	//right
	//cout << "right" << endl;
	for(int i=1; i<this->n_y-1; i++) {
		int j=this->n_x-1;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;

		//right edge
		//doesn't exist
		//neumann condition
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		grad_u_x = neumann_condition(x, y);
		grad_u_y = neumann_condition(x, y);
		q_x = -this->K.get_K_boundary(k, 1)*grad_u_x - this->K.get_K_boundary(k, 2)*grad_u_y;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			//C = (*concentration)(k+1, 0);	//doesn't exist
			C = 0.0;
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_x*C;
		//save_grad_y(k, 0) += q_x;
		save_grad_x_droite(k, 0) = grad_u_x;
		save_grad_y_droite(k, 0) = grad_u_y;
		if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;
	
		//up edge
		grad_u_x = (0.5*(this->u(k+N+1, 0) + this->u(k+1, 0)) - 0.5*(this->u(k+N, 0) + this->u(k, 0)))*this->delta_x;
		grad_u_y = (this->u(k+N, 0) - this->u(k, 0))/this->delta_y;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_y<0) { //goes from up to down
			C = (*concentration)(k+N, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_haut(k, 0) = grad_u_x;
			save_grad_y_haut(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;


		//left edge
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/this->delta_x;
		grad_u_y = (0.5*(this->u(k+N-1, 0) + this->u(k+N, 0)) - 0.5*(this->u(k-1, 0) + this->u(k, 0)))*this->delta_y;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			C = (*concentration)(k-1, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_gauche(k, 0) = grad_u_x;
			save_grad_y_gauche(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;
		//down edge
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))*this->delta_x;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/this->delta_y;
		//cout << "this->K.get_K(k, k+N, 1) :" << this->K.get_K(k, k+N, 1) << endl;
		//cout << "this->K.get_K(k, k+N, 2) :" << this->K.get_K(k, k+N, 2) << endl;
		q_y = -this->K.get_K(k, k+N, 1)*grad_u_x - this->K.get_K(k, k+N, 2)*grad_u_y;
		//test
				q_y = -q_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_y<0) { //goes from up to down
			C = (*concentration)(k-N, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_bas(k, 0) = grad_u_x;
			save_grad_y_bas(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;
		////save_grad_y(k, 0) = 110;
	}

	//top
	//cout << "top" << endl;
	for(int j=1; j<this->n_x-1; j++) {
		int i=this->n_y-1;
		//cout << "flux(" << i << "," << j << ")" << endl;
		//q=-K*grad(u)
		//flux=q*concentration*mesure(edge)

		k = i*this->n_x+j;
		//cout << k << endl;
		
		//right edge
		grad_u_x = (this->u(k+1, 0) - this->u(k, 0))/this->delta_x;
		grad_u_y = (0.5*(this->u(k, 0) + this->u(k+1, 0)) - 0.5*(this->u(k-N, 0) + this->u(k-N+1, 0)))*this->delta_y;
		q_x = -this->K.get_K(k, k+1, 1)*grad_u_x - this->K.get_K(k, k+1, 2)*grad_u_y;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			C = (*concentration)(k+1, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_droite(k, 0) = grad_u_x;
			save_grad_y_droite(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;

		//up edge
		//doesn't exist
		//neumann condition
		x = this->delta_x*(double)j + this->x_min + this->delta_x;
		y = this->delta_y*(double)i + this->y_min + this->delta_y;
		grad_u_x = neumann_condition(x, y);
		grad_u_y = neumann_condition(x, y);
		q_y = -this->K.get_K_boundary(k, 3)*grad_u_x - this->K.get_K_boundary(k, 4)*grad_u_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from up to down
			//C = (*concentration)(k+N, 0);	//doesn't exist
			C = 0.0;
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
		//save_grad_y(k, 0) += q_y;
		save_grad_x_haut(k, 0) = grad_u_x;
		save_grad_y_haut(k, 0) = grad_u_y;
		if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;

		//left edge
		grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/this->delta_x;
		grad_u_y = (0.5*(this->u(k-1, 0) + this->u(k, 0)) - 0.5*(this->u(k-N-1, 0) + this->u(k-N, 0)))*this->delta_y;
		q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
		//test
		q_x = -q_x;
		if(q_x>0) { //goes from left to right
			C = (*concentration)(k, 0);
		} else if(q_x<0) { //goes from right to left
			C = (*concentration)(k-1, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_x:" << q_x << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_y*q_x*C;
			//save_grad_x(k, 0) += q_x;
			save_grad_x_gauche(k, 0) = grad_u_x;
			save_grad_y_gauche(k, 0) = grad_u_y;
			if(q_x > max_speed || -q_x > max_speed) max_speed = q_x;
		//down edge
		grad_u_x = (0.5*(this->u(k+1, 0) + this->u(k-N+1, 0)) - 0.5*(this->u(k, 0) + this->u(k-N, 0)))*this->delta_x;
		grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/this->delta_y;
		q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
		//test
				q_y = -q_y;
		if(q_y>0) { //goes from down to up
			C = (*concentration)(k, 0);
		} else if(q_y<0) { //goes from up to down
			C = (*concentration)(k-N, 0);
		} else {
			C = 0.0;
		}
		//cout << " q_y:" << q_y << " grad_x:" << grad_u_x << " grad_y:" << grad_u_y << " C:" << C << endl;
		(*flux)(k, 0) += this->delta_x*q_y*C;
			//save_grad_y(k, 0) += q_y;
			save_grad_x_bas(k, 0) = grad_u_x;
			save_grad_y_bas(k, 0) = grad_u_y;
			if(q_y > max_speed || -q_y > max_speed) max_speed = q_y;
		////save_grad_y(k, 0) = 140;
	}
	(*flux).save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "flux");
	//save_grad_x.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_x");
	//save_grad_y.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_y");
	save_grad_x_droite.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_x_droite");
	save_grad_x_haut.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_x_haut");
	save_grad_x_gauche.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_x_gauche");
	save_grad_x_bas.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_x_bas");
	save_grad_y_droite.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_y_droite");
	save_grad_y_haut.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_y_haut");
	save_grad_y_gauche.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_y_gauche");
	save_grad_y_bas.save_reshape(this->n_x, this->n_y, this->x_min, this->x_max, this->y_min, this->y_max, "grad_y_bas");
	return max_speed;
}
