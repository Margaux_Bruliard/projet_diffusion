#ifndef FUNCTIONS_TYPE_HPP_INCLUDED
#define FUNCTIONS_TYPE_HPP_INCLUDED

#include <string>

typedef double (*functiontype1)(double, double);
typedef double (*functiontype2)(double, double, int);
typedef double (*functiontype3)(double, double, int, bool);
typedef double (*functiontype4)(double, double, string);


#endif // FUNCTIONS_TYPE_HPP_INCLUDED
