#ifndef DARCY_DOUBLE_HPP_INCLUDED
#define DARCY_DOUBLE_HPP_INCLUDED

#include "dense_matrix.hpp"
#include "permeability_matrix.hpp"
#include "functions_type.hpp"
#include "sparse_matrix.hpp"
#include "sparse_matrix_double.hpp"

class darcy_double {
    protected :
        permeability_matrix K;
        double delta_x;
        double delta_y;
        double x_max;
        double y_max;
        int n_x;    //#of points on x axis
        int n_y;    //#of points on y axis
        void set_A_f();    //set A with the 9-points scheme and f with the boundary conditions
        bool is_border(int);
        void set_mesh();
        functiontype2 function_boundary_condition;
        functiontype1 function_f;

    public :
        sparse_matrix_double A; //Au=f  //need to be accesible from solver
        dense_matrix f; //matrix (n*m, 1) -> vector //need to be accessible from solver
        dense_matrix boundary_condition; //matrix (n*m, 1) -> vector //need to be accessible from solver
        dense_matrix mesh;  //need to be accessible to create exact_sol
        dense_matrix u; //solution
        double x_min;
        double y_min;
        darcy_double();
        darcy_double(double, double, double, double, int, int, permeability_matrix, functiontype2, functiontype1);
        void print();
        int get_n_x();
        int get_n_y();
        double get_delta_x();
        double get_delta_y();
        double error_L2_square(dense_matrix *);
        double compute_flux(dense_matrix *, dense_matrix *, functiontype1);

};

#endif // DARCY_DOUBLE_HPP_INCLUDED
