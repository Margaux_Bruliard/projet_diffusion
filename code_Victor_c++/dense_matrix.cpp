#include "dense_matrix.hpp"
#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>

dense_matrix::dense_matrix() {
}

dense_matrix::dense_matrix(int _rows, int _cols) {
	this->cols = _cols;
	this->rows = _rows;
	this->m = vector<double>(this->cols*this->rows);
	//cout << dense_matrix::linear_index(1, 2) << endl;
	//cout << "_rows" << _rows << endl;
	//cout << "_cols" << _cols << endl;
	//cout << "this->cols" << this->cols << endl;
	//cout << "this->rows" << this->rows << endl;
}

int dense_matrix::get_rows() {
	return this->rows;
}

int dense_matrix::get_cols() {
	return this->cols;
}

int dense_matrix::linear_index(int _i, int _j) const {
	return _i*this->cols + _j;
}

int dense_matrix::i_index(int _k) const {
	//cout << endl << _k << "/" << this->cols << endl;
	return int(_k/this->cols);  //int division
}

int dense_matrix::j_index(int _k) const {
	//cout << endl << _k << "%" << this->cols << endl;
	return _k%this->cols;
}

double& dense_matrix::operator()(int _i, int _j) {
	return this->m[this->linear_index(_i, _j)];
}

const double& dense_matrix::operator()(int _i, int _j) const {
	return this->m[this->linear_index(_i, _j)];
}

void dense_matrix::print() {
	for(int i=0; i<this->rows; i++) {
		for(int j=0; j<this->cols; j++) {
			cout << this->m[this->linear_index(i, j)] << "\t";
			//cout << "(" << i << ", " << j << ")";
		}
		cout << endl;
	}
}

void dense_matrix::print_reshape(int n_x, int n_y) {	//only used to print vector solution
	for(int i=n_y-1; i>=0; i--) {
		for(int j=0; j<n_x; j++) {
			cout << this->m[i*n_x + j] << "\t";
		}
		cout << endl;
	}
}

void dense_matrix::save_reshape(int n_x, int n_y, double x_min, double x_max, double y_min, double y_max, const char *name) {	//only used to print vector solution
	ofstream save_file;
	//save_file.open("save_result.m");
	/*char * name_ext;
	strcpy(name_ext, name);
	strcat(name_ext, ".m");
	save_file.open(name_ext);
	save_file << name << " = [";*/
	char name_ext[120];
	strcpy(name_ext, name);
	strcat(name_ext, ".m");
	save_file.open(name_ext);

	save_file << "X = [" << x_min << "+(" << x_max << " - " << x_min << ")/(" << n_x << "+1)" << ":" << "(" << x_max << " - " << x_min << ")/(" << n_x << "+1)" << ":" << x_max << "-(" << x_max << " - " << x_min << ")/(" << n_x << "+1)" << "];\n";
	save_file << "Y = [" << y_min << "+(" << y_max << " - " << y_min << ")/(" << n_y << "+1)" << ":" << "(" << y_max << " - " << y_min << ")/(" << n_y << "+1)" << ":" << y_max << "-(" << y_max << " - " << y_min << ")/(" << n_y << "+1)" << "];\n";
    
	save_file << name << "_m = [";
	for(int i=n_y-1; i>=0; i--) {
		for(int j=0; j<n_x; j++) {
			save_file << this->m[i*n_x + j] << " ";
		}
		if(i!=0) save_file << "; ";
	}
	save_file << "];\n";
	save_file << "figure(2);\n";
	//save_file << "surf(X, Y, rot90("<<name<<"_m.'));\n";
	save_file << name << "_m=rot90("<<name<<"_m.');\n";
	save_file << "surf(X, Y, "<<name<<"_m);\n";
	save_file << "xlabel('x');\n";
	save_file << "ylabel('y');\n";
	save_file << "zlabel('z');\n";
	save_file.close();
	cout << "Matrix saved in " << name_ext << endl;
}

void dense_matrix::save_reshape_non_uniform(dense_matrix *x_coord, dense_matrix *y_coord, const char *name) {	//only used to print vector solution
	ofstream save_file;
	//save_file.open("save_result.m");
	/*char * name_ext;
	strcpy(name_ext, name);
	strcat(name_ext, ".m");
	save_file.open(name_ext);
	save_file << name << " = [";*/
	char name_ext[120];
	strcpy(name_ext, name);
	strcat(name_ext, ".m");
	save_file.open(name_ext);

	int n_x = x_coord->get_rows();
	int n_y = y_coord->get_rows();

	save_file << "X = [";
	for(int i=0; i<n_x-1; i++) {
		save_file << (*x_coord)(i, 0) << ", ";
	}
	save_file << (*x_coord)(n_x-1, 0) << "];\n";
	save_file << "Y = [";
	for(int i=0; i<n_y-1; i++) {
		save_file << (*y_coord)(i, 0) << ", ";
	}
	save_file << (*y_coord)(n_y-1, 0) << "];\n";

	save_file << name << "_m = [";
	for(int i=n_y-1; i>=0; i--) {
		for(int j=0; j<n_x; j++) {
			save_file << this->m[i*n_x + j] << " ";
		}
		if(i!=0) save_file << "; ";
	}
	save_file << "];\n";
	save_file << "figure(2);\n";
	//save_file << "surf(X, Y, rot90("<<name<<"_m.'));\n";
	save_file << name << "_m=rot90("<<name<<"_m.');\n";
	save_file << "surf(X, Y, "<<name<<"_m);\n";
	save_file << "xlabel('x');\n";
	save_file << "ylabel('y');\n";
	save_file << "zlabel('z');\n";
	save_file.close();
	cout << "Matrix saved in " << name_ext << endl;
}

double dense_matrix::vec_dot_product(dense_matrix *b) {
	double res = 0.0;
	for(int i=0; i<this->rows; i++) {
		res += this->m[this->linear_index(i, 0)]*(*b)(i, 0);
	}
	return res;
}

dense_matrix dense_matrix::operator-(dense_matrix matrix) {
	if(this->get_cols() != matrix.get_cols() || this->get_rows() != matrix.get_rows()) {
		cout << "ERROR DENSE_MATRIX OPERATOR -" << endl;
		return matrix;
	} else {
		dense_matrix b(this->get_rows(), this->get_cols());
		for(int i=0; i<b.get_rows(); i++) {
			for(int j=0; j<b.get_cols(); j++) {
				b(i, j) = this->m[this->linear_index(i, j)] - matrix(i, j);
			}
		}
		return b;
	}
}

dense_matrix dense_matrix::operator+(dense_matrix matrix) {
	if(this->get_cols() != matrix.get_cols() || this->get_rows() != matrix.get_rows()) {
		cout << "ERROR DENSE_MATRIX OPERATOR +" << endl;
		return matrix;
	} else {
		dense_matrix b(this->get_rows(), this->get_cols());
		for(int i=0; i<b.get_rows(); i++) {
			for(int j=0; j<b.get_cols(); j++) {
				b(i, j) = this->m[this->linear_index(i, j)] + matrix(i, j);
			}
		}
		return b;
	}
}

dense_matrix dense_matrix::operator*(double coef) {
	dense_matrix b(this->get_rows(), this->get_cols());
	for(int i=0; i<b.get_rows(); i++) {
		for(int j=0; j<b.get_cols(); j++) {
			b(i, j) = coef*this->m[this->linear_index(i, j)];
		}
	}
	return b;
}

double dense_matrix::vec_norm2() {
	if(this->get_cols() != 1) {
		cout << "ERROR DENSE_MATRIX NORM2" << endl;
	}
	double norm = 0.0;
	for(int i=0; i<this->get_rows(); i++) {
		norm += this->m[this->linear_index(i, 0)]*this->m[this->linear_index(i, 0)];
	}
	return sqrt(norm);
}

void dense_matrix::set_column(int col, dense_matrix *v) {
	for(int i=0; i<this->rows; i++) {
		(*this)(i, col) = (*v)(i, 0);
	}
}

void dense_matrix::get_column(int col, dense_matrix *v) {
	for(int i=0; i<this->rows; i++) {
		(*v)(i, 0) = (*this)(i, col);
	}
}

void dense_matrix::extract(int first_row, int last_row, int first_col, int last_col, dense_matrix *M) {
	for(int i=first_row; i<=last_row; i++) {
		for(int j=first_col; j<=last_col; j++) {
			(*M)(i-first_row, j-first_col) = (*this)(i, j);
		}
	}
}

double *dense_matrix::first_value_ptr() {
	return &(this->m[0]);
}
