#include <iostream>
#include "solver_double.hpp"
#include <ctime>

using namespace std;

solver_double::solver_double() {
	//this->rayon_sup = -1; //only for 9 point stencil
	//this->rayon_inf = -1; //only for 9 point stencil
}
void solver_double::solve_darcy_LU(darcy_double *d) {
	this->rayon_sup = (*d).get_n_x() + 2; //only for 9 point stencil
	this->rayon_inf = (*d).get_n_x() + 2; //only for 9 point stencil
	//cout << "rayon_sup: " << (*d).get_n_x() + 1 << endl;
	//cout << "rayon_inf: " << (*d).get_n_x() + 1 << endl;
	this->solve_LU(&d->A, &d->f, &(*d).u);
}

void solver_double::solve_LU(sparse_matrix_double *A, dense_matrix *f, dense_matrix *x) {   //Ax=f
	this->fact_LU(A);   //A=LU
	dense_matrix y;
	this->res_tri_inf(&this->L, f, &y);	//Ly=f
	this->res_tri_sup(&this->U, &y, x);	//Ux=y
}

void solver_double::fact_LU(sparse_matrix_double *A) {
	int n = A->get_cols();  //square cols=rows
	this->L = sparse_matrix_double(n, n, 0);
	this->U = sparse_matrix_double(n, n, 0);
	for(int i=0; i<n; i++) {	//for each ligne of U and column of L
		int lim_sup = i+this->rayon_sup;
		if(lim_sup>n) {
			lim_sup = n;
		}
		int lim_inf = i-this->rayon_sup;
		if(lim_inf<0) {
			lim_inf = 0;
		}
		//for(int j=i; j<n; j++) {
		/*****method B******/
		int index_cols_A_line_by_line = (*A).rows_first_position_line_by_line[i];
		int index_rows_A_column_by_column = (*A).cols_first_position_column_by_column[i];
		/*****method B******/
		for(int j=i; j<lim_sup; j++) {  //for each element in the upper triangle of U
			/*****method A******/
			//double value = (*A)(i, j);
			/*****method A******/
			/*****method B******/	//line_by_line	OK!
			double value = 0.0;
			while((*A).cols_index_line_by_line[index_cols_A_line_by_line] < j && index_cols_A_line_by_line <(*A).rows_first_position_line_by_line[i+1]) {
				index_cols_A_line_by_line++;
			}
			if((*A).cols_index_line_by_line[index_cols_A_line_by_line] == j && index_cols_A_line_by_line <(*A).rows_first_position_line_by_line[i+1]) {
				value = (*A).values_line_by_line[index_cols_A_line_by_line];
				index_cols_A_line_by_line++;
			}
			/*****method B******/
			//for(int k=0; k<i; k++) {
			int index_cols_L_line_by_line = L.rows_first_position_line_by_line[i];
			int index_rows_U_column_by_column = U.cols_first_position_column_by_column[j];	//A(j, i)
			for(int k=lim_inf; k<i; k++) {
				/*****method A******/
				//double L_value = this->L(i, k);
				/*****method A******/
				/*****method B******/	//line_by_line	OK!
				double L_value = 0.0;
				while(index_cols_L_line_by_line < L.cols_index_line_by_line.size() && L.cols_index_line_by_line[index_cols_L_line_by_line] < k && index_cols_L_line_by_line < L.rows_first_position_line_by_line[i+1]) {
					index_cols_L_line_by_line++;
				}
				if(index_cols_L_line_by_line < L.cols_index_line_by_line.size() && L.cols_index_line_by_line[index_cols_L_line_by_line] == k && index_cols_L_line_by_line <L.rows_first_position_line_by_line[i+1]) {
					L_value = L.values_line_by_line[index_cols_L_line_by_line];
					index_cols_L_line_by_line++;
				}
				/*****method B******/
				/*****method A******/
				//double U_value = this->U(k, j);
				/*****method A******/
				/*****method B******/	//column_by_column	OK!
				double U_value = 0.0;
				while(index_rows_U_column_by_column < U.rows_index_column_by_column.size() && U.rows_index_column_by_column[index_rows_U_column_by_column] < k && index_rows_U_column_by_column <U.cols_first_position_column_by_column[j+1]) {
					index_rows_U_column_by_column++;
				}
				if(index_rows_U_column_by_column < U.rows_index_column_by_column.size() && U.rows_index_column_by_column[index_rows_U_column_by_column] == k &&  U.cols_first_position_column_by_column[j]!= U.cols_first_position_column_by_column[j+1]) {
					U_value = U.values_column_by_column[index_rows_U_column_by_column];
					index_rows_U_column_by_column++;
				}
				/*****method B******/
				value = value - L_value*U_value;
			}
			this->U.add_element(i, j, value);
		}
		//for(int j=i+1; j<n; j++) {
		for(int j=i+1; j<lim_sup; j++) {  //for each element in the lower triangle of L
			/*****method A******/
			//double value = (*A)(j, i);
			/*****method A******/
			/*****method B******/	//column_by_column	OK!
			double value = 0.0;
			while((*A).rows_index_column_by_column[index_rows_A_column_by_column] < j && index_rows_A_column_by_column <(*A).cols_first_position_column_by_column[i+1]) {
				index_rows_A_column_by_column++;
			}
			if((*A).rows_index_column_by_column[index_rows_A_column_by_column] == j && index_rows_A_column_by_column <(*A).cols_first_position_column_by_column[i+1]) {
				value = (*A).values_column_by_column[index_rows_A_column_by_column];
				index_rows_A_column_by_column++;
			}
			/*****method B******/
			//for(int k=0; k<i; k++) {
			int index_cols_L_line_by_line = L.rows_first_position_line_by_line[j];
			int index_rows_U_column_by_column = U.cols_first_position_column_by_column[i];	//A(j, i)
			for(int k=lim_inf; k<i; k++) {
				/*****method A******/
				//double L_value = this->L(j, k);
				/*****method A******/
				/*****method B******/	//line_by_line	OK!
				double L_value = 0.0;
				while(index_cols_L_line_by_line < L.cols_index_line_by_line.size() && L.cols_index_line_by_line[index_cols_L_line_by_line] < k && index_cols_L_line_by_line < L.rows_first_position_line_by_line[j+1]) {
					index_cols_L_line_by_line++;
				}
				if(index_cols_L_line_by_line < L.cols_index_line_by_line.size() && L.cols_index_line_by_line[index_cols_L_line_by_line] == k && L.rows_first_position_line_by_line[j]!=L.rows_first_position_line_by_line[j+1]) {
					L_value = L.values_line_by_line[index_cols_L_line_by_line];
					index_cols_L_line_by_line++;
				}
				/*****method B******/
				/*****method A******/
				//double U_value = this->U(k, i);
				/*****method A******/
				/*****method B******/	//column_by_column	OK!
				double U_value = 0.0;
				while(index_rows_U_column_by_column < U.rows_index_column_by_column.size() && U.rows_index_column_by_column[index_rows_U_column_by_column] < k && index_rows_U_column_by_column <U.cols_first_position_column_by_column[i+1]) {
					index_rows_U_column_by_column++;
				}

				if(index_rows_U_column_by_column < U.rows_index_column_by_column.size() && U.rows_index_column_by_column[index_rows_U_column_by_column] == k && index_rows_U_column_by_column <U.cols_first_position_column_by_column[i+1]) {
					U_value = U.values_column_by_column[index_rows_U_column_by_column];
					index_rows_U_column_by_column++;
				}
				/*****method B******/
				value = value - L_value*U_value;
			}
			value = value/this->U(i, i);
			this->L.add_element(j, i, value);
		}
		this->L.add_element(i, i, 1.0);
	}
	//cout << "number of zeros: " << number_of_zeros << " | " << 100*number_of_zeros/(number_of_zeros+number_of_non_zeros) << "%" << endl;	
	//cout << "number of non zeros: " << number_of_non_zeros << endl;
	//U.print_line_by_line();
	//U.print_column_by_column();
}

void solver_double::print() {
	cout << endl << "L:" << endl;
	this->L.print_column_by_column();
	cout << endl << "U:" << endl;
	this->U.print_column_by_column();
}

void solver_double::res_tri_inf(sparse_matrix_double *L, dense_matrix *b, dense_matrix *x) {
	int n = L->get_cols();  //square cols=rows
	(*x) = dense_matrix(n, 1);
	for(int i=0; i<n; i++) {
		//cout << "(" << (*b)(i, 0) << "-" << this->somme_rti(L, &x, i) << ")/" << (*L)(i, i) << endl;
		(*x)(i, 0) = ((*b)(i, 0) - this->somme_rti(L, x, i))/(*L)(i, i);
	}
}

double solver_double::somme_rti(sparse_matrix_double *L, dense_matrix *x, int i) {
	double value = 0.0;
	int lim = i - this->rayon_inf;
	if(lim<0) {
		lim = 0;
	}
	for(int j=lim; j<i; j++) {
		value = value + (*L)(i, j)*(*x)(j, 0);
	}
	return value;
}

void solver_double::res_tri_sup(sparse_matrix_double *U, dense_matrix *b, dense_matrix *x) {
	int n = U->get_cols();  //square cols=rows
	(*x) = dense_matrix(n, 1);
	for(int i=n-1; i>=0; i--) {
		//cout << "(" << (*b)(i, 0) << "-" << this->somme_rts(U, &x, i, n) << ")/" << (*U)(i, i) << endl;
		(*x)(i, 0) = ((*b)(i, 0) - this->somme_rts(U, x, i, n))/(*U)(i, i);
	}
}

double solver_double::somme_rts(sparse_matrix_double *U, dense_matrix *x, int i, int n) {
	double value = 0.0;
	int lim = i+1 + this->rayon_sup;
	if(lim>n) {
		lim = n;
	}
	for(int j=i+1; j<lim; j++) {
		value = value + (*U)(i, j)*(*x)(j, 0);
	}
	return value;
}
