A=zeros(n*m,n*m);
F=zeros(n*m,1);
for i=2:n-1
for j=2:m-1
k=i+(j-1)*n;
        A(k,k)=2/dx^2+2/dy^2;
        A(k,k-1)=-1/dx^2;  A(k,k+1)=-1/dx^2;
        A(k,k-n)=-1/dy^2;  A(k,k+n)=-1/dy^2;
        F(k)= f(x(i),y(j));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Conditions aux bords
%%%%%%%%% ptA
j=1; i=1;
k=i+(j-1)*n;
A(k,k)=2/dx^2+2/dy^2;
A(k,k+1)=-1/dx^2;
A(k,k+n)=-1/dy^2;
 F(k)= f(x(i),y(j))+g1(x(i),y(j))/dx^2 + g1(x(i),y(j))/dy^2;
%%%%%%%%%  pt B
j=1; i=n;
k=i+(j-1)*n;
A(k,k)=2/dx^2+2/dy^2;
A(k,k-1)=-1/dx^2;
A(k,k+n)=-1/dy^2;
F(k)=f(x(i),y(j)) +g1(x(i),y(j))/dx^2 + g1(x(i),y(j))/dy^2;
%%%%%%%%%%  Pt C
i=n; j=m;
k=i+(j-1)*n;
A(k,k)=2/dx^2+2/dy^2;
A(k,k-1)=-1/dx^2;
A(k,k-n)=-1/dy^2;
F(k)=f(x(i),y(j)) + g1(x(i),y(j))/dx^2 + g1(x(i),y(j))/dy^2;
%%%%%%%%%   Pt D
j=m; i=1;
k=i+(j-1)*n;
A(k,k)=2/dx^2+2/dy^2;
A(k,k+1)=-1/dx^2;
A(k,k-n)=-1/dy^2;
 F(k)= f(x(i),y(j))+g1(x(i),y(j))/dx^2 + g1(x(i),y(j))/dy^2;
%%%%%%%%%%%  entre A et B 


j=1;
for i=2:n-1
    k=i+(j-1)*n;
    A(k,k)=2/dx^2+2/dy^2;
    A(k,k-1)=-1/dx^2;  
    A(k,k+1)=-1/dx^2;
    A(k,k+n)=-1/dy^2;
    F(k)= f(x(i),y(j))    +g1(x(i),y(j))/dy^2;
   %   printf("%f\n", f(x(i),y(j)) );
end

%%%%%%%%%%%  entre  B et C
i=n;
for j=2:m-1
    k=i+(j-1)*n;
    A(k,k)=2/dx^2+2/dy^2;
    A(k,k-1)=-1/dx^2;
    A(k,k-n)=-1/dy^2; 
    A(k,k+n)=-1/dy^2;
    F(k)=f(x(i),y(j)) +g1(x(i),y(j))/dx^2 ;

end

%%%%%%%%%%%  entre  C et D

j=m;
for i=2:n-1
    k=i+(j-1)*n;
    A(k,k)=2/dx^2+2/dy^2;
    A(k,k-1)=-1/dx^2; 
    A(k,k+1)=-1/dx^2;
    A(k,k-n)=-1/dy^2;
    F(k)=f(x(i),y(j))+ g1(x(i),y(j))/dy^2; 

end

%%%%%%%%%%%  entre  D et A

i=1;
for j=2:m-1
    k=i+(j-1)*n;
    A(k,k)=2/dx^2+2/dy^2;
    A(k,k+1)=-1/dx^2;
    A(k,k-n)=-1/dy^2;
    A(k,k+n)=-1/dy^2;
    F(k)= f(x(i),y(j))+g1(x(i),y(j))/dx^2;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %solution exacte
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%U = zeros ((n)*(m));
U=A\F;