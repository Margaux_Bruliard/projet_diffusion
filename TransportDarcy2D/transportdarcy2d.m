clear
clc
close all;
%%%%%%%%%%%%%%%%%%%%%%%%%
%define variables
%%%%%%%%%%%%%%%%%%%%%%%%%
xmin=-1;
ymin=-1;
xmax=1;
ymax=1;
n=50;
m=50;
t = 0 ;
tmax=0.5;
cfl=0.5;
ul=0;
ur=1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%discretise the domain
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dx=(xmax-xmin)/(n+1);
dy=(ymax-ymin)/(m+1);
x = xmin : dx : xmax;
y = ymin : dy : ymax;
dxg = (xmax-xmin)/(n-1);
dyg = (ymax-ymin)/(n-1);

xg = xmin : dxg : xmax;
yg = ymin : dyg : ymax;

%Uex=uexact(xg,yg);

%%%%%%%%%%%%%%%%%%%
%Matrice de regidité
%%%%%%%%%%%%%%%%%%%%

Darcy
'Fin Darcy'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 figure(1)
%surf(x,y,
[xx,yy] = ndgrid(xg,yg);
tri = delaunay(xx,yy);
trisurf(tri,xx,yy,U)
title('Exact solution')

%Un=[ul ; U ; ur];
%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %vitesse
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MaxV=-1;
for i=2:n-1
    for j=2:m-1
        k=i+(j-1)*n;                  
      %  V(i,j)=(U(k)-U(k-1))/dx;
        Vx(i,j)=-(U(k+1)-U(k))/dx;
        Vy(i,j)=-(U(k+n)-U(k))/dy;
        MaxV=max(MaxV, sqrt(Vx(i)^2+Vy(j)^2));
    end
    


%V(i,1)=(U(1)-uexact(x(i),y(j)))/dx;
%V(1,j)=-(-U(n))/dx;                        

end

    dtx=cfl*dx/MaxV;
    dty=cfl*dy/MaxV;
    dt=min(dtx,dty)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%set initial conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cnp1=zeros(n,m);
for i=1:n
   for j=1:m
     c0(i,j) = exp(-50*((x(i)+0.6).^2+(y(j)+0.6).^2));
   end
end
c = c0;

cnp1 = c0;

%dt= 0.001;%cfl*(dx*dy)/max(abs(V));
sol = zeros(1,n*m);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%loop through time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %nsteps = tmax/dt;
 
 kit=0;
    [xx,yy] = ndgrid(xg,yg);
    tri = delaunay(xx,yy);
 while t<tmax
     kit=kit+1;
     dt=min(dt,tmax-t);
     
 %for iter = 1 : 500
  
%     %%%%%%%%%%%%%%%%%%%%%%%%%%
%     %% calculate the scheme
%     %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%initialiser c1(i,j)
c1 = zeros(n*m,n*m);
%%%%%test sur la vitesse
     for i=2:n-1
       for j= 2 : m-1  
           if   Vx(i,j)>=0
               
          c1(i,j)=  c(i,j);
           else
               c1(i,j)=  c(i+1,j); 
           end
           if   Vy(i,j)>=0
               
          c1(i,j)=  c(i,j);
           else
               c1(i,j)=  c(i,j+1); 
           end
%%%%%%%%%%%%%%%%%
          %cnp1(i,j) = c(i,j) - (dt/dx) *Vx(i,j)*(c(i,j) -c(i-1,j))-(dt/dy)* Vy(i,j)*(c(i,j) -c(i,j-1)) ;
       cnp1(i,j) = c(i,j) - (dt/dx) *(Vx(i,j)*c1(i,j) -Vx(i-1,j)*c1(i-1,j))-(dt/dy)*( Vy(i,j)*c1(i,j) -Vy(i,j)*c1(i,j-1));

       end
        
     end

   for j=1:m
       cnp1(1,j) = cnp1(2,j);
       cnp1(n,j) = cnp1(n-1,j);
   end
   
   for i=1:n
       cnp1(i,1) = cnp1(i,2);
       cnp1(i,m) = cnp1(i,m-1);
   end
    
%     for i=1:n
%         for j= 1 : m
%         k =i + (j-1)*n;
%         sol(k) = cnp1(i,j);
%         end
%     end

%      %%%%%%%%%%%%
%      %calculate boundry conditions
%     cnp1(1)=cnp1(2);
%     cnp1(N+1) =cnp1(N);
%     %%%%%%%%%%%%
%     %update t and c
     t = t+dt;
     c = cnp1;
     
    %%%%%%%%%%%%%
%     %plot solution
     figure(2)
    %surf(x,y,

    figure(2)
    %trisurf(tri,xx,yy,sol)
    surf(xg,yg,cnp1)
    temps=num2str(t);
    temps = ['solution � ' temps];
    title(temps) 
    view(0,90)
%     shg+
%campos(2.7225  -10.6032    2.8845)
pause(0.0001);
 %    pause(0.000000000000001);
% 
%   %  plot(v)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt
t
kit