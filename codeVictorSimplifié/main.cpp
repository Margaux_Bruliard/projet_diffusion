#include <iostream>
#include "dense_matrix.hpp"
#include "darcy.hpp"
#include "darcy_double.hpp"
#include "permeability_matrix.hpp"
#include "functions_type.hpp"
#include "solver.hpp"
#include "solver_double.hpp"
#include "sparse_matrix.hpp"
#include "sparse_matrix_double.hpp"
#include "transport.hpp"
#include "starter.hpp"
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include "mesh.hpp"

using namespace std;
double dirichlet_darcy(double, double, int);
double function_f(double, double);
double function_f2(double, double);
double neumann_condition_u(double, double, int, bool);
double neumann_condition_C(double, double, int, bool);
double limiter(double, double, string);

int main(int argc, char *argv[])
{
	/*******************/
	//init of darcy's equation
	/*******************/
	//domain
	starter start(argv[1]);

	//mesh
	mesh mesh_c(argv[4]);

	//overwrite start.nx and ny, they are not used anymore
	start.n_x = mesh_c.x.get_rows();
	start.n_y = mesh_c.y.get_rows();

	//matrices K
	cout << "set K..." << endl;
	permeability_matrix K(start.x_min, start.x_max, start.y_min, start.y_max, start.n_y, start.n_x, &mesh_c);	//inverted n_x n_y in permeability_domain_save
	K.set_with_domains(argv[2]);


	cout << "set darcy..." << endl;
	//init darcy
	darcy d(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, &mesh_c, K, dirichlet_darcy, function_f, start.streamer);

	cout << "solve darcy:" << endl;
	//solving darcy
	solver s;

	s.solve_darcy_GMRES_without_inver(&d, start.gmres_without_inv_max_it, start.gmres_without_inv_tol, start.talkative);
	//save to files
	d.u.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, "result_darcy_GMRES_without_inver");

	//We have solved darcy

	/*******************/
	//matrices D
	cout << "set D..." << endl;
	permeability_matrix D(start.x_min, start.x_max, start.y_min, start.y_max, start.n_y, start.n_x, &mesh_c);	//inverted n_x n_y in permeability_domain_save
	D.set_with_domains(argv[3]);

	//init of transport's equation
	transport trans(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, &mesh_c, neumann_condition_u, start.cfl, D, function_f2, neumann_condition_C);

	cout << "set concentration..." << endl;
	trans.set_centered_gaussian(start.x_pos_gaussian, start.y_pos_gaussian, start.x_var_gaussian, start.y_var_gaussian, 1.0);
		
	cout << "solve transport:" << endl;
	trans.compute_iterativ(start.end_time, &d, &start, limiter);

	trans.concentration.save_reshape_non_uniform(&(mesh_c.x), &(mesh_c.y), "concentration_final");

	return 1;
}

double dirichlet_darcy(double x, double y, int position) {   //also exact_sol if the function_f is set right
	/*position :	Allows you too locate the side we are on
	5|...6...|7
	_		  _
	.		  .
	3		  4
	.		  .
	_		  _
	0|...1...|2
	*/
	double borcond = -0.5*(-x+1-y+1);
	return borcond;
}

double function_f(double x, double y) {
	//return (1/(x+2))*(1/(x+2));	
	//return (1/(x+2))*(1/(x+2))-(1/(y+2))*(1/(y+2));
	//return 9*sin(3*x)*cos(4*y)+16*sin(3*x)*cos(4*y);
	return 0.0;
	//return 0.0;
}

double function_f2(double x, double y) {
	//return 0.01*exp(-(((x-0.0)*(x-0.0)/(2*0.05*0.05))+((y-0.0)*(y-0.0)/(2*0.05*0.05))));
	return 0.0;
}

double neumann_condition_u(double x, double y, int position, bool grad_x) {
	/*position :	Allows you too locate the side we are on
		 3
	.|.......|.
	_		  _
	.		  .
   1.		  .2
	.		  .
	_		  _
	.|.......|.
		 0
	*/
	/*grad :
	true = dx
	false = dy*/
	//return 0.0;
	return 0.0;
}

double neumann_condition_C(double x, double y, int position, bool grad_x) {
	/*position :	Allows you too locate the side we are on
		 3
	.|.......|.
	_		  _
	.		  .
   1.		  .2
	.		  .
	_		  _
	.|.......|.
		 0
	*/
	/*grad :
	true = dx
	false = dy*/
	return 0.0;
}

double limiter(double r_numerator, double r_denominator, string name) {
	if(r_numerator < 0.000000001 && -r_numerator < 0.000000001) return 0;
	if(r_denominator < 0.000000001 && -r_denominator < 0.000000001) {	//limits r -> infinity
		if(name == "vanleer") {
			return 2.0;	//MUSCL van leer
		}
		if(name == "superbee") {
			return 2.0;	//SUPERBEE
		}
		if(name == "minmod") {
			return 1.0;	//MINMOD
		}
		if(name == "vanalbada") {
			return 1.0;	//van albada
		}
		if(name == "barthjespersen") {
			return 2.0;	//barth-jespersen
		}
	}
	double r = r_numerator/r_denominator;
	if(r<=0) {
		return 0.0;
	}
	if(name == "vanleer") {
		return (r + fabs(r))/(1.0 + fabs(r));	//MUSCL van leer
	}
	if(name == "superbee") {
		return max(max(0.0, min(1.0,2.0*r)), min(2.0, r));	//SUPERBEE
	}
	if(name == "minmod") {
		return max(0.0, min(1.0,r));	//MINMOD
	}
	if(name == "vanalbada") {
		return (r*r + r)/(r*r + 1);	//MINMOD
	}
	if(name == "barthjespersen") {
		return 0.5*(r+1)*min(min(1.0, 4*r/(r+1)), min(1.0, 4/(r+1)));	//MINMOD
	}
	cout << "Check limiter name" << endl;
	return 2;
	return 1;
	return 0;
}
