#include <iostream>
#include "permeability_matrix.hpp"
#include <fstream>

using namespace std;

permeability_matrix::permeability_matrix(double _x_min, double _x_max, double _y_min, double _y_max, int _n_x, int _n_y, mesh *mesh_c) : dense_matrix(2*_n_x, 2*_n_y) {
	cout << _n_x << " " << _n_y << endl;
	this->dx = (_x_max - _x_min)/(_n_x+1);
	this->dy = (_y_max - _y_min)/(_n_y+1);
	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;
	this->n_x = _n_x;
	this->n_y = _n_y;
	this->x_coord = mesh_c->x;
	this->y_coord = mesh_c->y;
	
	//this->dx = min(this->x_coord(i));	//find min
	//this->dy = min(this->y_coord(i));	//find min
	
}

void permeability_matrix::set_with_domains(char *name) {
	ifstream file(name);
	int n = 0;
	if(file.is_open()) {
		string param_name;
		file >> param_name >> n;	//number of domains without the default one
		if(n > 0) {
			this->domain = vector<permeability_domain>(n);
			double domain_x_min;
			double domain_x_max;
			double domain_y_min;
			double domain_y_max;
			double domain_v1;
			double domain_v2;
			double domain_v3;
			double domain_v4;
			//create all domains
			//READ VALUES FROM FILE
			//file >> param_name >> domain_v1 >> param_name >> domain_v2 >> param_name >> domain_v3 >> param_name >> domain_v4;
			//this->domain[i] = new domain variable;
		}

		file.close();
		//for each (x, y) in the simulation domain
			//if it belongs to domain[k]
				//(*this)(2*j, 2*i) = this->domain[k].v1;
				//(*this)(2*j, 2*i+1) = this->domain[k].v2;
				//(*this)(2*j+1, 2*i) = this->domain[k].v3;
				//(*this)(2*j+1, 2*i+1) = this->domain[k].v4;
				
	}
}

double permeability_matrix::get_K(int k1, int k2, int i) {
	//return mean of K at cell k1 and cell k2
}

double permeability_matrix::get_K_boundary(int k1, int i) {
	//return value of K at the boundary
}
