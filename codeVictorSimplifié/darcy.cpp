#include "darcy.hpp"
#include "dense_matrix.hpp"
#include "permeability_matrix.hpp"
#include <iostream>
#include <math.h>
#include <stdio.h>
#include "constants.hpp"
#include "mesh.hpp"

using namespace std;

darcy::darcy(double _x_min, double _x_max, double _y_min, double _y_max, int _n_x, int _n_y, mesh *mesh_c, permeability_matrix _K, functiontype2 _function_boundary_condition, functiontype1 _function_f, bool streamer) {
	//domain [_x_min;_x_max]*[_y_min;_y_max]
	//_n_x steps in [_x_min;_x_max]
	//_n_y steps in [_y_min;_y_max]
	this->delta_x = (_x_max - _x_min)/(_n_x+1);
	this->delta_y = (_y_max - _y_min)/(_n_y+1);
	this->delta_x_vec = dense_matrix(_n_x, 1);
	this->delta_y_vec = dense_matrix(_n_y, 1);

	this->delta_x_vec = mesh_c->dx;
	this->delta_y_vec = mesh_c->dy;
	this->x_coord = mesh_c->x;
	this->y_coord = mesh_c->y;

	this->n_x = x_coord.get_rows();
	this->n_y = y_coord.get_rows();

	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;

	this->u = dense_matrix(_n_x*_n_y, 1);
	this->A = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->f = dense_matrix(this->n_x*this->n_y, 1);
	this->boundary_condition = dense_matrix(this->n_x*this->n_y, 1);

	this->K = _K;
	this->function_boundary_condition = _function_boundary_condition;
	this->function_f = _function_f;
	set_A_f();
}

void darcy::set_A_f() {
	int i, j, k;
	double x, y;
	double dx = this->delta_x;
	double dy = this->delta_y;
	double dxx = dx*dx;
	double dyy = dy*dy;
	double dxy = dx*dy;
	int N = this->n_x;
	for(i=1; i<this->n_y-1; i++) {	//goes through each cells "in"
		for(j=1; j<this->n_x-1; j++) {
			k = i*this->n_x+j;
			dx = this->delta_x_vec(j, 0);
			dy = this->delta_y_vec(i, 0);
			dxx = dx*dx;
			dyy = dy*dy;
			dxy = dx*dy;
			//x = dx*(double)j + this->x_min + dx;
			//y = dy*(double)i + this->y_min + dy;
			x = this->x_coord(j, 0);
			y = this->y_coord(i, 0);
			//this->set_f_line(k);
			this->f(k, 0) = this->function_f(x, y);
			//this->set_A_line(k);
			//Uk
			this->A.add_element(k, k, (this->K.get_K(k, k+1, 1) + this->K.get_K(k, k-1, 1))/dxx + (this->K.get_K(k, k+N, 2) + this->K.get_K(k, k-N, 2))/dyy);
			//Uk+1
			this->A.add_element(k, k+1, -this->K.get_K(k, k+1, 1)/dxx - this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-1
			this->A.add_element(k, k-1, -this->K.get_K(k-1, k, 1)/dxx + this->K.get_K(k, k+N, 1)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk+N
			this->A.add_element(k, k+N, -this->K.get_K(k, k+N, 2)/dyy - this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N
			this->A.add_element(k, k-N, -this->K.get_K(k-N, k, 2)/dyy + this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk+N+1
			this->A.add_element(k, k+N+1, -this->K.get_K(k, k+1, 2)/(4*dxy) - this->K.get_K(k, k+N, 1)/(4*dxy));
			//Uk+N-1
			this->A.add_element(k, k+N-1, this->K.get_K(k, k+N, 1)/(4*dxy) + this->K.get_K(k-1, k, 2)/(4*dxy));
			//Uk-N+1
			this->A.add_element(k, k-N+1, this->K.get_K(k, k+1, 2)/(4*dxy) + this->K.get_K(k-N, k, 1)/(4*dxy));
			//Uk-N-1
			this->A.add_element(k, k-N-1, -this->K.get_K(k-1, k, 2)/(4*dxy) - this->K.get_K(k-N, k, 1)/(4*dxy));
		}
	}
	//borders now
	//same but taking into account the boundary conditions

	this->f = this->f + this->boundary_condition;
}

void darcy::print() {
	this->A.print();
	this->f.print();
}

int darcy::get_n_x() {
	return this->n_x;
}

int darcy::get_n_y() {
	return this->n_y;
}

double darcy::get_delta_x() {
	return this->delta_x;
}

double darcy::get_delta_y() {
	return this->delta_y;
}

void darcy::compute_grad_C(dense_matrix *concentration, dense_matrix *grad_C_x_left, dense_matrix *grad_C_y_left, dense_matrix *grad_C_x_down, dense_matrix *grad_C_y_down, functiontype3 neumann_condition_C) {	//u is the solution of darcy's equation
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;

	double grad_C_x = 0.0;
	double grad_C_y = 0.0;

	int i, j, k;
	int k_nx1, k_ny1;

	double x, y;
			
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//diffusion=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
			dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
			dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			dx_c = dx_r/2.0 + dx_l/2.0;
			dy_c = dy_u/2.0 + dy_d/2.0;

			//right edge
			
			//up edge
			
			//left edge
			//left hand side
			//right hand side
			grad_C_x = ((*concentration)(k, 0) - (*concentration)(k-1, 0))/dx_l;
			grad_C_y = (0.25*((*concentration)(k+N-1, 0) + (*concentration)(k+N, 0) + (*concentration)(k-1, 0) + (*concentration)(k, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dy_c;
			k_nx1 = i*(this->n_x+1)+j;
			(*grad_C_x_left)(k_nx1, 0) = grad_C_x;	//on edge
			(*grad_C_y_left)(k_nx1, 0) = grad_C_y;	//on edge
			

			//down edge
			//left hand side
			//right hand side
			grad_C_x = (0.25*((*concentration)(k, 0) + (*concentration)(k+1, 0) + (*concentration)(k-N, 0) + (*concentration)(k-N+1, 0)) - 0.25*((*concentration)(k-1, 0) + (*concentration)(k, 0) + (*concentration)(k-N-1, 0) + (*concentration)(k-N, 0)))/dx_c;
			grad_C_y = ((*concentration)(k, 0) - (*concentration)(k-N, 0))/dy_d;
			k_ny1 = i*this->n_x+j;
			(*grad_C_x_down)(k_ny1, 0) = grad_C_x;	//on edge
			(*grad_C_y_down)(k_ny1, 0) = grad_C_y;	//on edge
		}
	}

	//borders now
	//same but taking into account the boundary conditions
}

void darcy::compute_diffusion_C(dense_matrix *flux, dense_matrix *grad_C_x_left, dense_matrix *grad_C_y_left, dense_matrix *grad_C_x_down, dense_matrix *grad_C_y_down, permeability_matrix *D, double *max_speed_diffusion) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double DC_x = 0.0;
	double DC_y = 0.0;
	double grad_C_x = 0.0;
	double grad_C_y = 0.0;

	double speed_diffusion = 0.0;

	int k;
	int k_nx1, k_ny1;

	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else {
				//on the edge, dx_right=dx_left
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else {
				//on the edge, dy_up=dy_down
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			//right edge
			//right hand side
			k_nx1 = i*(this->n_x+1)+j;
			grad_C_x = (*grad_C_x_left)(k_nx1 + 1, 0);	//on edge
			grad_C_y = (*grad_C_y_left)(k_nx1 + 1, 0);	//on edge
			if(j==this->n_x-1) {
				DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
			} else {
				DC_x = D->get_K(k, k+1, 1)*grad_C_x + D->get_K(k, k+1, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k+1, 1)), fabs(D->get_K(k, k+1, 2)));
			}
			(*flux)(k, 0) -= dy_c*DC_x;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//up edge
			//right hand side
			k_ny1 = i*this->n_x+j;
			grad_C_x = (*grad_C_x_down)(k_ny1 + N, 0);	//on edge
			grad_C_y = (*grad_C_y_down)(k_ny1 + N, 0);	//on edge
			if(i==this->n_y-1) {
				DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
			} else {
				DC_y = D->get_K(k, k+N, 1)*grad_C_x + D->get_K(k, k+N, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k+N, 1)), fabs(D->get_K(k, k+N, 2)));
			}
			(*flux)(k, 0) -= dx_c*DC_y;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//left edge
			//right hand side
			k_nx1 = i*(this->n_x+1)+j;
			grad_C_x = (*grad_C_x_left)(k_nx1, 0);	//on edge
			grad_C_y = (*grad_C_y_left)(k_nx1, 0);	//on edge
			if(j==0) {
				DC_x = D->get_K_boundary(k, 1)*grad_C_x + D->get_K_boundary(k, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K_boundary(k, 1)), fabs(D->get_K_boundary(k, 2)));
			} else {
				DC_x = D->get_K(k-1, k, 1)*grad_C_x + D->get_K(k-1, k, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k-1, 1)), fabs(D->get_K(k, k-1, 2)));
			}
			DC_x = -DC_x;
			(*flux)(k, 0) -= dy_c*DC_x;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;

			//down edge
			//right hand side
			k_ny1 = i*this->n_x+j;
			grad_C_x = (*grad_C_x_down)(k_ny1, 0);	//on edge
			grad_C_y = (*grad_C_y_down)(k_ny1, 0);	//on edge
			if(i==0) {
				DC_y = D->get_K_boundary(k, 3)*grad_C_x + D->get_K_boundary(k, 4)*grad_C_y;
				speed_diffusion = max(fabs( D->get_K_boundary(k, 3)), fabs(D->get_K_boundary(k, 4)));
			} else {
				DC_y = D->get_K(k, k-N, 1)*grad_C_x + D->get_K(k, k-N, 2)*grad_C_y;
				speed_diffusion = max(fabs(D->get_K(k, k-N, 1)), fabs(D->get_K(k, k-N, 2)));
			}
			DC_y = -DC_y;
			(*flux)(k, 0) -= dx_c*DC_y;
			if(speed_diffusion > (*max_speed_diffusion)) (*max_speed_diffusion) = speed_diffusion;
		}
	}
	//divide by dx*dy
	for(int i=0; i<this->n_y; i++) {  //goes through each cells "in"
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else {
				//on the edge, dx_right=dx_left
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else {
				//on the edge, dy_up=dy_down
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			dxy = dx_c*dy_c;
			(*flux)(k, 0) /= dxy;
		}
	}
}

void darcy::compute_grad_h(dense_matrix *grad_h_x_left, dense_matrix *grad_h_y_left, dense_matrix *grad_h_x_down, dense_matrix *grad_h_y_down, functiontype3 neumann_condition_u) {	//u is the solution of darcy's equation
	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;

	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	int i, j, k;
	int k_nx1, k_ny1;

	double x, y;
			
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//convection=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
			dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
			dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			//right edge

			//up edge

			//left edge
			//left hand side
			grad_u_x = (this->u(k, 0) - this->u(k-1, 0))/dx_l;
			grad_u_y = (0.25*(this->u(k+N-1, 0) + this->u(k+N, 0) + this->u(k-1, 0) + this->u(k, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dy_c;
			k_nx1 = i*(this->n_x+1)+j;
			(*grad_h_x_left)(k_nx1, 0) = grad_u_x;	//on edge
			(*grad_h_y_left)(k_nx1, 0) = grad_u_y;	//on edge

			//down edge
			//left hand side
			grad_u_x = (0.25*(this->u(k, 0) + this->u(k+1, 0) + this->u(k-N, 0) + this->u(k-N+1, 0)) - 0.25*(this->u(k-1, 0) + this->u(k, 0) + this->u(k-N-1, 0) + this->u(k-N, 0)))/dx_c;
			grad_u_y = (this->u(k, 0) - this->u(k-N, 0))/dy_d;
			k_ny1 = i*this->n_x+j;
			(*grad_h_x_down)(k_ny1, 0) = grad_u_x;	//on edge
			(*grad_h_y_down)(k_ny1, 0) = grad_u_y;	//on edge
		}
	}

	//borders now
	//same but taking into account the boundary conditions
}

void darcy::compute_velocity_h(dense_matrix *grad_h_x_left, dense_matrix *grad_h_y_left, dense_matrix *grad_h_x_down, dense_matrix *grad_h_y_down, dense_matrix *ve_x_left, dense_matrix *ve_y_down, double *max_speed_convection) {	//u is the solution of darcy's equation
	int N = this->n_x;

	double q_x = 0.0;
	double q_y = 0.0;
	double grad_u_x = 0.0;
	double grad_u_y = 0.0;

	double speed_convection = 0.0;

	int i, j, k;
	int k_nx1, k_ny1;
			
	
	for(int i=1; i<this->n_y-1; i++) {  //goes through each cells "in"
		for(int j=1; j<this->n_x-1; j++) {
			//q=-K*grad(u)
			//convection=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			//right edge

			//up edge

			//left edge
			k_nx1 = i*(this->n_x+1)+j;
			grad_u_x = (*grad_h_x_left)(k_nx1, 0);
			grad_u_y = (*grad_h_y_left)(k_nx1, 0);
			q_x = -this->K.get_K(k-1, k, 1)*grad_u_x - this->K.get_K(k-1, k, 2)*grad_u_y;
			q_x = -q_x;	//multiply by edge's normal
			k_nx1 = i*(this->n_x+1)+j;
			(*ve_x_left)(k_nx1, 0) = q_x;	//on edge
			speed_convection = fabs(q_x);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;

			//down edge
			k_ny1 = i*this->n_x+j;
			grad_u_x = (*grad_h_x_down)(k_ny1, 0);
			grad_u_y = (*grad_h_y_down)(k_ny1, 0);
			q_y = -this->K.get_K(k, k-N, 1)*grad_u_x - this->K.get_K(k, k-N, 2)*grad_u_y;
			q_y = -q_y;	//multiply by edge's normal
			k_ny1 = i*this->n_x+j;
			(*ve_y_down)(k_ny1, 0) = q_y;	//on edge
			speed_convection = fabs(q_y);
			if(speed_convection > (*max_speed_convection)) (*max_speed_convection) = speed_convection;
		}
	}

	//borders now
	//same but taking into account the boundary conditions
}

void darcy::compute_convection_h(dense_matrix *concentration, dense_matrix *flux, dense_matrix *ve_x_left, dense_matrix *ve_y_down) {	//u is the solution of darcy's equation
	for(int k=0; k<this->n_x*this->n_y; k++) {
		(*flux)(k, 0) = 0.0;
	}

	int N = this->n_x;
	double dx_r, dx_l, dy_u, dy_d;
	double dx_c, dy_c;
	double dxy;

	double ve_x_r = 0.0;
	double ve_x_l = 0.0;

	double ve_y_u = 0.0;
	double ve_y_d = 0.0;

	double C = 0.0;

	int k;
	int k_nx1, k_ny1;

	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else {
				//on the edge, dx_right=dx_left
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else {
				//on the edge, dy_up=dy_down
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;

			k_nx1 = i*(this->n_x+1)+j;
			k_ny1 = i*this->n_x+j;
			ve_x_r = -(*ve_x_left)(k_nx1 + 1, 0);
			ve_x_l = (*ve_x_left)(k_nx1, 0);

			ve_y_u = -(*ve_y_down)(k_ny1 + N, 0);
			ve_y_d = (*ve_y_down)(k_ny1, 0);

			//right edge
			C = 0.0;
			if(ve_x_r>0) { //goes from left to right
				C = (*concentration)(k, 0);
			} else if(ve_x_r<0) { //goes from right to left
				if(j!=this->n_x-1) {
					C = (*concentration)(k+1, 0);
				}
			}
			(*flux)(k, 0) += dy_c*ve_x_r*C;

			//up edge
			C = 0.0;
			if(ve_y_u>0) { //goes from down to up
				C = (*concentration)(k, 0);
			} else if(ve_y_u<0) { //goes from up to down
				if(i!=this->n_y-1) {
					C = (*concentration)(k+N, 0);
				}
			}
			(*flux)(k, 0) += dx_c*ve_y_u*C;

			//left edge
			C = 0.0;
			if(ve_x_l>0) { //goes from left to right
				C = (*concentration)(k, 0);
			} else if(ve_x_l<0) { //goes from right to left
				if(j!=0) {
					C = (*concentration)(k-1, 0);
				}
			}
			(*flux)(k, 0) += dy_c*ve_x_l*C;

			//down edge
			C = 0.0;
			if(ve_y_d>0) { //goes from down to up
				C = (*concentration)(k, 0);
			} else if(ve_y_d<0) { //goes from up to down
				if(i!=0) {
					C = (*concentration)(k-N, 0);
				}
			}
			(*flux)(k, 0) += dx_c*ve_y_d*C;
		}
	}
	//divide by dx*dy
	for(int i=0; i<this->n_y; i++) {  //goes through each cells
		for(int j=0; j<this->n_x; j++) {
			//q=-K*grad(u)
			//flux=q*concentration*mesure(edge)
			k = i*this->n_x+j;
			if(j>0 && j<this->n_x-1) {
				dx_r = this->x_coord(j+1, 0) - this->x_coord(j, 0);
				dx_l = this->x_coord(j, 0) - this->x_coord(j-1, 0);
			} else {
				//on the edge, dx_right=dx_left
			}
			if(i>0 && i<this->n_y-1) {
				dy_u = this->y_coord(i+1, 0) - this->y_coord(i, 0);
				dy_d = this->y_coord(i, 0) - this->y_coord(i-1, 0);
			} else {
				//on the edge, dy_up=dy_down
			}
			dx_c = dx_r/2 + dx_l/2;
			dy_c = dy_u/2 + dy_d/2;
			dxy = dx_c*dy_c;
			
			(*flux)(k, 0) /= dxy;
		}
	}
}
