#include "mesh.hpp"
#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>
#include "dense_matrix.hpp"

mesh::mesh() {

}

mesh::mesh(char *name) {
	ifstream file(name);
	int N_domain_x = 0;
	int N_domain_y = 0;
	double delta_x, delta_y;
	double delta_x_prev, delta_y_prev;
	double delta_x_next, delta_y_next;
	int smooth_level_x = 0;
	int smooth_level_y = 0;

	if(file.is_open()) {
		string param_name;
		//get all file values
		//for x axis
		//read domain decomposition
		//read numder of cells per domains
		
		//for y axis
		//read domain decomposition
		//read numder of cells per domains

		//find desired size of the complete x axis and y axis
		//this->nx = ;
		//this->ny = ;

		//initialise
		this->dx = dense_matrix(this->nx, 1);
		this->dy = dense_matrix(this->ny, 1);
		this->x = dense_matrix(this->nx, 1);
		this->y = dense_matrix(this->ny, 1);
	
		//fill vectors x
		//first pass
		//set coordinates according to the file description

		//second pass
		//use gaussian filter to make the domain transitions smooth
		
	
		//fill vectors y
		//first pass
		//set coordinates according to the file description

		//second pass
		//use gaussian filter to make the domain transitions smooth
		
	} else {
		cout << "Could not open domain file !" << endl;
	}
}
