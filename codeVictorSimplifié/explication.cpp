int main(int argc, char *argv[])
{
	/*******************/
	//init of darcy's equation
	/*******************/
	//domain
	starter start(argv[1]);
		reads all input setings from file argv[1]

	//mesh

	mesh mesh_c(argv[4]);
		read the mesh description from file argv[4] and creates vector coord_x and coord_y containing the coord of both axis of the grid

	//overwrite start.nx and ny, they are not used anymore
	start.n_x = mesh_c.x.get_rows();
	start.n_y = mesh_c.y.get_rows();

	//matrices K
	cout << "set K..." << endl;
	permeability_matrix K(start.x_min, start.x_max, start.y_min, start.y_max, start.n_y, start.n_x, &mesh_c);	//inverted n_x n_y in permeability_domain_save
		Sets permeability_matrix's data concerning the simulation domain
		permeability_matrix contains 4 values (2 by 2 matrix) for each point on the grid. 
	K.set_with_domains(argv[2]);
		fills the matrix with the values as described in the file argv[2]


	cout << "set darcy..." << endl;
	//init darcy
	darcy d(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, &mesh_c, K, dirichlet_darcy, function_f, start.streamer);
		allocates memory and sets darcy's data concerning the simulation
		darcy contains the matrix A, the vector u and the vector f. We will later find u such that Au=f
		sets A and f such that Au=f will solve darcy's equation
			To set A and f, we go through each cells of the domain grid.
				for a cell k we compute the coefficient corresponding to the interaction between the cell k and its neighbors.
				they are 8 neighbors for most of the cells. So each line will have 9 values, 1 for the current cell we are looking at and 8 for its neighbors
				the cells on the boundary have to take into account the boundary conditions.

	cout << "solve darcy:" << endl;
	//solving darcy
	solver s;
		initialise the solver variable

	s.solve_darcy_GMRES_without_inver(&d, start.gmres_without_inv_max_it, start.gmres_without_inv_tol, start.talkative);
		solve darcy's equation using the GMRES (without inversion) algorithm.
		We solve Au=f.
		A and f are given to the solver through &d, the darcy variable
		d contains also a vector u. This vector u is set to the solution by the solver

	//save to files
	d.u.save_reshape(start.n_x, start.n_y, start.x_min, start.x_max, start.y_min, start.y_max, "result_darcy_GMRES_without_inver");

	//We have solved darcy

	/*******************/
	//matrices D
	cout << "set D..." << endl;
	permeability_matrix D(start.x_min, start.x_max, start.y_min, start.y_max, start.n_y, start.n_x, &mesh_c);	//inverted n_x n_y in permeability_domain_save
		Sets permeability_matrix's data concerning the simulation domain
		permeability_matrix contains 4 values (2 by 2 matrix) for each point on the grid. 
	D.set_with_domains(argv[3]);
		fills the matrix with the values as described in the file argv[3]

	//init of transport's equation
	transport trans(start.x_min, start.x_max, start.y_min, start.y_max, start.n_x, start.n_y, &mesh_c, neumann_condition_u, start.cfl, D, function_f2, neumann_condition_C);
		allocates all the physics variables (velocity, concentration, gradients, flux, boundary conditions,...) for the transport equation

	cout << "set concentration..." << endl;
	trans.set_centered_gaussian(start.x_pos_gaussian, start.y_pos_gaussian, start.x_var_gaussian, start.y_var_gaussian, 1.0);
		Set the concentration of the transport variable to a gaussian
		
	cout << "solve transport:" << endl;
	trans.compute_iterativ(start.end_time, &d, &start, limiter);
		Solve the transport equation using the solution of d (darcy) to compute the velocity
		we start by creating a file to save the solution as a matlab script
		for each timestep
			we compute the flux, the steps are done by the darcy variable d
				diffusion :	compute the gradient of the concentration
							compute the flux coming from the diffusion term
				convection :	compute the gradient of darcy's solution
								compute the velocity using this gradient
								compute the flux coming from the convection term
			compute the timestep dt
			update the concentration with the convection
			update the concentration with the diffusion
			update the concentration with the right hand side f2
			write the solution in the file


	trans.concentration.save_reshape_non_uniform(&(mesh_c.x), &(mesh_c.y), "concentration_final");

	return 1;
}
