#include "transport.hpp"
#include "dense_matrix.hpp"
#include "darcy.hpp"
#include "functions_type.hpp"
#include <iostream>
#include <fstream>
#include <math.h>

transport::transport(double _x_min, double _x_max, double _y_min, double _y_max, int _n_x, int _n_y, mesh *mesh_c, functiontype3 _neumann_condition_u, double _cfl, permeability_matrix _D, functiontype1 _function_f2, functiontype3  _neumann_condition_C) {
	this->dx = (_x_max - _x_min)/(_n_x+1);
	this->dy = (_y_max - _y_min)/(_n_y+1);
	this->x_min = _x_min;
	this->x_max = _x_max;
	this->y_min = _y_min;
	this->y_max = _y_max;

	this->x_coord = mesh_c->x;
	this->y_coord = mesh_c->y;
	this->n_x = x_coord.get_rows();
	this->n_y = y_coord.get_rows();
	//this->dx = min(this->x_coord(i));	//find min
	//this->dy = min(this->y_coord(i));	//find min

	this->neumann_condition_u = _neumann_condition_u;
	this->neumann_condition_C = _neumann_condition_C;

	//allocating all physics variables
	this->concentration = dense_matrix(_n_x*_n_y, 1);
	this->flux = dense_matrix(_n_x*_n_y, 1);
	this->convection = dense_matrix(_n_x*_n_y, 1);
	this->diffusion = dense_matrix(_n_x*_n_y, 1);
	this->grad_h_x_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_h_y_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_h_x_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->grad_h_y_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->ve_x_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->ve_y_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->f2 = dense_matrix(_n_x*_n_y, 1);
	this->cfl = _cfl;
	this->function_f2 = _function_f2;
	this->D = _D;
	//initialising vector f2 with the right values from function_f2
	for(int i=0; i<this->n_y; i++) {	//goes through each cells
		for(int j=0; j<this->n_x; j++) {
			int k = i*this->n_x+j;
			double x = this->dx*(double)j + this->x_min + this->dx;
			double y = this->dy*(double)i + this->y_min + this->dy;
			this->f2(k, 0) = this->function_f2(x, y);
		}
	}
	this->flux_system = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->convection_system = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->diffusion_system = sparse_matrix(this->n_x*this->n_y, this->n_x*this->n_y, 0);
	this->flux_right_hand_side = dense_matrix(_n_x*_n_y, 1);
	this->flux_boundary = dense_matrix(_n_x*_n_y, 1);
	this->convection_boundary = dense_matrix(_n_x*_n_y, 1);
	this->diffusion_boundary = dense_matrix(_n_x*_n_y, 1);
	this->grad_C_x_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_C_y_left = dense_matrix((this->n_x+1)*this->n_y, 1);	//on edges
	this->grad_C_x_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->grad_C_y_down = dense_matrix(this->n_x*(this->n_y+1), 1);	//on edges
	this->time = 0.0;
}

void transport::compute_iterativ(double t_end, darcy *d, starter *start, functiontype4 limiter, sparse_matrix *M, darcy_double *d_double) {
	double dt = 0.01;	//dt=cfl*dx/sqrt(v1^2+v2^2);
	this->time = 0.0;
	int iter = 0;
	int iter_max = start->iter_max;

	//set file name for solution output
	const char *file_name;
	file_name = "play_video.m";
	//start save file
	ofstream save_file;
	save_file.open(file_name);
	this->write_grid(&save_file);
	this->write_frame(&save_file);

	solver s;

	while(this->time < t_end && (iter<iter_max || iter_max == -1)) {

		iter++;

		cout << "Time  " << this->time << " Time iteration " << iter-1 << endl;
		double max_speed_convection = 0.0;
		double max_speed_diffusion = 0.0;

		if(start->diffusion) {
			(*d).compute_grad_C(&this->concentration, &this->grad_C_x_left, &this->grad_C_y_left, &this->grad_C_x_down, &this->grad_C_y_down, this->neumann_condition_C);
			(*d).compute_diffusion_C(&this->diffusion, &this->grad_C_x_left, &this->grad_C_y_left, &this->grad_C_x_down, &this->grad_C_y_down, &this->D, &max_speed_diffusion);
		}

		if(start->convection) {
			(*d).compute_grad_h(&this->grad_h_x_left, &this->grad_h_y_left, &this->grad_h_x_down, &this->grad_h_y_down, this->neumann_condition_u);
			(*d).compute_velocity_h(&this->grad_h_x_left, &this->grad_h_y_left, &this->grad_h_x_down, &this->grad_h_y_down, &this->ve_x_left, &this->ve_y_down, &max_speed_convection);
			
			(*d).compute_convection_h(&this->concentration, &this->convection, &this->ve_x_left, &this->ve_y_down);
		}

		if(start->convection && start->diffusion) {
			dt = this->set_dt(max_speed_convection, max_speed_diffusion, t_end);
		}

		if(start->convection) {
			this->concentration = this->concentration - this->convection*dt;	//elementwise operations
		}
		if(start->diffusion) {
			this->concentration = this->concentration - this->diffusion*dt;	//elementwise operations
		}

		this->concentration = this->concentration - this->f2*(dt/(this->dx*this->dy));	//elementwise operations

		this->time += dt;
		
		this->write_frame(&save_file);
	}
	this->convection.save_reshape_non_uniform(&(this->x_coord), &(this->y_coord), "convection_final");
	d->u.save_reshape_non_uniform(&(this->x_coord), &(this->y_coord), "u_final");
	cout << "Time  " << this->time << " Time iteration " << iter << endl;
	save_file.close();
	cout << "Transport saved in " << file_name << endl;
}

void transport::set_centered_gaussian(double pos_x, double pos_y, double sigma_x, double sigma_y, double l) {
	for(int i=this->n_y-1; i>=0; i--) {
		for(int j=0; j<this->n_x; j++) {	
			double x = this->x_coord(j, 0);
			double y = this->y_coord(i, 0);		
			this->concentration(i*this->n_x + j, 0) = l*exp(-(((x-pos_x)*(x-pos_x)/(2*sigma_x*sigma_x))+((y-pos_y)*(y-pos_y)/(2*sigma_y*sigma_y))));
		}
	}
}

void transport::write_grid(ofstream *file) {
	(*file) << "X = [";
	for(int i=0; i<this->n_x-1; i++) {
		(*file) << this->x_coord(i, 0) << ", ";
	}
	(*file) << this->x_coord(this->n_x-1, 0) << "];\n";
	(*file) << "Y = [";
	for(int i=0; i<this->n_y-1; i++) {
		(*file) << this->y_coord(i, 0) << ", ";
	}
	(*file) << this->y_coord(this->n_y-1, 0) << "];\n";
}

void transport::write_frame(ofstream *file) {
	(*file) << "concentration = [";
	for(int i=this->n_y-1; i>=0; i--) {
		for(int j=0; j<this->n_x; j++) {
			(*file) << this->concentration(i*this->n_x + j, 0) << " ";
		}
		if(i!=0) (*file) << "; ";
	}
	(*file) << "];\n";
	(*file) << "pause(0.05);\n";
	(*file) << "surf(X, Y, rot90(concentration.'),'EdgeColor','none');\n";
	(*file) << "xlabel('x');\n";
	(*file) << "ylabel('y');\n";
	if(this->x_max - this->x_min > this->y_max - this->y_min) {
		(*file) << "xlim([" << this->x_min << ", " << this->x_max << "])\n";
		(*file) << "ylim([" << this->x_min << ", " << this->x_max << "])\n";
	} else {
		(*file) << "xlim([" << this->y_min << ", " << this->y_max << "])\n";
		(*file) << "ylim([" << this->y_min << ", " << this->y_max << "])\n";
	}
	(*file) << "view(2)\n";
	//(*file) << "zlim([0, 1])\n";
	//(*file) << "caxis([0.0 1.0])\n";
}

double transport::set_dt(double speed_convection, double speed_diffusion, double t_end) {
	double dxy = this->dx*this->dy;
	double dxx = this->dx*this->dx;
	double dyy = this->dy*this->dy;
	double dt_convection = this->cfl*(dxy/(this->dx + this->dy)) /speed_convection;
	double dt_diffusion = this->cfl*(0.5*(dxx*dyy)/(dxx + dyy)) /speed_diffusion;
	double dt = min(dt_convection, dt_diffusion);
	if(speed_convection == -1.0) {
		dt = dt_diffusion;
	}
	if(speed_diffusion == -1.0) {
		dt = dt_convection;
	}
	if(this->time+dt > t_end && t_end != -1.0) {
		dt = t_end - this->time;
	}
	return dt;
}
