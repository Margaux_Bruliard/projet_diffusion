#include <iostream>
#include "solver.hpp"
#include <ctime>
#include <cmath>

using namespace std;

solver::solver() {
	//this->rayon_sup = -1; //only for 9 point stencil
	//this->rayon_inf = -1; //only for 9 point stencil
}


void solver::solve_darcy_GMRES_without_inver(darcy *d, int max_it, double tol, bool talkative) {
	d->u = dense_matrix(d->A.get_rows(), 1);
	//find u such that Au=f
	this->GMRES_without_inver(&d->A, &d->u, &d->f, max_it, tol, talkative);
}

void solver::GMRES_without_inver(sparse_matrix *A, dense_matrix *x, dense_matrix *b, int max_it, double tol, bool talkative) {	//A, x, b, Mconditioner has to be diagonal for now, n_before_restart, max_it, tol
	//Solve Au=f using GMRES algorithm
	int N = b->get_rows();
	double rho = b->vec_norm2();
	dense_matrix vector1_N(N, 1);	//has to be of size (N,1)
	dense_matrix vector2_N(N, 1);	//has to be of size (N,1)
	dense_matrix vector3_N(N, 1);	//has to be of size (N,1)
	dense_matrix c(max_it, 1);	//has to be of size (N,1)
	dense_matrix s(max_it, 1);	//has to be of size (N,1)
	sparse_matrix h(max_it+1, max_it, 0);
	dense_matrix g(max_it+1, 1);	//has to be of size (max_it+1,1)
	dense_matrix v(N, max_it);	//has to be of size (N,max_it)
	//initialized at the end :
	//y
	//g_dim_square
	//h_dim_square

	g(0, 0) = rho;
	double normh = 0.0;
	tol = tol*rho;
	//v(:, 0) = (*b)*(1/rho);
	vector1_N = (*b)*(1/rho);
	v.set_column(0, &vector1_N);
	int k = -1;	//matlab starts array at 1 so k=0 | C++ starts array at 0 so k=-1
	//Begin gmres loop.
	while((rho > tol) && (k < max_it-2)) {
		k++;
		//%  Matrix vector product.
		//v(:,k+1) = A*v(:,k);		%N by 2, 3, 4, ...
		v.get_column(k, &vector1_N);
		A->mult_vec(&vector1_N, &vector2_N);
		v.set_column(k+1, &vector2_N);
		//Begin modified GS. May need to reorthogonalize.
		for(int j=0; j<=k; j++) {
			//h(j,k) = v(:,j)'*v(:,k+1);	%bigger h
			v.get_column(j, &vector1_N);
			v.get_column(k+1, &vector2_N);
			h.set_element(j,k, vector1_N.vec_dot_product(&vector2_N));
			//v(:,k+1) = v(:,k+1) - h(j,k)*v(:,j);
			//vector1_N = v(:,j)
			//vector2_N = v(:,k+1)
			vector3_N = vector2_N - vector1_N*h(j, k);
			v.set_column(k+1, &vector3_N);
		}
		//h(k+1,k) = (v(:,k+1)'*v(:,k+1))^.5;
		v.get_column(k+1, &vector2_N);
		h.set_element(k+1, k, vector2_N.vec_norm2());
		if(h(k+1,k) != 0.0) {
			//v(:,k+1) = v(:,k+1)/h(k+1,k);
			vector3_N = vector2_N*(1/h(k+1,k));
			v.set_column(k+1, &vector3_N);
		}
		//Apply old Givens rotations to h(1:k,k).
		/*if k>1                                
			for i=1:k-1
				hik = c(i)*h(i,k)-s(i)*h(i+1,k);
				hipk       = s(i)*h(i,k)+c(i)*h(i+1,k);
				h(i,k)     = hik;
				h(i+1,k)   = hipk;
			end
		end*/
		if(k>0) {
			for(int i=0; i<=k-1; i++) {
				double hik = c(i, 0)*h(i, k) - s(i, 0)*h(i+1, k);
				double hipk = s(i, 0)*h(i, k) + c(i, 0)*h(i+1, k);
				h.set_element(i,k, hik);
				h.set_element(i+1,k, hipk);
			}
		}
		//normh = norm(h(k:k+1,k));
		normh = sqrt(h(k,k) * h(k, k) + h(k+1,k) * h(k+1, k));
		//May need better Givens implementation. 
		//Define and apply new Givens rotations to h(k:k+1,k).
		/*if normh ~= 0                              
		    c(k)        = h(k,k)/normh;
		    s(k)        = -h(k+1,k)/normh;
		    h(k,k)      = c(k)*h(k,k) - s(k)*h(k+1,k);
		    h(k+1,k)    = 0;
		    gk          = c(k)*g(k) - s(k)*g(k+1);
		    gkp         = s(k)*g(k) + c(k)*g(k+1);
		    g(k)        = gk;
		    g(k+1)      = gkp;
		end  */
		if(normh != 0.0) {
			c(k, 0) = h(k, k)/normh;
			s(k, 0) = -h(k+1, k)/normh;
			h.set_element(k, k, c(k, 0)*h(k, k) - s(k, 0)*h(k+1, k));
			h.set_element(k+1, k, 0.0);
			double gk = c(k, 0)*g(k, 0) - s(k, 0)*g(k+1, 0);
			double gkp = s(k, 0)*g(k, 0) + c(k, 0)*g(k+1, 0);
			g(k, 0) = gk;
			g(k+1, 0) = gkp;
		}
		//rho = abs(g(k+1));
		rho = abs(g(k+1, 0));
	}
	if(k<0) {
		cout << "Check for trivial/near 0 solution or a way to high tolerance" << endl;
		k = 0;
	}

	//End of gmres loop
	//h(1:k,1:k) is upper triangular matrix in QR.
	//y = h(1:k,1:k)\g(1:k);
	dense_matrix y(N, 1);	//has to be of size (N,1)
	sparse_matrix h_dim_square(k, k, 0);
	h.extract(0, k-1, 0, k-1, &h_dim_square);
	dense_matrix g_dim_square(N, 1);	//has to be of size (N,1)
	g.extract(0, k-1, 0, 0, &g_dim_square);

	res_tri_sup(&h_dim_square, &g_dim_square, &y, false);
	//Form linear combination.
	/*for i = 1:k                                 
		x(:) = x(:) + v(:,i)*y(i);
	end*/
	for(int i=0; i<k; i++) {
		v.get_column(i, &vector1_N);
		(*x) = (*x) + vector1_N*y(i, 0);
	}
	//return infos
	if(talkative) {
		cout << "GMRES without inversion" << endl;
		cout << "\tTol :" << tol << "\t Tol reached :" << rho << endl;
		cout << "\tIn " << k+1 << " iterations with a maximum of " << max_it << endl;
	}
}
