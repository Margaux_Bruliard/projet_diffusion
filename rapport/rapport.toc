\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {1.1}Objectif du projet}{2}
\contentsline {section}{\numberline {1.2}\IeC {\'E}quation \IeC {\`a} mod\IeC {\'e}liser}{2}
\contentsline {chapter}{\numberline {2}R\IeC {\'e}solution num\IeC {\'e}rique du probl\IeC {\`e}me lin\IeC {\'e}aire}{4}
\contentsline {section}{\numberline {2.1}Discr\IeC {\'e}tisation du domaine}{4}
\contentsline {section}{\numberline {2.2}Etude de la pression par une m\IeC {\'e}thode des volumes finis}{4}
\contentsline {subsection}{\numberline {2.2.1}Le sch\IeC {\'e}ma diamant}{4}
